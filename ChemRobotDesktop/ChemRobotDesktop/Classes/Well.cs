﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using Volume = System.Single;
using Id = System.Int32;

namespace ChemRobotDesktop.Classes
{

    public enum WELL_BOTTOM_SHAPE : int
    {
        FLAT,
        ROUND,
        V_SHAPE
    }

    public enum WELL_SHAPE : int
    {
        ROUND,
        RECTANGLE
    }

    public class Well
    {
        [JsonPropertyName("Depth")]
        public float Depth { get; set; }

        [JsonPropertyName("MaxLiquidVolume")]
        public float MaxLiquidVolume { get; set; }

        [JsonPropertyName("BottomShape")]
        public WELL_BOTTOM_SHAPE BottomShape { get; set; }

        [JsonPropertyName("Shape")]
        public WELL_SHAPE Shape { get; set; }

        // Опциональные поля, так как не каждый контейнер имеет все размеры.
        [JsonPropertyName("RectSize")]
        public PointF RectSize { get; set; }

        [JsonPropertyName("Diameter")]
        public float? Diameter { get; set; }

        [JsonPropertyName("Liquid")]
        public LiquidsMix? Liquid { get; set; } = new LiquidsMix();

        public void addLiquid(ref Liquid liquid, Volume volume)
        {
            if (Liquid != null) 
                this.Liquid.addLiquid(liquid, volume);
        }

        public void addLiquidUnit(LiquidUnit liquidUnit)
        {
            if (Liquid != null)
                Liquid.addLiquidUnit(liquidUnit);
        }

        public void addLiquidsMix(LiquidsMix liquidsMix)
        {
            if (Liquid != null)
                this.Liquid.addLiquidsMix(liquidsMix);
        }

        public LiquidsMix Extract(Volume volume)
        {
            if (Liquid != null)
                return this.Liquid.Extract(volume);

            else 
                return new LiquidsMix();
        }

        public Well DeepCopy()
        {
            Well newWell = new Well
            {
                Depth = this.Depth,
                MaxLiquidVolume = this.MaxLiquidVolume,
                BottomShape = this.BottomShape,
                Shape = this.Shape,
                RectSize = this.RectSize,
                Diameter = this.Diameter,
                Liquid = this.Liquid?.DeepCopy() // Предполагая, что LiquidsMix поддерживает DeepCopy
            };
            return newWell;
        }

        public void clearWell()
        {
            if (Liquid != null)
            {
                Liquid.Extract(Liquid.volume);
            }
        }
    }
}
