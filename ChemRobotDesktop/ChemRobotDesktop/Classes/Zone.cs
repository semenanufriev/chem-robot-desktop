﻿using ChemRobotDesktop.CustomControls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ChemRobotDesktop.Classes
{

    public class Zone
    {
        public int id;
        public Plate? plate;
        [JsonIgnore]
        public Image defaultImage;
        [JsonIgnore]
        public PictureBox pictureBox;
        public void updateState()
        {
            if (plate != null)
            {
                pictureBox.Image = plate.getImage(null, true);
                pictureBox.Name = id.ToString();
                pictureBox.Tag = new pictureBoxTag(pictureBox.Image);
            }
            else
            {
                pictureBox.Image = defaultImage;
                pictureBox.Name = id.ToString();
                pictureBox.Tag = new pictureBoxTag(pictureBox.Image);
            }
        }
        public Zone()
        {
            pictureBox = new PictureBox
            {
                Dock = DockStyle.Fill,
                SizeMode = PictureBoxSizeMode.StretchImage,
                BackColor = Color.Transparent,
                Padding = new Padding(5),
            };
            updateState();

        }

        public Zone(int id)
        {
            this.id = id;
            pictureBox = new PictureBox
            {
                Dock = DockStyle.Fill,
                SizeMode = PictureBoxSizeMode.StretchImage,
                BackColor = Color.Transparent,
                Padding = new Padding(5),
                Name = id.ToString()
            };
            updateState();

        }

        public void addPlate(Plate plate)
        {
            this.pictureBox = new PictureBox
            {
                Dock = DockStyle.Fill,
                SizeMode = PictureBoxSizeMode.StretchImage,
                BackColor = Color.Transparent,
                Padding = new Padding(5),
                Name = id.ToString()
            };
            this.plate = plate;
            updateState();
        }

        public void removePlate()
        {
            this.pictureBox = new PictureBox
            {
                Dock = DockStyle.Fill,
                SizeMode = PictureBoxSizeMode.StretchImage,
                BackColor = Color.Transparent,
                Padding = new Padding(5),
                Name = id.ToString()
            };
            this.plate = null;
            updateState();
        }




    }
}
