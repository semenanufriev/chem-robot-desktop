﻿using ChemRobotDesktop.CustomControls;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ChemRobotDesktop.Classes
{
    public class Project
    {
        public delegate void Method();
        public event Method ProjectLoaded;
        public string Name { get; set; }
        public string Description { get; set; }
        public string Author { get; set; }
        public List<Liquid> Liquids = new List<Liquid>();
        public List<ProtocolStep> ProtocolSteps = new List<ProtocolStep>();
        public List<Zone> StartingZones = new List<Zone>() ;
        public int Rows { get; set; }
        public int Cols { get; set; }
        public List<PointF> StartingPoints { get; set; }
        [JsonIgnore]
        public string pathToFile;
        public string pathToConfigFile;

        public void LoadConfig(string configFilePath)
        {
            pathToConfigFile = configFilePath;
            try
            {
                string configJson = File.ReadAllText(configFilePath);
                var config = JsonConvert.DeserializeObject<Config>(configJson);

                if (config != null)
                {
                    Rows = config.Rows;
                    Cols = config.Cols;
                    StartingPoints = config.StartingPoints;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ошибка при загрузке конфигурации: " + ex.Message);
            }
        }
    

    public class Config
    {
        public int Rows { get; set; }
        public int Cols { get; set; }
        public List<PointF> StartingPoints { get; set; }
    }

    public Project() {
            string pathToConfig = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"Config/config.txt"));
            LoadConfig(pathToConfig);
            StartingZones = new List<Zone>(Rows * Cols);

    }

        public void init()
        {
            StartingZones.Clear();
            for (int i = 0; i < Rows * Cols; i++)
            {
                Zone zone = new Zone();
                zone.id = i;
                zone.pictureBox.Name = i.ToString();
                StartingZones.Add(zone);
            }
            
        }

        // Метод для сохранения проекта в файл
        public void SaveToFile(string filePath)
        {
            if (filePath is null)
            {
                throw new ArgumentNullException(nameof(filePath));
            }

            try
            {
                string json = JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects
                });
                File.WriteAllText(filePath, json);
                pathToFile = filePath;
            }
            catch (Exception ex)
            {
                // Обработка ошибок при записи в файл
                Console.WriteLine("Ошибка при сохранении файла: " + ex.Message);
            }
        }
        public bool isOpened()
        {
            if(pathToFile != null && pathToFile != "")
            {
                return true;
            }
            return false;
        }

        public bool checkProjectChanged()
        {
            if (!isOpened())
            {
                return false;
            }
            try
            {
                string currentJson = JsonConvert.SerializeObject(this, Formatting.Indented, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects
                });
                Project originalProject = new Project();
                originalProject.LoadConfig(pathToConfigFile);
                originalProject.LoadFromFile(pathToFile);
                string originalJson = JsonConvert.SerializeObject(originalProject, Formatting.Indented, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects
                });

                if(currentJson != originalJson)
                {
                    return true;
                }
            }
            catch
            {
                Console.WriteLine("Какие-то прооблемы при поиске несохранённых изменений");
                return false;
            }

            return false;
        }

        // Метод для загрузки проекта из файла
        public void LoadFromFile(string filePath)
        {
            try
            {
                string json = File.ReadAllText(filePath);
                Project tmp = JsonConvert.DeserializeObject<Project>(json, new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.Objects
                });

                // Копирование данных из временного объекта в текущий экземпляр
                StartingZones.Clear();
                if (tmp != null)
                {
                    this.Name = tmp.Name;
                    this.Description = tmp.Description;
                    this.Author = tmp.Author;
                    this.Liquids = tmp.Liquids.ToList();
                    this.ProtocolSteps = tmp.ProtocolSteps.ToList();
                    this.StartingZones = tmp.StartingZones.ToList();
                    ProjectLoaded?.Invoke();
                }
                pathToFile = filePath;
            }
            catch (Exception ex)
            {
                // Обработка ошибок при чтении файла
                //MessageBox.Show("Ошибка при загрузке файла: " + ex.Message);
            }
        }

        public void ClearData()
        {
            Name = string.Empty;
            Description = string.Empty;
            Author = string.Empty;
            pathToFile = string.Empty;

            Liquids.Clear();
            ProtocolSteps.Clear();
            StartingZones.Clear();
            init();
            ProjectLoaded?.Invoke();
        }
    }
}

