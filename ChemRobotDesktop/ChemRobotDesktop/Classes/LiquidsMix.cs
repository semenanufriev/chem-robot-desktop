﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volume = System.Single;
using Id = System.Int32;

namespace ChemRobotDesktop
{
    [Serializable]
    public class LiquidUnit
    {
        public Liquid liquid { get; set; }
        public Volume volume { get; set; }
        public float proportion { get; set; }

        public LiquidUnit(Liquid liquid, Volume volume, float proportion)
        {
            this.liquid = liquid.deepCopy();
            this.volume = volume;
            this.proportion = proportion;
        }
        public LiquidUnit(Liquid liquid, Volume volume)
        {
            this.liquid = liquid.deepCopy();
            this.volume = volume;
            this.proportion = 1.0f;
        }

        public LiquidUnit() 
        {
            this.liquid = new Liquid();
            this.volume = 0;
            this.proportion = 1.0f;
        }

        public LiquidUnit DeepCopy()
        {
            LiquidUnit newUnit = new LiquidUnit();
            newUnit.liquid = this.liquid.deepCopy();
            newUnit.volume = this.volume;
            newUnit.proportion = this.proportion;
            return newUnit;
        }
    }
    [Serializable]
    public  class LiquidsMix
    {
        public Dictionary<Id, LiquidUnit> liquids { get; set; } = new Dictionary<Id, LiquidUnit>();
        public Volume volume { get; set; }

        public LiquidsMix() { }
        public LiquidsMix DeepCopy()
        {
            LiquidsMix newMix = new LiquidsMix();
            newMix.volume = this.volume;

            foreach (var pair in this.liquids)
            {
                newMix.liquids.Add(pair.Key, pair.Value.DeepCopy());
            }

            return newMix;
        }

        public void addLiquid(Liquid liquid, Volume volume)
        {
            // Если Liquid с таким ModelId уже есть в словаре, увеличиваем его объем
            // Иначе добавляем новый LiquidUnit в словарь
            if (liquids.ContainsKey(liquid.Id))
            {
                liquids[liquid.Id].volume += volume;
            }
            else
            {
                liquids.Add(liquid.Id, new LiquidUnit(liquid.deepCopy(), volume));
            }
            updateLiquidsMix();
        }

        public void addLiquidUnit(LiquidUnit liquidUnit)
        {
            // Аналогично методу addLiquid, добавляем или обновляем LiquidUnit
            if (liquids.ContainsKey(liquidUnit.liquid.Id))
            {
                liquids[liquidUnit.liquid.Id].volume += liquidUnit.volume;
            }
            else
            {
                liquids.Add(liquidUnit.liquid.Id, liquidUnit);
            }
            updateLiquidsMix();
        }

        public void addLiquidsMix(LiquidsMix liquidsMix)
        {
            // Добавляем каждый LiquidUnit из другого LiquidsMix в текущий словарь
            foreach (var pair in liquidsMix.liquids)
            {
                addLiquidUnit(pair.Value);
            }
        }

        private void updateVolume()
        {
            Volume tmpVolume = 0;
            foreach (var key in liquids.Keys.ToList())
            {
                if (liquids[key].volume < 0.001)
                {
                    liquids.Remove(key);
                }
                else
                {
                    tmpVolume += liquids[key].volume;
                }
            }

            this.volume = tmpVolume;
            
        }

        public LiquidsMix Extract(Volume volume)
        {
            LiquidsMix newLiquidMix = new LiquidsMix();
            if(volume > this.volume)
            {
                throw new Exception("Недостаточно жидкости");
            }
            foreach (var key in liquids.Keys.ToList())
            {
                Volume tmpVolume = volume * liquids[key].proportion;
                newLiquidMix.addLiquid(liquids[key].liquid, tmpVolume);
            }
            
            RemoveVolume(volume);
            

            return newLiquidMix;
        }

        private void RemoveVolume(Volume volume)
        {
            if (volume > this.volume)
            {
                throw new Exception("Недостаточно жидкости");
            }
            foreach (var key in liquids.Keys.ToList())
            {
                liquids[key].volume -= volume * liquids[key].proportion;
            }
            updateLiquidsMix();

        }

        private void updateProportions()
        {
            // Обновление пропорций для каждого LiquidUnit
            foreach (var pair in liquids)
            {
                pair.Value.proportion = pair.Value.volume / volume;
            }
        }

        private void updateLiquidsMix()
        {
            updateVolume();
            updateProportions();
        }


        public void updateLiquidsData(List<Liquid> newLiquids)
        {
            // Создаём множество ID из нового списка жидкостей для более эффективного поиска
            var newLiquidsIds = new HashSet<Id>(newLiquids.Select(liquid => liquid.Id));

            // Удаляем из словаря те LiquidUnit, которых нет в новом списке
            var idsToRemove = liquids.Keys.Where(id => !newLiquidsIds.Contains(id)).ToList();
            foreach (var id in idsToRemove)
            {
                liquids.Remove(id);
            }

            // Обновляем LiquidUnit для каждой жидкости из нового списка
            foreach (var newLiquid in newLiquids)
            {
                if (liquids.ContainsKey(newLiquid.Id))
                {
                    // Если LiquidUnit уже есть в словаре, обновляем его жидкость
                    liquids[newLiquid.Id].liquid = newLiquid.deepCopy();
                }
            }

            // Можно вызвать метод для обновления объема и пропорций, если необходимо
            updateLiquidsMix();
        }


    }
}
