﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using ChemRobotDesktop.Classes;

namespace ChemRobotDesktop
{

    class Translator
    {
        public List<string> instructionsList = new List<string>();
        public Project project;
        private string setSpeedCommand = " F30000";
        private string setZSpeedCommand = " F10000";
        private string setPipetteSpeedCommand = " F800";
        private string setAccelerationsCommand = " M201 X750 Y750 Z500 A100";
        private float PistonCoef = 0.035367765f; // [mm/мкл]
        private float pipetteLen = 38.0f; // mm
        private float drainExtraDistance = 0.0f; // mm
        static private float pipetteDropOffset = 5.0f; // отступ для сброса носика
        static private float pipettePutOnDistance = 5.0f;

        private float pipetteMaxVolume = 1000; // мкл
        private float pipetteMinVolume = 100;  // мкл
        private Point3D dropPlace;
        float maxH = 0;
        //static private float tipDropOffset = 4; 
        static private string goToPipetteZeroPosition = "G0 A" + pipetteDropOffset.ToString("0.00",
                       System.Globalization.CultureInfo.GetCultureInfo("en-US"));
        //private string dropNose = goToPipetteZeroPosition + "\n";
        private List<string> usedTips = new List<string>();





        public Translator()
        {

        }

        public void init(Project project)
        {
            this.project = project;

            maxH = 0;
            foreach (var zone in project.StartingZones)
            {
                if (zone.plate != null && zone.plate.Size != null && zone.plate.Size.Z > maxH)
                {
                    maxH = zone.plate.Size.Z; // Update maxZ if a larger value is found
                }
            }



        }

        private void refresh()
        {
            usedTips.Clear();
            instructionsList.Clear();
            //maxH = 0;
            /*

            if (this.robot != null)
            {
                foreach(Robot.Zone z in robot.zones)
                {
                    if(z.module.PLATE_TYPE == PLATE_TYPE.DROP_PLACE)
                    {
                        float X = z.module.currentPlate.Wells["A0"].X;
                        float Y = z.module.currentPlate.Wells["A0"].Y;
                        float Z = z.module.currentPlate.Wells["A0"].Z;
                        dropPlace = new Point3D(X, Y, Z);
                    }
                }
            }
            */
        }


        private Zone getZoneByPlateId(int plateId)
        {
            Zone zone = project.StartingZones.FirstOrDefault(z => z.plate != null && z.plate.ObjectId == plateId);
            return zone;
        }



        public List<string> goToDrop()
        {
            List<string> result = new List<string>();

            //Point3D dropPos = robot.modules.
            string tmp = "G0 A0";
            result.Append(tmp);
            tmp = "G0 A" + pipetteDropOffset.ToString("0.00",
                System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            result.Append(tmp);
            return result;
        }

        public List<string> dropTip()
        {
            List<string> result = new List<string>();
            string tmp = "G0 A0";
            result.Append(tmp);
            tmp = "G0 A" + pipetteDropOffset.ToString("0.00",
                System.Globalization.CultureInfo.GetCultureInfo("en-US"));
            result.Append(tmp);
            return result;
        }


        public List<string> createNewTask(bool withChecksum = false)
        {
            List<string> result = new List<string>();
            refresh();

            result.AddRange(CreateInitCommands()); // Добавляем начальные инструкции

            foreach(ProtocolStep step in project.ProtocolSteps) // Для каждого шага протокола
            {
                try
                {
                    result.AddRange(translateStep(step)); // Генерируем инструкции  для шага
                }
                catch(Exception e) // Выводим сообщение, если выпала какая-то ошибка
                {
                    MessageBox.Show(e.Message);
                    throw new Exception("Что-то пошло не так при формировании задания\n" + e.Message);
                }
            }
            result.AddRange(CreateFinishCommands());

            instructionsList = result;

            if(withChecksum)
            {
                List<string> GcodeChecked = new List<string>();
                int tmpNum = 1;
                for (int i = 0; i < result.Count; i++)
                {
                    if (result[i][0] != ';')
                    {
                        GcodeChecked.Add(AddChecksum(result[i], tmpNum));
                        tmpNum++;
                    }
                }
                return GcodeChecked;
            }

            return result;
        }



        public List<string> CreateFinishCommands()
        {
            List<string> result = new List<string>();
            string tmp;
            result.Add(goUpCommand());
            //result.Add("G1 X500 Y200")
            //tmp = "G28";// Go Home
            //result.Add(tmp);

            // Потом возможно ещё что-то добавится

            return result;
        }

        public List<string> CreateInitCommands()
        {
            List<string> result = new List<string>();
            string command;

            command = "G28";// Go Home
            result.Add(command);
            result.Add(setAccelerationsCommand);
            //tmp = "G0 A" + ; // отойти немного от нулевого положения
            command = "G0 A" + (pipetteDropOffset).ToString("0.00",
                System.Globalization.CultureInfo.GetCultureInfo("en-US")) + setPipetteSpeedCommand;
            result.Add(command);
            // Потом возможно ещё что-то добавится

            return result;
        }
        public List<string> translateStep(ProtocolStep step) // Сгенерировать все инструкции для одного шага
        {
            List<string> result = new List<string>();

            switch (step.Type)
            {
                case STEP_TYPE.TRANSFER:
                    //result.Add("; Шаг перелива из " + step.sourceZone.name + " в " + step.destinationZone.name);
                    result.AddRange(translateTransfer((ProtocolStepTransfer)step));
                    break;

                case STEP_TYPE.MIX:
                    //result.Add("; Шаг перемешивания в " + step.sourceZone.name);
                    result.AddRange(translateMix((ProtocolStepMix)step));
                    break;

                case STEP_TYPE.WAIT:
                    result.Add("; Шаг ожидания");
                    result.AddRange(translateDelay((ProtocolStepWait)step));
                    break;
            }



            return result;
        }
        public List<string> translateTransfer(ProtocolStepTransfer step) // Сгенерировать инструкции для шага "Перелить"
        {
            List<string> result = new List<string>();
            string sourceWellName;
            if (step.SourceWells.Count == 1) {
                sourceWellName = step.SourceWells[0];
            }
            else
            {

                throw new Exception("В качестве источника выбрано несколько клеток в:\nМодуль: " + "\nШаг номер: " + step.Id.ToString());

            }


            if (step.TipChangeMode == TIP_CHANGE_MODE.ONCE_AT_START) // Если надо поменять носик только в начале шага
            {
                result.Add(";Смена носика");
                result.AddRange(changeTipCommands());
            }


            foreach (string destinationName in step.DestinationWells)
            {
                if (step.TipChangeMode == TIP_CHANGE_MODE.FOR_EVERY_WELL) // Если надо поменять носик для каждого шага
                {
                    result.Add(";Смена носика");
                    result.AddRange(changeTipCommands());
                }
                
                result.Add(";Переместиться к ячейке источнику: " + sourceWellName);
                result.AddRange(goToWellCommands(getZoneByPlateId(step.SourcePlateId), sourceWellName)); // Переместиться к ячейке источнику

                result.Add(";Набрать нужный объём жидкости");
                result.AddRange(fillCommands(step.TransferVolume)); // Набрать нужный объём жидкости

                result.Add(";Переместиться к ячейке назначения: " + destinationName);
                result.AddRange(goToWellCommands(getZoneByPlateId(step.DestinationPlateId), destinationName)); // Переместиться к ячейке назначения

                result.Add(";Вылить содержимое пипетки");
                result.AddRange(drainCommands(step.TransferVolume)); // Вылить содержимое пипетки

            }



            return result;
        }

        public List<string> translateDelay(ProtocolStepWait step) // Сгенерировать инструкции для шага "Подождать"
        {
            List<string> result = new List<string>();

            string tmp = "G4 S" + step.WaitTimeSeconds.ToString(); // G4 - подождать кол-во секунд после S
            result.Add(tmp);

            // Вроде больше ничего не надо

            return result;
        }

        public List<string> translateMix(ProtocolStepMix step) // Сгенерировать инструкции для шага "Перемешать"
        {
            List<string> result = new List<string>();


            if (step.TipChangeMode == TIP_CHANGE_MODE.ONCE_AT_START) // Если надо поменять носик только в начале шага
            {
                result.Add(";Смена носика");
                result.AddRange(changeTipCommands());
            }


            foreach (string wellName in step.SourceWells) // Для каждой ячейки 
            {

                if (step.TipChangeMode == TIP_CHANGE_MODE.FOR_EVERY_WELL) // Если надо поменять носик для каждого шага
                {
                    result.Add(";Смена носика");
                    result.AddRange(changeTipCommands());
                }

                result.Add(";Перейти к ячейке, содержимое которой надо перемешать: " + wellName);
                result.AddRange(goToWellCommands(getZoneByPlateId(step.SourcePlateId), wellName)); // Перейти к ячейке, содержимое которой надо перемешать

                result.Add(";Перемешать содержимое ячейки");
                result.AddRange(mixLiquidsCommands(step, wellName)); // Перемешать содержимое ячейки
            }

            return result;
        }

        public List<string> mixLiquidsCommands(ProtocolStepMix step, string wellName) // Сгенерировать комманды для перемешивания содержимого ячейки
        {
            List<string> result = new List<string>();

            float volume;
            /* // Надо будет потом добавить опцию выбора максимального объёма
            if (step.maxVolume)
            {
                volume = step.sourceZone.module.plateStepsDynasty[step.id].Wells[wellName].ExistingLiquidVolume;
                if(volume > pipetteMaxVolume)
                {
                    volume = pipetteMaxVolume;
                }
                if(volume < pipetteMinVolume)
                {
                    throw new Exception("Слишком мало жидкости для перемешивания в:\nМодуль: " + step.sourceZone.name + "\nЯчейка: " + wellName + "\nШаг номер: " + step.id.ToString());
                }
            }
            else
            {
                volume = step.volume;
            }
            */

            volume = step.MixVolume;


            for (int i = 0; i < step.MixCount; i++)
            {
                result.Add("; Набрать " + volume.ToString() + " мкл");
                result.AddRange(fillCommands(volume));

                result.Add("; Слить");
                result.AddRange(drainCommands(volume));
            }



            return result;
        }

        public List<string> fillCommands(float volume) // Сгенерировать комманду для набора заданного объёма жидкости (возможно надо будет переделать название оси и пересчитать коэффициент перевода + подумать о скорости)
        {
            List<string> result = new List<string>();
            string command = "G91"; // Режим относительного позиционирования
            result.Add(command);
            float fillDistance = volume * PistonCoef;
            if(fillDistance > pipetteLen - pipetteDropOffset)
            {
                fillDistance = pipetteLen - pipetteDropOffset;
            }
            command = "G0 A" + (fillDistance).ToString("0.00",
                System.Globalization.CultureInfo.GetCultureInfo("en-US")) + setPipetteSpeedCommand;
            result.Add(command);
            command = "G90"; // Выход из режима относительного позиционирования
            result.Add(command);
            command = "G4 S1";
            result.Add(command);
            return result;

        }

        public List<string> drainCommands(float volume) // Сгенерировать комманду для слива всего объёма жидкости (надо будет перепроверить параметры: длина поршня и т.д.)
        {
            List<string> result = new List<string>();
            string command = "G91"; // Режим относительного позиционирования
            result.Add(command);
            float drainDistance = volume * PistonCoef;
            if (drainDistance > pipetteLen - pipetteDropOffset)
            {
                drainDistance = pipetteLen - pipetteDropOffset;
            }
            drainDistance += drainExtraDistance;
            command = "G0 A-" + (drainDistance).ToString("0.00",
                System.Globalization.CultureInfo.GetCultureInfo("en-US")) + setPipetteSpeedCommand;
            result.Add(command);
            command = "G90"; // Выход из режима относительного позиционирования
            result.Add(command);
            command = "G4 S1";
            result.Add(command);
            return result;

        }

        public List<string> dropTipCommand() // Сгенерировать комманду для сброса носика (именно сброса самого носика без всех перемещений)
        {
            List<string> result = new List<string>();
            string command = "G1 A0" + setPipetteSpeedCommand;
            result.Add(command);
            command = "G1 A" + pipetteDropOffset.ToString("0.00",
                System.Globalization.CultureInfo.GetCultureInfo("en-US")) + setPipetteSpeedCommand;
            result.Add(command);
            return result;
        }

        public List<string> changeTipCommands()
        {
            List<string> result = new List<string>();

            // Проверить, что есть plate для сброса (тип DROP_PLACE)
            Zone dropZone = project.StartingZones.FirstOrDefault(z => z.plate != null && z.plate.Type == PLATE_TYPE.DROP_PLACE);
            if (dropZone == null)
            {
                throw new Exception("Не найдена зона для сброса носиков.");
            }

            // Переместиться к месту сброса носиков
            result.AddRange(goToWellCommands(dropZone, "A1")); // Используем первую ячейку для сброса
            result.AddRange(dropTipCommand());

            // Найти следующий чистый носик среди всех модулей с носиками
            Zone tipsZone = null;
            string nextTipWell = null;
            foreach (var zone in project.StartingZones.Where(z => z.plate != null && z.plate.Type == PLATE_TYPE.TIP_RACK))
            {
                foreach (var wellEntry in zone.plate.Tips)
                {
                    if (!usedTips.Contains(wellEntry.Key))
                    {
                        nextTipWell = wellEntry.Key;
                        usedTips.Add(nextTipWell);
                        tipsZone = zone;
                        break;
                    }
                }
                if (nextTipWell != null)
                {
                    break;
                }
            }

            if (nextTipWell == null)
            {
                throw new Exception("Не осталось чистых носиков.");
            }

            // Переместиться к ячейке с новым носиком
            result.AddRange(goToWellCommands(tipsZone, nextTipWell));


            return result;
        }
        public bool needToChangeTip(ProtocolStep step)
        {
            bool result = false;



            return result;
        }

        public string goUpCommand()
        {
            string result = "";
            result += "G1 " + "Z" + /*robot.maxH*/maxH.ToString("0.00",
                       System.Globalization.CultureInfo.GetCultureInfo("en-US")) + setZSpeedCommand; // Подъём до высоты maxH и правильное форматирование (через точку и 2 знака после неё)
            
            return result;
        }// Сгенерировать комманду для подъёма чуть выше самого высокого модуля (возможно надо будет переделать)

        public string goToPositionCommand(Point3D target_position, int speed = -1) // Сгенерировать комманду для переходя к точке
        {
            string result;
            if (speed > 0)
            {
                result = "G1 X" + target_position.X.ToString("0.00",
                           System.Globalization.CultureInfo.GetCultureInfo("en-US")) + " Y" + target_position.Y.ToString("0.00",
                           System.Globalization.CultureInfo.GetCultureInfo("en-US")) + " Z" + target_position.Z.ToString("0.00",
                           System.Globalization.CultureInfo.GetCultureInfo("en-US")) + " F" + speed.ToString();
            }
            else
            {
                result = "G1 X" + target_position.X.ToString("0.00",
                          System.Globalization.CultureInfo.GetCultureInfo("en-US")) + " Y" + target_position.Y.ToString("0.00",
                          System.Globalization.CultureInfo.GetCultureInfo("en-US")) + " Z" + target_position.Z.ToString("0.00",
                          System.Globalization.CultureInfo.GetCultureInfo("en-US")) + setSpeedCommand;
            }

            return result;
        }

        public List<string> goToWellCommands(Zone zone, string wellName) // Сгенерировать комманды для переходя к ячейке
        {
            List<string> result = new List<string>();

            //-----------------------------
            // Подняться выше самого высокого модуля
            result.Add(";Подняться выше самого высокого модуля");
            result.Add(goUpCommand()); 
            //-----------------------------

            // Переместиться к точке над ячейкой
            Point3D pointAboveWell = getXY(zone, wellName);
            pointAboveWell.Z = maxH;// robot.maxH;
            string tmp = goToPositionCommand(pointAboveWell);
            result.Add(";Переместиться к точке над ячейкой");
            result.Add(tmp);
            //-----------------------------

            // опуститься до дна ячейки
            pointAboveWell.Z = getLowestPoint(zone, wellName);
            tmp = goToPositionCommand(pointAboveWell);
            result.Add(";Опуститься до дна ячейки");
            result.Add(tmp);
            //------------------------------


            return result;
        }

        public float getLowestPoint(Zone zone, string wellName) // Получить высоту дна ячейки (Возможно надо будет поменять)
        {
            float result = 100;
            if (zone.plate.Type == PLATE_TYPE.TIP_RACK)
            {
                result = zone.plate.Size.Z - pipettePutOnDistance;
            }
            else
            {
                result = zone.plate.Size.Z - zone.plate.Wells[wellName].Depth;
            }
            // Здесь надо будет что-то придумать с тем, чтобы опускаться не до самого дна, а например до уровня, до которого опустится жидкость после набора всего объёма в пипетку

            return result;
        }

        public Point3D getXY(Zone zone, string wellName) // Получить координаты XY ячейки, для модуля в зоне Z
        {
            Point3D point = new Point3D(0, 0, 0);
            PointF zero_coord = project.StartingPoints[zone.id];

            point.X = zero_coord.X + zone.plate.CellsPositions[wellName].X;
            point.Y = zero_coord.Y + (zone.plate.Size.Y - zone.plate.CellsPositions[wellName].Y); // Временный хак (все координаты хранятся в системе координат изображение, а там ось Y идёт сверху вниз) надо придумать, как сделать покрасивее

            // Надо будет добавить учёт размеров самой ячейки

            return point;
        }

        // Функция для вычисления контрольной суммы
        public int CalculateChecksum(string gcodeLine)
        {
            int checksum = 0;
            foreach (char c in gcodeLine)
            {
                checksum ^= c;
            }
            return checksum;
        }
        public string AddChecksum(string gcode, int lineNumber)
        {
            // Добавляем номер строки в начало команды
            string lineWithNumber = $"N{lineNumber} {gcode.Trim()}";

            // Вычисляем контрольную сумму
            int checksum = CalculateChecksum(lineWithNumber);

            // Форматируем строку G-кода с контрольной суммой
            string formattedGcode = $"{lineWithNumber}*{checksum}";

            // Возвращаем отформатированную строку G-кода
            return formattedGcode;
        }



    }
}
