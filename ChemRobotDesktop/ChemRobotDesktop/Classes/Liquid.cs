﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Drawing.Imaging;
using System.Xml.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;
using System.Text.Json.Serialization;
using System.Text.Json;

namespace ChemRobotDesktop
{

    public class JsonColorConverter : JsonConverter<Color>
    {
        public override Color Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            string colorString = reader.GetString();
            return ColorTranslator.FromHtml(colorString);
        }

        public override void Write(Utf8JsonWriter writer, Color colorValue, JsonSerializerOptions options)
        {
            writer.WriteStringValue(ColorTranslator.ToHtml(colorValue));
        }
    }


    [Serializable]
    public class Liquid
    {
        public string Name { get; set; }
        public string Description { get; set; }
        [JsonConverter(typeof(JsonColorConverter))]
        public Color Color { get; set; }
        public int Id { get; set; }
        public Liquid deepCopy()
        {
            Liquid clone = new Liquid();
            clone.Name = Name;
            clone.Description = Description;
            clone.Color = Color.FromArgb(Color.ToArgb()); // глубокое копирование цвета
            clone.Id = Id;
            return clone;
        }
        public void clear()
        {
            Name = null;
            Description = null;
            Color = Color.Transparent;
            Id = 0;
        }
    }
}
