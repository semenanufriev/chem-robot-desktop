﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
//using System.Xml;
using Newtonsoft.Json;
using System.IO;
using WellName = System.String;
using ChemRobotDesktop.CustomControls;

namespace ChemRobotDesktop.Classes
{


    public enum PLATE_TYPE : int
    {
        WELL_PLATE,
        RESERVOIR,
        TIP_RACK,
        DROP_PLACE
    }

    


    public class Plate
    {
        [JsonIgnore]
        public string[] YNames = new string[26];
        public Point ImageSize = new Point(127 * 6, 85 * 6);
        public string Name { get; set; }
        public int ModelId { get; set; }
        public int ObjectId { get; set; }
        public Dictionary<WellName, Well>? Wells { get; set; } = new Dictionary<WellName, Well>();
        public Dictionary<WellName, Tip>? Tips { get; set; } = new Dictionary<WellName, Tip>();
        public Dictionary<WellName, PointF> CellsPositions { get; set; } = new Dictionary<WellName, PointF>();
        public Point3D Size { get; set; }
        public int Rows { get; set; }
        public int Columns { get; set; }
        public PointF GridSpacing { get; set; }
        public PointF GridOffset { get; set; }

        public PLATE_TYPE Type;


        public Plate DeepCopy()
        {
            Plate newPlate = new Plate
            {
                YNames = (string[])this.YNames.Clone(),
                ImageSize = new Point(this.ImageSize.X, this.ImageSize.Y),
                Name = this.Name,
                ModelId = this.ModelId,
                ObjectId = this.ObjectId,
                Wells = this.Wells != null ? this.Wells.ToDictionary(entry => entry.Key, entry => entry.Value.DeepCopy()) : null,
                Tips = this.Tips != null ? this.Tips.ToDictionary(entry => entry.Key, entry => entry.Value.DeepCopy()) : null,
                CellsPositions = new Dictionary<WellName, PointF>(this.CellsPositions),
                Size = new Point3D(this.Size.X, this.Size.Y, this.Size.Z),
                Rows = this.Rows,
                Columns = this.Columns,
                GridSpacing = new PointF(this.GridSpacing.X, this.GridSpacing.Y),
                GridOffset = new PointF(this.GridOffset.X, this.GridOffset.Y),
                Type = this.Type
            };

            return newPlate;
        }

        public void SavePlateToFile(string filePath)
        {
            try
            {
                string json = JsonConvert.SerializeObject(this, Formatting.Indented);
                File.WriteAllText(filePath, json);
            }
            catch (Exception ex)
            {
                // Обработка ошибок при записи файла
                Console.WriteLine("Ошибка при сохранении файла: " + ex.Message);
            }
        }

        public Plate LoadPlateFromFile(string filePath)
        {
            try
            {
                string json = File.ReadAllText(filePath);
                return JsonConvert.DeserializeObject<Plate>(json);
            }
            catch (Exception ex)
            {
                // Обработка ошибок при чтении файла
                Console.WriteLine("Ошибка при загрузке файла: " + ex.Message);
                return null;
            }
        }

        public Plate() {
            for (char c = 'A'; c <= 'Z'; c++)
            {
                YNames[c - 'A'] = c.ToString();
            }
        }


        public void FillWellPositions()
        {
            if (Wells != null || Tips != null)
            {
                for (int i = 0; i < Rows; i++)
                {
                    for (int j = 0; j < Columns; j++)
                    {
                        PointF position = new PointF();
                        position.X = GridOffset.X + GridSpacing.X * j;
                        position.Y = GridOffset.Y + GridSpacing.Y * i;
                        CellsPositions[YNames[i] + (j + 1).ToString()] = position;
                    }
                }

            }
        }
       

        public static void DrawRoundedRectangle(Graphics g, Pen pen, float x, float y, float width, float height, float radius, Brush brush = null)
        {
            // Создаем графический путь
            System.Drawing.Drawing2D.GraphicsPath path = new System.Drawing.Drawing2D.GraphicsPath();

            // Верхний левый угол
            path.AddArc(x, y, radius, radius, 180, 90);

            // Верхняя линия
            path.AddLine(x + radius, y, x + width - radius, y);

            // Верхний правый угол
            path.AddArc(x + width - radius, y, radius, radius, 270, 90);

            // Правая линия
            path.AddLine(x + width, y + radius, x + width, y + height - radius);

            // Нижний правый угол
            path.AddArc(x + width - radius, y + height - radius, radius, radius, 0, 90);

            // Нижняя линия
            path.AddLine(x + width - radius, y + height, x + radius, y + height);

            // Нижний левый угол
            path.AddArc(x, y + height - radius, radius, radius, 90, 90);

            // Закрываем фигуру
            path.CloseFigure();

            // Заливаем фигуру
            if (brush != null)
            {
                g.FillPath(brush, path);
            }

            // Рисуем фигуру
            if (pen != null)
            {
                g.DrawPath(pen, path);
            }
        }

        public Image getImage(List<WellName> selectedWells = null, bool writePlateName = false)
        {
            switch (Type)
            {
                case PLATE_TYPE.TIP_RACK:
                    return getImageForTipRack(selectedWells, writePlateName);
                    break;
                default:
                    return getImageForLabware(selectedWells, writePlateName);
                    break;
            }
        }

        public Image getImageForLabware(List<WellName> selectedWells = null, bool writePlateName = false)
        {

            // Создаем новый битмап с размерами ImageSize
            Bitmap bitmap = new Bitmap(ImageSize.X, ImageSize.Y);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                // Установка качества рисования
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.Clear(Color.Transparent);

                // Рисуем прямоугольную рамку вокруг изображения
                Pen pen = new Pen(Color.Gray, ImageSize.X / 100);
                float cornerRadius = (float)ImageSize.X / 10.0f; // Размер радиуса для закругления углов
                DrawRoundedRectangle(graphics, pen, pen.Width, pen.Width, ImageSize.X - pen.Width * 2, ImageSize.Y - pen.Width * 2, cornerRadius, new SolidBrush(SystemColors.InactiveBorder));


                // Отрисовка ячеек
                if (Wells != null)
                {
                    foreach (var wellEntry in Wells)
                    {
                        WellName wellName = wellEntry.Key;
                        Well well = wellEntry.Value;

                        if (CellsPositions.TryGetValue(wellName, out PointF position))
                        {
                            Pen WellPen = new Pen(Color.Black, 3);

                            bool isSelected = false;
                            if (selectedWells != null && selectedWells.Contains(wellName))
                            {
                                isSelected = true;
                            }

                            switch (well.Shape)
                            {
                                case WELL_SHAPE.RECTANGLE:
                                    DrawRectangleWell(graphics, well, position, WellPen, isSelected);
                                    break;
                                case WELL_SHAPE.ROUND:
                                    DrawRoundWell(graphics, well, position, WellPen, isSelected);
                                    break;
                            }
                        }
                    }
                }






                // Добавление подписей для строк и столбцов
                float scaleFactor_1 = (float)ImageSize.Y / 200.0f;
                float labelFontSize = (int)(10 * scaleFactor_1);//GridSpacing.Y / 2; // Примерный размер шрифта для подписей
                using (Font labelFont = new Font("Arial", labelFontSize, FontStyle.Regular, GraphicsUnit.Pixel))
                {
                    Brush labelBrush = new SolidBrush(Color.Black);
                    DrawLabels(graphics, labelFont, labelBrush);
                }

                // Добавление название
                if (writePlateName)
                {
                    float scaleFactor = (float)ImageSize.Y / 200.0f;
                    cornerRadius = (float)ImageSize.X / 10.0f;
                    DrawLabwareName(graphics, this.Name, pen, scaleFactor, cornerRadius);
                }

            }
            //bitmap.Save("C:\\GitLab\\chem-robot-desktop\\ChemRobotDesktop\\ChemRobotDesktop\\Labware\\1.jpg");
            return bitmap;
        }


        public Image getImageForTipRack(List<WellName> selectedWells = null, bool writePlateName = false)
        {

            // Создаем новый битмап с размерами ImageSize
            Bitmap bitmap = new Bitmap(ImageSize.X, ImageSize.Y);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                // Установка качества рисования
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.Clear(Color.Transparent);

                // Рисуем прямоугольную рамку вокруг изображения
                Pen pen = new Pen(Color.Gray, ImageSize.X / 100);
                float cornerRadius = (float)ImageSize.X / 10.0f; // Размер радиуса для закругления углов
                DrawRoundedRectangle(graphics, pen, pen.Width, pen.Width, ImageSize.X - pen.Width * 2, ImageSize.Y - pen.Width * 2, cornerRadius, new SolidBrush(Color.LightGray));


                // Отрисовка ячеек
                if (Tips != null)
                {
                    foreach (var tipEntry in Tips)
                    {
                        WellName wellName = tipEntry.Key;
                        Tip tip = tipEntry.Value;

                        if (CellsPositions.TryGetValue(wellName, out PointF position))
                        {
                            Pen TipPen = new Pen(Color.Black, 3);

                            DrawTip(graphics, tip, position, TipPen);
                        }
                    }
                }



                // Добавление подписей для строк и столбцов
                float scaleFactor_1 = (float)ImageSize.Y / 200.0f;
                float labelFontSize = (int)(10 * scaleFactor_1);//GridSpacing.Y / 2; // Примерный размер шрифта для подписей
                using (Font labelFont = new Font("Arial", labelFontSize, FontStyle.Regular, GraphicsUnit.Pixel))
                {
                    Brush labelBrush = new SolidBrush(Color.Black);
                    DrawLabels(graphics, labelFont, labelBrush);
                }

                // Добавление название
                if (writePlateName)
                {
                    float scaleFactor = (float)ImageSize.Y / 200.0f;
                    cornerRadius = (float)ImageSize.X / 10.0f;
                    DrawLabwareName(graphics, this.Name, pen, scaleFactor, cornerRadius);
                }

            }
            //bitmap.Save("C:\\GitLab\\chem-robot-desktop\\ChemRobotDesktop\\ChemRobotDesktop\\Labware\\1.jpg");
            return bitmap;
        }


        private void DrawLabels(Graphics graphics, Font labelFont, Brush labelBrush)
        {
            float scaleX = ImageSize.X / Size.X;
            float scaleY = ImageSize.Y / Size.Y;
            float offsetX = 0, offsetY = 0;
            if (Wells != null)
            {
                if (Wells.ElementAt(0).Value.Shape == WELL_SHAPE.ROUND)
                {
                    offsetX = Wells.ElementAt(0).Value.Diameter.Value / 2;
                    offsetY = offsetX;
                }
                else
                {
                    offsetX = (float)Wells.ElementAt(0).Value.RectSize.X / 2;
                    offsetY = (float)Wells.ElementAt(0).Value.RectSize.Y / 2;
                }
            }
            else
            {
                offsetX = Tips.ElementAt(0).Value.Diameter / 2;
                offsetY = offsetX;
            }
            // Рисуем подписи для оси Y (буквы A-H или другие, в зависимости от Rows)
            float yLabelPosX = ImageSize.X / 50; // Близко к левому краю изображения
            for (int i = 0; i < Rows; i++)
            {
                string label = YNames[i % YNames.Length]; // Получаем название строки из YNames
                SizeF labelSize = graphics.MeasureString(label, labelFont);
                PointF position = new PointF(yLabelPosX, (GridOffset.Y + i * GridSpacing.Y + offsetY) * scaleY - labelSize.Height / 2);

                // Рисуем текст на фоне
                graphics.DrawString(label, labelFont, labelBrush, position);
            }

            // Рисуем подписи для оси X (числа 1-12 или другие, в зависимости от Columns)
            float xLabelPosY = ImageSize.Y / 50; // Близко к верхнему краю изображения
            for (int i = 0; i < Columns; i++)
            {
                string label = (i + 1).ToString();
                SizeF labelSize = graphics.MeasureString(label, labelFont);
                PointF position = new PointF((GridOffset.X + i * GridSpacing.X + offsetX) * scaleX - labelSize.Width / 2, xLabelPosY);

                // Рисуем текст на фоне
                graphics.DrawString(label, labelFont, labelBrush, position);
            }
        }


        private void DrawRectangleWell(Graphics graphics, Well well, PointF position, Pen WellPen, bool isSelected)
        {
            float scaleX = ImageSize.X / Size.X;
            float scaleY = ImageSize.Y / Size.Y;
            float x = position.X * scaleX;
            float y = position.Y * scaleY;
            if (isSelected)
            {
                WellPen.Color = Color.Blue;
            }
            float width = well.RectSize.X * scaleX;
            float height = well.RectSize.Y * scaleY;
            graphics.DrawRectangle(WellPen, x, y, width, height);

            if (well.Liquid != null && well.Liquid.liquids.Count > 0)
            {
                float totalVolume = well.Liquid.liquids.Values.Sum(l => l.volume);
                float currentHeight = y;

                foreach (var liquidUnit in well.Liquid.liquids.Values)
                {
                    float segmentHeight = (liquidUnit.volume / totalVolume) * height;
                    using (Brush brush = new SolidBrush(liquidUnit.liquid.Color))
                    {
                        graphics.FillRectangle(brush, x, currentHeight, width, segmentHeight);
                    }
                    currentHeight += segmentHeight;
                }
            }
        }

        private void DrawRoundWell(Graphics graphics, Well well, PointF position, Pen WellPen, bool isSelected)
        {
            float scaleX = ImageSize.X / Size.X;
            float scaleY = ImageSize.Y / Size.Y;
            float x = position.X * scaleX;
            float y = position.Y * scaleY;
            if (isSelected)
            {
                WellPen.Color = Color.Blue;
            }
            float diameter = well.Diameter.Value * Math.Min(scaleX, scaleY);
            graphics.DrawEllipse(WellPen, x, y, diameter, diameter);

            if (well.Liquid != null && well.Liquid.liquids.Count > 0)
            {
                float totalVolume = well.Liquid.liquids.Values.Sum(l => l.volume);
                float startAngle = 0;

                foreach (var liquidUnit in well.Liquid.liquids.Values)
                {
                    float sweepAngle = (liquidUnit.volume / totalVolume) * 360; // Вычисление угла сегмента
                    using (Brush brush = new SolidBrush(liquidUnit.liquid.Color))
                    {
                        graphics.FillPie(brush, x, y, diameter, diameter, startAngle, sweepAngle);
                    }
                    startAngle += sweepAngle;
                }
            }
        }

        private void DrawTip(Graphics graphics, Tip tip, PointF position, Pen TipPen)
        {
            float scaleX = ImageSize.X / Size.X;
            float scaleY = ImageSize.Y / Size.Y;
            float x = position.X * scaleX;
            float y = position.Y * scaleY;

            float diameter = tip.Diameter * Math.Min(scaleX, scaleY);
            graphics.DrawEllipse(TipPen, x, y, diameter, diameter);

            if (tip.Exist)
            {
                graphics.DrawEllipse(TipPen, x + diameter / 4, y + diameter / 4, diameter / 2, diameter / 2);
            }
        }


        private void DrawLabwareName(Graphics graphics, string name, Pen pen, float scaleFactor, float cornerRadius)
        {
            // Подбор размера шрифта
            int fontSize = (int)(20 * scaleFactor); // Например, масштабирование базового размера шрифта
            using (Font titleFont = new Font("Segoe UI", fontSize, FontStyle.Bold, GraphicsUnit.Pixel))
            {
                // Определение максимальной ширины области текста
                float textPadding = 5 * scaleFactor; // Отступ текста
                float maxWidth = ImageSize.X - textPadding;

                // Разбиение текста на строки
                string[] words = name.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                List<string> wrappedLines = new List<string>();
                StringBuilder currentLine = new StringBuilder();
                foreach (var word in words)
                {
                    var testLine = currentLine.Length == 0 ? word : currentLine + " " + word;
                    SizeF size = graphics.MeasureString(testLine, titleFont);

                    if (size.Width > maxWidth)
                    {
                        if (wrappedLines.Count == 2) // Проверяем, является ли текущая строка третьей
                        {
                            // Добавляем многоточие к последнему слову
                            string trimmedLine = currentLine.ToString().TrimEnd();
                            if (trimmedLine.LastIndexOf(' ') > 0)
                            {
                                trimmedLine = trimmedLine.Substring(0, trimmedLine.LastIndexOf(' ')) + " ...";
                            }
                            wrappedLines.Add(trimmedLine);
                            break; // Выходим из цикла, не добавляя больше строк
                        }
                        wrappedLines.Add(currentLine.ToString());
                        currentLine.Clear();
                        currentLine.Append(word);
                    }
                    else
                    {
                        if (currentLine.Length > 0)
                            currentLine.Append(" ");
                        currentLine.Append(word);
                    }
                }
                if (wrappedLines.Count < 3 && currentLine.Length > 0) // Добавляем оставшуюся строку, если нужно
                    wrappedLines.Add(currentLine.ToString());

                // Определение высоты заголовка
                float textHeight = wrappedLines.Count * graphics.MeasureString(name, titleFont).Height * 0.6f;
                int titleHeight = (int)(textHeight + textPadding * 2);

                // Определение позиции заголовка
                float titleYPosition = ImageSize.Y - titleHeight - pen.Width;

                // Рисуем прямоугольник с заголовком
                //DrawFilledRoundedRectangle(graphics, new SolidBrush(Color.FromArgb(175, 0, 0, 0)), pen.Width, titleYPosition, ImageSize.X - pen.Width * 2, titleHeight, cornerRadius);
                DrawRoundedRectangle(graphics, null, pen.Width, titleYPosition, ImageSize.X - pen.Width * 2, titleHeight, cornerRadius, new SolidBrush(Color.FromArgb(175, 0, 0, 0)));
                // Установка цвета текста
                Brush textBrush = new SolidBrush(Color.White);

                // Рисуем каждую строку
                float yPosition = titleYPosition;
                foreach (var line in wrappedLines)
                {
                    graphics.DrawString(line, titleFont, textBrush, new PointF(pen.Width + textPadding, yPosition));
                    yPosition += graphics.MeasureString(line, titleFont).Height * 0.6f;
                }
            }
        }

        public Image getImageTipRack(List<WellName> selectedWells = null, bool writePlateName = false)
        {
            // Рассчитываем масштабы преобразования миллиметров в пиксели
            float scaleX = ImageSize.X / Size.X;
            float scaleY = ImageSize.Y / Size.Y;

            // Создаем новый битмап с размерами ImageSize
            Bitmap bitmap = new Bitmap(ImageSize.X, ImageSize.Y);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                // Установка качества рисования
                graphics.SmoothingMode = SmoothingMode.AntiAlias;
                graphics.Clear(Color.Transparent);
                // Рисуем прямоугольную рамку вокруг изображения

                //graphics.DrawRectangle(Pens.Black, offset, offset, ImageSize.X - offset, ImageSize.Y - offset);
                Pen pen = new Pen(Color.Gray, ImageSize.X / 100);
                //graphics.DrawRectangle(pen, offset, offset, ImageSize.X - offset - pen.Width, ImageSize.Y - offset - pen.Width);
                float cornerRadius = (float)ImageSize.X / 10.0f; // Размер радиуса для закругления углов

                // Рисуем прямоугольник с закругленными углами
                //DrawRoundedRectangle(graphics, pen, pen.Width, pen.Width, ImageSize.X - pen.Width * 2, ImageSize.Y - pen.Width * 2, cornerRadius);
                DrawRoundedRectangle(graphics, pen, pen.Width, pen.Width, ImageSize.X - pen.Width * 2, ImageSize.Y - pen.Width * 2, cornerRadius, new SolidBrush(SystemColors.InactiveBorder));
                //DrawFilledRoundedRectangle(graphics, new SolidBrush(SystemColors.InactiveBorder), pen.Width * 2, pen.Width * 2, ImageSize.X - pen.Width * 4, ImageSize.Y - pen.Width * 4, cornerRadius);



                if (Wells != null)
                {
                    foreach (var wellEntry in Wells)
                    {
                        WellName wellName = wellEntry.Key;
                        Well well = wellEntry.Value;

                        if (CellsPositions.TryGetValue(wellName, out PointF position))
                        {
                            Pen WellPen = new Pen(Color.Black, 3);

                            int offset = 0;
                            if (selectedWells != null && selectedWells.Contains(wellName))
                            {
                                WellPen.Color = Color.Blue;
                                WellPen.Width = WellPen.Width * 2;
                                //offset = ((int)WellPen.Width);
                            }

                            // Преобразование позиции из мм в пиксели
                            float x = position.X * scaleX;
                            float y = position.Y * scaleY;

                            // Рисуем колодец в зависимости от его формы
                            switch (well.Shape)
                            {
                                case WELL_SHAPE.RECTANGLE:
                                    float width = well.RectSize.X * scaleX;
                                    float height = well.RectSize.Y * scaleY;
                                    //graphics.DrawRectangle(WellPen, x, y, width, height);
                                    graphics.DrawRectangle(WellPen, x - offset, y - offset, width + offset * 2, height + offset * 2);

                                    if (well.Liquid != null && well.Liquid.liquids.Count > 0)
                                    {
                                        float totalVolume = well.Liquid.liquids.Values.Sum(l => l.volume);
                                        float currentHeight = y;

                                        foreach (var liquidUnit in well.Liquid.liquids.Values)
                                        {
                                            float segmentHeight = (liquidUnit.volume / totalVolume) * height;
                                            using (Brush brush = new SolidBrush(liquidUnit.liquid.Color))
                                            {
                                                graphics.FillRectangle(brush, x, currentHeight, width, segmentHeight);
                                            }
                                            currentHeight += segmentHeight;
                                        }
                                    }

                                    break;

                                case WELL_SHAPE.ROUND:
                                    float diameter = well.Diameter.Value * Math.Min(scaleX, scaleY);
                                    graphics.DrawEllipse(WellPen, x - offset, y - offset, diameter + offset * 2, diameter + offset * 2);
                                    if (well.Diameter.HasValue)
                                    {

                                        if (well.Liquid != null && well.Liquid.liquids.Count > 0)
                                        {
                                            float totalVolume = well.Liquid.liquids.Values.Sum(l => l.volume);
                                            float startAngle = 0;
                                            float sweepAngle;

                                            foreach (var liquidUnit in well.Liquid.liquids.Values)
                                            {
                                                sweepAngle = (liquidUnit.volume / totalVolume) * 360; // Вычисление угла сегмента
                                                using (Brush brush = new SolidBrush(liquidUnit.liquid.Color))
                                                {
                                                    graphics.FillPie(brush, x + WellPen.Width / 4, y + WellPen.Width / 4, diameter - WellPen.Width / 2, diameter - WellPen.Width / 2, startAngle, sweepAngle);
                                                }
                                                startAngle += sweepAngle;
                                            }
                                        }
                                    }
                                    break;
                            }
                        }
                    }
                }

                if (writePlateName)
                {
                    // Подбор размера шрифта
                    float scaleFactor = (float)ImageSize.Y / 200.0f;
                    int fontSize = (int)(20 * scaleFactor); // Например, масштабирование базового размера шрифта
                    using (Font titleFont = new Font("Segoe UI", fontSize, FontStyle.Bold, GraphicsUnit.Pixel))
                    {
                        // Определение максимальной ширины области текста
                        float textPadding = 5 * scaleFactor; // Отступ текста
                        float maxWidth = ImageSize.X - textPadding;

                        // Разбиение текста на строки
                        string[] words = this.Name.Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                        List<string> wrappedLines = new List<string>();
                        StringBuilder currentLine = new StringBuilder();
                        foreach (var word in words)
                        {
                            var testLine = currentLine.Length == 0 ? word : currentLine + " " + word;
                            SizeF size = graphics.MeasureString(testLine, titleFont);

                            if (size.Width > maxWidth)
                            {
                                if (wrappedLines.Count == 2) // Проверяем, является ли текущая строка третьей
                                {
                                    // Добавляем многоточие к последнему слову
                                    string trimmedLine = currentLine.ToString().TrimEnd();
                                    if (trimmedLine.LastIndexOf(' ') > 0)
                                    {
                                        trimmedLine = trimmedLine.Substring(0, trimmedLine.LastIndexOf(' ')) + " ...";
                                    }
                                    wrappedLines.Add(trimmedLine);
                                    break; // Выходим из цикла, не добавляя больше строк
                                }
                                wrappedLines.Add(currentLine.ToString());
                                currentLine.Clear();
                                currentLine.Append(word);
                            }
                            else
                            {
                                if (currentLine.Length > 0)
                                    currentLine.Append(" ");
                                currentLine.Append(word);
                            }
                        }
                        if (wrappedLines.Count < 3 && currentLine.Length > 0) // Добавляем оставшуюся строку, если нужно
                            wrappedLines.Add(currentLine.ToString());

                        // Определение высоты заголовка
                        float TextScaleFactor = 0.6f;
                        SizeF textSize = graphics.MeasureString(this.Name, titleFont);
                        float textHeight = wrappedLines.Count * textSize.Height * TextScaleFactor;

                        int titleHeight = (int)((textHeight + textPadding * 2));

                        // Определение позиции заголовка
                        float titleYPosition = ImageSize.Y - titleHeight - pen.Width;

                        // Рисуем прямоугольник с заголовком
                        //DrawFilledRoundedRectangle(graphics, new SolidBrush(Color.FromArgb(175, 0, 0, 0)), pen.Width, titleYPosition, ImageSize.X - pen.Width * 2, titleHeight, cornerRadius);
                        DrawRoundedRectangle(graphics, null, pen.Width, titleYPosition, ImageSize.X - pen.Width * 2, titleHeight, cornerRadius, new SolidBrush(Color.FromArgb(175, 0, 0, 0)));
                        // Установка цвета текста
                        Brush textBrush = new SolidBrush(Color.White);

                        // Рисуем каждую строку
                        float lineHeight = textSize.Height * TextScaleFactor;
                        float yPosition = titleYPosition;
                        foreach (var line in wrappedLines)
                        {
                            graphics.DrawString(line, titleFont, textBrush, new PointF(pen.Width + textPadding, yPosition));
                            yPosition += lineHeight;
                        }
                    }
                }



                // Установим шрифт для подписей
                float scaleFactor_1 = (float)ImageSize.Y / 200.0f;
                float labelFontSize = (int)(10 * scaleFactor_1);//GridSpacing.Y / 2; // Примерный размер шрифта для подписей
                //float labelFontSize = GridSpacing.Y / 4; // Примерный размер шрифта для подписей
                using (Font labelFont = new Font("Arial", labelFontSize, FontStyle.Regular, GraphicsUnit.Pixel))
                {
                    Brush labelBrush = new SolidBrush(Color.Black);
                    Brush labelBackgroundBrush = new SolidBrush(Color.LightGray); // Цвет фона подписей

                    // Рисуем подписи для оси Y (буквы A-H или другие, в зависимости от Rows)
                    float yLabelPosX = ImageSize.X / 50; // Близко к левому краю изображения
                    float offsetX = 0, offsetY = 0;
                    if (Wells.ElementAt(0).Value.Shape == WELL_SHAPE.ROUND)
                    {
                        offsetX = Wells.ElementAt(0).Value.Diameter.Value / 2;
                        offsetY = offsetX;
                    }
                    else
                    {
                        offsetX = (float)Wells.ElementAt(0).Value.RectSize.X / 2;
                        offsetY = (float)Wells.ElementAt(0).Value.RectSize.Y / 2;
                    }


                    for (int i = 0; i < Rows; i++)
                    {
                        string label = YNames[i % YNames.Length]; // Получаем название строки из YNames
                        SizeF labelSize = graphics.MeasureString(label, labelFont);
                        PointF position = new PointF(yLabelPosX, (GridOffset.Y + i * GridSpacing.Y + offsetY) * scaleY - labelSize.Height / 2);

                        // Рисуем фон для подписи
                        //graphics.FillRectangle(labelBackgroundBrush, position.X, position.Y, labelSize.Width, labelSize.Height);

                        // Рисуем текст на фоне
                        graphics.DrawString(label, labelFont, labelBrush, position);
                    }

                    // Рисуем подписи для оси X (числа 1-12 или другие, в зависимости от Columns)

                    float xLabelPosY = ImageSize.Y / 50; ; // Близко к верхнему краю изображения
                    for (int i = 0; i < Columns; i++)
                    {
                        string label = (i + 1).ToString();
                        SizeF labelSize = graphics.MeasureString(label, labelFont);
                        PointF position = new PointF((GridOffset.X + i * GridSpacing.X + offsetX) * scaleX - labelSize.Width / 2, xLabelPosY);

                        // Рисуем фон для подписи
                        //graphics.FillRectangle(labelBackgroundBrush, position.X, position.Y - labelSize.Height / 2, labelSize.Width, labelSize.Height);

                        // Рисуем текст на фоне
                        graphics.DrawString(label, labelFont, labelBrush, position);
                    }
                }
            }
            bitmap.Save("C:\\GitLab\\chem-robot-desktop\\ChemRobotDesktop\\ChemRobotDesktop\\Labware\\1.jpg");
            return bitmap;
        }



        public List<WellName> GetWellsInsideRectangle(RectangleF selectionRectangle) // Rect values (0-1)
        {
            if(Type == PLATE_TYPE.TIP_RACK)
            {
                return GetTipsInsideRectangle(selectionRectangle);
            }
            else
            {
                return GetWellsInsideRectangleMethod(selectionRectangle);
            }

        }

        public List<WellName> GetWellsInsideRectangleMethod(RectangleF selectionRectangle) // Rect values (0-1)
        {
            // Преобразуем RectangleF из долей в пиксельные координаты
            RectangleF pixelRectangle = new RectangleF(
                selectionRectangle.X * ImageSize.X,
                selectionRectangle.Y * ImageSize.Y,
                selectionRectangle.Width * ImageSize.X,
                selectionRectangle.Height * ImageSize.Y
            );

            // Рассчитываем масштабы преобразования миллиметров в пиксели
            float scaleX = ImageSize.X / Size.X;
            float scaleY = ImageSize.Y / Size.Y;

            var wellNames = new List<WellName>();

            foreach (var wellEntry in Wells)
            {
                WellName wellName = wellEntry.Key;
                Well well = wellEntry.Value;

                if (CellsPositions.TryGetValue(wellName, out PointF wellPosition))
                {
                    // Координаты и размеры колодца в пикселях
                    float wellX = wellPosition.X * scaleX;
                    float wellY = wellPosition.Y * scaleY;

                    RectangleF wellRectangle;
                    switch (well.Shape)
                    {
                        case WELL_SHAPE.RECTANGLE:
                            wellRectangle = new RectangleF(
                                wellX,
                                wellY,
                                well.RectSize.X * scaleX,
                                well.RectSize.Y * scaleY
                            );

                            if (pixelRectangle.IntersectsWith(wellRectangle))
                            {
                                wellNames.Add(wellName);
                            }

                            break;

                        case WELL_SHAPE.ROUND:
                            if (well.Diameter.HasValue)
                            {
                                float diameter = well.Diameter.Value * Math.Min(scaleX, scaleY);
                                wellRectangle = new RectangleF(
                                    wellX,
                                    wellY,
                                    diameter,
                                    diameter
                                );

                                if (pixelRectangle.IntersectsWith(wellRectangle))
                                {
                                    wellNames.Add(wellName);
                                }
                            }
                            break;
                    }
                }
            }

            return wellNames;
        }

        public List<WellName> GetTipsInsideRectangle(RectangleF selectionRectangle) // Rect values (0-1)
        {
            // Преобразуем RectangleF из долей в пиксельные координаты
            RectangleF pixelRectangle = new RectangleF(
                selectionRectangle.X * ImageSize.X,
                selectionRectangle.Y * ImageSize.Y,
                selectionRectangle.Width * ImageSize.X,
                selectionRectangle.Height * ImageSize.Y
            );

            // Рассчитываем масштабы преобразования миллиметров в пиксели
            float scaleX = ImageSize.X / Size.X;
            float scaleY = ImageSize.Y / Size.Y;

            var tipNames = new List<WellName>();

            foreach (var tipEntry in Tips)
            {
                WellName wellName = tipEntry.Key;
                Tip tip = tipEntry.Value;

                if (CellsPositions.TryGetValue(wellName, out PointF wellPosition))
                {
                    // Координаты и размеры колодца в пикселях
                    float wellX = wellPosition.X * scaleX;
                    float wellY = wellPosition.Y * scaleY;

                    RectangleF wellRectangle;
                        float diameter = tip.Diameter * Math.Min(scaleX, scaleY);
                    wellRectangle = new RectangleF(
                        wellX,
                        wellY,
                        diameter,
                        diameter
                    );

                    if (pixelRectangle.IntersectsWith(wellRectangle))
                    {
                        tipNames.Add(wellName);
                    };
                    
                }
            }

            return tipNames;
        }

        public void updateLiquidsData(List<Liquid> liquids)
        {
            if (Wells != null)
            {
                foreach (WellName wellName in Wells.Keys)
                {
                    Wells[wellName].Liquid.updateLiquidsData(liquids);
                }
            }
        }

        public void clearWells(List<WellName> selectedWells)
        {
            if (Wells != null)
            {
                foreach (var well in selectedWells)
                {
                    Wells[well].clearWell();
                }
            }
        }

    }




    public class Tip
    {
        public float MaxVolume { get; set; }
        public float MinVolume { get; set; }
        public float Height { get; set; } // From the end of the pipette to the nose edge of the tip
        public float Diameter { get; set; }
        public bool Exist { get; set; }

        public Tip()
        {
            MaxVolume = 0;
            MinVolume = 0;
            Height = 0.0f;
            Diameter = 1.0f;
            Exist = false;
        }

        // Конструктор с параметрами
        public Tip(float maxVolume, float minVolume, float height, bool exist, float diameter)
        {
            MaxVolume = maxVolume;
            MinVolume = minVolume;
            Height = height;
            Exist = exist;
            Diameter = diameter;
        }

        public Tip DeepCopy()
        {
            return new Tip
            {
                MaxVolume = this.MaxVolume,
                Height = this.Height,
                Diameter = this.Diameter,
                Exist = this.Exist
            };
        }
    }
}
