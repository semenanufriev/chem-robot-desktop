﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WellName = System.String;
using Volume = System.Single;
using Id = System.Int32;
using System.Web;

namespace ChemRobotDesktop.Classes
{
    public enum STEP_TYPE : int
    {
        TRANSFER,
        MIX,
        WAIT
    };

    public enum TIP_CHANGE_MODE : int
    {
        NEVER,
        ONCE_AT_START,
        FOR_EVERY_WELL
    };

    public abstract class ProtocolStep
    {
        public int Id;
        public STEP_TYPE Type;


        public abstract ProtocolStep DeepCopy();

        protected List<Zone> DeepCopyZones(List<Zone> zones)
        {
            List<Zone> newZones = new List<Zone>();

            foreach (var zone in zones)
            {
                Zone newZone = new Zone
                {
                    id = zone.id,
                    plate = zone.plate?.DeepCopy(), // Глубокое копирование Plate, если оно не null
                    defaultImage = zone.defaultImage != null ? (Image)zone.defaultImage.Clone() : null,
                    pictureBox = zone.pictureBox
                };

                newZones.Add(newZone);
            }

            return newZones;
        }

        protected bool CheckAndManageTips(List<Zone> zones, TIP_CHANGE_MODE tipChangeMode, int numberOfDestinationWells)
        {
            // Получаем список всех доступных носиков во всех зонах
            var availableTips = zones
                .Where(z => z.plate?.Type == PLATE_TYPE.TIP_RACK)
                .SelectMany(z => z.plate.Tips)
                .Where(t => t.Value != null && t.Value.Exist)
                .ToList();

            int requiredTips = 0;

            switch (tipChangeMode)
            {
                case TIP_CHANGE_MODE.NEVER:
                    // В этом режиме носики не используются
                    break;
                case TIP_CHANGE_MODE.ONCE_AT_START:
                    // Требуется только один носик
                    requiredTips = 1;
                    break;
                case TIP_CHANGE_MODE.FOR_EVERY_WELL:
                    // Требуется по одному носику на каждый конечный well
                    requiredTips = numberOfDestinationWells;
                    break;
                    // Другие варианты можно добавить здесь
            }

            if (availableTips.Count < requiredTips)
            {
                throw new InvalidOperationException("Недостаточно носиков.");
            }

            // Использование и обновление статуса носиков
            if (requiredTips > 0)
            {
                foreach (var tip in availableTips.Take(requiredTips))
                {
                    tip.Value.Exist = false; // Обновляем статус носика как использованный
                }
            }

            return true;
        }


        // Абстрактный метод, который должны реализовать наследники
        protected abstract List<Zone> ExecuteStepLogic(List<Zone> zones);

        public virtual List<string> GetStepGcode(List<Zone> zones, List<PointF> StartingPoints)
        {

            return null;
        }

        // Финальный метод, который вызывает логику, определенную в наследниках
        public List<Zone> ExecuteStep(List<Zone> zones)
        {
            List<Zone> newZones = DeepCopyZones(zones);
            try
            {
                return ExecuteStepLogic(newZones);
            }
            catch (Exception e)
            {
                throw e;
            }
        }


    }


    public class ProtocolStepTransfer : ProtocolStep
    {
        public List<WellName> SourceWells = new List<WellName>();
        public List<WellName> DestinationWells = new List<WellName>();
        public int SourcePlateId;
        public int DestinationPlateId;
        public Volume TransferVolume { get; set; }
        public TIP_CHANGE_MODE TipChangeMode { get; set; }

        public ProtocolStepTransfer(int id, int sourcePlateId, int destinationPlateId, List<WellName> sourceWells, List<WellName> destinationWells, Volume transferVolume, TIP_CHANGE_MODE tipChangeMode)
        {
            this.Id = id;
            this.SourcePlateId = sourcePlateId;
            this.DestinationPlateId = destinationPlateId;
            this.SourceWells = sourceWells.ToList();
            this.DestinationWells = destinationWells.ToList();
            this.Type = STEP_TYPE.TRANSFER;
            this.TransferVolume = transferVolume;
            this.TipChangeMode = tipChangeMode;
        }

        public override ProtocolStep DeepCopy()
        {
            return new ProtocolStepTransfer(
                Id,
                SourcePlateId,
                DestinationPlateId,
                new List<WellName>(SourceWells),
                new List<WellName>(DestinationWells),
                TransferVolume,
                TipChangeMode
            );
        }

        private void checkForErrors(List<Zone> zones)
        {
            Zone sourceZone = zones.FirstOrDefault(z => z.plate != null && z.plate.ObjectId == this.SourcePlateId);
            if (sourceZone == null || sourceZone.plate.Wells == null)
            {
                throw new InvalidOperationException("Источник не найден или недействителен.");
            }

            if (this.SourceWells.Count < 1)
            {
                throw new InvalidOperationException("Исходные ячейки не выбраны.");
            }

            Volume totalTransferVolume = this.TransferVolume * this.DestinationWells.Count;
            if (this.TransferVolume <= 0)
            {
                throw new InvalidOperationException("Переносимый объём должен быть больше 0.");
            }

            if (!sourceZone.plate.Wells.ContainsKey(this.SourceWells[0]) ||
                sourceZone.plate.Wells[this.SourceWells[0]].Liquid == null ||
                sourceZone.plate.Wells[this.SourceWells[0]].Liquid.volume < totalTransferVolume)
            {
                throw new InvalidOperationException("Недостаточно жидкости в источнике.");
            }


            Zone destZone = zones.FirstOrDefault(z => z.plate != null && z.plate.ObjectId == this.DestinationPlateId);
            foreach (var destWellName in this.DestinationWells)
            {  
                if (destZone == null || destZone.plate.Wells == null || !destZone.plate.Wells.ContainsKey(destWellName))
                {
                    throw new InvalidOperationException("Итоговое обородурование не найдено или недействительно.");
                }
            }

            if (this.DestinationWells.Count < 1)
            {
                throw new InvalidOperationException("Итоговые ячейки не выбраны.");
            }

            List<string> overfilledWells = new List<string>();

            foreach (var destWellName in this.DestinationWells)
            {
                if (!destZone.plate.Wells.ContainsKey(destWellName))
                {
                    overfilledWells.Add(destWellName.ToString() + " (не найдены)");
                    continue;
                }

                Well destWell = destZone.plate.Wells[destWellName];
                Volume currentVolume = destWell.Liquid != null ? destWell.Liquid.volume : 0;
                Volume maxVolume = destWell.MaxLiquidVolume;

                if (currentVolume + this.TransferVolume > maxVolume)
                {
                    overfilledWells.Add(destWellName.ToString());
                }
            }

            if (overfilledWells.Count > 0)
            {
                string errorMessage = "Следующее оборудование будет переполнено: " + string.Join(", ", overfilledWells);
                throw new InvalidOperationException(errorMessage);
            }

            foreach (STEP_TYPE stepType in Enum.GetValues(typeof(STEP_TYPE)))
            {
                if (stepType == STEP_TYPE.TRANSFER || stepType == STEP_TYPE.MIX)
                {
                    bool hasDropPlace = zones.Any(zone => zone.plate != null && zone.plate.Type == PLATE_TYPE.DROP_PLACE);
                    if (!hasDropPlace)
                    {
                        string errorMessageTips = "Нет места для сброса носиков.";
                        throw new InvalidOperationException(errorMessageTips);
                    }
                }
            }
        }

        protected override List<Zone> ExecuteStepLogic(List<Zone> zones)
        {
            try
            {
                checkForErrors(zones);
                CheckAndManageTips(zones, this.TipChangeMode, this.DestinationWells.Count);
            }
            catch(Exception ex)
            {
                throw ex;
            }

            // Находим зону исходной пластины
            Zone sourceZone = zones.FirstOrDefault(z => z.plate != null && z.plate.ObjectId == this.SourcePlateId);


            // Переливаем жидкость из исходной ячейки в каждую из ячеек назначения
            Zone destZone = zones.FirstOrDefault(z => z.plate != null && z.plate.ObjectId == this.DestinationPlateId);
            foreach (var destWellName in this.DestinationWells)
            {
                
                if (destZone != null && destZone.plate.Wells != null && destZone.plate.Wells.ContainsKey(destWellName))
                {        
                    // Переливаем жидкость
                    var liquidToTransfer = sourceZone.plate.Wells[this.SourceWells[0]].Liquid.Extract(this.TransferVolume);
                    destZone.plate.Wells[destWellName].addLiquidsMix(liquidToTransfer);
                }
            }

            return zones;
        }
    }


    public class ProtocolStepMix : ProtocolStep
    {
        public List<WellName> SourceWells = new List<WellName>();
        public int SourcePlateId;
        public int MixCount { get; set; }
        public float MixVolume { get; set; }
        public TIP_CHANGE_MODE TipChangeMode { get; set; }

        public ProtocolStepMix(int id, int plateId, List<WellName> mixWells, int mixCount, float mixVolume, TIP_CHANGE_MODE tipChangeMode)
        {
            this.Id = id;
            this.SourcePlateId = plateId;
            this.SourceWells = mixWells;
            this.Type = STEP_TYPE.MIX;
            this.MixCount = mixCount;
            this.MixVolume = mixVolume;
            this.TipChangeMode = tipChangeMode;
        }

        public override ProtocolStep DeepCopy()
        {
            return new ProtocolStepMix(
                Id,
                SourcePlateId,
                new List<WellName>(SourceWells),
                MixCount,
                MixVolume,
                TipChangeMode
            );
        }




        protected override List<Zone> ExecuteStepLogic(List<Zone> zones)
        {
            Zone mixZone = zones.FirstOrDefault(z => z.plate != null && z.plate.ObjectId == this.SourcePlateId);
            if (mixZone == null || mixZone.plate.Wells == null)
            {
                throw new InvalidOperationException("Зона смешивания не найдена или недействительна.");
            }

            if (this.SourceWells.Count < 1)
            {
                throw new InvalidOperationException("Ячейки не выбраны.");
            }
            try
            {
                CheckAndManageTips(zones, this.TipChangeMode, this.SourceWells.Count);
                //CheckAndManageTips(zones, this.TipChangeMode, this.SourceWells.Count);
            }
            catch(Exception e)
            {
                throw e;
            }

            List<WellName> notEnoughLiquidWells = new List<WellName>();

            foreach (var wellName in this.SourceWells)
            {
                if (!mixZone.plate.Wells.ContainsKey(wellName))
                {
                    throw new InvalidOperationException("Ячейки в зоне смешивания не найдены.");
                }
                if (mixZone.plate.Wells[wellName].Liquid.volume < MixVolume)
                {
                    notEnoughLiquidWells.Add(wellName);
                }


            }
            if (notEnoughLiquidWells.Count > 0)
            {
                String errorText = "Недостаточно жидкости в ячейках: " + String.Join(", ", notEnoughLiquidWells); 
                throw new InvalidOperationException(errorText);
            }


            return zones;
        }
    }


    public class ProtocolStepWait : ProtocolStep
    {
        public int WaitTimeSeconds { get; set; }

        public ProtocolStepWait(int id, int waitTimeSeconds)
        {
            this.Id = id;
            this.WaitTimeSeconds = waitTimeSeconds;
            this.Type = STEP_TYPE.WAIT;
        }
        public override ProtocolStep DeepCopy()
        {
            return new ProtocolStepWait(
                Id,
                WaitTimeSeconds
            );
        }

        protected override List<Zone> ExecuteStepLogic(List<Zone> zones)
        {
            if(WaitTimeSeconds == 0)
            {
                throw new InvalidOperationException("Время ожидания должно быть больше 0.");
            }
            return zones;
        }
    }
}



