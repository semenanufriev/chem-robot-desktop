﻿using ChemRobotDesktop.Classes;
using ChemRobotDesktop.CustomControls;
using static ChemRobotDesktop.CustomControls.PlatesList;


namespace ChemRobotDesktop
{
    public partial class MainForm : Form
    {

        public Project project = new Project();
        PlatesList plates = new PlatesList();
        private Form overlayForm = null;

        public MainForm()
        {
            InitializeComponent();
            this.AutoScaleMode = AutoScaleMode.None;
            initControls();
            CreateOverlayForm();


        }

            private void initControls()
            {
                project.init();
                this.Controls.Add(plates);
                labwareEditor1.init(ref plates);
                liquidsMenu2.init(ref project.Liquids);
                liquidsMenu2.LiquidsUpdated += protocolDesignerMenu.liquidsUpdated;
                protocolDesignerMenu.init(ref plates, ref project);
                projectMenu2.init(ref project);
                project.ProjectLoaded += updateProject;
                this.FormClosing += Form1_FormClosing;
                plates.plateSelectionWindowShowed += ShowOverlay;
                plates.plateSelected += (id) => HideOverlay();
                projectMenu2.DarkOn += ShowOverlay;
                projectMenu2.DarkOff += HideOverlay;
            }

        public void updateProject()
        {
            if (project != null)
            {
                liquidsMenu2.init(ref project.Liquids);
                protocolDesignerMenu.init(ref plates, ref project);
                projectMenu2.init(ref project);
            }
        }



        private void tabControl1_DrawItem(object sender, DrawItemEventArgs e)
        {
            TabControl tabControl = (TabControl)sender;
            Graphics g = e.Graphics;
            Font tabFont = e.Font;

            // Установка цвета и форматирования текста
            StringFormat stringFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };

            // Проход по всем вкладкам и отрисовка текста
            for (int index = 0; index < tabControl.TabCount; index++)
            {
                TabPage tabPage = tabControl.TabPages[index];
                Rectangle tabBounds = tabControl.GetTabRect(index);

                // Выбор цвета фона и текста в зависимости от того, активна вкладка или нет
                Color backColor = e.State == DrawItemState.Selected ? Color.Red : Color.Green;
                Color textColor = e.State == DrawItemState.Selected ? Color.Black : Color.Gray;

                // Заливка фона вкладки
                using (SolidBrush backgroundBrush = new SolidBrush(backColor))
                {
                    g.FillRectangle(backgroundBrush, tabBounds);
                }

                // Рисование текста вкладки
                using (SolidBrush textBrush = new SolidBrush(textColor))
                {
                    g.DrawString(tabPage.Text, tabFont, textBrush, tabBounds, stringFormat);
                }
            }
        }



        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            // Ваш метод показа затемнения
            ShowOverlay();

            if (e.CloseReason == CloseReason.UserClosing)
            {
                bool changed = project.checkProjectChanged();
                if (changed)
                {
                    // Используем MessageBoxButtons.YesNoCancel для трех вариантов
                    DialogResult result = MessageBox.Show(
                        "Вы хотите сохранить изменения перед закрытием?",
                        "Подтверждение",
                        MessageBoxButtons.YesNoCancel);

                    if (result == DialogResult.Yes)
                    {
                        // Сохранить изменения
                        project.SaveToFile(project.pathToFile);
                    }
                    else if (result == DialogResult.No)
                    {
                        // Закрыть без сохранения
                    }
                    else if (result == DialogResult.Cancel)
                    {
                        // Отмена закрытия
                        HideOverlay();
                        e.Cancel = true; // Отменяем закрытие формы
                    }
                }
                else
                {
                    if (MessageBox.Show("Закрыть приложение?", "Подтверждение", MessageBoxButtons.YesNo) == DialogResult.No)
                    {
                        HideOverlay();
                        e.Cancel = true; // Отменяем закрытие формы
                    }
                }
            }

        }
        private void CreateOverlayForm()
        {
            overlayForm = new Form()
            {
                FormBorderStyle = FormBorderStyle.None,
                ShowInTaskbar = false,
                StartPosition = FormStartPosition.Manual,
                Opacity = 0.5,
                BackColor = Color.Black,
                TopMost = false,
                Owner = this
            };
            UpdateOverlayFormBounds();
        }

        private void UpdateOverlayFormBounds()
        {
            if (overlayForm != null)
            {
                Rectangle bounds = this.RectangleToScreen(this.ClientRectangle);
                overlayForm.SetBounds(bounds.X, bounds.Y, bounds.Width, bounds.Height);
            }
        }

        public void ShowOverlay()
        {
            overlayForm.Bounds = this.ClientRectangle;
            UpdateOverlayFormBounds();
            overlayForm.Show();
        }

        private void HideOverlay()
        {
            overlayForm.Hide();
            this.Activate();
        }

        protected override void OnLocationChanged(EventArgs e)
        {
            base.OnLocationChanged(e);
            UpdateOverlayFormBounds();
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);
            UpdateOverlayFormBounds();
        }


        public void deleteLog()
        {

        }

    }
}