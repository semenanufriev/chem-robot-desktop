{
  "$type": "ChemRobotDesktop.Classes.Project, ChemRobotDesktop",
  "Liquids": [
    {
      "$type": "ChemRobotDesktop.Liquid, ChemRobotDesktop",
      "Name": "hjk",
      "Description": "",
      "Color": "93, 4, 95",
      "Id": 0
    }
  ],
  "ProtocolSteps": [
    {
      "$type": "ChemRobotDesktop.Classes.ProtocolStepTransfer, ChemRobotDesktop",
      "SourceWells": [
        "A1"
      ],
      "DestinationWells": [
        "B2",
        "B3",
        "C2",
        "C3",
        "D2"
      ],
      "SourcePlateId": 2,
      "DestinationPlateId": 1,
      "Id": 1,
      "Type": 0,
      "TransferVolume": 800.0,
      "TipChangeMode": 0
    }
  ],
  "StartingZones": [
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 0,
      "plate": {
        "$type": "ChemRobotDesktop.Classes.Plate, ChemRobotDesktop",
        "ImageSize": "762, 510",
        "Type": 0,
        "Name": "Реальный планшет с 25 пробирками 2мл",
        "ModelId": 18,
        "ObjectId": 1,
        "Wells": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[ChemRobotDesktop.Classes.Well, ChemRobotDesktop]], System.Private.CoreLib",
          "A1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "A2": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "A3": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "A4": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "A5": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B2": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B3": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B4": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B5": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C2": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C3": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C4": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C5": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D2": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D3": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D4": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D5": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E2": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E3": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E4": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E5": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 38.0,
            "MaxLiquidVolume": 2000.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 10.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          }
        },
        "Tips": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[ChemRobotDesktop.Classes.Tip, ChemRobotDesktop]], System.Private.CoreLib"
        },
        "CellsPositions": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[System.Drawing.PointF, System.Drawing.Primitives]], System.Private.CoreLib",
          "A1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 8.75,
            "Y": 7.8
          },
          "A2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 33.75,
            "Y": 7.8
          },
          "A3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.75,
            "Y": 7.8
          },
          "A4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 83.75,
            "Y": 7.8
          },
          "A5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 108.75,
            "Y": 7.8
          },
          "B1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 8.75,
            "Y": 22.8
          },
          "B2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 33.75,
            "Y": 22.8
          },
          "B3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.75,
            "Y": 22.8
          },
          "B4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 83.75,
            "Y": 22.8
          },
          "B5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 108.75,
            "Y": 22.8
          },
          "C1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 8.75,
            "Y": 37.8
          },
          "C2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 33.75,
            "Y": 37.8
          },
          "C3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.75,
            "Y": 37.8
          },
          "C4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 83.75,
            "Y": 37.8
          },
          "C5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 108.75,
            "Y": 37.8
          },
          "D1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 8.75,
            "Y": 52.8
          },
          "D2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 33.75,
            "Y": 52.8
          },
          "D3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.75,
            "Y": 52.8
          },
          "D4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 83.75,
            "Y": 52.8
          },
          "D5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 108.75,
            "Y": 52.8
          },
          "E1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 8.75,
            "Y": 67.8
          },
          "E2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 33.75,
            "Y": 67.8
          },
          "E3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.75,
            "Y": 67.8
          },
          "E4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 83.75,
            "Y": 67.8
          },
          "E5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 108.75,
            "Y": 67.8
          }
        },
        "Size": {
          "$type": "System.Windows.Forms.DataVisualization.Charting.Point3D, WinForms.DataVisualization",
          "X": 127.5,
          "Y": 85.5,
          "Z": 53.0,
          "PointF": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 127.5,
            "Y": 85.5
          }
        },
        "Rows": 5,
        "Columns": 5,
        "GridSpacing": {
          "$type": "System.Drawing.PointF, System.Drawing.Primitives",
          "IsEmpty": false,
          "X": 25.0,
          "Y": 15.0
        },
        "GridOffset": {
          "$type": "System.Drawing.PointF, System.Drawing.Primitives",
          "IsEmpty": false,
          "X": 8.75,
          "Y": 7.8
        }
      }
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 1,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 2,
      "plate": {
        "$type": "ChemRobotDesktop.Classes.Plate, ChemRobotDesktop",
        "ImageSize": "762, 510",
        "Type": 3,
        "Name": "место для сброса носиков",
        "ModelId": 17,
        "ObjectId": 4,
        "Wells": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[ChemRobotDesktop.Classes.Well, ChemRobotDesktop]], System.Private.CoreLib",
          "A1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 30.0,
            "MaxLiquidVolume": 10000.0,
            "BottomShape": 0,
            "Shape": 1,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": false,
              "X": 110.0,
              "Y": 69.0
            },
            "Diameter": null,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          }
        },
        "Tips": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[ChemRobotDesktop.Classes.Tip, ChemRobotDesktop]], System.Private.CoreLib"
        },
        "CellsPositions": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[System.Drawing.PointF, System.Drawing.Primitives]], System.Private.CoreLib",
          "A1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 10.0,
            "Y": 10.0
          }
        },
        "Size": {
          "$type": "System.Windows.Forms.DataVisualization.Charting.Point3D, WinForms.DataVisualization",
          "X": 127.5,
          "Y": 85.5,
          "Z": 70.0,
          "PointF": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 127.5,
            "Y": 85.5
          }
        },
        "Rows": 1,
        "Columns": 1,
        "GridSpacing": {
          "$type": "System.Drawing.PointF, System.Drawing.Primitives",
          "IsEmpty": false,
          "X": 10.0,
          "Y": 10.0
        },
        "GridOffset": {
          "$type": "System.Drawing.PointF, System.Drawing.Primitives",
          "IsEmpty": false,
          "X": 10.0,
          "Y": 10.0
        }
      }
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 3,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 4,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 5,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 6,
      "plate": {
        "$type": "ChemRobotDesktop.Classes.Plate, ChemRobotDesktop",
        "ImageSize": "762, 510",
        "Type": 2,
        "Name": "96 1000 mL tips",
        "ModelId": 10,
        "ObjectId": 3,
        "Wells": null,
        "Tips": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[ChemRobotDesktop.Classes.Tip, ChemRobotDesktop]], System.Private.CoreLib",
          "A1": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "A2": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "A3": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "A4": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "A5": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "A6": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "A7": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "A8": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "A9": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "A10": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "A11": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "A12": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B1": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B2": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B3": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B4": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B5": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B6": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B7": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B8": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B9": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B10": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B11": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "B12": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C1": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C2": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C3": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C4": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C5": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C6": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C7": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C8": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C9": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C10": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C11": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "C12": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D1": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D2": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D3": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D4": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D5": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D6": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D7": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D8": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D9": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D10": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D11": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "D12": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E1": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E2": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E3": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E4": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E5": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E6": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E7": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E8": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E9": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E10": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E11": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "E12": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F1": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F2": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F3": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F4": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F5": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F6": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F7": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F8": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F9": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F10": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F11": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "F12": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G1": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G2": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G3": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G4": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G5": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G6": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G7": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G8": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G9": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G10": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G11": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "G12": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H1": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H2": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H3": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H4": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H5": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H6": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H7": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H8": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H9": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H10": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H11": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          },
          "H12": {
            "$type": "ChemRobotDesktop.Classes.Tip, ChemRobotDesktop",
            "MaxVolume": 1000.0,
            "MinVolume": 0.0,
            "Height": 40.0,
            "Diameter": 4.0,
            "Exist": true
          }
        },
        "CellsPositions": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[System.Drawing.PointF, System.Drawing.Primitives]], System.Private.CoreLib",
          "A1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 13.0,
            "Y": 11.0
          },
          "A2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 22.0,
            "Y": 11.0
          },
          "A3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 31.0,
            "Y": 11.0
          },
          "A4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 11.0
          },
          "A5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 49.0,
            "Y": 11.0
          },
          "A6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.0,
            "Y": 11.0
          },
          "A7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 67.0,
            "Y": 11.0
          },
          "A8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 76.0,
            "Y": 11.0
          },
          "A9": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 85.0,
            "Y": 11.0
          },
          "A10": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 94.0,
            "Y": 11.0
          },
          "A11": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 103.0,
            "Y": 11.0
          },
          "A12": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 112.0,
            "Y": 11.0
          },
          "B1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 13.0,
            "Y": 20.0
          },
          "B2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 22.0,
            "Y": 20.0
          },
          "B3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 31.0,
            "Y": 20.0
          },
          "B4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 20.0
          },
          "B5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 49.0,
            "Y": 20.0
          },
          "B6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.0,
            "Y": 20.0
          },
          "B7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 67.0,
            "Y": 20.0
          },
          "B8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 76.0,
            "Y": 20.0
          },
          "B9": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 85.0,
            "Y": 20.0
          },
          "B10": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 94.0,
            "Y": 20.0
          },
          "B11": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 103.0,
            "Y": 20.0
          },
          "B12": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 112.0,
            "Y": 20.0
          },
          "C1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 13.0,
            "Y": 29.0
          },
          "C2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 22.0,
            "Y": 29.0
          },
          "C3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 31.0,
            "Y": 29.0
          },
          "C4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 29.0
          },
          "C5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 49.0,
            "Y": 29.0
          },
          "C6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.0,
            "Y": 29.0
          },
          "C7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 67.0,
            "Y": 29.0
          },
          "C8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 76.0,
            "Y": 29.0
          },
          "C9": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 85.0,
            "Y": 29.0
          },
          "C10": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 94.0,
            "Y": 29.0
          },
          "C11": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 103.0,
            "Y": 29.0
          },
          "C12": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 112.0,
            "Y": 29.0
          },
          "D1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 13.0,
            "Y": 38.0
          },
          "D2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 22.0,
            "Y": 38.0
          },
          "D3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 31.0,
            "Y": 38.0
          },
          "D4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 38.0
          },
          "D5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 49.0,
            "Y": 38.0
          },
          "D6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.0,
            "Y": 38.0
          },
          "D7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 67.0,
            "Y": 38.0
          },
          "D8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 76.0,
            "Y": 38.0
          },
          "D9": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 85.0,
            "Y": 38.0
          },
          "D10": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 94.0,
            "Y": 38.0
          },
          "D11": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 103.0,
            "Y": 38.0
          },
          "D12": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 112.0,
            "Y": 38.0
          },
          "E1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 13.0,
            "Y": 47.0
          },
          "E2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 22.0,
            "Y": 47.0
          },
          "E3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 31.0,
            "Y": 47.0
          },
          "E4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 47.0
          },
          "E5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 49.0,
            "Y": 47.0
          },
          "E6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.0,
            "Y": 47.0
          },
          "E7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 67.0,
            "Y": 47.0
          },
          "E8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 76.0,
            "Y": 47.0
          },
          "E9": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 85.0,
            "Y": 47.0
          },
          "E10": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 94.0,
            "Y": 47.0
          },
          "E11": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 103.0,
            "Y": 47.0
          },
          "E12": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 112.0,
            "Y": 47.0
          },
          "F1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 13.0,
            "Y": 56.0
          },
          "F2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 22.0,
            "Y": 56.0
          },
          "F3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 31.0,
            "Y": 56.0
          },
          "F4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 56.0
          },
          "F5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 49.0,
            "Y": 56.0
          },
          "F6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.0,
            "Y": 56.0
          },
          "F7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 67.0,
            "Y": 56.0
          },
          "F8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 76.0,
            "Y": 56.0
          },
          "F9": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 85.0,
            "Y": 56.0
          },
          "F10": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 94.0,
            "Y": 56.0
          },
          "F11": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 103.0,
            "Y": 56.0
          },
          "F12": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 112.0,
            "Y": 56.0
          },
          "G1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 13.0,
            "Y": 65.0
          },
          "G2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 22.0,
            "Y": 65.0
          },
          "G3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 31.0,
            "Y": 65.0
          },
          "G4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 65.0
          },
          "G5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 49.0,
            "Y": 65.0
          },
          "G6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.0,
            "Y": 65.0
          },
          "G7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 67.0,
            "Y": 65.0
          },
          "G8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 76.0,
            "Y": 65.0
          },
          "G9": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 85.0,
            "Y": 65.0
          },
          "G10": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 94.0,
            "Y": 65.0
          },
          "G11": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 103.0,
            "Y": 65.0
          },
          "G12": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 112.0,
            "Y": 65.0
          },
          "H1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 13.0,
            "Y": 74.0
          },
          "H2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 22.0,
            "Y": 74.0
          },
          "H3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 31.0,
            "Y": 74.0
          },
          "H4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 74.0
          },
          "H5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 49.0,
            "Y": 74.0
          },
          "H6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 58.0,
            "Y": 74.0
          },
          "H7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 67.0,
            "Y": 74.0
          },
          "H8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 76.0,
            "Y": 74.0
          },
          "H9": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 85.0,
            "Y": 74.0
          },
          "H10": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 94.0,
            "Y": 74.0
          },
          "H11": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 103.0,
            "Y": 74.0
          },
          "H12": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 112.0,
            "Y": 74.0
          }
        },
        "Size": {
          "$type": "System.Windows.Forms.DataVisualization.Charting.Point3D, WinForms.DataVisualization",
          "X": 127.5,
          "Y": 85.5,
          "Z": 40.0,
          "PointF": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 127.5,
            "Y": 85.5
          }
        },
        "Rows": 8,
        "Columns": 12,
        "GridSpacing": {
          "$type": "System.Drawing.PointF, System.Drawing.Primitives",
          "IsEmpty": false,
          "X": 9.0,
          "Y": 9.0
        },
        "GridOffset": {
          "$type": "System.Drawing.PointF, System.Drawing.Primitives",
          "IsEmpty": false,
          "X": 13.0,
          "Y": 11.0
        }
      }
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 7,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 8,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 9,
      "plate": {
        "$type": "ChemRobotDesktop.Classes.Plate, ChemRobotDesktop",
        "ImageSize": "762, 510",
        "Type": 1,
        "Name": "Резервуар_крышка",
        "ModelId": 15,
        "ObjectId": 2,
        "Wells": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[ChemRobotDesktop.Classes.Well, ChemRobotDesktop]], System.Private.CoreLib",
          "A1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 60.0,
            "MaxLiquidVolume": 10000.0,
            "BottomShape": 0,
            "Shape": 1,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": false,
              "X": 110.0,
              "Y": 69.0
            },
            "Diameter": null,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib",
                "0": {
                  "$type": "ChemRobotDesktop.LiquidUnit, ChemRobotDesktop",
                  "liquid": {
                    "$type": "ChemRobotDesktop.Liquid, ChemRobotDesktop",
                    "Name": "hjk",
                    "Description": "",
                    "Color": "93, 4, 95",
                    "Id": 0
                  },
                  "volume": 10000.0,
                  "proportion": 1.0
                }
              },
              "volume": 10000.0
            }
          }
        },
        "Tips": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[ChemRobotDesktop.Classes.Tip, ChemRobotDesktop]], System.Private.CoreLib"
        },
        "CellsPositions": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[System.Drawing.PointF, System.Drawing.Primitives]], System.Private.CoreLib",
          "A1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 10.0,
            "Y": 10.0
          }
        },
        "Size": {
          "$type": "System.Windows.Forms.DataVisualization.Charting.Point3D, WinForms.DataVisualization",
          "X": 127.5,
          "Y": 85.5,
          "Z": 70.0,
          "PointF": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 127.5,
            "Y": 85.5
          }
        },
        "Rows": 1,
        "Columns": 1,
        "GridSpacing": {
          "$type": "System.Drawing.PointF, System.Drawing.Primitives",
          "IsEmpty": false,
          "X": 10.0,
          "Y": 10.0
        },
        "GridOffset": {
          "$type": "System.Drawing.PointF, System.Drawing.Primitives",
          "IsEmpty": false,
          "X": 10.0,
          "Y": 10.0
        }
      }
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 10,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 11,
      "plate": null
    }
  ],
  "pathToConfigFile": "C:\\GitLab\\chem-robot-desktop\\ChemRobotDesktop\\ChemRobotDesktop\\bin\\Debug\\net7.0-windows\\Config\\config.txt",
  "Name": "обновлённый тест",
  "Description": "",
  "Author": "",
  "Rows": 3,
  "Cols": 4,
  "StartingPoints": [
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 0.0,
      "Y": 200.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 150.0,
      "Y": 200.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 300.0,
      "Y": 200.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 450.0,
      "Y": 200.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 0.0,
      "Y": 100.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 150.0,
      "Y": 100.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 300.0,
      "Y": 100.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 450.0,
      "Y": 100.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": true,
      "X": 0.0,
      "Y": 0.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 150.0,
      "Y": 0.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 300.0,
      "Y": 0.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 450.0,
      "Y": 0.0
    }
  ]
}