{
  "$type": "ChemRobotDesktop.Classes.Project, ChemRobotDesktop",
  "Liquids": [],
  "ProtocolSteps": [],
  "StartingZones": [
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 0,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 1,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 2,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 3,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 4,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 5,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 6,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 7,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 8,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 9,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 10,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 11,
      "plate": null
    }
  ],
  "Name": null,
  "Description": null,
  "Author": null,
  "Rows": 3,
  "Cols": 4,
  "StartingPoints": [
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": true,
      "X": 0.0,
      "Y": 0.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 1.0,
      "Y": 0.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 2.0,
      "Y": 0.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 3.0,
      "Y": 0.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 0.0,
      "Y": 1.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 1.0,
      "Y": 1.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 2.0,
      "Y": 1.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 3.0,
      "Y": 1.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 0.0,
      "Y": 2.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 1.0,
      "Y": 2.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 2.0,
      "Y": 2.0
    },
    {
      "$type": "System.Drawing.PointF, System.Drawing.Primitives",
      "IsEmpty": false,
      "X": 3.0,
      "Y": 2.0
    }
  ]
}