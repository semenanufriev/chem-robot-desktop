{
  "$type": "ChemRobotDesktop.Classes.Project, ChemRobotDesktop",
  "Liquids": [
    {
      "$type": "ChemRobotDesktop.Liquid, ChemRobotDesktop",
      "Name": "g",
      "Description": "",
      "Color": "177, 206, 159",
      "Id": 0
    }
  ],
  "ProtocolSteps": [
    {
      "$type": "ChemRobotDesktop.Classes.ProtocolStepTransfer, ChemRobotDesktop",
      "SourceWells": [
        "A1"
      ],
      "DestinationWells": [
        "A8"
      ],
      "SourcePlateId": 0,
      "DestinationPlateId": 0,
      "Id": 0,
      "Type": 0,
      "TransferVolume": 1.0,
      "TipChangeMode": 1
    }
  ],
  "StartingZones": [
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 0,
      "plate": {
        "$type": "ChemRobotDesktop.Classes.Plate, ChemRobotDesktop",
        "ImageSize": "762, 510",
        "Type": 0,
        "Name": "TEST TIP RACK 48",
        "ModelId": 1,
        "ObjectId": 0,
        "Wells": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[ChemRobotDesktop.Classes.Well, ChemRobotDesktop]], System.Private.CoreLib",
          "A1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib",
                "0": {
                  "$type": "ChemRobotDesktop.LiquidUnit, ChemRobotDesktop",
                  "liquid": {
                    "$type": "ChemRobotDesktop.Liquid, ChemRobotDesktop",
                    "Name": "g",
                    "Description": "",
                    "Color": "177, 206, 159",
                    "Id": 0
                  },
                  "volume": 1.0,
                  "proportion": 1.0
                }
              },
              "volume": 1.0
            }
          },
          "A2": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "A3": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "A4": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "A5": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "A6": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "A7": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "A8": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B2": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B3": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B4": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B5": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B6": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B7": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "B8": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C2": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C3": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C4": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C5": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C6": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C7": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "C8": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D2": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D3": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D4": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D5": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D6": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D7": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "D8": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E2": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E3": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E4": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E5": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E6": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E7": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "E8": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "F1": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "F2": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "F3": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "F4": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "F5": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "F6": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "F7": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          },
          "F8": {
            "$type": "ChemRobotDesktop.Classes.Well, ChemRobotDesktop",
            "Depth": 8.0,
            "MaxLiquidVolume": 3.0,
            "BottomShape": 0,
            "Shape": 0,
            "RectSize": {
              "$type": "System.Drawing.PointF, System.Drawing.Primitives",
              "IsEmpty": true,
              "X": 0.0,
              "Y": 0.0
            },
            "Diameter": 5.0,
            "Liquid": {
              "$type": "ChemRobotDesktop.LiquidsMix, ChemRobotDesktop",
              "liquids": {
                "$type": "System.Collections.Generic.Dictionary`2[[System.Int32, System.Private.CoreLib],[ChemRobotDesktop.LiquidUnit, ChemRobotDesktop]], System.Private.CoreLib"
              },
              "volume": 0.0
            }
          }
        },
        "WellPositions": {
          "$type": "System.Collections.Generic.Dictionary`2[[System.String, System.Private.CoreLib],[System.Drawing.PointF, System.Drawing.Primitives]], System.Private.CoreLib",
          "A1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 10.0,
            "Y": 10.0
          },
          "A2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 20.0,
            "Y": 10.0
          },
          "A3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 30.0,
            "Y": 10.0
          },
          "A4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 10.0
          },
          "A5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 50.0,
            "Y": 10.0
          },
          "A6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 60.0,
            "Y": 10.0
          },
          "A7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 70.0,
            "Y": 10.0
          },
          "A8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 80.0,
            "Y": 10.0
          },
          "B1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 10.0,
            "Y": 20.0
          },
          "B2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 20.0,
            "Y": 20.0
          },
          "B3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 30.0,
            "Y": 20.0
          },
          "B4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 20.0
          },
          "B5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 50.0,
            "Y": 20.0
          },
          "B6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 60.0,
            "Y": 20.0
          },
          "B7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 70.0,
            "Y": 20.0
          },
          "B8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 80.0,
            "Y": 20.0
          },
          "C1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 10.0,
            "Y": 30.0
          },
          "C2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 20.0,
            "Y": 30.0
          },
          "C3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 30.0,
            "Y": 30.0
          },
          "C4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 30.0
          },
          "C5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 50.0,
            "Y": 30.0
          },
          "C6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 60.0,
            "Y": 30.0
          },
          "C7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 70.0,
            "Y": 30.0
          },
          "C8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 80.0,
            "Y": 30.0
          },
          "D1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 10.0,
            "Y": 40.0
          },
          "D2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 20.0,
            "Y": 40.0
          },
          "D3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 30.0,
            "Y": 40.0
          },
          "D4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 40.0
          },
          "D5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 50.0,
            "Y": 40.0
          },
          "D6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 60.0,
            "Y": 40.0
          },
          "D7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 70.0,
            "Y": 40.0
          },
          "D8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 80.0,
            "Y": 40.0
          },
          "E1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 10.0,
            "Y": 50.0
          },
          "E2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 20.0,
            "Y": 50.0
          },
          "E3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 30.0,
            "Y": 50.0
          },
          "E4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 50.0
          },
          "E5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 50.0,
            "Y": 50.0
          },
          "E6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 60.0,
            "Y": 50.0
          },
          "E7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 70.0,
            "Y": 50.0
          },
          "E8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 80.0,
            "Y": 50.0
          },
          "F1": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 10.0,
            "Y": 60.0
          },
          "F2": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 20.0,
            "Y": 60.0
          },
          "F3": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 30.0,
            "Y": 60.0
          },
          "F4": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 40.0,
            "Y": 60.0
          },
          "F5": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 50.0,
            "Y": 60.0
          },
          "F6": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 60.0,
            "Y": 60.0
          },
          "F7": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 70.0,
            "Y": 60.0
          },
          "F8": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 80.0,
            "Y": 60.0
          }
        },
        "Size": {
          "$type": "System.Windows.Forms.DataVisualization.Charting.Point3D, WinForms.DataVisualization",
          "X": 127.5,
          "Y": 85.5,
          "Z": 10.0,
          "PointF": {
            "$type": "System.Drawing.PointF, System.Drawing.Primitives",
            "IsEmpty": false,
            "X": 127.5,
            "Y": 85.5
          }
        },
        "Rows": 6,
        "Columns": 8,
        "WellSpacing": {
          "$type": "System.Drawing.PointF, System.Drawing.Primitives",
          "IsEmpty": false,
          "X": 10.0,
          "Y": 10.0
        },
        "GridOffset": {
          "$type": "System.Drawing.PointF, System.Drawing.Primitives",
          "IsEmpty": false,
          "X": 10.0,
          "Y": 10.0
        }
      }
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 1,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 2,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 3,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 4,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 5,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 6,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 7,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 8,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 9,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 10,
      "plate": null
    },
    {
      "$type": "ChemRobotDesktop.Classes.Zone, ChemRobotDesktop",
      "id": 11,
      "plate": null
    }
  ],
  "rows": 3,
  "cols": 4,
  "Name": null,
  "Description": null,
  "Author": null
}