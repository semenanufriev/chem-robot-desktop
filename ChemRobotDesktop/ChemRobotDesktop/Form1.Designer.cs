﻿using ChemRobotDesktop.CustomControls;

namespace ChemRobotDesktop
{
    partial class MainForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tabPage4 = new TabPage();
            labwareEditor1 = new LabwareEditor();
            tabPage1 = new TabPage();
            protocolDesignerMenu = new ProtocolDesignerMenu();
            LiquidsPage = new TabPage();
            liquidsMenu2 = new LiquidsMenu();
            FilePage = new TabPage();
            projectMenu2 = new ProjectMenu();
            tabControl1 = new TabControl();
            tabPage4.SuspendLayout();
            tabPage1.SuspendLayout();
            LiquidsPage.SuspendLayout();
            FilePage.SuspendLayout();
            tabControl1.SuspendLayout();
            SuspendLayout();
            // 
            // tabPage4
            // 
            tabPage4.Controls.Add(labwareEditor1);
            tabPage4.Location = new Point(4, 109);
            tabPage4.Margin = new Padding(0);
            tabPage4.Name = "tabPage4";
            tabPage4.Size = new Size(192, 0);
            tabPage4.TabIndex = 5;
            tabPage4.Text = "Редактор оборудования";
            tabPage4.UseVisualStyleBackColor = true;
            // 
            // labwareEditor1
            // 
            labwareEditor1.AutoScroll = true;
            labwareEditor1.AutoSize = true;
            labwareEditor1.Dock = DockStyle.Fill;
            labwareEditor1.Location = new Point(0, 0);
            labwareEditor1.Margin = new Padding(0);
            labwareEditor1.Name = "labwareEditor1";
            labwareEditor1.Size = new Size(192, 0);
            labwareEditor1.TabIndex = 0;
            // 
            // tabPage1
            // 
            tabPage1.BackColor = Color.Transparent;
            tabPage1.Controls.Add(protocolDesignerMenu);
            tabPage1.Location = new Point(4, 74);
            tabPage1.Name = "tabPage1";
            tabPage1.Size = new Size(192, 22);
            tabPage1.TabIndex = 2;
            tabPage1.Text = "Рабочее пространство";
            // 
            // protocolDesignerMenu
            // 
            protocolDesignerMenu.Dock = DockStyle.Fill;
            protocolDesignerMenu.Location = new Point(0, 0);
            protocolDesignerMenu.Margin = new Padding(4);
            protocolDesignerMenu.Name = "protocolDesignerMenu";
            protocolDesignerMenu.Size = new Size(192, 22);
            protocolDesignerMenu.TabIndex = 0;
            // 
            // LiquidsPage
            // 
            LiquidsPage.Controls.Add(liquidsMenu2);
            LiquidsPage.Location = new Point(4, 39);
            LiquidsPage.Margin = new Padding(0);
            LiquidsPage.Name = "LiquidsPage";
            LiquidsPage.Size = new Size(192, 57);
            LiquidsPage.TabIndex = 1;
            LiquidsPage.Text = "Жидкости";
            LiquidsPage.UseVisualStyleBackColor = true;
            // 
            // liquidsMenu2
            // 
            liquidsMenu2.Dock = DockStyle.Fill;
            liquidsMenu2.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point);
            liquidsMenu2.Location = new Point(0, 0);
            liquidsMenu2.Name = "liquidsMenu2";
            liquidsMenu2.Size = new Size(192, 57);
            liquidsMenu2.TabIndex = 0;
            // 
            // FilePage
            // 
            FilePage.Controls.Add(projectMenu2);
            FilePage.Location = new Point(4, 39);
            FilePage.Margin = new Padding(0);
            FilePage.Name = "FilePage";
            FilePage.Size = new Size(1364, 748);
            FilePage.TabIndex = 0;
            FilePage.Text = "Проект";
            FilePage.UseVisualStyleBackColor = true;
            // 
            // projectMenu2
            // 
            projectMenu2.BackColor = SystemColors.InactiveBorder;
            projectMenu2.Dock = DockStyle.Fill;
            projectMenu2.Location = new Point(0, 0);
            projectMenu2.Margin = new Padding(4);
            projectMenu2.Name = "projectMenu2";
            projectMenu2.Size = new Size(1364, 748);
            projectMenu2.TabIndex = 0;
            // 
            // tabControl1
            // 
            tabControl1.Controls.Add(FilePage);
            tabControl1.Controls.Add(LiquidsPage);
            tabControl1.Controls.Add(tabPage1);
            tabControl1.Controls.Add(tabPage4);
            tabControl1.Dock = DockStyle.Fill;
            tabControl1.Font = new Font("Segoe UI", 15.75F, FontStyle.Regular, GraphicsUnit.Point);
            tabControl1.Location = new Point(0, 0);
            tabControl1.Margin = new Padding(0);
            tabControl1.Multiline = true;
            tabControl1.Name = "tabControl1";
            tabControl1.Padding = new Point(0, 0);
            tabControl1.RightToLeft = RightToLeft.No;
            tabControl1.SelectedIndex = 0;
            tabControl1.Size = new Size(1372, 791);
            tabControl1.TabIndex = 0;
            tabControl1.DrawItem += tabControl1_DrawItem;
            // 
            // MainForm
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.InactiveBorder;
            ClientSize = new Size(1372, 791);
            Controls.Add(tabControl1);
            MinimumSize = new Size(500, 398);
            Name = "MainForm";
            Text = "Form1";
            tabPage4.ResumeLayout(false);
            tabPage4.PerformLayout();
            tabPage1.ResumeLayout(false);
            LiquidsPage.ResumeLayout(false);
            FilePage.ResumeLayout(false);
            tabControl1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion
        private LiquidsMenu liquidsMenu1;
        private CustomControls.ProjectMenu projectMenu1;
        private CustomControls.PlatesList platesList1;
        private ProtocolDesignerMenu protocolDesignerMenu1;
        private TabPage tabPage4;
        //private LabwareEditor labwareEditor1;
        private TabPage tabPage1;
        private ProtocolDesignerMenu protocolDesignerMenu;
        private TabPage LiquidsPage;
        private TabPage FilePage;
        private TabControl tabControl1;
        private ProjectMenu projectMenu2;
        private LabwareEditor labwareEditor1;
        private LiquidsMenu liquidsMenu2;
    }
}