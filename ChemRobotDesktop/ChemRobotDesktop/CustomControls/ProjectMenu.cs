﻿using ChemRobotDesktop.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace ChemRobotDesktop.CustomControls
{
    
    public partial class ProjectMenu : UserControl
    {
        public delegate void Method();
        public event Method DarkOn;
        public event Method DarkOff;


        public Project project;
        public ProjectMenu()
        {
            InitializeComponent();
            //showTip();
            isProjectOpened();
        }

        public void init(ref Project project)
        {
            this.project = project;
            protocolNameBox.Text = project.Name;
            authorBox.Text = project.Author;
            descriptionBox.Text = project.Description;
            isProjectOpened();

        }


        private void CreateProtocolButton_Click(object sender, EventArgs e)
        {
            DarkOn?.Invoke();
            ProjectScreen projectScreen = new ProjectScreen(ref project);
            projectScreen.ProjectCreated += processNewProject;
            projectScreen.showInWindow();
            DarkOff?.Invoke();
            
            int o =8;
        }


        private void ImportProtocolButton_Click(object sender, EventArgs e)
        {
            openFileDialog1.Filter = "Protocol files (*.robot)|*.robot";
            //string projectsDirectory = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"..\..\..\Projects"));
            string projectsDirectory = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"Projects"));
            if (!Directory.Exists(projectsDirectory))
            {
                Directory.CreateDirectory(projectsDirectory);
            }
            openFileDialog1.InitialDirectory = projectsDirectory;
            openFileDialog1.ShowDialog();
            string file = openFileDialog1.FileName;
            if (file != "")
            {
                project.LoadFromFile(file);

                //CompactRobot loadedRobot = LoadObjectFromFile(file);
                //robot.loadData(loadedRobot);
                openFileDialog1.FileName = "";
                //showTip();
            }
            isProjectOpened();
        }

        public void ExportProtocolButton_Click(object sender, EventArgs e)
        {
            saveFileDialog1.Filter = "Protocol files (*.robot)|*.robot|Все файлы (*.*)|*.*";
            //string projectsDirectory = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"..\..\..\Projects"));
            string projectsDirectory = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"Projects"));
            if (!Directory.Exists(projectsDirectory))
            {
                Directory.CreateDirectory(projectsDirectory);
            }

            saveFileDialog1.DefaultExt = "robot";
            saveFileDialog1.FileName = protocolNameBox.Text + "." + saveFileDialog1.DefaultExt;
            saveFileDialog1.InitialDirectory = projectsDirectory;
            saveFileDialog1.ShowDialog();
            //protocolNameBox.Text = 
            
            string file = saveFileDialog1.FileName;
            if (file != "")
            {
                project.SaveToFile(file);
                //SaveProject.Invoke(file);
                //SaveObjectToFile(robot, file);
                saveFileDialog1.FileName = "";
                //showTip();

            }

        }

        private void protocolNameBox_TextChanged(object sender, EventArgs e)
        {
            project.Name = protocolNameBox.Text;
        }

        private void authorBox_TextChanged(object sender, EventArgs e)
        {
            project.Author = authorBox.Text;
        }

        private void descriptionBox_TextChanged(object sender, EventArgs e)
        {
            project.Description = descriptionBox.Text;
        }

        public void isProjectOpened()
        {
            if (project != null && project.isOpened())
            {
                tableLayoutPanel2.RowStyles[3].SizeType = SizeType.Absolute;
                tableLayoutPanel2.RowStyles[3].Height = 157;
                tableLayoutPanel2.RowStyles[0].SizeType = SizeType.AutoSize;
                tableLayoutPanel2.RowStyles[1].SizeType = SizeType.AutoSize;
                tableLayoutPanel2.RowStyles[2].SizeType = SizeType.AutoSize;
            }
            else
            {
                tableLayoutPanel2.RowStyles[3].SizeType = SizeType.Percent;
                tableLayoutPanel2.RowStyles[3].Height = 100;
                tableLayoutPanel2.RowStyles[0].SizeType = SizeType.Absolute;
                tableLayoutPanel2.RowStyles[0].Height = 0;
                tableLayoutPanel2.RowStyles[1].SizeType = SizeType.Absolute;
                tableLayoutPanel2.RowStyles[1].Height = 0;
                tableLayoutPanel2.RowStyles[2].SizeType = SizeType.Absolute;
                tableLayoutPanel2.RowStyles[2].Height = 0;
            }
        }
        public void processNewProject(Project project)
        {
            
            init(ref project);
        }

    }
}
