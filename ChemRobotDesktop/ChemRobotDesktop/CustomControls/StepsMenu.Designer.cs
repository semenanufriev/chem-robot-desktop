﻿namespace ChemRobotDesktop.CustomControls
{
    partial class StepsMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            stepstPanel = new TableLayoutPanel();
            stepTypesBox = new ComboBox();
            panel1 = new Panel();
            addStepButton = new Button();
            toolTip1 = new ToolTip(components);
            contextMenuStrip1 = new ContextMenuStrip(components);
            удалитьToolStripMenuItem = new ToolStripMenuItem();
            contextMenuStrip2 = new ContextMenuStrip(components);
            tableLayoutPanel1 = new TableLayoutPanel();
            startingStateButton = new Button();
            finalStateButton = new Button();
            label1 = new Label();
            panel1.SuspendLayout();
            contextMenuStrip1.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // stepstPanel
            // 
            stepstPanel.Anchor = AnchorStyles.None;
            stepstPanel.ColumnCount = 1;
            stepstPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            stepstPanel.Location = new Point(25, 140);
            stepstPanel.Margin = new Padding(0);
            stepstPanel.Name = "stepstPanel";
            stepstPanel.RowCount = 1;
            stepstPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            stepstPanel.Size = new Size(200, 53);
            stepstPanel.TabIndex = 0;
            // 
            // stepTypesBox
            // 
            stepTypesBox.Dock = DockStyle.Bottom;
            stepTypesBox.Font = new Font("Segoe UI", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            stepTypesBox.FormattingEnabled = true;
            stepTypesBox.Location = new Point(0, 17);
            stepTypesBox.Margin = new Padding(3, 2, 3, 3);
            stepTypesBox.Name = "stepTypesBox";
            stepTypesBox.Size = new Size(200, 38);
            stepTypesBox.TabIndex = 3;
            stepTypesBox.Click += stepTypesBox_Click;
            // 
            // panel1
            // 
            panel1.Controls.Add(addStepButton);
            panel1.Controls.Add(stepTypesBox);
            panel1.Location = new Point(23, 263);
            panel1.Margin = new Padding(23, 0, 23, 0);
            panel1.Name = "panel1";
            panel1.Size = new Size(200, 55);
            panel1.TabIndex = 0;
            // 
            // addStepButton
            // 
            addStepButton.Anchor = AnchorStyles.Bottom;
            addStepButton.FlatStyle = FlatStyle.Flat;
            addStepButton.Font = new Font("Segoe UI", 15F, FontStyle.Regular, GraphicsUnit.Point);
            addStepButton.Location = new Point(2, 10);
            addStepButton.Margin = new Padding(2, 3, 2, 3);
            addStepButton.Name = "addStepButton";
            addStepButton.Size = new Size(196, 45);
            addStepButton.TabIndex = 4;
            addStepButton.Text = "Добавить этап";
            addStepButton.UseVisualStyleBackColor = true;
            addStepButton.Click += showStepsListButton_Click;
            // 
            // contextMenuStrip1
            // 
            contextMenuStrip1.ImageScalingSize = new Size(20, 20);
            contextMenuStrip1.Items.AddRange(new ToolStripItem[] { удалитьToolStripMenuItem });
            contextMenuStrip1.Name = "contextMenuStrip1";
            contextMenuStrip1.Size = new Size(181, 26);
            // 
            // удалитьToolStripMenuItem
            // 
            удалитьToolStripMenuItem.Name = "удалитьToolStripMenuItem";
            удалитьToolStripMenuItem.Size = new Size(180, 22);
            удалитьToolStripMenuItem.Text = "toolStripMenuItem1";
            // 
            // contextMenuStrip2
            // 
            contextMenuStrip2.ImageScalingSize = new Size(20, 20);
            contextMenuStrip2.Name = "contextMenuStrip2";
            contextMenuStrip2.Size = new Size(61, 4);
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.AutoScroll = true;
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(stepstPanel, 0, 3);
            tableLayoutPanel1.Controls.Add(panel1, 0, 6);
            tableLayoutPanel1.Controls.Add(startingStateButton, 0, 2);
            tableLayoutPanel1.Controls.Add(finalStateButton, 0, 4);
            tableLayoutPanel1.Controls.Add(label1, 0, 0);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Margin = new Padding(0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 8;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 55F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.Size = new Size(250, 800);
            tableLayoutPanel1.TabIndex = 5;
            tableLayoutPanel1.Paint += tableLayoutPanel1_Paint;
            // 
            // startingStateButton
            // 
            startingStateButton.Anchor = AnchorStyles.None;
            startingStateButton.FlatStyle = FlatStyle.Popup;
            startingStateButton.Font = new Font("Segoe UI", 15F, FontStyle.Regular, GraphicsUnit.Point);
            startingStateButton.Location = new Point(25, 93);
            startingStateButton.Name = "startingStateButton";
            startingStateButton.Padding = new Padding(20, 0, 20, 0);
            startingStateButton.Size = new Size(200, 44);
            startingStateButton.TabIndex = 5;
            startingStateButton.Text = "Начало";
            startingStateButton.UseVisualStyleBackColor = true;
            // 
            // finalStateButton
            // 
            finalStateButton.Anchor = AnchorStyles.None;
            finalStateButton.FlatStyle = FlatStyle.Popup;
            finalStateButton.Font = new Font("Segoe UI", 15F, FontStyle.Regular, GraphicsUnit.Point);
            finalStateButton.Location = new Point(25, 196);
            finalStateButton.Name = "finalStateButton";
            finalStateButton.Size = new Size(200, 44);
            finalStateButton.TabIndex = 6;
            finalStateButton.Text = "Конец";
            finalStateButton.UseVisualStyleBackColor = true;
            finalStateButton.Click += finalStateButton_Click;
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Top;
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 17F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(83, 0);
            label1.Name = "label1";
            label1.Size = new Size(83, 31);
            label1.TabIndex = 1;
            label1.Text = "Этапы";
            // 
            // StepsMenu
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.GradientActiveCaption;
            Controls.Add(tableLayoutPanel1);
            Name = "StepsMenu";
            Size = new Size(250, 800);
            panel1.ResumeLayout(false);
            contextMenuStrip1.ResumeLayout(false);
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel stepstPanel;
        public ComboBox stepTypesBox;
        private ToolTip toolTip1;
        private ContextMenuStrip contextMenuStrip1;
        private ToolStripMenuItem удалитьToolStripMenuItem;
        private ContextMenuStrip contextMenuStrip2;
        private TableLayoutPanel tableLayoutPanel1;
        private Panel panel1;
        private Label label1;
        private Button addStepButton;
        private Button startingStateButton;
        private Button finalStateButton;
    }
}
