﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChemRobotDesktop.Classes;

namespace ChemRobotDesktop.CustomControls
{
    public partial class ProtocolDesignerMenu : UserControl
    {
        public List<ProtocolStep> ProtocolSteps = new List<ProtocolStep>();
        int nextStepId = 0;
        Project project;

        public ProtocolDesignerMenu()
        {
            InitializeComponent();
            stepsMenu1.AddStepEvent += showStepEditor;
            stepsMenu1.EditStepEvent += showStepEditor;
            stepsMenu1.StepSelectedEvent += workspace1.currnetStepChanged;
            workspace1.StartingStateChanged += stepsMenu1.updateStepsList;
            connectionControl1.ProtocolStartButtonClicked += startProtocol;
        }

        public void init(ref PlatesList platesList, ref Project project)
        {
            this.project = project;
            this.ProtocolSteps = project.ProtocolSteps;
            workspace1.init(ref platesList, ref project.Liquids, ref project.ProtocolSteps, ref project.StartingZones, project.Rows, project.Cols);
            stepsMenu1.init(ref project.ProtocolSteps);
            stepsMenu1.updateStepsList(-1, workspace1.currentZones);
            if (tableLayoutPanel1.Controls.Count > 0)
            {
                Control firstCellControl = tableLayoutPanel1.GetControlFromPosition(0, 0);
                if (firstCellControl != null)
                {
                    tableLayoutPanel1.Controls.Remove(firstCellControl);
                    firstCellControl.Dispose(); // Освобождаем ресурсы, если это необходимо
                }
            }
            if (ProtocolSteps.Count > 0)
            {
                nextStepId = ProtocolSteps.Max(step => step.Id) + 1;
            }
        }

        public void UpdateProject()
        {
            workspace1.UpdateProject();
            stepsMenu1.updateStepsList(-1, workspace1.startingZones);
        }


        public void liquidsUpdated()
        {
            workspace1.liquidsUpdated();
            stepsMenu1.updateStepsList(workspace1.currentStep, workspace1.currentZones);
        }

        private void showStepEditor(STEP_TYPE type, ProtocolStep step = null)
        {
            int stepId = nextStepId;
            if (step != null)
            {
                stepId = ProtocolSteps.FindIndex(Step => Step.Id == step.Id);// step.Id;
            }
            tableLayoutPanel1.SuspendLayout();
            switch (type)
            {
                case STEP_TYPE.TRANSFER:
                    {
                        // Очищаем содержимое первой ячейки, если оно есть
                        if (tableLayoutPanel1.Controls.Count > 0)
                        {
                            Control firstCellControl = tableLayoutPanel1.GetControlFromPosition(0, 0);
                            if (firstCellControl != null)
                            {
                                tableLayoutPanel1.Controls.Remove(firstCellControl);
                                firstCellControl.Dispose(); // Освобождаем ресурсы, если это необходимо
                            }
                        }


                        // Устанавливаем режим автоматического размера
                        tableLayoutPanel1.AutoSize = true;

                        // Создаем и добавляем TransferStepMenu
                        TransferStepMenu transferStepMenu;
                        if (step == null)
                        {
                            transferStepMenu = new TransferStepMenu(stepId, workspace1.currentZones, step); // Здеь надо будет передавать не начальные зоны, а зоны текущего шага
                        }
                        else
                        {

                            transferStepMenu = new TransferStepMenu(stepId - 1, workspace1.GetZonesAtStep(stepId - 1), step);
                        }
                        transferStepMenu.StepEditCanceledEvent += closeStepEditor;
                        transferStepMenu.StepCreatedEvent += addStep;
                        transferStepMenu.StepDeletedEvent += removeStep;

                        transferStepMenu.Dock = DockStyle.Fill;
                        tableLayoutPanel1.Controls.Add(transferStepMenu, 0, 0); // Добавляем в первую ячейку
                        break;
                    }

                case STEP_TYPE.MIX:
                    {
                        if (tableLayoutPanel1.Controls.Count > 0)
                        {
                            Control firstCellControl = tableLayoutPanel1.GetControlFromPosition(0, 0);
                            if (firstCellControl != null)
                            {
                                tableLayoutPanel1.Controls.Remove(firstCellControl);
                                firstCellControl.Dispose(); // Освобождаем ресурсы, если это необходимо
                            }
                        }


                        // Устанавливаем режим автоматического размера
                        tableLayoutPanel1.AutoSize = true;

                        // Создаем и добавляем TransferStepMenu
                        MixStepMenu mixStepMenu;
                        if (step == null)
                        {
                            mixStepMenu = new MixStepMenu(stepId, workspace1.currentZones, step); // Здеь надо будет передавать не начальные зоны, а зоны текущего шага
                        }
                        else
                        {
                            mixStepMenu = new MixStepMenu(stepId - 1, workspace1.GetZonesAtStep(stepId - 1), step);
                        }
                        //MixStepMenu mixStepMenu  = new MixStepMenu(stepId, workspace1.currentZones, step);
                        mixStepMenu.StepEditCanceledEvent += closeStepEditor;
                        mixStepMenu.StepCreatedEvent += addStep;
                        mixStepMenu.StepDeletedEvent += removeStep;
                        mixStepMenu.Dock = DockStyle.Fill;
                        tableLayoutPanel1.Controls.Add(mixStepMenu, 0, 0); // Добавляем в первую ячейку
                        break;
                    }
                case STEP_TYPE.WAIT:
                    {

                        if (tableLayoutPanel1.Controls.Count > 0)
                        {
                            Control firstCellControl = tableLayoutPanel1.GetControlFromPosition(0, 0);
                            if (firstCellControl != null)
                            {
                                tableLayoutPanel1.Controls.Remove(firstCellControl);
                                firstCellControl.Dispose(); // Освобождаем ресурсы, если это необходимо
                            }
                        }


                        // Устанавливаем режим автоматического размера
                        tableLayoutPanel1.AutoSize = true;

                        // Создаем и добавляем TransferStepMenu
                        WaitStepMenu waitStepMenu;
                        if (step == null)
                        {
                            waitStepMenu = new WaitStepMenu(stepId, workspace1.currentZones, step); // Здеь надо будет передавать не начальные зоны, а зоны текущего шага
                        }
                        else
                        {
                            waitStepMenu = new WaitStepMenu(stepId - 1, workspace1.GetZonesAtStep(stepId - 1), step);
                        }
                        waitStepMenu.StepEditCanceledEvent += closeStepEditor;
                        waitStepMenu.StepCreatedEvent += addStep;
                        waitStepMenu.StepDeletedEvent += removeStep;
                        waitStepMenu.Dock = DockStyle.Fill;
                        tableLayoutPanel1.Controls.Add(waitStepMenu, 0, 0); // Добавляем в первую ячейку
                        break;
                    }
                    // Добавить обработку других типов шагов здесь, если необходимо
            }
            tableLayoutPanel1.ResumeLayout();
        }

        private void closeStepEditor(ProtocolStep protocolStep)
        {
            tableLayoutPanel1.SuspendLayout();
            if (tableLayoutPanel1.Controls.Count > 0)
            {
                Control firstCellControl = tableLayoutPanel1.GetControlFromPosition(0, 0);
                if (firstCellControl != null)
                {
                    tableLayoutPanel1.Controls.Remove(firstCellControl);
                    firstCellControl.Dispose();
                }
            }
            tableLayoutPanel1.ResumeLayout();
        }

        private void addStep(ProtocolStep protocolStep)
        {
            tableLayoutPanel1.SuspendLayout();

            // Определяем индекс для вставки на основе текущего выбранного шага
            int insertIndex = workspace1.currentStep + 1;
            if (workspace1.currentStep == ProtocolSteps.Count)
            {
                insertIndex = ProtocolSteps.Count;
            }

            // Поиск существующего шага с таким же Id
            int existingStepIndex = ProtocolSteps.FindIndex(step => step.Id == protocolStep.Id);

            if (existingStepIndex >= 0)
            {
                // Замена существующего шага
                ProtocolSteps[existingStepIndex] = protocolStep.DeepCopy();
                // Обновляем список шагов, если заменяем шаг на той же позиции, обновление не требуется
                if (existingStepIndex != insertIndex)
                {
                    stepsMenu1.updateStepsList(existingStepIndex, workspace1.startingZones);
                }
            }
            else
            {
                // Сдвигаем оставшиеся шаги в списке, начиная с позиции вставки
                ProtocolSteps.Insert(insertIndex, protocolStep.DeepCopy());
                // Обновляем список шагов и Id следующего шага
                stepsMenu1.updateStepsList(insertIndex, workspace1.startingZones);
                nextStepId = ProtocolSteps.Max(step => step.Id) + 1;
            }
            closeStepEditor(null);
            tableLayoutPanel1.ResumeLayout();
        }

        private void removeStep(ProtocolStep protocolStep)
        {
            // Удаляем шаг с заданным Id из списка
            ProtocolSteps.RemoveAll(step => step.Id == protocolStep.Id);
            closeStepEditor(protocolStep);

            // Обновляем пользовательский интерфейс или выполняем другие необходимые действия
            // после удаления шага
            // Например, обновляем отображение списка шагов
            stepsMenu1.updateStepsList(0, workspace1.currentZones);
        }

        private void startProtocol()
        {
            bool protocolIsValid = stepsMenu1.checkForErrors(workspace1.startingZones);
            if (protocolIsValid)
            {
                Translator translator = new Translator();
                translator.init(project);
                List<string> result = translator.createNewTask(true);
                string res = "";
                foreach (string step in result)
                {
                    res += step + " ";
                }
                bool success = connectionControl1.runProtocol(result);
            }
        }


    }
}
