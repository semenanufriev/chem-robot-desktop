﻿namespace ChemRobotDesktop.CustomControls
{
    partial class MixStepMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tableLayoutPanel1 = new TableLayoutPanel();
            tableLayoutPanel2 = new TableLayoutPanel();
            label3 = new Label();
            label2 = new Label();
            sourcePlateBox = new ComboBox();
            selectSourceWellsButton = new Button();
            label1 = new Label();
            tableLayoutPanel3 = new TableLayoutPanel();
            mixCountBox = new NumericUpDown();
            tipChangeModeBox = new ComboBox();
            label7 = new Label();
            volumeBox = new NumericUpDown();
            label6 = new Label();
            label4 = new Label();
            tableLayoutPanel4 = new TableLayoutPanel();
            deleteStepButton = new Button();
            cancelButton = new Button();
            saveButton = new Button();
            tableLayoutPanel1.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)mixCountBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)volumeBox).BeginInit();
            tableLayoutPanel4.SuspendLayout();
            SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.BackColor = Color.Transparent;
            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(tableLayoutPanel2, 0, 1);
            tableLayoutPanel1.Controls.Add(label1, 0, 0);
            tableLayoutPanel1.Controls.Add(tableLayoutPanel3, 0, 2);
            tableLayoutPanel1.Controls.Add(tableLayoutPanel4, 0, 3);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.Padding = new Padding(3, 4, 3, 4);
            tableLayoutPanel1.RowCount = 4;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 53F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 107F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 165F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 80F));
            tableLayoutPanel1.Size = new Size(1143, 427);
            tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 6;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 240F));
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 183F));
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 34F));
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 240F));
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 183F));
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 23F));
            tableLayoutPanel2.Controls.Add(label3, 1, 0);
            tableLayoutPanel2.Controls.Add(label2, 0, 0);
            tableLayoutPanel2.Controls.Add(sourcePlateBox, 0, 1);
            tableLayoutPanel2.Controls.Add(selectSourceWellsButton, 1, 1);
            tableLayoutPanel2.Dock = DockStyle.Fill;
            tableLayoutPanel2.Location = new Point(6, 61);
            tableLayoutPanel2.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 2;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 34.7368431F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 65.26316F));
            tableLayoutPanel2.Size = new Size(873, 99);
            tableLayoutPanel2.TabIndex = 0;
            // 
            // label3
            // 
            label3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label3.Location = new Point(243, 9);
            label3.Name = "label3";
            label3.Size = new Size(75, 25);
            label3.TabIndex = 5;
            label3.Text = "Ячейки:";
            // 
            // label2
            // 
            label2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(3, 9);
            label2.Name = "label2";
            label2.Size = new Size(94, 25);
            label2.TabIndex = 0;
            label2.Text = "Источник:";
            // 
            // sourcePlateBox
            // 
            sourcePlateBox.Anchor = AnchorStyles.Top;
            sourcePlateBox.DropDownStyle = ComboBoxStyle.DropDownList;
            sourcePlateBox.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            sourcePlateBox.FormattingEnabled = true;
            sourcePlateBox.Location = new Point(6, 38);
            sourcePlateBox.Margin = new Padding(3, 4, 3, 4);
            sourcePlateBox.Name = "sourcePlateBox";
            sourcePlateBox.Size = new Size(228, 42);
            sourcePlateBox.TabIndex = 2;
            // 
            // selectSourceWellsButton
            // 
            selectSourceWellsButton.Anchor = AnchorStyles.Top;
            selectSourceWellsButton.FlatStyle = FlatStyle.Popup;
            selectSourceWellsButton.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            selectSourceWellsButton.Location = new Point(246, 38);
            selectSourceWellsButton.Margin = new Padding(3, 4, 3, 4);
            selectSourceWellsButton.Name = "selectSourceWellsButton";
            selectSourceWellsButton.Size = new Size(171, 47);
            selectSourceWellsButton.TabIndex = 7;
            selectSourceWellsButton.Text = "Выбрать ячейки";
            selectSourceWellsButton.UseVisualStyleBackColor = true;
            selectSourceWellsButton.Click += selectSourceWellsButton_Click;
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 18F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(6, 13);
            label1.Margin = new Padding(3, 0, 3, 3);
            label1.Name = "label1";
            label1.Size = new Size(149, 41);
            label1.TabIndex = 1;
            label1.Text = "Смешать";
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 3;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 240F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 379F));
            tableLayoutPanel3.Controls.Add(mixCountBox, 2, 1);
            tableLayoutPanel3.Controls.Add(tipChangeModeBox, 0, 1);
            tableLayoutPanel3.Controls.Add(label7, 0, 0);
            tableLayoutPanel3.Controls.Add(volumeBox, 1, 1);
            tableLayoutPanel3.Controls.Add(label6, 1, 0);
            tableLayoutPanel3.Controls.Add(label4, 2, 0);
            tableLayoutPanel3.Dock = DockStyle.Fill;
            tableLayoutPanel3.Location = new Point(6, 168);
            tableLayoutPanel3.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 2;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel3.Size = new Size(873, 157);
            tableLayoutPanel3.TabIndex = 2;
            // 
            // mixCountBox
            // 
            mixCountBox.BackColor = SystemColors.GradientActiveCaption;
            mixCountBox.Location = new Point(497, 82);
            mixCountBox.Margin = new Padding(3, 4, 3, 4);
            mixCountBox.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            mixCountBox.Name = "mixCountBox";
            mixCountBox.Size = new Size(171, 41);
            mixCountBox.TabIndex = 7;
            mixCountBox.Value = new decimal(new int[] { 1, 0, 0, 0 });
            // 
            // tipChangeModeBox
            // 
            tipChangeModeBox.Anchor = AnchorStyles.Top;
            tipChangeModeBox.DropDownStyle = ComboBoxStyle.DropDownList;
            tipChangeModeBox.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tipChangeModeBox.FormattingEnabled = true;
            tipChangeModeBox.Location = new Point(6, 82);
            tipChangeModeBox.Margin = new Padding(3, 4, 3, 4);
            tipChangeModeBox.Name = "tipChangeModeBox";
            tipChangeModeBox.Size = new Size(228, 36);
            tipChangeModeBox.TabIndex = 4;
            tipChangeModeBox.SelectedIndexChanged += tipChangeModeBox_SelectedIndexChanged;
            // 
            // label7
            // 
            label7.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label7.AutoSize = true;
            label7.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label7.Location = new Point(3, 53);
            label7.Name = "label7";
            label7.Size = new Size(191, 25);
            label7.TabIndex = 2;
            label7.Text = "Режим смены носика:";
            // 
            // volumeBox
            // 
            volumeBox.BackColor = SystemColors.GradientActiveCaption;
            volumeBox.Location = new Point(243, 82);
            volumeBox.Margin = new Padding(3, 4, 3, 4);
            volumeBox.Maximum = new decimal(new int[] { 1000, 0, 0, 0 });
            volumeBox.Name = "volumeBox";
            volumeBox.Size = new Size(171, 41);
            volumeBox.TabIndex = 5;
            // 
            // label6
            // 
            label6.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label6.Location = new Point(243, 53);
            label6.Name = "label6";
            label6.Size = new Size(231, 25);
            label6.TabIndex = 1;
            label6.Text = "Объём смешивания (мкл) :";
            // 
            // label4
            // 
            label4.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label4.Location = new Point(497, 53);
            label4.Name = "label4";
            label4.Size = new Size(224, 25);
            label4.TabIndex = 6;
            label4.Text = "Количество смешиваний :";
            // 
            // tableLayoutPanel4
            // 
            tableLayoutPanel4.ColumnCount = 4;
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 240F));
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 183F));
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 183F));
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 23F));
            tableLayoutPanel4.Controls.Add(deleteStepButton, 0, 0);
            tableLayoutPanel4.Controls.Add(cancelButton, 3, 0);
            tableLayoutPanel4.Controls.Add(saveButton, 2, 0);
            tableLayoutPanel4.Dock = DockStyle.Fill;
            tableLayoutPanel4.Location = new Point(6, 333);
            tableLayoutPanel4.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel4.Name = "tableLayoutPanel4";
            tableLayoutPanel4.RowCount = 1;
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel4.Size = new Size(873, 86);
            tableLayoutPanel4.TabIndex = 3;
            // 
            // deleteStepButton
            // 
            deleteStepButton.Anchor = AnchorStyles.None;
            deleteStepButton.FlatStyle = FlatStyle.Popup;
            deleteStepButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            deleteStepButton.Location = new Point(55, 21);
            deleteStepButton.Margin = new Padding(3, 4, 3, 4);
            deleteStepButton.Name = "deleteStepButton";
            deleteStepButton.Size = new Size(130, 43);
            deleteStepButton.TabIndex = 8;
            deleteStepButton.Text = "Удалить";
            deleteStepButton.UseVisualStyleBackColor = true;
            deleteStepButton.Click += deleteStepButton_Click;
            // 
            // cancelButton
            // 
            cancelButton.Anchor = AnchorStyles.None;
            cancelButton.FlatStyle = FlatStyle.Popup;
            cancelButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            cancelButton.Location = new Point(712, 21);
            cancelButton.Margin = new Padding(3, 4, 3, 4);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(138, 43);
            cancelButton.TabIndex = 10;
            cancelButton.Text = "Отменить";
            cancelButton.UseVisualStyleBackColor = true;
            cancelButton.Click += cancelButton_Click;
            // 
            // saveButton
            // 
            saveButton.Anchor = AnchorStyles.None;
            saveButton.FlatStyle = FlatStyle.Popup;
            saveButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            saveButton.Location = new Point(529, 21);
            saveButton.Margin = new Padding(3, 4, 3, 4);
            saveButton.Name = "saveButton";
            saveButton.Size = new Size(138, 43);
            saveButton.TabIndex = 11;
            saveButton.Text = "Сохранить";
            saveButton.UseVisualStyleBackColor = true;
            saveButton.Click += saveButton_Click;
            // 
            // MixStepMenu
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.GradientInactiveCaption;
            Controls.Add(tableLayoutPanel1);
            Margin = new Padding(3, 4, 3, 4);
            Name = "MixStepMenu";
            Size = new Size(1143, 427);
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            tableLayoutPanel2.ResumeLayout(false);
            tableLayoutPanel2.PerformLayout();
            tableLayoutPanel3.ResumeLayout(false);
            tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)mixCountBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)volumeBox).EndInit();
            tableLayoutPanel4.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel tableLayoutPanel1;
        private TableLayoutPanel tableLayoutPanel2;
        private Label label3;
        private Label label2;
        private ComboBox sourcePlateBox;
        private Button selectSourceWellsButton;
        private Label label1;
        private TableLayoutPanel tableLayoutPanel3;
        private ComboBox tipChangeModeBox;
        private Label label6;
        private Label label7;
        private NumericUpDown volumeBox;
        private TableLayoutPanel tableLayoutPanel4;
        private Button deleteStepButton;
        private Button cancelButton;
        private Button saveButton;
        private NumericUpDown mixCountBox;
        private Label label4;
    }
}
