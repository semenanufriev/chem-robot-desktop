﻿namespace ChemRobotDesktop.CustomControls
{
    partial class WaitStepMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tableLayoutPanel1 = new TableLayoutPanel();
            label1 = new Label();
            tableLayoutPanel3 = new TableLayoutPanel();
            hoursBox = new NumericUpDown();
            secondsBox = new NumericUpDown();
            label7 = new Label();
            minutesBox = new NumericUpDown();
            label6 = new Label();
            label4 = new Label();
            tableLayoutPanel4 = new TableLayoutPanel();
            deleteStepButton = new Button();
            cancelButton = new Button();
            saveButton = new Button();
            tableLayoutPanel1.SuspendLayout();
            tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)hoursBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)secondsBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)minutesBox).BeginInit();
            tableLayoutPanel4.SuspendLayout();
            SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.BackColor = Color.Transparent;
            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(label1, 0, 0);
            tableLayoutPanel1.Controls.Add(tableLayoutPanel3, 0, 1);
            tableLayoutPanel1.Controls.Add(tableLayoutPanel4, 0, 2);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.Padding = new Padding(3, 4, 3, 4);
            tableLayoutPanel1.RowCount = 3;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 53F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 108F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 80F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 27F));
            tableLayoutPanel1.Size = new Size(1078, 249);
            tableLayoutPanel1.TabIndex = 2;
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 16.2F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(6, 19);
            label1.Name = "label1";
            label1.Size = new Size(162, 38);
            label1.TabIndex = 1;
            label1.Text = "Ожидание";
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 3;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 126F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 126F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 622F));
            tableLayoutPanel3.Controls.Add(hoursBox, 0, 1);
            tableLayoutPanel3.Controls.Add(secondsBox, 2, 1);
            tableLayoutPanel3.Controls.Add(label7, 0, 0);
            tableLayoutPanel3.Controls.Add(minutesBox, 1, 1);
            tableLayoutPanel3.Controls.Add(label6, 1, 0);
            tableLayoutPanel3.Controls.Add(label4, 2, 0);
            tableLayoutPanel3.Dock = DockStyle.Fill;
            tableLayoutPanel3.Location = new Point(6, 61);
            tableLayoutPanel3.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 2;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 34.6666679F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 65.3333359F));
            tableLayoutPanel3.Size = new Size(873, 100);
            tableLayoutPanel3.TabIndex = 2;
            // 
            // hoursBox
            // 
            hoursBox.BackColor = SystemColors.GradientActiveCaption;
            hoursBox.Location = new Point(3, 38);
            hoursBox.Margin = new Padding(3, 4, 3, 4);
            hoursBox.Name = "hoursBox";
            hoursBox.Size = new Size(114, 41);
            hoursBox.TabIndex = 8;
            // 
            // secondsBox
            // 
            secondsBox.BackColor = SystemColors.GradientActiveCaption;
            secondsBox.Location = new Point(255, 38);
            secondsBox.Margin = new Padding(3, 4, 3, 4);
            secondsBox.Maximum = new decimal(new int[] { 60, 0, 0, 0 });
            secondsBox.Name = "secondsBox";
            secondsBox.Size = new Size(114, 41);
            secondsBox.TabIndex = 7;
            // 
            // label7
            // 
            label7.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label7.AutoSize = true;
            label7.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label7.Location = new Point(3, 9);
            label7.Name = "label7";
            label7.Size = new Size(66, 25);
            label7.TabIndex = 2;
            label7.Text = "Часов:";
            // 
            // minutesBox
            // 
            minutesBox.BackColor = SystemColors.GradientActiveCaption;
            minutesBox.Location = new Point(129, 38);
            minutesBox.Margin = new Padding(3, 4, 3, 4);
            minutesBox.Maximum = new decimal(new int[] { 60, 0, 0, 0 });
            minutesBox.Name = "minutesBox";
            minutesBox.Size = new Size(114, 41);
            minutesBox.TabIndex = 5;
            // 
            // label6
            // 
            label6.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label6.AutoSize = true;
            label6.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label6.Location = new Point(129, 9);
            label6.Name = "label6";
            label6.Size = new Size(68, 25);
            label6.TabIndex = 1;
            label6.Text = "Минут:";
            // 
            // label4
            // 
            label4.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            label4.Location = new Point(255, 9);
            label4.Name = "label4";
            label4.Size = new Size(74, 25);
            label4.TabIndex = 6;
            label4.Text = "Секунд:";
            // 
            // tableLayoutPanel4
            // 
            tableLayoutPanel4.ColumnCount = 4;
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 240F));
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 183F));
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 183F));
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 23F));
            tableLayoutPanel4.Controls.Add(deleteStepButton, 0, 0);
            tableLayoutPanel4.Controls.Add(cancelButton, 3, 0);
            tableLayoutPanel4.Controls.Add(saveButton, 2, 0);
            tableLayoutPanel4.Dock = DockStyle.Fill;
            tableLayoutPanel4.Location = new Point(6, 169);
            tableLayoutPanel4.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel4.Name = "tableLayoutPanel4";
            tableLayoutPanel4.RowCount = 1;
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel4.Size = new Size(873, 72);
            tableLayoutPanel4.TabIndex = 3;
            // 
            // deleteStepButton
            // 
            deleteStepButton.Anchor = AnchorStyles.None;
            deleteStepButton.FlatStyle = FlatStyle.Popup;
            deleteStepButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            deleteStepButton.Location = new Point(55, 14);
            deleteStepButton.Margin = new Padding(3, 4, 3, 4);
            deleteStepButton.Name = "deleteStepButton";
            deleteStepButton.Size = new Size(130, 43);
            deleteStepButton.TabIndex = 8;
            deleteStepButton.Text = "Удалить";
            deleteStepButton.UseVisualStyleBackColor = true;
            deleteStepButton.Click += deleteStepButton_Click;
            // 
            // cancelButton
            // 
            cancelButton.Anchor = AnchorStyles.None;
            cancelButton.FlatStyle = FlatStyle.Popup;
            cancelButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            cancelButton.Location = new Point(712, 14);
            cancelButton.Margin = new Padding(3, 4, 3, 4);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(138, 43);
            cancelButton.TabIndex = 10;
            cancelButton.Text = "Отменить";
            cancelButton.UseVisualStyleBackColor = true;
            cancelButton.Click += cancelButton_Click;
            // 
            // saveButton
            // 
            saveButton.Anchor = AnchorStyles.None;
            saveButton.FlatStyle = FlatStyle.Popup;
            saveButton.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            saveButton.Location = new Point(529, 14);
            saveButton.Margin = new Padding(3, 4, 3, 4);
            saveButton.Name = "saveButton";
            saveButton.Size = new Size(138, 43);
            saveButton.TabIndex = 11;
            saveButton.Text = "Сохранить";
            saveButton.UseVisualStyleBackColor = true;
            saveButton.Click += saveButton_Click;
            // 
            // WaitStepMenu
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.GradientInactiveCaption;
            Controls.Add(tableLayoutPanel1);
            Margin = new Padding(3, 4, 3, 4);
            Name = "WaitStepMenu";
            Size = new Size(1078, 249);
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            tableLayoutPanel3.ResumeLayout(false);
            tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)hoursBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)secondsBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)minutesBox).EndInit();
            tableLayoutPanel4.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel tableLayoutPanel1;
        private Label label1;
        private TableLayoutPanel tableLayoutPanel3;
        private NumericUpDown mixCountBox;
        private ComboBox tipChangeModeBox;
        private Label label7;
        private NumericUpDown minutesBox;
        private NumericUpDown secondsBox;
        private Label label6;
        private Label label4;
        private TableLayoutPanel tableLayoutPanel4;
        private Button deleteStepButton;
        private Button cancelButton;
        private Button saveButton;
        private NumericUpDown hoursBox;
    }
}
