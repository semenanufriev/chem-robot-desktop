﻿namespace ChemRobotDesktop.CustomControls
{
    partial class PlatesList
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            components = new System.ComponentModel.Container();
            tableLayoutPanel1 = new TableLayoutPanel();
            cancelButton = new Button();
            label1 = new Label();
            dropPlateTable = new TableLayoutPanel();
            dropPlaceLabel = new Label();
            reservoirPlateTable = new TableLayoutPanel();
            reservoirLable = new Label();
            wellPlateTable = new TableLayoutPanel();
            wellPlateLabel = new Label();
            tipRackTable = new TableLayoutPanel();
            tipRackLabel = new Label();
            toolTip1 = new ToolTip(components);
            tableLayoutPanel1.SuspendLayout();
            dropPlateTable.SuspendLayout();
            reservoirPlateTable.SuspendLayout();
            wellPlateTable.SuspendLayout();
            tipRackTable.SuspendLayout();
            SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.AutoSize = true;
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(cancelButton, 0, 5);
            tableLayoutPanel1.Controls.Add(label1, 0, 0);
            tableLayoutPanel1.Controls.Add(dropPlateTable, 0, 3);
            tableLayoutPanel1.Controls.Add(reservoirPlateTable, 0, 3);
            tableLayoutPanel1.Controls.Add(wellPlateTable, 0, 2);
            tableLayoutPanel1.Controls.Add(tipRackTable, 0, 1);
            tableLayoutPanel1.Location = new Point(3, 4);
            tableLayoutPanel1.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 6;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 53F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 80F));
            tableLayoutPanel1.Size = new Size(445, 695);
            tableLayoutPanel1.TabIndex = 0;
            tableLayoutPanel1.Paint += tableLayoutPanel1_Paint;
            // 
            // cancelButton
            // 
            cancelButton.Anchor = AnchorStyles.None;
            cancelButton.FlatStyle = FlatStyle.Popup;
            cancelButton.Font = new Font("Segoe UI", 13.8F, FontStyle.Regular, GraphicsUnit.Point);
            cancelButton.Location = new Point(130, 622);
            cancelButton.Margin = new Padding(3, 4, 3, 4);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(185, 64);
            cancelButton.TabIndex = 0;
            cancelButton.Text = "Отменить";
            cancelButton.UseVisualStyleBackColor = true;
            cancelButton.Click += plateSelectedMethod;
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.None;
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            label1.Location = new Point(110, 6);
            label1.Name = "label1";
            label1.Size = new Size(224, 41);
            label1.TabIndex = 1;
            label1.Text = "Оборудование";
            // 
            // dropPlateTable
            // 
            dropPlateTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            dropPlateTable.ColumnCount = 1;
            dropPlateTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            dropPlateTable.Controls.Add(dropPlaceLabel, 0, 0);
            dropPlateTable.Dock = DockStyle.Fill;
            dropPlateTable.Location = new Point(11, 465);
            dropPlateTable.Margin = new Padding(11, 13, 11, 13);
            dropPlateTable.Name = "dropPlateTable";
            dropPlateTable.RowCount = 2;
            dropPlateTable.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            dropPlateTable.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            dropPlateTable.Size = new Size(423, 135);
            dropPlateTable.TabIndex = 1;
            // 
            // dropPlaceLabel
            // 
            dropPlaceLabel.Cursor = Cursors.Hand;
            dropPlaceLabel.Dock = DockStyle.Fill;
            dropPlaceLabel.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
            dropPlaceLabel.Location = new Point(4, 5);
            dropPlaceLabel.Margin = new Padding(3, 4, 3, 4);
            dropPlaceLabel.Name = "dropPlaceLabel";
            dropPlaceLabel.Size = new Size(415, 58);
            dropPlaceLabel.TabIndex = 2;
            dropPlaceLabel.Text = "МЕСТО СБРОСА";
            dropPlaceLabel.TextAlign = ContentAlignment.MiddleCenter;
            dropPlaceLabel.Click += dropPlaceLabel_Click;
            // 
            // reservoirPlateTable
            // 
            reservoirPlateTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            reservoirPlateTable.ColumnCount = 1;
            reservoirPlateTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            reservoirPlateTable.Controls.Add(reservoirLable, 0, 0);
            reservoirPlateTable.Dock = DockStyle.Fill;
            reservoirPlateTable.Location = new Point(11, 332);
            reservoirPlateTable.Margin = new Padding(11, 13, 11, 13);
            reservoirPlateTable.Name = "reservoirPlateTable";
            reservoirPlateTable.RowCount = 2;
            reservoirPlateTable.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            reservoirPlateTable.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            reservoirPlateTable.Size = new Size(423, 107);
            reservoirPlateTable.TabIndex = 1;
            // 
            // reservoirLable
            // 
            reservoirLable.Cursor = Cursors.Hand;
            reservoirLable.Dock = DockStyle.Fill;
            reservoirLable.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
            reservoirLable.Location = new Point(4, 5);
            reservoirLable.Margin = new Padding(3, 4, 3, 4);
            reservoirLable.Name = "reservoirLable";
            reservoirLable.Size = new Size(415, 44);
            reservoirLable.TabIndex = 2;
            reservoirLable.Text = "РЕЗЕРВУАР";
            reservoirLable.TextAlign = ContentAlignment.MiddleCenter;
            reservoirLable.Click += reservoirLable_Click;
            // 
            // wellPlateTable
            // 
            wellPlateTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            wellPlateTable.ColumnCount = 1;
            wellPlateTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            wellPlateTable.Controls.Add(wellPlateLabel, 0, 0);
            wellPlateTable.Dock = DockStyle.Fill;
            wellPlateTable.Location = new Point(11, 199);
            wellPlateTable.Margin = new Padding(11, 13, 11, 13);
            wellPlateTable.Name = "wellPlateTable";
            wellPlateTable.RowCount = 2;
            wellPlateTable.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            wellPlateTable.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            wellPlateTable.Size = new Size(423, 107);
            wellPlateTable.TabIndex = 1;
            // 
            // wellPlateLabel
            // 
            wellPlateLabel.Cursor = Cursors.Hand;
            wellPlateLabel.Dock = DockStyle.Fill;
            wellPlateLabel.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
            wellPlateLabel.Location = new Point(4, 5);
            wellPlateLabel.Margin = new Padding(3, 4, 3, 4);
            wellPlateLabel.Name = "wellPlateLabel";
            wellPlateLabel.Size = new Size(415, 44);
            wellPlateLabel.TabIndex = 2;
            wellPlateLabel.Text = "ПЛАНШЕТ С ПРОБИРКАМИ";
            wellPlateLabel.TextAlign = ContentAlignment.MiddleCenter;
            wellPlateLabel.Click += wellPlateLabel_Click;
            // 
            // tipRackTable
            // 
            tipRackTable.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tipRackTable.ColumnCount = 1;
            tipRackTable.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tipRackTable.Controls.Add(tipRackLabel, 0, 0);
            tipRackTable.Dock = DockStyle.Fill;
            tipRackTable.Location = new Point(11, 66);
            tipRackTable.Margin = new Padding(11, 13, 11, 13);
            tipRackTable.Name = "tipRackTable";
            tipRackTable.RowCount = 2;
            tipRackTable.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tipRackTable.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tipRackTable.Size = new Size(423, 107);
            tipRackTable.TabIndex = 0;
            // 
            // tipRackLabel
            // 
            tipRackLabel.Cursor = Cursors.Hand;
            tipRackLabel.Dock = DockStyle.Fill;
            tipRackLabel.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
            tipRackLabel.Location = new Point(4, 5);
            tipRackLabel.Margin = new Padding(3, 4, 3, 4);
            tipRackLabel.Name = "tipRackLabel";
            tipRackLabel.Size = new Size(415, 44);
            tipRackLabel.TabIndex = 2;
            tipRackLabel.Text = "ПЛАНШЕТ С ПИПЕТКАМИ";
            tipRackLabel.TextAlign = ContentAlignment.MiddleCenter;
            tipRackLabel.Click += tipRackLabel_Click;
            // 
            // toolTip1
            // 
            toolTip1.AutoPopDelay = 5000;
            toolTip1.InitialDelay = 500;
            toolTip1.OwnerDraw = true;
            toolTip1.ReshowDelay = 1;
            toolTip1.Draw += toolTip_Draw;
            toolTip1.Popup += toolTip_Popup;
            // 
            // PlatesList
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoSize = true;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;
            BackColor = SystemColors.InactiveBorder;
            Controls.Add(tableLayoutPanel1);
            Margin = new Padding(3, 4, 3, 4);
            Name = "PlatesList";
            Size = new Size(451, 703);
            Load += PlatesList_Load;
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            dropPlateTable.ResumeLayout(false);
            reservoirPlateTable.ResumeLayout(false);
            wellPlateTable.ResumeLayout(false);
            tipRackTable.ResumeLayout(false);
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private TableLayoutPanel tableLayoutPanel1;
        private Label label1;
        private TableLayoutPanel dropPlateTable;
        private TableLayoutPanel reservoirPlateTable;
        private TableLayoutPanel wellPlateTable;
        private TableLayoutPanel tipRackTable;
        private Label dropPlaceLabel;
        private Label reservoirLable;
        private Label wellPlateLabel;
        private Label tipRackLabel;
        private ToolTip toolTip1;
        private Button cancelButton;
    }
}
