﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using ChemRobotDesktop.Classes;

namespace ChemRobotDesktop.CustomControls
{
    public partial class PlateParamsMenu : UserControl
    {
        public Plate plate;
        public PLATE_TYPE plate_type = PLATE_TYPE.WELL_PLATE;
        public event EventHandler PlateParamChanged;
        int plateId;

        public PlateParamsMenu()
        {
            InitializeComponent();
            sizesPicture.SizeMode = PictureBoxSizeMode.Zoom;
            hightPicture.SizeMode = PictureBoxSizeMode.Zoom;
            rowsColsPicture.SizeMode = PictureBoxSizeMode.Zoom;
            diameterPicture.SizeMode = PictureBoxSizeMode.Zoom;
            depthPicture.SizeMode = PictureBoxSizeMode.Zoom;
            spacePicture.SizeMode = PictureBoxSizeMode.Zoom;
            offsetPicture.SizeMode = PictureBoxSizeMode.Zoom;



            sizesPicture.Image = ChemRobotDesktop.Properties.Resources.expl_sizes;

            hightPicture.Image = ChemRobotDesktop.Properties.Resources.expl_height;
            rowsColsPicture.Image = ChemRobotDesktop.Properties.Resources.expl_grid;
            diameterPicture.Image = ChemRobotDesktop.Properties.Resources.expl_diameter;
            depthPicture.Image = ChemRobotDesktop.Properties.Resources.expl_diameter;
            spacePicture.Image = ChemRobotDesktop.Properties.Resources.expl_spacing_round;
            offsetPicture.Image = ChemRobotDesktop.Properties.Resources.expl_offset_round;



            lenghtBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            widthBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            heightBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            volumeBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            rowsBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            colsBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            xSpaceBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            ySpaceBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            xOffsetBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            yOffsetBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            diameterBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            wellXBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            wellYBox.MouseWheel += new MouseEventHandler(stopMouseWheel);
            depthBox.MouseWheel += new MouseEventHandler(stopMouseWheel);

            tableLayoutPanel3.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;

            // Подписываемся на событие CellPaint
            tableLayoutPanel3.CellPaint += new TableLayoutCellPaintEventHandler(tableLayoutPanel_CellPaint);


            plate = new Plate();
            updateWellShape();
            updateWellBottomShape(null, null);
            PlateParamChangedMethod(null, null);

        }



        private void stopMouseWheel(object sender, MouseEventArgs e)
        {
            // Отменяем стандартное поведение колесика мыши
            ((HandledMouseEventArgs)e).Handled = true;
        }

        private void updateWellShape()
        {
            if (CircularWellRadioButton.Checked)
            {
                showCircularMode();


                return;
            }
            if (RectangularRadioButton.Checked)
            {
                showRectangularMode();

                return;
            }
        }


        private void updateWellBottomShape(object sender, EventArgs e)
        {
            if (FlatBottomRadioButton.Checked)
            {
                depthPicture.Image = Properties.Resources.expl_bottom_shape_flat;


                return;
            }
            if (RoundBottomRadioButton.Checked)
            {
                depthPicture.Image = Properties.Resources.expl_bottom_shape_round;

                return;
            }
            if (VBottomRadioButton.Checked)
            {
                depthPicture.Image = Properties.Resources.expl_bottom_shape_v_shape;
                return;
            }
        }

        private void showCircularMode()
        {
            if (CircularWellRadioButton.Checked)
            {
                WellSizeTableLayoutPanel.RowStyles[1].SizeType = SizeType.Absolute;
                WellSizeTableLayoutPanel.RowStyles[1].Height = 0;

                WellSizeTableLayoutPanel.RowStyles[0].SizeType = SizeType.Percent;
                WellSizeTableLayoutPanel.RowStyles[0].Height = 100;
                diameterPicture.Image = Properties.Resources.expl_diameter;
                spacePicture.Image = Properties.Resources.expl_spacing_round;
                offsetPicture.Image = Properties.Resources.expl_offset_round;

                return;
            }
        }

        private void showRectangularMode()
        {
            if (RectangularRadioButton.Checked)
            {
                WellSizeTableLayoutPanel.RowStyles[0].SizeType = SizeType.Absolute;
                WellSizeTableLayoutPanel.RowStyles[0].Height = 0;

                WellSizeTableLayoutPanel.RowStyles[1].SizeType = SizeType.Percent;
                WellSizeTableLayoutPanel.RowStyles[1].Height = 100;

                diameterPicture.Image = Properties.Resources.expl_rect_size;
                spacePicture.Image = Properties.Resources.expl_spacing_rect;
                offsetPicture.Image = Properties.Resources.expl_offset_rect;

                return;
            }
        }


        private void CircularWellRadioButton_CheckedChanged(object sender, EventArgs e)
        {
            RadioButton radioButton = sender as RadioButton;
            if (radioButton != null)
            {
                updateWellShape();
            }
        }

        private void tableLayoutPanel_CellPaint(object sender, TableLayoutCellPaintEventArgs e)
        {
            // Условие для отрисовки верхней границы у определенных ячеек
            if ((e.Row != 6) && (e.Row != 7) && (e.Row != 8) && (e.Row != 10))
            {
                using (var pen = new Pen(Color.Black))
                {
                    // Отрисовка верхней границы ячейки
                    e.Graphics.DrawLine(pen, e.CellBounds.Left, e.CellBounds.Top, e.CellBounds.Right, e.CellBounds.Top);
                }
            }
        }

        public virtual void PlateParamChangedMethod(object sender, EventArgs e)
        {


            fillPlateParams();
            PlateParamChanged?.Invoke(this, EventArgs.Empty);
        }

        private void lenghtBox_ValueChanged(object sender, EventArgs e)
        {

            PlateParamChanged?.Invoke(this, EventArgs.Empty);
        }


        public void createNewEditor(PLATE_TYPE type, int id)
        {
            plateId = id;
            plate_type = type;
            depthBox.Value = 0;
            CircularWellRadioButton.Checked = true;
            diameterBox.Value = 5;
            wellXBox.Value = 5;
            wellYBox.Value = 5;
            FlatBottomRadioButton.Checked = true;
            volumeBox.Value = 0;
            rowsBox.Value = 1;
            colsBox.Value = 1;
            lenghtBox.Value = (decimal)127.5f;
            widthBox.Value = (decimal)85.5f;
            heightBox.Value = (decimal)10;
            xOffsetBox.Value = (decimal)10;
            yOffsetBox.Value = (decimal)10;
            xSpaceBox.Value = 10;
            ySpaceBox.Value = 10;

            nameBox.Text = "";
        }

        public Plate savePlate(string pathToPlateFolder)
        {
            fillPlateParams();
            string fileName = pathToPlateFolder + "/" + plate.Name + "_" + plate.ModelId.ToString() + ".plate";
            plate.SavePlateToFile(fileName);
            return plate;
        }

        public void fillPlateParams()
        {
            plate = new Plate();
            plate.ModelId = plateId;
            plate.Type = plate_type;
            Well well = new Well();
            well.Depth = (float)depthBox.Value;
            if (CircularWellRadioButton.Checked)
            {
                well.Shape = WELL_SHAPE.ROUND;
                well.Diameter = (float)diameterBox.Value;
            }
            else
            {
                well.Shape = WELL_SHAPE.RECTANGLE;
                PointF RectSize = new PointF((float)wellXBox.Value, (float)wellYBox.Value);
                well.RectSize = RectSize;
            }
            if (FlatBottomRadioButton.Checked)
            {
                well.BottomShape = WELL_BOTTOM_SHAPE.FLAT;
            }
            if (RoundBottomRadioButton.Checked)
            {
                well.BottomShape = WELL_BOTTOM_SHAPE.ROUND;
            }
            if (VBottomRadioButton.Checked)
            {
                well.BottomShape = WELL_BOTTOM_SHAPE.V_SHAPE;
            }


            well.MaxLiquidVolume = (float)volumeBox.Value;



            plate.Rows = (int)rowsBox.Value;
            plate.Columns = (int)colsBox.Value;
            Point3D plateSize = new Point3D();
            plateSize.X = (float)lenghtBox.Value;
            plateSize.Y = (float)widthBox.Value;
            plateSize.Z = (float)heightBox.Value;
            plate.Size = plateSize;
            PointF gridOffset = new PointF();
            gridOffset.X = (float)xOffsetBox.Value;
            gridOffset.Y = (float)yOffsetBox.Value;
            plate.GridOffset = gridOffset;


            PointF wellSpacing = new PointF();
            wellSpacing.X = (float)xSpaceBox.Value;
            wellSpacing.Y = (float)ySpaceBox.Value;
            plate.GridSpacing = wellSpacing; // AND MORE
            plate.Name = nameBox.Text;

            if (plate.Type == PLATE_TYPE.TIP_RACK)
            {
                //Tip(int maxVolume, float height, bool exist, float diameter)
                Tip tip = new Tip(1000, 100, 40, true, 4);
                plate.Wells = null;
                plate.Tips = new Dictionary<string, Tip>();
                for (int i = 0; i < plate.Rows; i++)
                {
                    for (int j = 0; j < plate.Columns; j++)
                    {
                        plate.Tips[plate.YNames[i] + (j + 1).ToString()] = tip.DeepCopy();
                    }
                }

            }
            else
            {

                for (int i = 0; i < plate.Rows; i++)
                {
                    for (int j = 0; j < plate.Columns; j++)
                    {
                        plate.Wells[plate.YNames[i] + (j + 1).ToString()] = well.DeepCopy();
                    }
                }
            }
            plate.FillWellPositions();
        }


        public void fillEditorWithPlateData(Plate plate)
        {
            if (plate != null)
            {
                // Заполнение общих данных о плите
                plateId = plate.ModelId;
                plate_type = plate.Type; // Предполагается, что plate_type - это поле класса
                nameBox.Text = plate.Name;
                rowsBox.Value = plate.Rows;
                colsBox.Value = plate.Columns;
                lenghtBox.Value = (decimal)plate.Size.X;
                widthBox.Value = (decimal)plate.Size.Y;
                heightBox.Value = (decimal)plate.Size.Z;
                xOffsetBox.Value = (decimal)plate.GridOffset.X;
                yOffsetBox.Value = (decimal)plate.GridOffset.Y;
                xSpaceBox.Value = (decimal)plate.GridSpacing.X;
                ySpaceBox.Value = (decimal)plate.GridSpacing.Y;

                // Предполагается, что в плите все колодцы одинаковые, поэтому берем данные из первого
                Well firstWell = null;
                if (plate.Wells != null && plate.Wells.Count > 0)
                {
                    firstWell = plate.Wells.FirstOrDefault().Value;
                }

                 // Или другой подход к получению первого колодца

                if (firstWell != null)
                {
                    // Заполнение данных о колодце
                    depthBox.Value = (decimal)firstWell.Depth;
                    volumeBox.Value = (decimal)firstWell.MaxLiquidVolume;

                    switch (firstWell.Shape)
                    {
                        case WELL_SHAPE.ROUND:
                            CircularWellRadioButton.Checked = true;
                            diameterBox.Value = (decimal)firstWell.Diameter;
                            break;
                        case WELL_SHAPE.RECTANGLE:
                            RectangularRadioButton.Checked = true; // Предполагается, что у вас есть такая кнопка
                            wellXBox.Value = (decimal)firstWell.RectSize.X;
                            wellYBox.Value = (decimal)firstWell.RectSize.Y;
                            break;
                    }

                    switch (firstWell.BottomShape)
                    {
                        case WELL_BOTTOM_SHAPE.FLAT:
                            FlatBottomRadioButton.Checked = true;
                            break;
                        case WELL_BOTTOM_SHAPE.ROUND:
                            RoundBottomRadioButton.Checked = true;
                            break;
                        case WELL_BOTTOM_SHAPE.V_SHAPE:
                            VBottomRadioButton.Checked = true;
                            break;
                    }
                }
            }
        }

        private void sizesPicture_Click(object sender, EventArgs e)
        {

        }
    }
}
