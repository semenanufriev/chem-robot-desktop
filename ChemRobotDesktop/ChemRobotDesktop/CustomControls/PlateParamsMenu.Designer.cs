﻿namespace ChemRobotDesktop.CustomControls
{
    partial class PlateParamsMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            tableLayoutPanel3 = new TableLayoutPanel();
            tableLayoutPanel2 = new TableLayoutPanel();
            label11 = new Label();
            tableLayoutPanel24 = new TableLayoutPanel();
            label18 = new Label();
            tableLayoutPanel28 = new TableLayoutPanel();
            label32 = new Label();
            tableLayoutPanel30 = new TableLayoutPanel();
            label34 = new Label();
            diameterPicture = new PictureBox();
            tableLayoutPanel31 = new TableLayoutPanel();
            label36 = new Label();
            tableLayoutPanel32 = new TableLayoutPanel();
            label38 = new Label();
            offsetPicture = new PictureBox();
            spacePicture = new PictureBox();
            depthPicture = new PictureBox();
            tableLayoutPanel14 = new TableLayoutPanel();
            label12 = new Label();
            label4 = new Label();
            ySpaceBox = new NumericUpDown();
            xSpaceBox = new NumericUpDown();
            sizesPicture = new PictureBox();
            hightPicture = new PictureBox();
            rowsColsPicture = new PictureBox();
            volumePicture = new PictureBox();
            tableLayoutPanel5 = new TableLayoutPanel();
            label19 = new Label();
            RectangularRadioButton = new RadioButton();
            CircularWellRadioButton = new RadioButton();
            tableLayoutPanel13 = new TableLayoutPanel();
            label9 = new Label();
            depthBox = new NumericUpDown();
            tableLayoutPanel11 = new TableLayoutPanel();
            volumeBox = new NumericUpDown();
            label7 = new Label();
            tableLayoutPanel9 = new TableLayoutPanel();
            colsBox = new NumericUpDown();
            label6 = new Label();
            label5 = new Label();
            rowsBox = new NumericUpDown();
            tableLayoutPanel8 = new TableLayoutPanel();
            heightBox = new NumericUpDown();
            label8 = new Label();
            tableLayoutPanel7 = new TableLayoutPanel();
            widthBox = new NumericUpDown();
            label1 = new Label();
            label2 = new Label();
            lenghtBox = new NumericUpDown();
            tableLayoutPanel21 = new TableLayoutPanel();
            label23 = new Label();
            label24 = new Label();
            yOffsetBox = new NumericUpDown();
            xOffsetBox = new NumericUpDown();
            WellSizeTableLayoutPanel = new TableLayoutPanel();
            tableLayoutPanel18 = new TableLayoutPanel();
            wellYBox = new NumericUpDown();
            label13 = new Label();
            label14 = new Label();
            wellXBox = new NumericUpDown();
            tableLayoutPanel19 = new TableLayoutPanel();
            label20 = new Label();
            diameterBox = new NumericUpDown();
            tableLayoutPanel17 = new TableLayoutPanel();
            label17 = new Label();
            VBottomRadioButton = new RadioButton();
            RoundBottomRadioButton = new RadioButton();
            FlatBottomRadioButton = new RadioButton();
            tableLayoutPanel15 = new TableLayoutPanel();
            label3 = new Label();
            tableLayoutPanel1 = new TableLayoutPanel();
            label10 = new Label();
            nameBox = new TextBox();
            tableLayoutPanel3.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            tableLayoutPanel24.SuspendLayout();
            tableLayoutPanel28.SuspendLayout();
            tableLayoutPanel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)diameterPicture).BeginInit();
            tableLayoutPanel31.SuspendLayout();
            tableLayoutPanel32.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)offsetPicture).BeginInit();
            ((System.ComponentModel.ISupportInitialize)spacePicture).BeginInit();
            ((System.ComponentModel.ISupportInitialize)depthPicture).BeginInit();
            tableLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)ySpaceBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)xSpaceBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)sizesPicture).BeginInit();
            ((System.ComponentModel.ISupportInitialize)hightPicture).BeginInit();
            ((System.ComponentModel.ISupportInitialize)rowsColsPicture).BeginInit();
            ((System.ComponentModel.ISupportInitialize)volumePicture).BeginInit();
            tableLayoutPanel5.SuspendLayout();
            tableLayoutPanel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)depthBox).BeginInit();
            tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)volumeBox).BeginInit();
            tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)colsBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)rowsBox).BeginInit();
            tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)heightBox).BeginInit();
            tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)widthBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)lenghtBox).BeginInit();
            tableLayoutPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)yOffsetBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)xOffsetBox).BeginInit();
            WellSizeTableLayoutPanel.SuspendLayout();
            tableLayoutPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)wellYBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)wellXBox).BeginInit();
            tableLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)diameterBox).BeginInit();
            tableLayoutPanel17.SuspendLayout();
            tableLayoutPanel15.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 4;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.33333F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.3333359F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 33.3333359F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 57F));
            tableLayoutPanel3.Controls.Add(tableLayoutPanel2, 0, 0);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel24, 0, 9);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel28, 0, 5);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel30, 0, 4);
            tableLayoutPanel3.Controls.Add(diameterPicture, 1, 6);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel31, 0, 3);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel32, 0, 2);
            tableLayoutPanel3.Controls.Add(offsetPicture, 1, 10);
            tableLayoutPanel3.Controls.Add(spacePicture, 1, 9);
            tableLayoutPanel3.Controls.Add(depthPicture, 1, 8);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel14, 2, 9);
            tableLayoutPanel3.Controls.Add(sizesPicture, 1, 1);
            tableLayoutPanel3.Controls.Add(hightPicture, 1, 2);
            tableLayoutPanel3.Controls.Add(rowsColsPicture, 1, 3);
            tableLayoutPanel3.Controls.Add(volumePicture, 1, 4);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel5, 2, 5);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel13, 2, 8);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel11, 2, 4);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel9, 2, 3);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel8, 2, 2);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel7, 2, 1);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel21, 2, 10);
            tableLayoutPanel3.Controls.Add(WellSizeTableLayoutPanel, 2, 6);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel17, 2, 7);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel15, 0, 1);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel1, 2, 0);
            tableLayoutPanel3.Dock = DockStyle.Fill;
            tableLayoutPanel3.Location = new Point(0, 0);
            tableLayoutPanel3.Margin = new Padding(0);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 11;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel3.Size = new Size(914, 2575);
            tableLayoutPanel3.TabIndex = 2;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 1;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.Controls.Add(label11, 0, 0);
            tableLayoutPanel2.Location = new Point(3, 4);
            tableLayoutPanel2.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 2;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 80F));
            tableLayoutPanel2.Size = new Size(257, 223);
            tableLayoutPanel2.TabIndex = 3;
            // 
            // label11
            // 
            label11.Anchor = AnchorStyles.None;
            label11.AutoSize = true;
            label11.Location = new Point(90, 12);
            label11.Name = "label11";
            label11.Size = new Size(77, 20);
            label11.TabIndex = 0;
            label11.Text = "Название";
            // 
            // tableLayoutPanel24
            // 
            tableLayoutPanel24.ColumnCount = 1;
            tableLayoutPanel24.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel24.Controls.Add(label18, 0, 0);
            tableLayoutPanel24.Location = new Point(3, 2083);
            tableLayoutPanel24.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel24.Name = "tableLayoutPanel24";
            tableLayoutPanel24.RowCount = 2;
            tableLayoutPanel24.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel24.RowStyles.Add(new RowStyle(SizeType.Percent, 80F));
            tableLayoutPanel24.Size = new Size(257, 223);
            tableLayoutPanel24.TabIndex = 3;
            // 
            // label18
            // 
            label18.Anchor = AnchorStyles.None;
            label18.AutoSize = true;
            label18.Location = new Point(46, 12);
            label18.Name = "label18";
            label18.Size = new Size(164, 20);
            label18.TabIndex = 0;
            label18.Text = "Шаг решетки && отступ";
            label18.TextAlign = ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel28
            // 
            tableLayoutPanel28.ColumnCount = 1;
            tableLayoutPanel28.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel28.Controls.Add(label32, 0, 0);
            tableLayoutPanel28.Location = new Point(3, 1159);
            tableLayoutPanel28.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel28.Name = "tableLayoutPanel28";
            tableLayoutPanel28.RowCount = 2;
            tableLayoutPanel28.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel28.RowStyles.Add(new RowStyle(SizeType.Percent, 80F));
            tableLayoutPanel28.Size = new Size(257, 223);
            tableLayoutPanel28.TabIndex = 7;
            // 
            // label32
            // 
            label32.Anchor = AnchorStyles.None;
            label32.AutoSize = true;
            label32.Location = new Point(37, 12);
            label32.Name = "label32";
            label32.Size = new Size(182, 20);
            label32.TabIndex = 0;
            label32.Text = "Форма && размер ячейки";
            // 
            // tableLayoutPanel30
            // 
            tableLayoutPanel30.ColumnCount = 1;
            tableLayoutPanel30.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel30.Controls.Add(label34, 0, 0);
            tableLayoutPanel30.Location = new Point(3, 928);
            tableLayoutPanel30.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel30.Name = "tableLayoutPanel30";
            tableLayoutPanel30.RowCount = 2;
            tableLayoutPanel30.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel30.RowStyles.Add(new RowStyle(SizeType.Percent, 80F));
            tableLayoutPanel30.Size = new Size(257, 223);
            tableLayoutPanel30.TabIndex = 8;
            // 
            // label34
            // 
            label34.Anchor = AnchorStyles.None;
            label34.AutoSize = true;
            label34.Location = new Point(100, 12);
            label34.Name = "label34";
            label34.Size = new Size(57, 20);
            label34.TabIndex = 0;
            label34.Text = "Объём";
            // 
            // diameterPicture
            // 
            diameterPicture.Dock = DockStyle.Fill;
            diameterPicture.Location = new Point(290, 1390);
            diameterPicture.Margin = new Padding(5, 4, 5, 4);
            diameterPicture.Name = "diameterPicture";
            diameterPicture.Size = new Size(275, 223);
            diameterPicture.TabIndex = 1;
            diameterPicture.TabStop = false;
            // 
            // tableLayoutPanel31
            // 
            tableLayoutPanel31.ColumnCount = 1;
            tableLayoutPanel31.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel31.Controls.Add(label36, 0, 0);
            tableLayoutPanel31.Location = new Point(3, 697);
            tableLayoutPanel31.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel31.Name = "tableLayoutPanel31";
            tableLayoutPanel31.RowCount = 2;
            tableLayoutPanel31.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel31.RowStyles.Add(new RowStyle(SizeType.Percent, 80F));
            tableLayoutPanel31.Size = new Size(257, 223);
            tableLayoutPanel31.TabIndex = 9;
            // 
            // label36
            // 
            label36.Anchor = AnchorStyles.None;
            label36.AutoSize = true;
            label36.Location = new Point(95, 12);
            label36.Name = "label36";
            label36.Size = new Size(66, 20);
            label36.TabIndex = 0;
            label36.Text = "Решетка";
            // 
            // tableLayoutPanel32
            // 
            tableLayoutPanel32.ColumnCount = 1;
            tableLayoutPanel32.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel32.Controls.Add(label38, 0, 0);
            tableLayoutPanel32.Location = new Point(3, 466);
            tableLayoutPanel32.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel32.Name = "tableLayoutPanel32";
            tableLayoutPanel32.RowCount = 2;
            tableLayoutPanel32.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel32.RowStyles.Add(new RowStyle(SizeType.Percent, 80F));
            tableLayoutPanel32.Size = new Size(257, 223);
            tableLayoutPanel32.TabIndex = 10;
            // 
            // label38
            // 
            label38.Anchor = AnchorStyles.None;
            label38.AutoSize = true;
            label38.Location = new Point(73, 12);
            label38.Name = "label38";
            label38.Size = new Size(110, 20);
            label38.TabIndex = 0;
            label38.Text = "Общая высота";
            // 
            // offsetPicture
            // 
            offsetPicture.Dock = DockStyle.Fill;
            offsetPicture.Location = new Point(290, 2314);
            offsetPicture.Margin = new Padding(5, 4, 5, 4);
            offsetPicture.Name = "offsetPicture";
            offsetPicture.Size = new Size(275, 257);
            offsetPicture.TabIndex = 1;
            offsetPicture.TabStop = false;
            // 
            // spacePicture
            // 
            spacePicture.Dock = DockStyle.Fill;
            spacePicture.Location = new Point(290, 2083);
            spacePicture.Margin = new Padding(5, 4, 5, 4);
            spacePicture.Name = "spacePicture";
            spacePicture.Size = new Size(275, 223);
            spacePicture.TabIndex = 1;
            spacePicture.TabStop = false;
            // 
            // depthPicture
            // 
            depthPicture.Dock = DockStyle.Fill;
            depthPicture.Location = new Point(290, 1852);
            depthPicture.Margin = new Padding(5, 4, 5, 4);
            depthPicture.Name = "depthPicture";
            depthPicture.Size = new Size(275, 223);
            depthPicture.TabIndex = 1;
            depthPicture.TabStop = false;
            // 
            // tableLayoutPanel14
            // 
            tableLayoutPanel14.ColumnCount = 1;
            tableLayoutPanel14.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel14.Controls.Add(label12, 0, 2);
            tableLayoutPanel14.Controls.Add(label4, 0, 0);
            tableLayoutPanel14.Controls.Add(ySpaceBox, 0, 3);
            tableLayoutPanel14.Controls.Add(xSpaceBox, 0, 1);
            tableLayoutPanel14.Dock = DockStyle.Fill;
            tableLayoutPanel14.Location = new Point(573, 2083);
            tableLayoutPanel14.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel14.Name = "tableLayoutPanel14";
            tableLayoutPanel14.RowCount = 4;
            tableLayoutPanel14.RowStyles.Add(new RowStyle(SizeType.Percent, 18.7500019F));
            tableLayoutPanel14.RowStyles.Add(new RowStyle(SizeType.Percent, 31.2500019F));
            tableLayoutPanel14.RowStyles.Add(new RowStyle(SizeType.Percent, 18.7500019F));
            tableLayoutPanel14.RowStyles.Add(new RowStyle(SizeType.Percent, 31.2499981F));
            tableLayoutPanel14.Size = new Size(279, 223);
            tableLayoutPanel14.TabIndex = 20;
            // 
            // label12
            // 
            label12.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label12.AutoSize = true;
            label12.Location = new Point(5, 131);
            label12.Margin = new Padding(5, 0, 5, 0);
            label12.Name = "label12";
            label12.Size = new Size(171, 20);
            label12.TabIndex = 16;
            label12.Text = "Шаг решетки в ширину";
            // 
            // label4
            // 
            label4.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label4.AutoSize = true;
            label4.Location = new Point(5, 21);
            label4.Margin = new Padding(5, 0, 5, 0);
            label4.Name = "label4";
            label4.Size = new Size(157, 20);
            label4.TabIndex = 15;
            label4.Text = "Шаг решетки в длину";
            // 
            // ySpaceBox
            // 
            ySpaceBox.DecimalPlaces = 1;
            ySpaceBox.Location = new Point(5, 155);
            ySpaceBox.Margin = new Padding(5, 4, 5, 4);
            ySpaceBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            ySpaceBox.Name = "ySpaceBox";
            ySpaceBox.Size = new Size(171, 27);
            ySpaceBox.TabIndex = 10;
            ySpaceBox.Value = new decimal(new int[] { 10, 0, 0, 0 });
            ySpaceBox.ValueChanged += PlateParamChangedMethod;
            // 
            // xSpaceBox
            // 
            xSpaceBox.DecimalPlaces = 1;
            xSpaceBox.Location = new Point(5, 45);
            xSpaceBox.Margin = new Padding(5, 4, 5, 4);
            xSpaceBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            xSpaceBox.Name = "xSpaceBox";
            xSpaceBox.Size = new Size(171, 27);
            xSpaceBox.TabIndex = 9;
            xSpaceBox.Value = new decimal(new int[] { 10, 0, 0, 0 });
            xSpaceBox.ValueChanged += PlateParamChangedMethod;
            // 
            // sizesPicture
            // 
            sizesPicture.Dock = DockStyle.Fill;
            sizesPicture.Image = Properties.Resources.expl_sizes;
            sizesPicture.Location = new Point(290, 235);
            sizesPicture.Margin = new Padding(5, 4, 5, 4);
            sizesPicture.Name = "sizesPicture";
            sizesPicture.Size = new Size(275, 223);
            sizesPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            sizesPicture.TabIndex = 0;
            sizesPicture.TabStop = false;
            sizesPicture.Click += sizesPicture_Click;
            // 
            // hightPicture
            // 
            hightPicture.Dock = DockStyle.Fill;
            hightPicture.Location = new Point(290, 466);
            hightPicture.Margin = new Padding(5, 4, 5, 4);
            hightPicture.Name = "hightPicture";
            hightPicture.Size = new Size(275, 223);
            hightPicture.TabIndex = 1;
            hightPicture.TabStop = false;
            // 
            // rowsColsPicture
            // 
            rowsColsPicture.Dock = DockStyle.Fill;
            rowsColsPicture.Location = new Point(290, 697);
            rowsColsPicture.Margin = new Padding(5, 4, 5, 4);
            rowsColsPicture.Name = "rowsColsPicture";
            rowsColsPicture.Size = new Size(275, 223);
            rowsColsPicture.TabIndex = 1;
            rowsColsPicture.TabStop = false;
            // 
            // volumePicture
            // 
            volumePicture.Dock = DockStyle.Fill;
            volumePicture.Location = new Point(290, 928);
            volumePicture.Margin = new Padding(5, 4, 5, 4);
            volumePicture.Name = "volumePicture";
            volumePicture.Size = new Size(275, 223);
            volumePicture.TabIndex = 1;
            volumePicture.TabStop = false;
            // 
            // tableLayoutPanel5
            // 
            tableLayoutPanel5.ColumnCount = 1;
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel5.Controls.Add(label19, 0, 0);
            tableLayoutPanel5.Controls.Add(RectangularRadioButton, 0, 2);
            tableLayoutPanel5.Controls.Add(CircularWellRadioButton, 0, 1);
            tableLayoutPanel5.Dock = DockStyle.Fill;
            tableLayoutPanel5.Location = new Point(573, 1159);
            tableLayoutPanel5.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel5.Name = "tableLayoutPanel5";
            tableLayoutPanel5.RowCount = 3;
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Percent, 40F));
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Percent, 40F));
            tableLayoutPanel5.Size = new Size(279, 223);
            tableLayoutPanel5.TabIndex = 12;
            // 
            // label19
            // 
            label19.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label19.AutoSize = true;
            label19.Location = new Point(5, 24);
            label19.Margin = new Padding(5, 0, 5, 0);
            label19.Name = "label19";
            label19.Size = new Size(110, 20);
            label19.TabIndex = 17;
            label19.Text = "Форма ячейки";
            // 
            // RectangularRadioButton
            // 
            RectangularRadioButton.Anchor = AnchorStyles.Left;
            RectangularRadioButton.AutoSize = true;
            RectangularRadioButton.Location = new Point(3, 166);
            RectangularRadioButton.Margin = new Padding(3, 4, 3, 4);
            RectangularRadioButton.Name = "RectangularRadioButton";
            RectangularRadioButton.Size = new Size(141, 24);
            RectangularRadioButton.TabIndex = 17;
            RectangularRadioButton.Text = "Прямоугольная";
            RectangularRadioButton.TextAlign = ContentAlignment.MiddleCenter;
            RectangularRadioButton.UseVisualStyleBackColor = true;
            RectangularRadioButton.CheckedChanged += CircularWellRadioButton_CheckedChanged;
            RectangularRadioButton.Click += PlateParamChangedMethod;
            // 
            // CircularWellRadioButton
            // 
            CircularWellRadioButton.Anchor = AnchorStyles.Left;
            CircularWellRadioButton.AutoSize = true;
            CircularWellRadioButton.Checked = true;
            CircularWellRadioButton.Location = new Point(3, 76);
            CircularWellRadioButton.Margin = new Padding(3, 4, 3, 4);
            CircularWellRadioButton.Name = "CircularWellRadioButton";
            CircularWellRadioButton.Size = new Size(85, 24);
            CircularWellRadioButton.TabIndex = 18;
            CircularWellRadioButton.TabStop = true;
            CircularWellRadioButton.Text = "Круглая";
            CircularWellRadioButton.TextAlign = ContentAlignment.MiddleCenter;
            CircularWellRadioButton.UseVisualStyleBackColor = true;
            CircularWellRadioButton.CheckedChanged += CircularWellRadioButton_CheckedChanged;
            CircularWellRadioButton.Click += PlateParamChangedMethod;
            // 
            // tableLayoutPanel13
            // 
            tableLayoutPanel13.ColumnCount = 1;
            tableLayoutPanel13.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel13.Controls.Add(label9, 0, 0);
            tableLayoutPanel13.Controls.Add(depthBox, 0, 1);
            tableLayoutPanel13.Dock = DockStyle.Fill;
            tableLayoutPanel13.Location = new Point(573, 1852);
            tableLayoutPanel13.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel13.Name = "tableLayoutPanel13";
            tableLayoutPanel13.RowCount = 2;
            tableLayoutPanel13.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel13.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel13.Size = new Size(279, 223);
            tableLayoutPanel13.TabIndex = 18;
            // 
            // label9
            // 
            label9.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label9.AutoSize = true;
            label9.Location = new Point(5, 91);
            label9.Margin = new Padding(5, 0, 5, 0);
            label9.Name = "label9";
            label9.Size = new Size(66, 20);
            label9.TabIndex = 16;
            label9.Text = "Глубина";
            // 
            // depthBox
            // 
            depthBox.DecimalPlaces = 1;
            depthBox.Location = new Point(5, 115);
            depthBox.Margin = new Padding(5, 4, 5, 4);
            depthBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            depthBox.Name = "depthBox";
            depthBox.Size = new Size(171, 27);
            depthBox.TabIndex = 8;
            // 
            // tableLayoutPanel11
            // 
            tableLayoutPanel11.ColumnCount = 1;
            tableLayoutPanel11.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel11.Controls.Add(volumeBox, 0, 1);
            tableLayoutPanel11.Controls.Add(label7, 0, 0);
            tableLayoutPanel11.Dock = DockStyle.Fill;
            tableLayoutPanel11.Location = new Point(573, 928);
            tableLayoutPanel11.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel11.Name = "tableLayoutPanel11";
            tableLayoutPanel11.RowCount = 2;
            tableLayoutPanel11.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel11.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel11.Size = new Size(279, 223);
            tableLayoutPanel11.TabIndex = 8;
            // 
            // volumeBox
            // 
            volumeBox.Location = new Point(5, 115);
            volumeBox.Margin = new Padding(5, 4, 5, 4);
            volumeBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            volumeBox.Name = "volumeBox";
            volumeBox.Size = new Size(171, 27);
            volumeBox.TabIndex = 6;
            volumeBox.ValueChanged += PlateParamChangedMethod;
            // 
            // label7
            // 
            label7.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label7.AutoSize = true;
            label7.Location = new Point(5, 91);
            label7.Margin = new Padding(5, 0, 5, 0);
            label7.Name = "label7";
            label7.Size = new Size(150, 20);
            label7.TabIndex = 16;
            label7.Text = "Объём ячейки (мкл)";
            // 
            // tableLayoutPanel9
            // 
            tableLayoutPanel9.ColumnCount = 1;
            tableLayoutPanel9.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel9.Controls.Add(colsBox, 0, 3);
            tableLayoutPanel9.Controls.Add(label6, 0, 0);
            tableLayoutPanel9.Controls.Add(label5, 0, 2);
            tableLayoutPanel9.Controls.Add(rowsBox, 0, 1);
            tableLayoutPanel9.Dock = DockStyle.Fill;
            tableLayoutPanel9.Location = new Point(573, 697);
            tableLayoutPanel9.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel9.Name = "tableLayoutPanel9";
            tableLayoutPanel9.RowCount = 4;
            tableLayoutPanel9.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel9.RowStyles.Add(new RowStyle(SizeType.Percent, 33.3333321F));
            tableLayoutPanel9.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel9.RowStyles.Add(new RowStyle(SizeType.Percent, 33.3333321F));
            tableLayoutPanel9.Size = new Size(279, 223);
            tableLayoutPanel9.TabIndex = 4;
            // 
            // colsBox
            // 
            colsBox.Location = new Point(5, 152);
            colsBox.Margin = new Padding(5, 4, 5, 4);
            colsBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            colsBox.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            colsBox.Name = "colsBox";
            colsBox.Size = new Size(171, 27);
            colsBox.TabIndex = 5;
            colsBox.Value = new decimal(new int[] { 1, 0, 0, 0 });
            colsBox.ValueChanged += PlateParamChangedMethod;
            // 
            // label6
            // 
            label6.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label6.AutoSize = true;
            label6.Location = new Point(5, 17);
            label6.Margin = new Padding(5, 0, 5, 0);
            label6.Name = "label6";
            label6.Size = new Size(132, 20);
            label6.TabIndex = 10;
            label6.Text = "Количество строк";
            // 
            // label5
            // 
            label5.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label5.AutoSize = true;
            label5.Location = new Point(5, 128);
            label5.Margin = new Padding(5, 0, 5, 0);
            label5.Name = "label5";
            label5.Size = new Size(159, 20);
            label5.TabIndex = 12;
            label5.Text = "Количество столбцов";
            // 
            // rowsBox
            // 
            rowsBox.Location = new Point(5, 41);
            rowsBox.Margin = new Padding(5, 4, 5, 4);
            rowsBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            rowsBox.Minimum = new decimal(new int[] { 1, 0, 0, 0 });
            rowsBox.Name = "rowsBox";
            rowsBox.Size = new Size(171, 27);
            rowsBox.TabIndex = 4;
            rowsBox.Value = new decimal(new int[] { 1, 0, 0, 0 });
            rowsBox.ValueChanged += PlateParamChangedMethod;
            // 
            // tableLayoutPanel8
            // 
            tableLayoutPanel8.ColumnCount = 1;
            tableLayoutPanel8.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel8.Controls.Add(heightBox, 0, 1);
            tableLayoutPanel8.Controls.Add(label8, 0, 0);
            tableLayoutPanel8.Dock = DockStyle.Fill;
            tableLayoutPanel8.Location = new Point(573, 466);
            tableLayoutPanel8.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel8.Name = "tableLayoutPanel8";
            tableLayoutPanel8.RowCount = 2;
            tableLayoutPanel8.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel8.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel8.Size = new Size(279, 223);
            tableLayoutPanel8.TabIndex = 4;
            // 
            // heightBox
            // 
            heightBox.DecimalPlaces = 1;
            heightBox.Location = new Point(5, 115);
            heightBox.Margin = new Padding(5, 4, 5, 4);
            heightBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            heightBox.Name = "heightBox";
            heightBox.Size = new Size(171, 27);
            heightBox.TabIndex = 3;
            heightBox.Value = new decimal(new int[] { 20, 0, 0, 0 });
            // 
            // label8
            // 
            label8.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label8.AutoSize = true;
            label8.Location = new Point(5, 91);
            label8.Margin = new Padding(5, 0, 5, 0);
            label8.Name = "label8";
            label8.Size = new Size(95, 20);
            label8.TabIndex = 14;
            label8.Text = "Высота (мм)";
            // 
            // tableLayoutPanel7
            // 
            tableLayoutPanel7.ColumnCount = 1;
            tableLayoutPanel7.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel7.Controls.Add(widthBox, 0, 3);
            tableLayoutPanel7.Controls.Add(label1, 0, 0);
            tableLayoutPanel7.Controls.Add(label2, 0, 2);
            tableLayoutPanel7.Controls.Add(lenghtBox, 0, 1);
            tableLayoutPanel7.Dock = DockStyle.Fill;
            tableLayoutPanel7.Location = new Point(573, 235);
            tableLayoutPanel7.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel7.Name = "tableLayoutPanel7";
            tableLayoutPanel7.RowCount = 4;
            tableLayoutPanel7.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel7.RowStyles.Add(new RowStyle(SizeType.Percent, 33.3333321F));
            tableLayoutPanel7.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel7.RowStyles.Add(new RowStyle(SizeType.Percent, 33.3333321F));
            tableLayoutPanel7.Size = new Size(279, 223);
            tableLayoutPanel7.TabIndex = 4;
            // 
            // widthBox
            // 
            widthBox.DecimalPlaces = 1;
            widthBox.Location = new Point(5, 152);
            widthBox.Margin = new Padding(5, 4, 5, 4);
            widthBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            widthBox.Name = "widthBox";
            widthBox.Size = new Size(171, 27);
            widthBox.TabIndex = 2;
            widthBox.Value = new decimal(new int[] { 855, 0, 0, 65536 });
            widthBox.ValueChanged += PlateParamChangedMethod;
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label1.AutoSize = true;
            label1.Location = new Point(5, 17);
            label1.Margin = new Padding(5, 0, 5, 0);
            label1.Name = "label1";
            label1.Size = new Size(89, 20);
            label1.TabIndex = 2;
            label1.Text = "Длина (мм)";
            // 
            // label2
            // 
            label2.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label2.AutoSize = true;
            label2.Location = new Point(5, 128);
            label2.Margin = new Padding(5, 0, 5, 0);
            label2.Name = "label2";
            label2.Size = new Size(103, 20);
            label2.TabIndex = 4;
            label2.Text = "Ширина (мм)";
            // 
            // lenghtBox
            // 
            lenghtBox.DecimalPlaces = 1;
            lenghtBox.Location = new Point(5, 41);
            lenghtBox.Margin = new Padding(5, 4, 5, 4);
            lenghtBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            lenghtBox.Name = "lenghtBox";
            lenghtBox.Size = new Size(171, 27);
            lenghtBox.TabIndex = 1;
            lenghtBox.Value = new decimal(new int[] { 1278, 0, 0, 65536 });
            lenghtBox.ValueChanged += PlateParamChangedMethod;
            // 
            // tableLayoutPanel21
            // 
            tableLayoutPanel21.ColumnCount = 1;
            tableLayoutPanel21.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel21.Controls.Add(label23, 0, 2);
            tableLayoutPanel21.Controls.Add(label24, 0, 0);
            tableLayoutPanel21.Controls.Add(yOffsetBox, 0, 3);
            tableLayoutPanel21.Controls.Add(xOffsetBox, 0, 1);
            tableLayoutPanel21.Dock = DockStyle.Fill;
            tableLayoutPanel21.Location = new Point(573, 2314);
            tableLayoutPanel21.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel21.Name = "tableLayoutPanel21";
            tableLayoutPanel21.RowCount = 4;
            tableLayoutPanel21.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel21.RowStyles.Add(new RowStyle(SizeType.Percent, 33.3333321F));
            tableLayoutPanel21.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel21.RowStyles.Add(new RowStyle(SizeType.Percent, 33.3333321F));
            tableLayoutPanel21.Size = new Size(279, 257);
            tableLayoutPanel21.TabIndex = 21;
            // 
            // label23
            // 
            label23.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label23.AutoSize = true;
            label23.Location = new Point(5, 149);
            label23.Margin = new Padding(5, 0, 5, 0);
            label23.Name = "label23";
            label23.Size = new Size(147, 20);
            label23.TabIndex = 16;
            label23.Text = "Интервал в ширину";
            // 
            // label24
            // 
            label24.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label24.AutoSize = true;
            label24.Location = new Point(5, 22);
            label24.Margin = new Padding(5, 0, 5, 0);
            label24.Name = "label24";
            label24.Size = new Size(133, 20);
            label24.TabIndex = 15;
            label24.Text = "Интервал в длину";
            // 
            // yOffsetBox
            // 
            yOffsetBox.DecimalPlaces = 1;
            yOffsetBox.Location = new Point(5, 173);
            yOffsetBox.Margin = new Padding(5, 4, 5, 4);
            yOffsetBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            yOffsetBox.Name = "yOffsetBox";
            yOffsetBox.Size = new Size(171, 27);
            yOffsetBox.TabIndex = 10;
            yOffsetBox.Value = new decimal(new int[] { 10, 0, 0, 0 });
            yOffsetBox.ValueChanged += PlateParamChangedMethod;
            // 
            // xOffsetBox
            // 
            xOffsetBox.DecimalPlaces = 1;
            xOffsetBox.Location = new Point(5, 46);
            xOffsetBox.Margin = new Padding(5, 4, 5, 4);
            xOffsetBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            xOffsetBox.Name = "xOffsetBox";
            xOffsetBox.Size = new Size(171, 27);
            xOffsetBox.TabIndex = 9;
            xOffsetBox.Value = new decimal(new int[] { 10, 0, 0, 0 });
            xOffsetBox.ValueChanged += PlateParamChangedMethod;
            // 
            // WellSizeTableLayoutPanel
            // 
            WellSizeTableLayoutPanel.ColumnCount = 1;
            WellSizeTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            WellSizeTableLayoutPanel.Controls.Add(tableLayoutPanel18, 0, 1);
            WellSizeTableLayoutPanel.Controls.Add(tableLayoutPanel19, 0, 0);
            WellSizeTableLayoutPanel.Dock = DockStyle.Fill;
            WellSizeTableLayoutPanel.Location = new Point(573, 1390);
            WellSizeTableLayoutPanel.Margin = new Padding(3, 4, 3, 4);
            WellSizeTableLayoutPanel.Name = "WellSizeTableLayoutPanel";
            WellSizeTableLayoutPanel.RowCount = 2;
            WellSizeTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            WellSizeTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            WellSizeTableLayoutPanel.Size = new Size(279, 223);
            WellSizeTableLayoutPanel.TabIndex = 10;
            // 
            // tableLayoutPanel18
            // 
            tableLayoutPanel18.ColumnCount = 1;
            tableLayoutPanel18.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel18.Controls.Add(wellYBox, 0, 3);
            tableLayoutPanel18.Controls.Add(label13, 0, 0);
            tableLayoutPanel18.Controls.Add(label14, 0, 2);
            tableLayoutPanel18.Controls.Add(wellXBox, 0, 1);
            tableLayoutPanel18.Dock = DockStyle.Fill;
            tableLayoutPanel18.Location = new Point(3, 115);
            tableLayoutPanel18.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel18.Name = "tableLayoutPanel18";
            tableLayoutPanel18.RowCount = 4;
            tableLayoutPanel18.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel18.RowStyles.Add(new RowStyle(SizeType.Percent, 33.3333321F));
            tableLayoutPanel18.RowStyles.Add(new RowStyle(SizeType.Percent, 16.666666F));
            tableLayoutPanel18.RowStyles.Add(new RowStyle(SizeType.Percent, 33.3333321F));
            tableLayoutPanel18.Size = new Size(273, 104);
            tableLayoutPanel18.TabIndex = 5;
            // 
            // wellYBox
            // 
            wellYBox.DecimalPlaces = 1;
            wellYBox.Location = new Point(5, 72);
            wellYBox.Margin = new Padding(5, 4, 5, 4);
            wellYBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            wellYBox.Name = "wellYBox";
            wellYBox.Size = new Size(168, 27);
            wellYBox.TabIndex = 2;
            wellYBox.Value = new decimal(new int[] { 855, 0, 0, 65536 });
            wellYBox.ValueChanged += PlateParamChangedMethod;
            // 
            // label13
            // 
            label13.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label13.AutoSize = true;
            label13.Location = new Point(5, 0);
            label13.Margin = new Padding(5, 0, 5, 0);
            label13.Name = "label13";
            label13.Size = new Size(142, 17);
            label13.TabIndex = 2;
            label13.Text = "Длина ячейки (мм)";
            // 
            // label14
            // 
            label14.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label14.AutoSize = true;
            label14.Location = new Point(5, 51);
            label14.Margin = new Padding(5, 0, 5, 0);
            label14.Name = "label14";
            label14.Size = new Size(156, 17);
            label14.TabIndex = 4;
            label14.Text = "Ширина ячейки (мм)";
            // 
            // wellXBox
            // 
            wellXBox.DecimalPlaces = 1;
            wellXBox.Location = new Point(5, 21);
            wellXBox.Margin = new Padding(5, 4, 5, 4);
            wellXBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            wellXBox.Name = "wellXBox";
            wellXBox.Size = new Size(168, 27);
            wellXBox.TabIndex = 1;
            wellXBox.Value = new decimal(new int[] { 1278, 0, 0, 65536 });
            wellXBox.ValueChanged += PlateParamChangedMethod;
            // 
            // tableLayoutPanel19
            // 
            tableLayoutPanel19.ColumnCount = 1;
            tableLayoutPanel19.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel19.Controls.Add(label20, 0, 0);
            tableLayoutPanel19.Controls.Add(diameterBox, 0, 1);
            tableLayoutPanel19.Dock = DockStyle.Fill;
            tableLayoutPanel19.Location = new Point(3, 4);
            tableLayoutPanel19.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel19.Name = "tableLayoutPanel19";
            tableLayoutPanel19.RowCount = 2;
            tableLayoutPanel19.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel19.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel19.Size = new Size(273, 103);
            tableLayoutPanel19.TabIndex = 11;
            // 
            // label20
            // 
            label20.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label20.AutoSize = true;
            label20.Location = new Point(5, 31);
            label20.Margin = new Padding(5, 0, 5, 0);
            label20.Name = "label20";
            label20.Size = new Size(70, 20);
            label20.TabIndex = 16;
            label20.Text = "Диаметр";
            // 
            // diameterBox
            // 
            diameterBox.DecimalPlaces = 1;
            diameterBox.Location = new Point(5, 55);
            diameterBox.Margin = new Padding(5, 4, 5, 4);
            diameterBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            diameterBox.Name = "diameterBox";
            diameterBox.Size = new Size(168, 27);
            diameterBox.TabIndex = 7;
            diameterBox.Value = new decimal(new int[] { 5, 0, 0, 0 });
            diameterBox.ValueChanged += PlateParamChangedMethod;
            // 
            // tableLayoutPanel17
            // 
            tableLayoutPanel17.ColumnCount = 1;
            tableLayoutPanel17.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel17.Controls.Add(label17, 0, 0);
            tableLayoutPanel17.Controls.Add(VBottomRadioButton, 0, 3);
            tableLayoutPanel17.Controls.Add(RoundBottomRadioButton, 0, 2);
            tableLayoutPanel17.Controls.Add(FlatBottomRadioButton, 0, 1);
            tableLayoutPanel17.Dock = DockStyle.Fill;
            tableLayoutPanel17.Location = new Point(573, 1621);
            tableLayoutPanel17.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel17.Name = "tableLayoutPanel17";
            tableLayoutPanel17.RowCount = 4;
            tableLayoutPanel17.RowStyles.Add(new RowStyle(SizeType.Percent, 20.4081631F));
            tableLayoutPanel17.RowStyles.Add(new RowStyle(SizeType.Percent, 26.5306129F));
            tableLayoutPanel17.RowStyles.Add(new RowStyle(SizeType.Percent, 26.5306129F));
            tableLayoutPanel17.RowStyles.Add(new RowStyle(SizeType.Percent, 26.5306129F));
            tableLayoutPanel17.Size = new Size(279, 223);
            tableLayoutPanel17.TabIndex = 13;
            // 
            // label17
            // 
            label17.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label17.AutoSize = true;
            label17.Location = new Point(5, 25);
            label17.Margin = new Padding(5, 0, 5, 0);
            label17.Name = "label17";
            label17.Size = new Size(139, 20);
            label17.TabIndex = 15;
            label17.Text = "Форма дна ячейки";
            // 
            // VBottomRadioButton
            // 
            VBottomRadioButton.Anchor = AnchorStyles.Left;
            VBottomRadioButton.AutoSize = true;
            VBottomRadioButton.Location = new Point(3, 181);
            VBottomRadioButton.Margin = new Padding(3, 4, 3, 4);
            VBottomRadioButton.Name = "VBottomRadioButton";
            VBottomRadioButton.Size = new Size(103, 24);
            VBottomRadioButton.TabIndex = 14;
            VBottomRadioButton.Text = "В форме V";
            VBottomRadioButton.TextAlign = ContentAlignment.MiddleCenter;
            VBottomRadioButton.UseVisualStyleBackColor = true;
            VBottomRadioButton.CheckedChanged += updateWellBottomShape;
            // 
            // RoundBottomRadioButton
            // 
            RoundBottomRadioButton.Anchor = AnchorStyles.Left;
            RoundBottomRadioButton.AutoSize = true;
            RoundBottomRadioButton.Location = new Point(3, 121);
            RoundBottomRadioButton.Margin = new Padding(3, 4, 3, 4);
            RoundBottomRadioButton.Name = "RoundBottomRadioButton";
            RoundBottomRadioButton.Size = new Size(98, 24);
            RoundBottomRadioButton.TabIndex = 15;
            RoundBottomRadioButton.Text = "Округлый";
            RoundBottomRadioButton.TextAlign = ContentAlignment.MiddleCenter;
            RoundBottomRadioButton.UseVisualStyleBackColor = true;
            RoundBottomRadioButton.CheckedChanged += updateWellBottomShape;
            // 
            // FlatBottomRadioButton
            // 
            FlatBottomRadioButton.Anchor = AnchorStyles.Left;
            FlatBottomRadioButton.AutoSize = true;
            FlatBottomRadioButton.Checked = true;
            FlatBottomRadioButton.Location = new Point(3, 62);
            FlatBottomRadioButton.Margin = new Padding(3, 4, 3, 4);
            FlatBottomRadioButton.Name = "FlatBottomRadioButton";
            FlatBottomRadioButton.Size = new Size(90, 24);
            FlatBottomRadioButton.TabIndex = 16;
            FlatBottomRadioButton.TabStop = true;
            FlatBottomRadioButton.Text = "Плоский";
            FlatBottomRadioButton.TextAlign = ContentAlignment.MiddleCenter;
            FlatBottomRadioButton.UseVisualStyleBackColor = true;
            FlatBottomRadioButton.CheckedChanged += updateWellBottomShape;
            // 
            // tableLayoutPanel15
            // 
            tableLayoutPanel15.ColumnCount = 1;
            tableLayoutPanel15.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel15.Controls.Add(label3, 0, 0);
            tableLayoutPanel15.Location = new Point(3, 235);
            tableLayoutPanel15.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel15.Name = "tableLayoutPanel15";
            tableLayoutPanel15.RowCount = 2;
            tableLayoutPanel15.RowStyles.Add(new RowStyle(SizeType.Percent, 20F));
            tableLayoutPanel15.RowStyles.Add(new RowStyle(SizeType.Percent, 80F));
            tableLayoutPanel15.Size = new Size(257, 223);
            tableLayoutPanel15.TabIndex = 2;
            // 
            // label3
            // 
            label3.Anchor = AnchorStyles.None;
            label3.AutoSize = true;
            label3.Location = new Point(67, 12);
            label3.Name = "label3";
            label3.Size = new Size(123, 20);
            label3.TabIndex = 0;
            label3.Text = "Общая площадь";
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Controls.Add(label10, 0, 0);
            tableLayoutPanel1.Controls.Add(nameBox, 0, 1);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(573, 4);
            tableLayoutPanel1.Margin = new Padding(3, 4, 3, 4);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Size = new Size(279, 223);
            tableLayoutPanel1.TabIndex = 22;
            // 
            // label10
            // 
            label10.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label10.AutoSize = true;
            label10.Location = new Point(5, 91);
            label10.Margin = new Padding(5, 0, 5, 0);
            label10.Name = "label10";
            label10.Size = new Size(77, 20);
            label10.TabIndex = 3;
            label10.Text = "Название";
            // 
            // nameBox
            // 
            nameBox.Location = new Point(3, 115);
            nameBox.Margin = new Padding(3, 4, 3, 4);
            nameBox.Name = "nameBox";
            nameBox.Size = new Size(271, 27);
            nameBox.TabIndex = 4;
            // 
            // PlateParamsMenu
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoSize = true;
            Controls.Add(tableLayoutPanel3);
            Margin = new Padding(3, 4, 3, 4);
            Name = "PlateParamsMenu";
            Size = new Size(914, 2575);
            tableLayoutPanel3.ResumeLayout(false);
            tableLayoutPanel2.ResumeLayout(false);
            tableLayoutPanel2.PerformLayout();
            tableLayoutPanel24.ResumeLayout(false);
            tableLayoutPanel24.PerformLayout();
            tableLayoutPanel28.ResumeLayout(false);
            tableLayoutPanel28.PerformLayout();
            tableLayoutPanel30.ResumeLayout(false);
            tableLayoutPanel30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)diameterPicture).EndInit();
            tableLayoutPanel31.ResumeLayout(false);
            tableLayoutPanel31.PerformLayout();
            tableLayoutPanel32.ResumeLayout(false);
            tableLayoutPanel32.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)offsetPicture).EndInit();
            ((System.ComponentModel.ISupportInitialize)spacePicture).EndInit();
            ((System.ComponentModel.ISupportInitialize)depthPicture).EndInit();
            tableLayoutPanel14.ResumeLayout(false);
            tableLayoutPanel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)ySpaceBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)xSpaceBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)sizesPicture).EndInit();
            ((System.ComponentModel.ISupportInitialize)hightPicture).EndInit();
            ((System.ComponentModel.ISupportInitialize)rowsColsPicture).EndInit();
            ((System.ComponentModel.ISupportInitialize)volumePicture).EndInit();
            tableLayoutPanel5.ResumeLayout(false);
            tableLayoutPanel5.PerformLayout();
            tableLayoutPanel13.ResumeLayout(false);
            tableLayoutPanel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)depthBox).EndInit();
            tableLayoutPanel11.ResumeLayout(false);
            tableLayoutPanel11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)volumeBox).EndInit();
            tableLayoutPanel9.ResumeLayout(false);
            tableLayoutPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)colsBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)rowsBox).EndInit();
            tableLayoutPanel8.ResumeLayout(false);
            tableLayoutPanel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)heightBox).EndInit();
            tableLayoutPanel7.ResumeLayout(false);
            tableLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)widthBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)lenghtBox).EndInit();
            tableLayoutPanel21.ResumeLayout(false);
            tableLayoutPanel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)yOffsetBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)xOffsetBox).EndInit();
            WellSizeTableLayoutPanel.ResumeLayout(false);
            tableLayoutPanel18.ResumeLayout(false);
            tableLayoutPanel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)wellYBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)wellXBox).EndInit();
            tableLayoutPanel19.ResumeLayout(false);
            tableLayoutPanel19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)diameterBox).EndInit();
            tableLayoutPanel17.ResumeLayout(false);
            tableLayoutPanel17.PerformLayout();
            tableLayoutPanel15.ResumeLayout(false);
            tableLayoutPanel15.PerformLayout();
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private TableLayoutPanel tableLayoutPanel3;
        private TableLayoutPanel tableLayoutPanel24;
        private Label label18;
        private TableLayoutPanel tableLayoutPanel28;
        private Label label32;
        private TableLayoutPanel tableLayoutPanel30;
        private Label label34;
        private PictureBox diameterPicture;
        private TableLayoutPanel tableLayoutPanel31;
        private Label label36;
        private TableLayoutPanel tableLayoutPanel32;
        private Label label38;
        private PictureBox offsetPicture;
        private PictureBox spacePicture;
        private PictureBox depthPicture;
        private TableLayoutPanel tableLayoutPanel14;
        private Label label12;
        private Label label4;
        private NumericUpDown ySpaceBox;
        private NumericUpDown xSpaceBox;
        private PictureBox sizesPicture;
        private PictureBox hightPicture;
        private PictureBox rowsColsPicture;
        private PictureBox volumePicture;
        private TableLayoutPanel tableLayoutPanel5;
        private Label label19;
        private RadioButton RectangularRadioButton;
        private RadioButton CircularWellRadioButton;
        private TableLayoutPanel tableLayoutPanel13;
        private Label label9;
        private NumericUpDown depthBox;
        private TableLayoutPanel tableLayoutPanel11;
        private NumericUpDown volumeBox;
        private Label label7;
        private TableLayoutPanel tableLayoutPanel9;
        private NumericUpDown colsBox;
        private Label label6;
        private Label label5;
        private NumericUpDown rowsBox;
        private TableLayoutPanel tableLayoutPanel8;
        private NumericUpDown heightBox;
        private Label label8;
        private TableLayoutPanel tableLayoutPanel7;
        private NumericUpDown widthBox;
        private Label label1;
        private Label label2;
        private NumericUpDown lenghtBox;
        private TableLayoutPanel tableLayoutPanel21;
        private Label label23;
        private Label label24;
        private NumericUpDown yOffsetBox;
        private NumericUpDown xOffsetBox;
        private TableLayoutPanel WellSizeTableLayoutPanel;
        private TableLayoutPanel tableLayoutPanel18;
        private NumericUpDown wellYBox;
        private Label label13;
        private Label label14;
        private NumericUpDown wellXBox;
        private TableLayoutPanel tableLayoutPanel19;
        private Label label20;
        private NumericUpDown diameterBox;
        private TableLayoutPanel tableLayoutPanel17;
        private Label label17;
        private RadioButton VBottomRadioButton;
        private RadioButton RoundBottomRadioButton;
        private RadioButton FlatBottomRadioButton;
        private TableLayoutPanel tableLayoutPanel15;
        private Label label3;
        private TableLayoutPanel tableLayoutPanel1;
        private Label label10;
        private TextBox nameBox;
        private TableLayoutPanel tableLayoutPanel2;
        private Label label11;
    }
}
