﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using ChemRobotDesktop.Classes;
using Newtonsoft.Json;

namespace ChemRobotDesktop.CustomControls
{
    public partial class PlatesList : UserControl
    {

        bool tipRackVisible = true;
        bool wellPlateVisible = true;
        bool ReservoirVisible = true;
        bool dropVisible = true;
        int rowHeight = 100;
        Size toolTipSize = new Size(500, 250);
        bool toolTipShown = false;
        TableLayoutPanel tipRackItemsTable = new TableLayoutPanel();
        TableLayoutPanel wellPlateItemsTable = new TableLayoutPanel();
        TableLayoutPanel reservoirItemsTable = new TableLayoutPanel();
        TableLayoutPanel dropItemsTable = new TableLayoutPanel();
        public Dictionary<int, Plate> plates = new Dictionary<int, Plate>();
        public delegate void PlateSelected(int? id);
        public delegate void PlateSelectionWindowShowed();
        public event PlateSelected plateSelected;
        public event PlateSelectionWindowShowed plateSelectionWindowShowed;


        public PlatesList()
        {
            InitializeComponent();
            //fillTipRackTabel();
            //fillWellPlateTabel();
            this.Size = new Size(350, 500);
            tipRackItemsTable.Dock = DockStyle.Fill;
            wellPlateItemsTable.Dock = DockStyle.Fill;
            reservoirItemsTable.Dock = DockStyle.Fill;
            dropItemsTable.Dock = DockStyle.Fill;
            tipRackTable.Controls.Add(tipRackItemsTable, 0, 1);
            wellPlateTable.Controls.Add(wellPlateItemsTable, 0, 1);
            reservoirPlateTable.Controls.Add(reservoirItemsTable, 0, 1);
            dropPlateTable.Controls.Add(dropItemsTable, 0, 1);
            toolTip1.OwnerDraw = true;
            toolTip1.Draw += new DrawToolTipEventHandler(toolTip_Draw);
            fillPlatesList();


        }

        private void refreshTables()
        {
            fillTabelPlates(ref tipRackItemsTable, PLATE_TYPE.TIP_RACK);
            fillTabelPlates(ref wellPlateItemsTable, PLATE_TYPE.WELL_PLATE);
            fillTabelPlates(ref reservoirItemsTable, PLATE_TYPE.RESERVOIR);
            fillTabelPlates(ref dropItemsTable, PLATE_TYPE.DROP_PLACE);
            swapTableSize(tipRackTable, ref tipRackVisible);
            swapTableSize(wellPlateTable, ref wellPlateVisible);
            swapTableSize(reservoirPlateTable, ref ReservoirVisible);
            swapTableSize(dropPlateTable, ref dropVisible);

        }


        public void fillPlatesList()
        {
            //string labwareDirectory = "Labware";
            string labwareDirectory = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"Labware"));

            if (!Directory.Exists(labwareDirectory))
            {
                labwareDirectory = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), "../../../Labware"));
            }
            //string labwareDirectory = "C:\\GitLab\\chem-robot-desktop\\ChemRobotDesktop\\ChemRobotDesktop\\Labware"; // Change later
            plates.Clear();
            try
            {
                // Получаем все файлы с расширением .json в папке
                string[] files = Directory.GetFiles(labwareDirectory, "*.plate");

                foreach (var file in files)
                {
                    try
                    {
                        // Читаем содержимое файла и десериализуем его в объект Plate
                        string json = File.ReadAllText(file);
                        Plate plate = JsonConvert.DeserializeObject<Plate>(json);

                        if (plate != null && !plates.ContainsKey(plate.ModelId))
                        {
                            plate.FillWellPositions();
                            // Добавляем plate в словарь, используя ModelId как ключ
                            plates.Add(plate.ModelId, plate);
                        }
                    }
                    catch (JsonException jsonEx)
                    {
                        // Обработка ошибок десериализации
                        Console.WriteLine($"Ошибка при десериализации файла {file}: {jsonEx.Message}");
                    }
                    catch (IOException ioEx)
                    {
                        // Обработка ошибок ввода/вывода
                        Console.WriteLine($"Ошибка при чтении файла {file}: {ioEx.Message}");
                    }
                }
                refreshTables();
            }
            catch (DirectoryNotFoundException dirEx)
            {
                // Обработка ошибок, если папка не найдена
                Console.WriteLine($"Папка не найдена: {dirEx.Message}");
            }
        }

        public void fillTabelPlates(ref TableLayoutPanel table, PLATE_TYPE type)
        {
            table.Controls.Clear();
            int plateNum = plates.Count(p => p.Value.Type == type);
            table.ColumnCount = 1;
            table.RowCount = plateNum;
            table.Margin = new System.Windows.Forms.Padding(0);
            table.CellBorderStyle = TableLayoutPanelCellBorderStyle.None;

            int i = 0;
            foreach (var plateEntry in plates)
            {
                Plate plate = plateEntry.Value;
                if (plate.Type == type)
                {

                    Button button = new Button();
                    button.Tag = plate;
                    button.Dock = DockStyle.Fill;
                    button.Text = plate.Name;
                    button.Font = new Font("Segoe UI", 10f);
                    button.FlatStyle = FlatStyle.Flat;
                    button.Margin = new Padding(10, 5, 10, 5);
                    button.BackColor = SystemColors.ControlLight;
                    button.MouseLeave += new EventHandler(MouseLeaveHandler);
                    button.MouseMove += new MouseEventHandler(button_MouseMove);
                    button.Click += new EventHandler(plateSelectedMethod);
                    table.Controls.Add(button, 0, i);


                    if (table.RowStyles.Count <= i)
                    {
                        // Если нет, добавляем новый RowStyle
                        table.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
                    }
                    else
                    {
                        // Если существует, просто настраиваем его
                        table.RowStyles[i].SizeType = SizeType.Percent;
                        table.RowStyles[i].Height = 100;
                    }
                    i++;
                }
                table.ColumnStyles.Clear();
                table.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
            }
            table.Size = new Size(table.Width, rowHeight * plateNum);


        }



        private void swapTableSize(TableLayoutPanel tableLayoutPanel, ref bool visible)
        {

            Control control = tableLayoutPanel.GetControlFromPosition(0, 1);
            if (control is TableLayoutPanel)
            {
                TableLayoutPanel innerTableLayoutPanel = (TableLayoutPanel)control;

                // Получаем количество строк во вложенной таблице
                int rows = innerTableLayoutPanel.RowCount;

                if (visible)
                {
                    tableLayoutPanel.RowStyles[0].SizeType = SizeType.Absolute;
                    tableLayoutPanel.RowStyles[0].Height = rowHeight;


                    tableLayoutPanel.Size = new Size(tableLayoutPanel.Width, rowHeight);

                    for (int i = 1; i < tableLayoutPanel.RowCount; i++)
                    {
                        tableLayoutPanel.RowStyles[i].SizeType = SizeType.Absolute;
                        tableLayoutPanel.RowStyles[i].Height = 0;
                    }

                    visible = false;
                }
                else
                {

                    tableLayoutPanel.RowStyles[0].SizeType = SizeType.Absolute;
                    tableLayoutPanel.RowStyles[0].Height = rowHeight;

                    tableLayoutPanel.Size = new Size(tableLayoutPanel.Width, rowHeight + rowHeight * rows);
                    for (int i = 1; i < tableLayoutPanel.RowCount; i++)
                    {
                        tableLayoutPanel.RowStyles[i].SizeType = SizeType.Absolute;
                        tableLayoutPanel.RowStyles[i].Height = rowHeight;

                    }

                    visible = true;
                }
            }
        }

        protected void toolTip_Draw(object sender, DrawToolTipEventArgs e)
        {

            ToolTip toolTip = sender as ToolTip;
            if (toolTip != null)
            {
                Plate plate = (Plate)toolTip.Tag;

                Image _plateImage = plate.getImage();
                int margin = 10;
                Graphics g = e.Graphics;
                g.Clear(Color.White);
                g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

                // Устанавливаем шрифт для названия
                float titleFontSize = e.Bounds.Height * 0.1f; // Размер шрифта для названия
                float titleHeight;
                using (Font titleFont = new Font("Arial", titleFontSize, FontStyle.Regular, GraphicsUnit.Pixel))
                {
                    Brush textBrush = new SolidBrush(Color.Black);
                    StringFormat titleFormat = new StringFormat();
                    titleFormat.Trimming = StringTrimming.Word; // Обрезаем по словам
                    titleFormat.LineAlignment = StringAlignment.Near;
                    titleFormat.Alignment = StringAlignment.Near;
                    titleFormat.FormatFlags = StringFormatFlags.NoClip; // Разрешаем тексту переноситься на новые строки

                    // Область для названия с предполагаемым местом для переноса текста
                    RectangleF titleRect = new RectangleF(margin, margin, e.Bounds.Width - (2 * margin), e.Bounds.Height - margin);
                    SizeF titleSize = g.MeasureString(plate.Name, titleFont, titleRect.Size, titleFormat);

                    // Рисуем название
                    g.DrawString(plate.Name, titleFont, textBrush, titleRect, titleFormat);

                    // Обновляем высоту, занимаемую названием, с учетом переносов
                    titleHeight = titleSize.Height;
                }

                // Настройка отступов для изображения под названием
                float marginTopImage = margin + titleHeight + margin; // Отступ сверху для изображения

                // Размеры области изображения с учетом отступов
                float imageWidth = e.Bounds.Width * 0.6f - (0 * margin);
                float originalRatio = (float)_plateImage.Width / _plateImage.Height;
                float imageHeight = imageWidth / originalRatio;
                float scaledHeight = Math.Min(imageHeight, (e.Bounds.Height - marginTopImage - margin)); // Убедитесь, что изображение не выходит за пределы tooltip
                float scaledWidth = scaledHeight * originalRatio; // Сохраняем пропорции

                // Рисуем изображение plate
                if (_plateImage != null)
                {
                    RectangleF imageRect = new RectangleF(margin, marginTopImage, scaledWidth, scaledHeight);
                    g.DrawImage(_plateImage, imageRect);
                }

                // Устанавливаем шрифт для описания
                float labelFontSize = titleFontSize * 0.75f; // Размер шрифта для описания
                using (Font textFont = new Font("Arial", labelFontSize, FontStyle.Regular, GraphicsUnit.Pixel))
                {
                    Brush textBrush = new SolidBrush(Color.Black);
                    // Начало области текста справа от изображения
                    float textPosX = margin + scaledWidth + margin;
                    float textPosY = marginTopImage;

                    // Рисуем описание плиты
                    string description = "";
                    if (plate.Tips != null && plate.Tips.Count != 0)
                    {
                        description = $"Описание:\n\nКол-во носиков: {plate.Tips.Count}\nМаксимальный объём носика: {plate.Tips.ElementAt(0).Value.MaxVolume} мкл";
                    }
                    if (plate.Wells != null && plate.Wells.Count != 0)
                    {
                        description = $"Описание:\n\nКол-во ячеек: {plate.Wells.Count}\nОбъём ячейки: {plate.Wells.ElementAt(0).Value.MaxLiquidVolume} мкл\nОбщий объём: {plate.Wells.ElementAt(0).Value.MaxLiquidVolume * plate.Wells.Count} мкл";
                    }

                    RectangleF descriptionRect = new RectangleF(textPosX, textPosY, e.Bounds.Width - textPosX - margin, e.Bounds.Height - textPosY - margin);
                    g.DrawString(description, textFont, textBrush, descriptionRect);
                }
            }
        }

        private void toolTip_Popup(object sender, PopupEventArgs e)
        {
            e.ToolTipSize = toolTipSize;
        }


        private void tipRackLabel_Click(object sender, EventArgs e)
        {
            bool tmp = tipRackVisible;
            hideList();
            tipRackVisible = tmp;
            swapTableSize(tipRackTable, ref tipRackVisible);
        }

        private void wellPlateLabel_Click(object sender, EventArgs e)
        {
            bool tmp = wellPlateVisible;
            hideList();
            wellPlateVisible = tmp;
            swapTableSize(wellPlateTable, ref wellPlateVisible);
        }

        private void reservoirLable_Click(object sender, EventArgs e)
        {
            bool tmp = ReservoirVisible;
            hideList();
            ReservoirVisible = tmp;
            swapTableSize(reservoirPlateTable, ref ReservoirVisible);
        }
        private void dropPlaceLabel_Click(object sender, EventArgs e)
        {
            bool tmp = dropVisible;
            hideList();
            dropVisible = tmp;
            swapTableSize(dropPlateTable, ref dropVisible);
        }


        private void PlatesList_Load(object sender, EventArgs e)
        {

        }

        private void MouseEnterHandler(object sender, EventArgs e)
        {
            Button button = sender as Button;
            toolTip1.Tag = button.Tag;
            toolTip1.Show("test", tableLayoutPanel1, 100, 200);


            if (!toolTipShown)
            {
                toolTipShown = true;
                toolTip1.Show("test", tableLayoutPanel1, 100, 200);
            }


            else
            {
                toolTipShown = false;
                toolTip1.Hide(tableLayoutPanel1);

            }
        }
        private void button_MouseMove(object sender, MouseEventArgs e)
        {
            Button button = sender as Button;
            // e.Location уже в координатах клиентской области кнопки
            if (button.ClientRectangle.Contains(e.Location))
            {
                if (!toolTipShown)
                {
                    toolTipShown = true;
                    toolTip1.Tag = button.Tag;
                    toolTip1.Show("test", tableLayoutPanel1, 400, 200);
                    //popup.Show(tipData, PictureBox, e.Location.X, e.Location.Y + PictureBox.Height / 15);
                    //pictureBox.Invalidate();
                }
            }

            else
            {
                toolTipShown = false;
                toolTip1.Hide(tableLayoutPanel1);

            }
            // Курсор находится над кнопкой

        }




        private void MouseLeaveHandler(object sender, EventArgs e)
        {
            toolTipShown = false;
            toolTip1.Hide(tableLayoutPanel1);
        }

        private void button_MouseEnter(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                Plate plate = button.Tag as Plate; // Предполагаем, что Tag кнопки хранит объект Plate
                if (plate != null)
                {
                    toolTip1.Tag = plate; // Устанавливаем Tag для ToolTip
                    toolTip1.Show("test", tableLayoutPanel1); // Вызываем Show для активации события Draw
                }
            }
        }

        private void button_MouseLeave(object sender, EventArgs e)
        {
            toolTip1.Hide(tableLayoutPanel1);
            toolTip1.Tag = null; // Очищаем Tag
        }


        private void hideList()
        {
            tipRackVisible = true;
            wellPlateVisible = true;
            ReservoirVisible = true;
            dropVisible = true;
            tableLayoutPanel1.SuspendLayout();
            swapTableSize(tipRackTable, ref tipRackVisible);
            swapTableSize(wellPlateTable, ref wellPlateVisible);
            swapTableSize(reservoirPlateTable, ref ReservoirVisible);
            swapTableSize(dropPlateTable, ref dropVisible);
            tableLayoutPanel1.ResumeLayout();
        }


        public void showInWindow()
        {
            plateSelectionWindowShowed?.Invoke();
            hideList();
            Control control = this;
            Form tmpForm = new Form();
            tmpForm.ControlBox = false;
            tmpForm.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            tmpForm.StartPosition = FormStartPosition.CenterParent;
            tmpForm.BackColor = SystemColors.InactiveBorder;


            control.Show();

            control.Location = new Point(0, 0);
            //control.Margin = new Padding(20);
            int padding = 10;
            control.Anchor = AnchorStyles.Top;
            control.Padding = new Padding(padding, padding, 0, padding);
            tmpForm.ClientSize = control.Size;
            tmpForm.AutoSize = true;
            tmpForm.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            tmpForm.MaximumSize = control.Size + new Size(SystemInformation.VerticalScrollBarWidth * 1, 20);
            tmpForm.MinimumSize = control.Size + new Size(SystemInformation.VerticalScrollBarWidth * 1, 0);
            tmpForm.AutoScroll = true;

            tmpForm.FormClosed += (sender, e) => tmpForm.Controls.Remove(control);

            tmpForm.Controls.Add(control);
            //control.Dock = DockStyle.Bottom;


            tmpForm.FormClosed += (sender, e) =>
            {
                tmpForm.Controls.Remove(control);
            };

            tmpForm.Load += (sender, e) => tmpForm.BringToFront();

            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
            timer.Interval = 5; // период проверки в миллисекундах
            timer.Tick += (sender, e) =>
            {
                if (control.IsDisposed)
                {
                    tmpForm.Close();
                    timer.Stop();
                    return;
                }
                if (!control.Visible)
                {
                    tmpForm.Close();
                    tmpForm.Controls.Remove(control);
                    timer.Stop();
                }
            };
            timer.Start();
            //control.Hide();
            tmpForm.ShowDialog();
            timer.Dispose();

        }

        public void plateSelectedMethod(object sender, EventArgs e)
        {
            Button button = sender as Button;
            if (button != null)
            {
                if (button.Tag != null)
                {
                    plateSelected?.Invoke(((Plate)button.Tag).ModelId);
                }
                else
                {
                    plateSelected.Invoke(null);
                }
                this.Hide();
                tipRackVisible = true;
                wellPlateVisible = true;
                ReservoirVisible = true;
                dropVisible = true;
                swapTableSize(tipRackTable, ref tipRackVisible);
                swapTableSize(wellPlateTable, ref wellPlateVisible);
                swapTableSize(reservoirPlateTable, ref ReservoirVisible);
                swapTableSize(dropPlateTable, ref dropVisible);
            }
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
