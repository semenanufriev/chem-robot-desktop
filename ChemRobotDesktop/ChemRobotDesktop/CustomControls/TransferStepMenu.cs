﻿using ChemRobotDesktop.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Runtime.InteropServices.JavaScript.JSType;
using WellName = System.String;

namespace ChemRobotDesktop.CustomControls
{
    public partial class TransferStepMenu : UserControl
    {
        public delegate void Method(ProtocolStep protocolStep);
        public event Method StepCreatedEvent;
        public event Method StepDeletedEvent;
        public event Method StepEditCanceledEvent;
        private ProtocolStepTransfer protocolStep;
        private int stepId;
        private List<Zone> Zones;
        List<WellName> selectedSourceWells = new List<WellName>();
        List<WellName> selectedDestinationWells = new List<WellName>();

        public TransferStepMenu(int stepId, List<Zone> Zones, ProtocolStep step = null)
        {
            InitializeComponent();
            this.Zones = Zones;
            this.stepId = stepId;
            sourcePlateBox.Items.Clear();

            foreach (var zone in Zones)
            {
                if (zone.plate != null && (zone.plate.Type == PLATE_TYPE.WELL_PLATE || zone.plate.Type == PLATE_TYPE.RESERVOIR))
                {
                    // Создаем элемент ComboBox, который содержит название Plate и её objectId
                    sourcePlateBox.Items.Add(new KeyValuePair<int, string>(zone.plate.ObjectId, zone.plate.Name));
                    destinationPlateBox.Items.Add(new KeyValuePair<int, string>(zone.plate.ObjectId, zone.plate.Name));
                }
            }

            // Настройка ComboBox для отображения названий Plate
            sourcePlateBox.DisplayMember = "Value";
            sourcePlateBox.ValueMember = "Key";
            destinationPlateBox.DisplayMember = "Value";
            destinationPlateBox.ValueMember = "Key";

            tipChangeModeBox.Items.Add(new KeyValuePair<TIP_CHANGE_MODE, string>(TIP_CHANGE_MODE.ONCE_AT_START, "Один раз в начале этапа"));
            tipChangeModeBox.Items.Add(new KeyValuePair<TIP_CHANGE_MODE, string>(TIP_CHANGE_MODE.NEVER, "Никогда"));
            tipChangeModeBox.Items.Add(new KeyValuePair<TIP_CHANGE_MODE, string>(TIP_CHANGE_MODE.FOR_EVERY_WELL, "Для каждой ячейки"));
            tipChangeModeBox.DisplayMember = "Value";
            tipChangeModeBox.ValueMember = "Key";
            tipChangeModeBox.SelectedIndex = 0;
            fillParamsFromStep(step);
        }

        private void fillParamsFromStep(ProtocolStep step)
        {
            if (step is ProtocolStepTransfer transferStep)
            {
                // Устанавливаем выбранные значения для sourcePlateBox и destinationPlateBox
                sourcePlateBox.SelectedItem = sourcePlateBox.Items.Cast<KeyValuePair<int, string>>().FirstOrDefault(item => item.Key == transferStep.SourcePlateId);
                destinationPlateBox.SelectedItem = destinationPlateBox.Items.Cast<KeyValuePair<int, string>>().FirstOrDefault(item => item.Key == transferStep.DestinationPlateId);
                selectedSourceWells = transferStep.SourceWells.ToList();
                selectedDestinationWells = transferStep.DestinationWells.ToList();
                // Устанавливаем выбранное значение для tipChangeModeBox
                tipChangeModeBox.SelectedItem = tipChangeModeBox.Items.Cast<KeyValuePair<TIP_CHANGE_MODE, string>>().FirstOrDefault(item => item.Key == transferStep.TipChangeMode);

                // Устанавливаем объем переноса
                volumeBox.Value = (decimal)transferStep.TransferVolume;
                updateWellsButtonsText();
                stepId = transferStep.Id;
                this.protocolStep = transferStep;
            }
        }

        private void updateWellsButtonsText()
        {
            if (selectedSourceWells.Count > 0)
            {
                selectSourceWellsButton.Text = "Выберите ячейки (" + selectedSourceWells.Count.ToString() + ")";
            }
            else
            {
                selectSourceWellsButton.Text = "Выберите ячейки";
            }

            if (selectedDestinationWells.Count > 0)
            {
                selectDestinationWellsButton.Text = "Выберите ячейки (" + selectedDestinationWells.Count.ToString() + ")";
            }
            else
            {
                selectDestinationWellsButton.Text = "Выберите ячейки";
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            //  protocolStep = new  ProtocolStepTransfer(stepId, int sourcePlateId, int destinationPlateId, WellName sourceWell, List<WellName> destinationWells, Volume transferVolume, TIP_CHANGE_MODE tipChangeMode)
            int sourcePlateId = -1, destinationPlateId = -1;
            if (sourcePlateBox.SelectedItem is KeyValuePair<int, string> selectedSourcePair)
            {
                sourcePlateId = selectedSourcePair.Key;
            }

            if (destinationPlateBox.SelectedItem is KeyValuePair<int, string> selectedDestinationPair)
            {

                destinationPlateId = selectedDestinationPair.Key;
            }

            TIP_CHANGE_MODE tipChangeMode = TIP_CHANGE_MODE.NEVER;
            if (tipChangeModeBox.SelectedItem is KeyValuePair<TIP_CHANGE_MODE, string> tipChangeModePair)
            {

                tipChangeMode = tipChangeModePair.Key;
            }

            float volume = (float)volumeBox.Value;
            protocolStep = new ProtocolStepTransfer(stepId, sourcePlateId, destinationPlateId, selectedSourceWells, selectedDestinationWells, volume, tipChangeMode);
            try
            {
                protocolStep.ExecuteStep(Zones);
                StepCreatedEvent.Invoke(protocolStep);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {


            StepEditCanceledEvent.Invoke(protocolStep);
        }

        private void selectSourceWellsButton_Click(object sender, EventArgs e)
        {
            PlateWindow plateWindow = new PlateWindow();
            if (sourcePlateBox.SelectedItem is KeyValuePair<int, string> selectedPair)
            {

                int selectedPlateId = selectedPair.Key;
                foreach (var zone in Zones)
                {
                    if (zone.plate != null && zone.plate.ObjectId == selectedPlateId)
                    {
                        selectedSourceWells = plateWindow.showWellsSelectionMenu(zone.plate, selectedSourceWells);
                        while (selectedSourceWells != null && selectedSourceWells.Count > 1)
                        {
                            MessageBox.Show("На данный момент поддерживается только режим с одним источником.\nПожалуйста, выберите одну ячейку.");
                            selectedSourceWells = plateWindow.showWellsSelectionMenu(zone.plate, selectedSourceWells);

                        }
                        updateWellsButtonsText();
                        break;

                    }
                }
            }


        }

        private void selectDestinationWellsButton_Click(object sender, EventArgs e)
        {

            if (destinationPlateBox.SelectedItem is KeyValuePair<int, string> selectedPair)
            {

                int selectedPlateId = selectedPair.Key;
                foreach (var zone in Zones)
                {
                    if (zone.plate != null && zone.plate.ObjectId == selectedPlateId)
                    {
                        PlateWindow plateWindow = new PlateWindow();
                        selectedDestinationWells = plateWindow.showWellsSelectionMenu(zone.plate, selectedDestinationWells);

                        updateWellsButtonsText();
                        break;

                    }
                }
            }
        }

        private void deleteStepButton_Click(object sender, EventArgs e)
        {
            if (protocolStep != null)
            {
                StepDeletedEvent.Invoke(protocolStep);
            }
        }

        private void sourcePlateBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedSourceWells.Clear();
            updateWellsButtonsText();
        }

        private void destinationPlateBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            selectedDestinationWells.Clear();
            updateWellsButtonsText();
        }

        private void tipChangeModeBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
