﻿using ChemRobotDesktop.CustomControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChemRobotDesktop.Classes;
using WellName = System.String;
using System.Diagnostics.Contracts;
using System.Numerics;

namespace ChemRobotDesktop.CustomControls
{
    public partial class PlateControl : UserControl
    {


        public Plate plate = new Plate();
        private Point mouseDownLocation;
        private Point currentMouseLocation;
        private RectangleF selectionRect;
        private RectangleF realSelectionRect;
        private bool isSelecting = false;
        public List<WellName> SelectedWellNames = new List<WellName>();
        Size toolTipSize = new Size(500, 250);
        //ToolTip popup = new ToolTip();
        public PlateControl()
        {
            InitializeComponent();
            ToolTip toolTip1 = new ToolTip();
            toolTip1.OwnerDraw = true;
            //toolTip1.Draw += new DrawToolTipEventHandler(toolTip_Draw);
        }






        protected void toolTip_Draw(object sender, DrawToolTipEventArgs e)
        {
            Image _plateImage = plate.getImage();
            int margin = 10;
            Graphics g = e.Graphics;
            g.Clear(Color.White);
            g.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;

            // Устанавливаем шрифт для названия
            float titleFontSize = e.Bounds.Height * 0.1f; // Размер шрифта для названия
            float titleHeight;
            using (Font titleFont = new Font("Arial", titleFontSize, FontStyle.Regular, GraphicsUnit.Pixel))
            {
                Brush textBrush = new SolidBrush(Color.Black);
                StringFormat titleFormat = new StringFormat();
                titleFormat.Trimming = StringTrimming.Word; // Обрезаем по словам
                titleFormat.LineAlignment = StringAlignment.Near;
                titleFormat.Alignment = StringAlignment.Near;
                titleFormat.FormatFlags = StringFormatFlags.NoClip; // Разрешаем тексту переноситься на новые строки

                // Область для названия с предполагаемым местом для переноса текста
                RectangleF titleRect = new RectangleF(margin, margin, e.Bounds.Width - (2 * margin), e.Bounds.Height - margin);
                SizeF titleSize = g.MeasureString(plate.Name, titleFont, titleRect.Size, titleFormat);

                // Рисуем название
                g.DrawString(plate.Name, titleFont, textBrush, titleRect, titleFormat);

                // Обновляем высоту, занимаемую названием, с учетом переносов
                titleHeight = titleSize.Height;
            }

            // Настройка отступов для изображения под названием
            float marginTopImage = margin + titleHeight + margin; // Отступ сверху для изображения

            // Размеры области изображения с учетом отступов
            float imageWidth = e.Bounds.Width * 0.6f - (0 * margin);
            float originalRatio = (float)_plateImage.Width / _plateImage.Height;
            float imageHeight = imageWidth / originalRatio;
            float scaledHeight = Math.Min(imageHeight, (e.Bounds.Height - marginTopImage - margin)); // Убедитесь, что изображение не выходит за пределы tooltip
            float scaledWidth = scaledHeight * originalRatio; // Сохраняем пропорции

            // Рисуем изображение plate
            if (_plateImage != null)
            {
                RectangleF imageRect = new RectangleF(margin, marginTopImage, scaledWidth, scaledHeight);
                g.DrawImage(_plateImage, imageRect);
            }

            // Устанавливаем шрифт для описания
            float labelFontSize = titleFontSize * 0.75f; // Размер шрифта для описания
            using (Font textFont = new Font("Arial", labelFontSize, FontStyle.Regular, GraphicsUnit.Pixel))
            {
                Brush textBrush = new SolidBrush(Color.Black);
                // Начало области текста справа от изображения
                float textPosX = margin + scaledWidth + margin;
                float textPosY = marginTopImage;

                // Рисуем описание плиты
                string description = $"Details:\n\n# of wells: {plate.Wells.Count}\nWell Volume: {plate.Wells.ElementAt(0).Value.MaxLiquidVolume} мкл\nОбщий объём: {plate.Wells.ElementAt(0).Value.MaxLiquidVolume * plate.Wells.Count} μL";
                RectangleF descriptionRect = new RectangleF(textPosX, textPosY, e.Bounds.Width - textPosX - margin, e.Bounds.Height - textPosY - margin);
                g.DrawString(description, textFont, textBrush, descriptionRect);
            }
        }

        public void init(List<WellName> selectedWells = null)
        {

            PictureBox.Size = new Size(plate.ImageSize.X, plate.ImageSize.Y);
            //toolTipSize = new Size(plate.ImageSize.X, plate.ImageSize.Y);
            PictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            PictureBox.Image = plate.getImage();
            if (selectedWells != null)
            {
                this.SelectedWellNames = selectedWells;
                PictureBox.Image = plate.getImage(selectedWells = selectedWells);
            }
            float originalAspectRatio = (float)(PictureBox.Width) / (float)(PictureBox.Height);

            //PictureBox.Dock = DockStyle.Fill;





            PictureBox.Paint += new PaintEventHandler(pictureBoxPaintHandler);

            PictureBox.Paint += new PaintEventHandler(pictureBoxPaintHandler);
            PictureBox.MouseDown += new MouseEventHandler(MouseDownHandler);
            PictureBox.MouseMove += new MouseEventHandler(MouseMoveHandler);
            PictureBox.MouseUp += new MouseEventHandler(MouseUpHandler);
            PictureBox.MouseClick += new MouseEventHandler(MouseClickHandler);


        }

        private void MouseClickHandler(object sender, MouseEventArgs e)
        {

        }


        private void MouseDownHandler(object sender, MouseEventArgs e)
        {


            if (e.Button == MouseButtons.Left && Control.ModifierKeys == Keys.Control)
            {
                //FindClickedCell(e.Location);
                isSelecting = true;

            }



            else
            {
                if (e.Button == MouseButtons.Left)
                {

                    mouseDownLocation = e.Location;
                    selectionRect = new Rectangle(mouseDownLocation.X, mouseDownLocation.Y, 0, 0);
                }
            }
            //mouseDownLocation = e.Location;
        }
        private bool toolTipShown = false;
        private void MouseMoveHandler(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left && mouseDownLocation != Point.Empty)
            {

                Point currentMouseLocation = e.Location;

                int minX = Math.Min(mouseDownLocation.X, currentMouseLocation.X);
                int minY = Math.Min(mouseDownLocation.Y, currentMouseLocation.Y);
                int maxX = Math.Max(mouseDownLocation.X, currentMouseLocation.X);
                int maxY = Math.Max(mouseDownLocation.Y, currentMouseLocation.Y);
                selectionRect = new Rectangle(minX, minY, maxX - minX, maxY - minY);

                PictureBox.Invalidate();
            }
            if (e.Button == MouseButtons.None)
            {
                float sizeCoef = PictureBox.Width / plate.Size.X;
                PointF point = new PointF(e.Location.X / sizeCoef, (PictureBox.Height - e.Location.Y) / sizeCoef);
                mouseDownLocation = e.Location;
                selectionRect = new Rectangle(mouseDownLocation.X, mouseDownLocation.Y, 0, 0);
                updateRealSelectionRect();
                List<WellName> selectedWells = plate.GetWellsInsideRectangle(realSelectionRect);


                if (selectedWells.Count != 0/* && plate.Wells[selectedWells.ElementAt(0)].Liquid.volume > 0*/)
                {
                    string tipData = GenerateTipText(plate.Wells[selectedWells.ElementAt(0)].Liquid);
                    if (!toolTipShown)
                    {
                        toolTipShown = true;

                        //toolTip1.Show("test", PictureBox, e.Location.X, e.Location.Y + PictureBox.Height / 15);
                        popup.Show(tipData, PictureBox, e.Location.X, e.Location.Y + PictureBox.Height / 15);
                        //pictureBox.Invalidate();
                    }

                }
                else
                {
                    toolTipShown = false;
                    //toolTip1.Hide(PictureBox);
                    popup.Hide(PictureBox);

                }

            }

        }


        private string GenerateTipText(LiquidsMix mix)
        {
            int maxNameLength = 30;
            StringBuilder tipData = new StringBuilder();
            if (mix.liquids.Count != 0)
            {
                // Находим длину самого длинного названия жидкости
                int longestNameLength = mix.liquids.Max(l => l.Value.liquid.Name.Length);
                if (longestNameLength > maxNameLength)
                {
                    longestNameLength = maxNameLength;
                }

                // Выравниваем длину до кратности 8, добавляем 8 если уже кратно
                int tabSize = 8;
                int paddedLength = (longestNameLength + tabSize - 1) / tabSize * tabSize;

                foreach (var key in mix.liquids.Keys)
                {
                    string name = mix.liquids[key].liquid.Name + ":";

                    // Обрезаем название, если оно слишком длинное
                    if (name.Length > maxNameLength)
                    {
                        name = name.Substring(0, maxNameLength - 4) + "...:";
                    }
                    // Расчет количества необходимых табуляций для дополнения
                    int nameLength = name.Length;
                    int tabsToAdd = (paddedLength - nameLength) / tabSize + 1; // +1 для минимального отступа

                    // Добавляем табуляции после названия
                    tipData.Append(name);
                    tipData.Append(new string('\t', tabsToAdd));

                    float percent = 100.0f * mix.liquids[key].proportion;
                    string volumeAndPercent = $"{mix.liquids[key].volume} мкл\t{percent:N2}%\n";
                    tipData.Append(volumeAndPercent);
                }

                // Добавляем подчеркивание для обозначения конца секции
                string lineString = new string('_', paddedLength + tabSize * 2 + 4);
                tipData.AppendLine(new string('_', paddedLength + tabSize * 2 + 4)); // Примерное добавление для объема и процентов

                int underlineLength = lineString.Length;
                int totalLengthInChars = underlineLength; // Пересчет в количество символов

                string totalVolume = $"Общий объём: {mix.volume} мкл";
                int totalVolumeLength = totalVolume.Length;

                // Рассчитываем количество пробелов для центрирования
                int spacesBeforeTotal = (totalLengthInChars - totalVolumeLength) * 3 / 4; // Опытным путём определил, что "_" - это 1.5 пробела 

                // Добавляем выровненную строку "Total volume"
                tipData.Append(new string(' ', spacesBeforeTotal)).AppendLine(totalVolume);
            }
            return tipData.ToString();
        }

        private void MouseUpHandler(object sender, MouseEventArgs e)
        {
            Point currentLocation = e.Location;
            if (e.Button == MouseButtons.Left)
            {
                if (Control.ModifierKeys == Keys.Shift)
                {
                    updateSelectedCells(false);
                }
                else
                {
                    updateSelectedCells(true);
                }
                isSelecting = false;

                PictureBox.Invalidate();
            }

            // Очистка прямоугольника выделения
            selectionRect = RectangleF.Empty;

            // Перерисовка PictureBox без выделенного прямоугольника
            PictureBox.Invalidate();
            mouseDownLocation = Point.Empty;
        }

        private void updateRealSelectionRect()
        {
            float sizeCoef = PictureBox.Width / plate.Size.X;
            realSelectionRect.X = selectionRect.X / (float)PictureBox.Width;
            realSelectionRect.Y = selectionRect.Y / (float)PictureBox.Height;
            realSelectionRect.Width = selectionRect.Width / (float)PictureBox.Width;
            realSelectionRect.Height = selectionRect.Height / (float)PictureBox.Height;


        }


        private bool updateSelectedCells(bool addMode = true)
        {
            bool result = false;
            updateRealSelectionRect();

            List<WellName> selectedWells = plate.GetWellsInsideRectangle(realSelectionRect);
            if (addMode)
            {
                SelectedWellNames = SelectedWellNames.Union(selectedWells).ToList();
            }
            else
            {
                SelectedWellNames = SelectedWellNames.Except(selectedWells).ToList();
            }
            PictureBox.Image = plate.getImage(SelectedWellNames);

            // Обработка выбранных ячеек
            //MessageBox.Show($"Выбранные ячейки: {string.Join(", ", selectedWells)}");
            //image = GeneratePlateImage();
            //pictureBox.BackgroundImage = image;

            return result;
        }

        public void updateImage()
        {
            PictureBox.Image = plate.getImage(SelectedWellNames);
        }



        private void pictureBoxPaintHandler(object sender, PaintEventArgs e)
        {


            if (PictureBox.Image != null && !selectionRect.IsEmpty)
            {
                // Используем объект Graphics из параметров события Paint
                Graphics g = e.Graphics;

                // Преобразуем реальные координаты выделения из пространства PictureBox в пространство изображения
                updateRealSelectionRect();

                // Создаем прямоугольник для отрисовки на основе реального выделения
                Rectangle rect = new Rectangle(
                    (int)(realSelectionRect.X * PictureBox.Width),
                    (int)(realSelectionRect.Y * PictureBox.Height),
                    (int)(realSelectionRect.Width * PictureBox.Width),
                    (int)(realSelectionRect.Height * PictureBox.Height)
                );

                // Рисуем полупрозрачный прямоугольник выделения
                using (Brush brush = new SolidBrush(Color.FromArgb(128, 72, 145, 220))) // Полупрозрачный синий
                {
                    g.FillRectangle(brush, rect);
                }
                // Рисуем границу прямоугольника выделения
                g.DrawRectangle(Pens.Blue, rect);
            }
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {
            e.ToolTipSize = toolTipSize;
        }


        public void showInWindow()
        {



            Control control = this;
            Form tmpForm = new Form();
            tmpForm.BackColor = SystemColors.InactiveBorder;
            control.Show();

            control.Location = new Point(0, 0);
            //control.Margin = new Padding(20);
            int padding = 10;
            control.Anchor = AnchorStyles.Top;
            control.Padding = new Padding(padding, padding, 0, padding);
            tmpForm.ClientSize = control.Size;
            tmpForm.AutoSize = true;
            tmpForm.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            tmpForm.MaximumSize = control.Size;
            tmpForm.MinimumSize = control.Size;
            tmpForm.AutoScroll = false;
            foreach (Form frm in Application.OpenForms)
            {
                frm.Opacity = 0.5;  // Затемнить каждую форму
            }

            tmpForm.FormClosed += (sender, e) =>
            {
                // Восстановление нормальной прозрачности всех форм приложения
                foreach (Form frm in Application.OpenForms)
                {
                    frm.Opacity = 1.0;
                }
                tmpForm.Controls.Remove(control);
            };

            tmpForm.FormClosed += (sender, e) => tmpForm.Controls.Remove(control);

            tmpForm.Controls.Add(control);
            //control.Dock = DockStyle.Bottom;

            tmpForm.Load += (sender, e) => tmpForm.BringToFront();

            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
            timer.Interval = 5; // период проверки в миллисекундах
            timer.Tick += (sender, e) =>
            {
                if (control.IsDisposed)
                {
                    tmpForm.Close();
                    timer.Stop();
                    return;
                }
                if (!control.Visible)
                {
                    tmpForm.Close();
                    tmpForm.Controls.Remove(control);
                    //tmpForm.Controls.Clear();
                    //control.Dispose();
                    timer.Stop();
                }
            };
            timer.Start();
            //control.Hide();
            tmpForm.ShowDialog();
            timer.Dispose();

        }

        private void PictureBox_Click(object sender, EventArgs e)
        {

        }
    }
}
