﻿namespace ChemRobotDesktop.CustomControls
{
    partial class LabwareEditor
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            splitContainer1 = new SplitContainer();
            FileActionsTableLayoutPanel = new TableLayoutPanel();
            label10 = new Label();
            panel2 = new Panel();
            CreateProtocolButton = new Button();
            plateTypeComboBox = new ComboBox();
            editlButton = new Button();
            splitContainer2 = new SplitContainer();
            plateParamsMenu1 = new PlateParamsMenu();
            tableLayoutPanel1 = new TableLayoutPanel();
            RobotPictureBox = new PictureBox();
            tableLayoutPanel3 = new TableLayoutPanel();
            PlatePictureBox = new PictureBox();
            tableLayoutPanel5 = new TableLayoutPanel();
            cancelButton = new Button();
            saveButton = new Button();
            plateTypeLabel = new Label();
            panel1 = new Panel();
            tableLayoutPanel2 = new TableLayoutPanel();
            tableLayoutPanel4 = new TableLayoutPanel();
            tableLayoutPanel6 = new TableLayoutPanel();
            tableLayoutPanel10 = new TableLayoutPanel();
            tableLayoutPanel16 = new TableLayoutPanel();
            radioButton3 = new RadioButton();
            radioButton4 = new RadioButton();
            tableLayoutPanel20 = new TableLayoutPanel();
            label21 = new Label();
            label22 = new Label();
            tableLayoutPanel12 = new TableLayoutPanel();
            tableLayoutPanel23 = new TableLayoutPanel();
            tableLayoutPanel29 = new TableLayoutPanel();
            tableLayoutPanel22 = new TableLayoutPanel();
            label11 = new Label();
            label15 = new Label();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            FileActionsTableLayoutPanel.SuspendLayout();
            panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer2).BeginInit();
            splitContainer2.Panel1.SuspendLayout();
            splitContainer2.Panel2.SuspendLayout();
            splitContainer2.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)RobotPictureBox).BeginInit();
            tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)PlatePictureBox).BeginInit();
            tableLayoutPanel5.SuspendLayout();
            tableLayoutPanel16.SuspendLayout();
            tableLayoutPanel20.SuspendLayout();
            tableLayoutPanel22.SuspendLayout();
            SuspendLayout();
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.FixedPanel = FixedPanel.Panel1;
            splitContainer1.IsSplitterFixed = true;
            splitContainer1.Location = new Point(0, 0);
            splitContainer1.Margin = new Padding(0);
            splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.BackColor = SystemColors.GradientActiveCaption;
            splitContainer1.Panel1.Controls.Add(FileActionsTableLayoutPanel);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.BackColor = SystemColors.InactiveBorder;
            splitContainer1.Panel2.Controls.Add(splitContainer2);
            splitContainer1.Size = new Size(1920, 764);
            splitContainer1.SplitterDistance = 250;
            splitContainer1.SplitterWidth = 1;
            splitContainer1.TabIndex = 2;
            // 
            // FileActionsTableLayoutPanel
            // 
            FileActionsTableLayoutPanel.BackColor = Color.Transparent;
            FileActionsTableLayoutPanel.ColumnCount = 1;
            FileActionsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            FileActionsTableLayoutPanel.Controls.Add(label10, 0, 0);
            FileActionsTableLayoutPanel.Controls.Add(panel2, 0, 2);
            FileActionsTableLayoutPanel.Controls.Add(editlButton, 0, 4);
            FileActionsTableLayoutPanel.Dock = DockStyle.Fill;
            FileActionsTableLayoutPanel.Location = new Point(0, 0);
            FileActionsTableLayoutPanel.Margin = new Padding(0);
            FileActionsTableLayoutPanel.Name = "FileActionsTableLayoutPanel";
            FileActionsTableLayoutPanel.RowCount = 7;
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle());
            FileActionsTableLayoutPanel.Size = new Size(250, 764);
            FileActionsTableLayoutPanel.TabIndex = 2;
            FileActionsTableLayoutPanel.Paint += FileActionsTableLayoutPanel_Paint;
            // 
            // label10
            // 
            label10.Dock = DockStyle.Fill;
            label10.Font = new Font("Segoe UI", 17F, FontStyle.Bold, GraphicsUnit.Point);
            label10.Location = new Point(3, 3);
            label10.Margin = new Padding(3);
            label10.Name = "label10";
            label10.Size = new Size(244, 34);
            label10.TabIndex = 0;
            label10.Text = "Оборудование";
            label10.TextAlign = ContentAlignment.TopCenter;
            // 
            // panel2
            // 
            panel2.Controls.Add(CreateProtocolButton);
            panel2.Controls.Add(plateTypeComboBox);
            panel2.Dock = DockStyle.Fill;
            panel2.Location = new Point(9, 93);
            panel2.Margin = new Padding(9, 3, 9, 3);
            panel2.Name = "panel2";
            panel2.Size = new Size(232, 44);
            panel2.TabIndex = 5;
            // 
            // CreateProtocolButton
            // 
            CreateProtocolButton.Anchor = AnchorStyles.None;
            CreateProtocolButton.BackColor = SystemColors.InactiveCaption;
            CreateProtocolButton.FlatStyle = FlatStyle.Popup;
            CreateProtocolButton.Font = new Font("Segoe UI", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            CreateProtocolButton.Location = new Point(16, 0);
            CreateProtocolButton.Margin = new Padding(8, 3, 8, 3);
            CreateProtocolButton.Name = "CreateProtocolButton";
            CreateProtocolButton.Size = new Size(200, 44);
            CreateProtocolButton.TabIndex = 1;
            CreateProtocolButton.Text = "Создать";
            CreateProtocolButton.UseVisualStyleBackColor = false;
            CreateProtocolButton.Click += CreateProtocolButton_Click;
            // 
            // plateTypeComboBox
            // 
            plateTypeComboBox.Anchor = AnchorStyles.None;
            plateTypeComboBox.BackColor = SystemColors.InactiveCaption;
            plateTypeComboBox.FlatStyle = FlatStyle.Popup;
            plateTypeComboBox.Font = new Font("Segoe UI", 14F, FontStyle.Regular, GraphicsUnit.Point);
            plateTypeComboBox.FormattingEnabled = true;
            plateTypeComboBox.Items.AddRange(new object[] { "Носики", "Пробирки", "Резервуар" });
            plateTypeComboBox.Location = new Point(41, 2);
            plateTypeComboBox.Margin = new Padding(3, 8, 3, 3);
            plateTypeComboBox.Name = "plateTypeComboBox";
            plateTypeComboBox.Size = new Size(150, 33);
            plateTypeComboBox.TabIndex = 4;
            plateTypeComboBox.SelectedIndexChanged += plateTypeComboBox_SelectedIndexChanged;
            // 
            // editlButton
            // 
            editlButton.Anchor = AnchorStyles.None;
            editlButton.BackColor = SystemColors.InactiveCaption;
            editlButton.FlatStyle = FlatStyle.Popup;
            editlButton.Font = new Font("Segoe UI", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            editlButton.Location = new Point(25, 163);
            editlButton.Margin = new Padding(8, 3, 8, 3);
            editlButton.Name = "editlButton";
            editlButton.Size = new Size(200, 44);
            editlButton.TabIndex = 3;
            editlButton.Text = "Редактировать";
            editlButton.UseVisualStyleBackColor = false;
            editlButton.Click += editlButton_Click;
            // 
            // splitContainer2
            // 
            splitContainer2.Dock = DockStyle.Fill;
            splitContainer2.FixedPanel = FixedPanel.Panel1;
            splitContainer2.IsSplitterFixed = true;
            splitContainer2.Location = new Point(0, 0);
            splitContainer2.Margin = new Padding(0);
            splitContainer2.Name = "splitContainer2";
            // 
            // splitContainer2.Panel1
            // 
            splitContainer2.Panel1.AutoScroll = true;
            splitContainer2.Panel1.Controls.Add(plateParamsMenu1);
            splitContainer2.Panel1MinSize = 820;
            // 
            // splitContainer2.Panel2
            // 
            splitContainer2.Panel2.AutoScroll = true;
            splitContainer2.Panel2.Controls.Add(tableLayoutPanel1);
            splitContainer2.Panel2.Controls.Add(panel1);
            splitContainer2.Size = new Size(1669, 764);
            splitContainer2.SplitterDistance = 820;
            splitContainer2.SplitterWidth = 1;
            splitContainer2.TabIndex = 0;
            // 
            // plateParamsMenu1
            // 
            plateParamsMenu1.AutoSize = true;
            plateParamsMenu1.Location = new Point(3, 3);
            plateParamsMenu1.Name = "plateParamsMenu1";
            plateParamsMenu1.Size = new Size(700, 2414);
            plateParamsMenu1.TabIndex = 0;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Controls.Add(RobotPictureBox, 0, 1);
            tableLayoutPanel1.Controls.Add(tableLayoutPanel3, 0, 0);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 50F));
            tableLayoutPanel1.Size = new Size(848, 764);
            tableLayoutPanel1.TabIndex = 2;
            // 
            // RobotPictureBox
            // 
            RobotPictureBox.Dock = DockStyle.Fill;
            RobotPictureBox.Image = Properties.Resources.hand_with_hammer_clean;
            RobotPictureBox.Location = new Point(3, 385);
            RobotPictureBox.Name = "RobotPictureBox";
            RobotPictureBox.Size = new Size(842, 376);
            RobotPictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            RobotPictureBox.TabIndex = 1;
            RobotPictureBox.TabStop = false;
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.ColumnCount = 1;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel3.Controls.Add(PlatePictureBox, 0, 1);
            tableLayoutPanel3.Controls.Add(tableLayoutPanel5, 0, 2);
            tableLayoutPanel3.Controls.Add(plateTypeLabel, 0, 0);
            tableLayoutPanel3.Dock = DockStyle.Fill;
            tableLayoutPanel3.Location = new Point(3, 3);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 3;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 30F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 60F));
            tableLayoutPanel3.Size = new Size(842, 376);
            tableLayoutPanel3.TabIndex = 2;
            // 
            // PlatePictureBox
            // 
            PlatePictureBox.Dock = DockStyle.Fill;
            PlatePictureBox.Location = new Point(3, 33);
            PlatePictureBox.Name = "PlatePictureBox";
            PlatePictureBox.Size = new Size(836, 280);
            PlatePictureBox.SizeMode = PictureBoxSizeMode.Zoom;
            PlatePictureBox.TabIndex = 0;
            PlatePictureBox.TabStop = false;
            // 
            // tableLayoutPanel5
            // 
            tableLayoutPanel5.ColumnCount = 4;
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 170F));
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 170F));
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel5.Controls.Add(cancelButton, 2, 0);
            tableLayoutPanel5.Controls.Add(saveButton, 1, 0);
            tableLayoutPanel5.Dock = DockStyle.Fill;
            tableLayoutPanel5.Location = new Point(3, 319);
            tableLayoutPanel5.Name = "tableLayoutPanel5";
            tableLayoutPanel5.RowCount = 1;
            tableLayoutPanel5.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel5.Size = new Size(836, 54);
            tableLayoutPanel5.TabIndex = 0;
            // 
            // cancelButton
            // 
            cancelButton.Anchor = AnchorStyles.None;
            cancelButton.FlatStyle = FlatStyle.Popup;
            cancelButton.Location = new Point(428, 7);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(150, 40);
            cancelButton.TabIndex = 0;
            cancelButton.Text = "Отменить";
            cancelButton.UseVisualStyleBackColor = true;
            cancelButton.Click += hideEditor;
            // 
            // saveButton
            // 
            saveButton.Anchor = AnchorStyles.None;
            saveButton.FlatStyle = FlatStyle.Popup;
            saveButton.Location = new Point(258, 7);
            saveButton.Name = "saveButton";
            saveButton.Size = new Size(150, 40);
            saveButton.TabIndex = 1;
            saveButton.Text = "Сохранить";
            saveButton.UseVisualStyleBackColor = true;
            saveButton.Click += saveButton_Click;
            // 
            // plateTypeLabel
            // 
            plateTypeLabel.AutoSize = true;
            plateTypeLabel.Dock = DockStyle.Fill;
            plateTypeLabel.Font = new Font("Segoe UI", 13.8F, FontStyle.Bold, GraphicsUnit.Point);
            plateTypeLabel.Location = new Point(3, 0);
            plateTypeLabel.Name = "plateTypeLabel";
            plateTypeLabel.Size = new Size(836, 30);
            plateTypeLabel.TabIndex = 1;
            plateTypeLabel.Text = "Предпросмотр";
            plateTypeLabel.TextAlign = ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            panel1.Anchor = AnchorStyles.Top | AnchorStyles.Right;
            panel1.AutoSize = true;
            panel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            panel1.Location = new Point(1852, 0);
            panel1.Name = "panel1";
            panel1.Size = new Size(0, 0);
            panel1.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.ColumnCount = 1;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
            tableLayoutPanel2.Location = new Point(0, 0);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 2;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel2.Size = new Size(200, 100);
            tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            tableLayoutPanel4.ColumnCount = 1;
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
            tableLayoutPanel4.Location = new Point(0, 0);
            tableLayoutPanel4.Name = "tableLayoutPanel4";
            tableLayoutPanel4.RowCount = 2;
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel4.Size = new Size(200, 100);
            tableLayoutPanel4.TabIndex = 0;
            // 
            // tableLayoutPanel6
            // 
            tableLayoutPanel6.ColumnCount = 1;
            tableLayoutPanel6.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
            tableLayoutPanel6.Location = new Point(0, 0);
            tableLayoutPanel6.Name = "tableLayoutPanel6";
            tableLayoutPanel6.RowCount = 4;
            tableLayoutPanel6.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel6.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel6.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel6.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel6.Size = new Size(200, 100);
            tableLayoutPanel6.TabIndex = 0;
            // 
            // tableLayoutPanel10
            // 
            tableLayoutPanel10.ColumnCount = 1;
            tableLayoutPanel10.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
            tableLayoutPanel10.Location = new Point(0, 0);
            tableLayoutPanel10.Name = "tableLayoutPanel10";
            tableLayoutPanel10.RowCount = 2;
            tableLayoutPanel10.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel10.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel10.Size = new Size(200, 100);
            tableLayoutPanel10.TabIndex = 0;
            // 
            // tableLayoutPanel16
            // 
            tableLayoutPanel16.ColumnCount = 1;
            tableLayoutPanel16.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel16.Controls.Add(radioButton3, 0, 1);
            tableLayoutPanel16.Location = new Point(0, 0);
            tableLayoutPanel16.Name = "tableLayoutPanel16";
            tableLayoutPanel16.RowCount = 2;
            tableLayoutPanel16.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel16.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel16.Size = new Size(200, 100);
            tableLayoutPanel16.TabIndex = 0;
            // 
            // radioButton3
            // 
            radioButton3.Anchor = AnchorStyles.None;
            radioButton3.AutoSize = true;
            radioButton3.Checked = true;
            radioButton3.Location = new Point(67, 50);
            radioButton3.Name = "radioButton3";
            radioButton3.Size = new Size(66, 19);
            radioButton3.TabIndex = 13;
            radioButton3.TabStop = true;
            radioButton3.Text = "Circular";
            radioButton3.TextAlign = ContentAlignment.MiddleCenter;
            radioButton3.UseVisualStyleBackColor = true;
            // 
            // radioButton4
            // 
            radioButton4.Anchor = AnchorStyles.None;
            radioButton4.AutoSize = true;
            radioButton4.Location = new Point(54, 53);
            radioButton4.Name = "radioButton4";
            radioButton4.Size = new Size(91, 19);
            radioButton4.TabIndex = 14;
            radioButton4.TabStop = true;
            radioButton4.Text = " Rectangular";
            radioButton4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel20
            // 
            tableLayoutPanel20.ColumnCount = 1;
            tableLayoutPanel20.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel20.Controls.Add(label21, 0, 2);
            tableLayoutPanel20.Location = new Point(0, 0);
            tableLayoutPanel20.Name = "tableLayoutPanel20";
            tableLayoutPanel20.RowCount = 3;
            tableLayoutPanel20.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel20.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel20.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel20.Size = new Size(200, 100);
            tableLayoutPanel20.TabIndex = 0;
            // 
            // label21
            // 
            label21.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label21.AutoSize = true;
            label21.Location = new Point(4, 85);
            label21.Margin = new Padding(4, 0, 4, 0);
            label21.Name = "label21";
            label21.Size = new Size(58, 15);
            label21.TabIndex = 16;
            label21.Text = "Y spacing";
            // 
            // label22
            // 
            label22.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label22.AutoSize = true;
            label22.Location = new Point(4, 0);
            label22.Margin = new Padding(4, 0, 4, 0);
            label22.Name = "label22";
            label22.Size = new Size(58, 15);
            label22.TabIndex = 15;
            label22.Text = "X spacing";
            // 
            // tableLayoutPanel12
            // 
            tableLayoutPanel12.ColumnCount = 1;
            tableLayoutPanel12.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
            tableLayoutPanel12.Location = new Point(0, 0);
            tableLayoutPanel12.Name = "tableLayoutPanel12";
            tableLayoutPanel12.RowCount = 2;
            tableLayoutPanel12.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel12.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel12.Size = new Size(200, 100);
            tableLayoutPanel12.TabIndex = 0;
            // 
            // tableLayoutPanel23
            // 
            tableLayoutPanel23.ColumnCount = 1;
            tableLayoutPanel23.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
            tableLayoutPanel23.Location = new Point(0, 0);
            tableLayoutPanel23.Name = "tableLayoutPanel23";
            tableLayoutPanel23.RowCount = 2;
            tableLayoutPanel23.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel23.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel23.Size = new Size(200, 100);
            tableLayoutPanel23.TabIndex = 0;
            // 
            // tableLayoutPanel29
            // 
            tableLayoutPanel29.ColumnCount = 1;
            tableLayoutPanel29.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 20F));
            tableLayoutPanel29.Location = new Point(0, 0);
            tableLayoutPanel29.Name = "tableLayoutPanel29";
            tableLayoutPanel29.RowCount = 2;
            tableLayoutPanel29.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel29.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel29.Size = new Size(200, 100);
            tableLayoutPanel29.TabIndex = 0;
            // 
            // tableLayoutPanel22
            // 
            tableLayoutPanel22.ColumnCount = 1;
            tableLayoutPanel22.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel22.Controls.Add(label11, 0, 1);
            tableLayoutPanel22.Dock = DockStyle.Fill;
            tableLayoutPanel22.Location = new Point(0, 0);
            tableLayoutPanel22.Name = "tableLayoutPanel22";
            tableLayoutPanel22.RowCount = 2;
            tableLayoutPanel22.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel22.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel22.Size = new Size(200, 100);
            tableLayoutPanel22.TabIndex = 0;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label11.Location = new Point(3, 20);
            label11.Name = "label11";
            label11.Size = new Size(110, 21);
            label11.TabIndex = 1;
            label11.Text = "Total Footprint";
            // 
            // label15
            // 
            label15.Anchor = AnchorStyles.None;
            label15.AutoSize = true;
            label15.Location = new Point(58, 0);
            label15.Name = "label15";
            label15.Size = new Size(84, 15);
            label15.TabIndex = 0;
            label15.Text = "Total Footprint";
            // 
            // LabwareEditor
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoScroll = true;
            AutoSize = true;
            Controls.Add(splitContainer1);
            Margin = new Padding(0);
            Name = "LabwareEditor";
            Size = new Size(1920, 764);
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            FileActionsTableLayoutPanel.ResumeLayout(false);
            panel2.ResumeLayout(false);
            splitContainer2.Panel1.ResumeLayout(false);
            splitContainer2.Panel1.PerformLayout();
            splitContainer2.Panel2.ResumeLayout(false);
            splitContainer2.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)splitContainer2).EndInit();
            splitContainer2.ResumeLayout(false);
            tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)RobotPictureBox).EndInit();
            tableLayoutPanel3.ResumeLayout(false);
            tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)PlatePictureBox).EndInit();
            tableLayoutPanel5.ResumeLayout(false);
            tableLayoutPanel16.ResumeLayout(false);
            tableLayoutPanel16.PerformLayout();
            tableLayoutPanel20.ResumeLayout(false);
            tableLayoutPanel20.PerformLayout();
            tableLayoutPanel22.ResumeLayout(false);
            tableLayoutPanel22.PerformLayout();
            ResumeLayout(false);
        }

        #endregion
        private SplitContainer splitContainer1;
        private TableLayoutPanel tableLayoutPanel2;
        private TableLayoutPanel tableLayoutPanel4;
        private TableLayoutPanel tableLayoutPanel6;
        private TableLayoutPanel tableLayoutPanel10;
        private TableLayoutPanel tableLayoutPanel16;
        private RadioButton radioButton3;
        private RadioButton radioButton4;
        private SplitContainer splitContainer2;
        private Panel panel1;
        private TableLayoutPanel tableLayoutPanel20;
        private Label label21;
        private Label label22;
        private TableLayoutPanel tableLayoutPanel12;
        private TableLayoutPanel tableLayoutPanel23;
        private TableLayoutPanel tableLayoutPanel29;
        private TableLayoutPanel tableLayoutPanel22;
        private Label label11;
        private Label label15;
        private TableLayoutPanel FileActionsTableLayoutPanel;
        private Label label10;
        private Button CreateProtocolButton;
        private TableLayoutPanel tableLayoutPanel1;
        private PictureBox PlatePictureBox;
        private PictureBox RobotPictureBox;
        private TableLayoutPanel tableLayoutPanel3;
        private TableLayoutPanel tableLayoutPanel5;
        private Button cancelButton;
        private Button saveButton;
        private ComboBox plateTypeComboBox;
        private Button editlButton;
        private Panel panel2;
        private Label plateTypeLabel;
        //private PlateParamsMenu plateParamsMenu1;
        private PlateParamsMenu plateParamsMenu1;
    }
}
