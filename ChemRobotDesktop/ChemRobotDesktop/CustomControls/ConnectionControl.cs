﻿using System;
using System.IO.Ports;
using System.Management;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChemRobotDesktop.CustomControls
{
    public partial class ConnectionControl : UserControl
    {
        public delegate void Method();
        public event Method ProtocolStartButtonClicked;
        private SerialPort port = new SerialPort();
        private System.Windows.Forms.Timer checkPortTimer = new System.Windows.Forms.Timer();
        private const int CheckPortInterval = 500;
        public bool connected = false;
        public string portName = "";
        private int prevPortsNum = 0;
        public ConnectionControl()
        {
            InitializeComponent();
            SetupTimer();
        }

        public bool searchTargetPort()
        {
            //statusBox.ForeColor = Color.Orange;
            //statusBox.Text = "Connecting";
            this.Invoke((MethodInvoker)delegate // Используйте Invoke для обновления элементов управления UI из другого потока
            {
                statusBox.ForeColor = Color.Orange;
                statusBox.Text = "Соединение";
            });

            bool res = false;
            for (int i = 0; i < 5; i++)
            {
                string[] ports = SerialPort.GetPortNames();
                foreach (string newPortName in ports)
                {
                    if (!port.IsOpen)
                    {
                        Console.WriteLine("Попробуйте: " + newPortName);
                        res = connect(newPortName);
                        if (res == true)
                        {
                            break;
                        }
                    }

                }
                if (res == true)
                {
                    break;
                }
            }
            if (res)
            {
                this.Invoke((MethodInvoker)delegate // Используйте Invoke для обновления элементов управления UI из другого потока
                {
                    statusBox.ForeColor = Color.Green;
                    statusBox.Text = "Подключено";
                });

            }
            else
            {
                this.Invoke((MethodInvoker)delegate // Используйте Invoke для обновления элементов управления UI из другого потока
                {
                    statusBox.ForeColor = Color.Red;
                    statusBox.Text = "Робот не подключен";
                });

            }
            return res;
        }




        private void SetupTimer()
        {
            checkPortTimer.Interval = CheckPortInterval;
            checkPortTimer.Tick += CheckPortTimer_Tick;
            checkPortTimer.Start();
        }

        private async void CheckPortTimer_Tick(object sender, EventArgs e)
        {
            if (!this.IsHandleCreated)
                return; // Выходим, если дескриптор еще не создан

            string[] ports = SerialPort.GetPortNames();

            bool connected = checkConnection();
            if (!connected)
            {
                if (ports.Length > prevPortsNum)
                {
                    bool success = await Task.Run(() => searchTargetPort());
                    if (success)
                    {
                        this.BeginInvoke((MethodInvoker)delegate // Используйте Invoke для обновления элементов управления UI из другого потока
                        {
                            statusBox.ForeColor = Color.Green;
                            statusBox.Text = "Подключено";
                        });
                    }
                }
                else
                {
                    this.BeginInvoke((MethodInvoker)delegate // Используйте Invoke для обновления элементов управления UI из другого потока
                    {
                        statusBox.ForeColor = Color.Red;
                        statusBox.Text = "Робот не подключен";
                    });
                }
            }
            prevPortsNum = ports.Length;

        }

        private string SendCommand1(string command)
        {
            port.WriteLine(command);
            Thread.Sleep(500); // Ожидание ответа
            return port.ReadExisting().Trim();
        }

        private async Task<string> SendCommandAsync(string command, int timeoutMilliseconds)
        {
            await Task.Run(() => port.WriteLine(command));
            var readTask = Task.Run(() => port.ReadExisting().Trim());
            if (await Task.WhenAny(readTask, Task.Delay(timeoutMilliseconds)) == readTask)
            {
                return readTask.Result;
            }
            else
            {
                throw new TimeoutException("Время ожидания истекло");
            }
        }

        private bool checkConnection()
        {
            if (connected)
            {
                string[] ports = SerialPort.GetPortNames();
                if (!ports.Contains(portName))
                {
                    portName = "";
                    connected = false;
                    return false;
                }
                else
                {
                    try
                    {
                        port.Open();
                        port.Close();

                    }
                    catch (Exception ex)
                    {
                        port.Close();
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
        // async Task<bool> connect(string new_port_name)
        bool connect(string new_port_name)
        {
            bool res = false;
            if (port.IsOpen)
            {
                port.Close();
            }

            try
            {
                port.PortName = new_port_name;
                port.BaudRate = 115200;
                port.DataBits = 8;
                port.Parity = Parity.None;
                port.StopBits = StopBits.One;
                port.Handshake = Handshake.RequestToSend;
                port.ReadBufferSize = 8192;
                port.DtrEnable = true;
                port.RtsEnable = true;
                port.WriteTimeout = 500;
                port.ReadTimeout = 3000;
                port.Open();
                port.WriteLine("M107");
                //bool isSent = await TrySendCommandAsync(port, "M107", 1000);
                //port.Write("M107" + "\r\n");
                Thread.Sleep(500); // Ожидание ответа
                string answer = port.ReadExisting().Trim();
                if (answer == "ok")
                {
                    //MessageBox.Show("Port: " + new_port_name + " is open");
                    Console.WriteLine("Порт: " + new_port_name + " открыт");
                    connected = true;
                    portName = new_port_name;
                    //startButton.BackColor = Color.Green;
                    //statusBox.Text = "Connected";
                    res = true;
                }
                else
                {
                    //port.Close();
                    res = false;
                }
                port.Close();
                return res;

            }
            catch (TimeoutException)
            {
                port.Close();
                // Обработка случая, когда чтение или запись превысили таймаут
                //MessageBox.Show("Operation timed out");
                Console.WriteLine("Время ожидания порта истекло: " + new_port_name);
                res = false;
            }
            catch (UnauthorizedAccessException ex)
            {
                port.Close();
                Console.WriteLine("Доступ к порту закрыт: " + new_port_name + " - " + ex.Message);
            }
            catch (IOException ex)
            {
                port.Close();
                Console.WriteLine("IO error on port: " + new_port_name + " - " + ex.Message);
            }
            catch (ArgumentException ex)
            {
                port.Close();
                Console.WriteLine("Неправильное название порта: " + new_port_name + " - " + ex.Message);
            }
            catch (InvalidOperationException ex)
            {
                port.Close();
                Console.WriteLine("Порт уже открыт: " + new_port_name + " - " + ex.Message);
            }
            catch (Exception ex)
            {
                port.Close();
                // Для всех остальных видов исключений
                Console.WriteLine("Ошибка присоединения к порту: " + new_port_name + " - " + ex.Message);
            }
            port.Close();
            return false;
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private async void startButton_Click(object sender, EventArgs e)
        {
            if (connected)
            {
                ProtocolStartButtonClicked.Invoke();
            }
        }

        public bool runProtocol(List<string> protocol)
        {
            try
            {
                if (connected)
                {

                    SerialPortCommunicator(portName); // Замените "COM3" на нужный номер порта

                    if (OpenPort())
                    {

                        if (SendCommands(protocol))
                        {
                            MessageBox.Show("Все команды успешно выполнены.");
                        }

                        ClosePort();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return false;
            }
            return true;
        }

        public void SerialPortCommunicator(string portName)
        {
            port.PortName = portName;
            port.BaudRate = 115200;
            port.DataBits = 8;
            port.Parity = Parity.None;
            port.StopBits = StopBits.One;
            port.Handshake = Handshake.RequestToSend;
            port.ReadBufferSize = 8192;
            port.DtrEnable = true;
            port.RtsEnable = true;
            port.WriteTimeout = 500;
            port.ReadTimeout = 3000;
        }
        public bool OpenPort()
        {
            try
            {
                port.Open();
                return true;
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Не удалось открыть порт {port.PortName}: {ex.Message}");
                return false;
            }
        }

        public void ClosePort()
        {
            if (port.IsOpen)
            {
                port.Close();
            }
        }
        public bool SendCommands(List<string> commands)
        {
            List<string> initialCommands = new List<string>
        {
            "M30 run.gco",
            "M110 N0",
            "M28 run.gco"
        };

            // Добавляем пользовательские команды после инициализирующих команд
            initialCommands.AddRange(commands);

            // Добавляем заключительную команду
            initialCommands.Add("M29");
            initialCommands.Add("M32 run.gco");

            foreach (var command in initialCommands)
            {
                if (!SendCommand(command))
                {
                    MessageBox.Show($"Error executing command: {command}", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
            }

            return true;
        }

        private bool SendCommand(string command)
        {
            try
            {
                port.WriteLine(command);
                string response = ReadResponse();
                Console.WriteLine(command + " --> " + response);
                if (!response.EndsWith("ok") || response.Contains("Error"))
                {
                    port.WriteLine("M29"); // На всякий случай выйти из режима записи в файл
                    MessageBox.Show($"Command '{command}' failed with response: {response}", "Command Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return false;
                }
                return true;
            }
            catch (TimeoutException)
            {
                MessageBox.Show($"Command '{command}' timed out.", "Timeout Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
        }

        private string ReadResponse()
        {
            string response = string.Empty;
            try
            {
                string line;
                bool receivedOk = false;

                // Читаем строки до тех пор, пока не получим "ok" на конце одной из них.
                while (!receivedOk && port.IsOpen)
                {
                    line = port.ReadLine();
                    response += line + "\n";
                    if (line.Trim().Equals("ok"))
                    {
                        receivedOk = true;
                    }
                }
            }
            catch (TimeoutException)
            {
                // Обработка исключения тайм-аута
                MessageBox.Show("Response read timeout.", "Timeout Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return response.Trim();
        }

        private void statusBox_Click(object sender, EventArgs e)
        {

        }
    }


}
