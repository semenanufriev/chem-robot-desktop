﻿namespace ChemRobotDesktop.CustomControls
{
    partial class ModuleEditor
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            panel1 = new Panel();
            sizesPicture = new PictureBox();
            panel2 = new Panel();
            widthBox = new NumericUpDown();
            lenghtBox = new NumericUpDown();
            label1 = new Label();
            label2 = new Label();
            lenghtBox1 = new TextBox();
            widthBox1 = new TextBox();
            panel3 = new Panel();
            hightPicture = new PictureBox();
            panel4 = new Panel();
            heightBox = new NumericUpDown();
            label8 = new Label();
            heightBox1 = new TextBox();
            panel5 = new Panel();
            rowsColsPicture = new PictureBox();
            panel6 = new Panel();
            colsBox = new NumericUpDown();
            label5 = new Label();
            rowsBox = new NumericUpDown();
            label6 = new Label();
            rowsBox1 = new TextBox();
            colsBox1 = new TextBox();
            panel7 = new Panel();
            volumePicture = new PictureBox();
            panel8 = new Panel();
            volumeBox = new NumericUpDown();
            label7 = new Label();
            volumeBox1 = new TextBox();
            panel9 = new Panel();
            diameterPicture = new PictureBox();
            panel10 = new Panel();
            diameterBox = new NumericUpDown();
            label11 = new Label();
            diameterBox1 = new TextBox();
            panel11 = new Panel();
            panel13 = new Panel();
            yOffsetBox = new NumericUpDown();
            label15 = new Label();
            xOffsetBox = new NumericUpDown();
            label16 = new Label();
            panel12 = new Panel();
            offsetPicture = new PictureBox();
            panel17 = new Panel();
            depthPicture = new PictureBox();
            panel18 = new Panel();
            depthBox = new NumericUpDown();
            label12 = new Label();
            depthBox1 = new TextBox();
            panel19 = new Panel();
            spacePicture = new PictureBox();
            panel20 = new Panel();
            ySpaceBox = new NumericUpDown();
            label13 = new Label();
            xSpaceBox = new NumericUpDown();
            label14 = new Label();
            xSpaceBox1 = new TextBox();
            ySpaceBox1 = new TextBox();
            tableLayoutPanel1 = new TableLayoutPanel();
            yOffsetBox1 = new TextBox();
            xOffsetBox1 = new TextBox();
            okButton = new Button();
            typeBox = new ComboBox();
            nameBox = new TextBox();
            descriptionBox = new RichTextBox();
            label3 = new Label();
            label4 = new Label();
            label9 = new Label();
            label17 = new Label();
            saveButton = new Button();
            cancelButton = new Button();
            imageBox = new PictureBox();
            panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)sizesPicture).BeginInit();
            panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)widthBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)lenghtBox).BeginInit();
            panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)hightPicture).BeginInit();
            panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)heightBox).BeginInit();
            panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)rowsColsPicture).BeginInit();
            panel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)colsBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)rowsBox).BeginInit();
            panel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)volumePicture).BeginInit();
            panel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)volumeBox).BeginInit();
            panel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)diameterPicture).BeginInit();
            panel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)diameterBox).BeginInit();
            panel11.SuspendLayout();
            panel13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)yOffsetBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)xOffsetBox).BeginInit();
            panel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)offsetPicture).BeginInit();
            panel17.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)depthPicture).BeginInit();
            panel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)depthBox).BeginInit();
            panel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)spacePicture).BeginInit();
            panel20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)ySpaceBox).BeginInit();
            ((System.ComponentModel.ISupportInitialize)xSpaceBox).BeginInit();
            tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)imageBox).BeginInit();
            SuspendLayout();
            // 
            // panel1
            // 
            panel1.Controls.Add(sizesPicture);
            panel1.Dock = DockStyle.Fill;
            panel1.Location = new Point(1, 1);
            panel1.Margin = new Padding(0);
            panel1.Name = "panel1";
            panel1.Size = new Size(300, 231);
            panel1.TabIndex = 0;
            // 
            // sizesPicture
            // 
            sizesPicture.Dock = DockStyle.Fill;
            sizesPicture.Location = new Point(0, 0);
            sizesPicture.Margin = new Padding(4, 5, 4, 5);
            sizesPicture.Name = "sizesPicture";
            sizesPicture.Size = new Size(300, 231);
            sizesPicture.TabIndex = 0;
            sizesPicture.TabStop = false;
            // 
            // panel2
            // 
            panel2.Controls.Add(widthBox);
            panel2.Controls.Add(lenghtBox);
            panel2.Controls.Add(label1);
            panel2.Controls.Add(label2);
            panel2.Dock = DockStyle.Fill;
            panel2.Location = new Point(302, 1);
            panel2.Margin = new Padding(0);
            panel2.Name = "panel2";
            panel2.Size = new Size(409, 231);
            panel2.TabIndex = 1;
            // 
            // widthBox
            // 
            widthBox.DecimalPlaces = 1;
            widthBox.Location = new Point(8, 117);
            widthBox.Margin = new Padding(4, 5, 4, 5);
            widthBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            widthBox.Name = "widthBox";
            widthBox.Size = new Size(160, 27);
            widthBox.TabIndex = 2;
            widthBox.Value = new decimal(new int[] { 855, 0, 0, 65536 });
            widthBox.ValueChanged += okButton_Click;
            // 
            // lenghtBox
            // 
            lenghtBox.DecimalPlaces = 1;
            lenghtBox.Location = new Point(8, 48);
            lenghtBox.Margin = new Padding(4, 5, 4, 5);
            lenghtBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            lenghtBox.Name = "lenghtBox";
            lenghtBox.Size = new Size(160, 27);
            lenghtBox.TabIndex = 1;
            lenghtBox.Value = new decimal(new int[] { 1278, 0, 0, 65536 });
            lenghtBox.ValueChanged += okButton_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(4, 23);
            label1.Margin = new Padding(4, 0, 4, 0);
            label1.Name = "label1";
            label1.Size = new Size(89, 20);
            label1.TabIndex = 2;
            label1.Text = "Длина (мм)";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(4, 91);
            label2.Margin = new Padding(4, 0, 4, 0);
            label2.Name = "label2";
            label2.Size = new Size(103, 20);
            label2.TabIndex = 4;
            label2.Text = "Ширина (мм)";
            // 
            // lenghtBox1
            // 
            lenghtBox1.Location = new Point(1943, 125);
            lenghtBox1.Margin = new Padding(4, 5, 4, 5);
            lenghtBox1.Name = "lenghtBox1";
            lenghtBox1.Size = new Size(132, 27);
            lenghtBox1.TabIndex = 1;
            // 
            // widthBox1
            // 
            widthBox1.Location = new Point(1943, 194);
            widthBox1.Margin = new Padding(4, 5, 4, 5);
            widthBox1.Name = "widthBox1";
            widthBox1.Size = new Size(132, 27);
            widthBox1.TabIndex = 3;
            // 
            // panel3
            // 
            panel3.Controls.Add(hightPicture);
            panel3.Dock = DockStyle.Fill;
            panel3.Location = new Point(1, 233);
            panel3.Margin = new Padding(0);
            panel3.Name = "panel3";
            panel3.Size = new Size(300, 231);
            panel3.TabIndex = 2;
            // 
            // hightPicture
            // 
            hightPicture.Dock = DockStyle.Fill;
            hightPicture.Location = new Point(0, 0);
            hightPicture.Margin = new Padding(4, 5, 4, 5);
            hightPicture.Name = "hightPicture";
            hightPicture.Size = new Size(300, 231);
            hightPicture.TabIndex = 1;
            hightPicture.TabStop = false;
            // 
            // panel4
            // 
            panel4.Controls.Add(heightBox);
            panel4.Controls.Add(label8);
            panel4.Dock = DockStyle.Fill;
            panel4.Location = new Point(302, 233);
            panel4.Margin = new Padding(0);
            panel4.Name = "panel4";
            panel4.Size = new Size(409, 231);
            panel4.TabIndex = 3;
            // 
            // heightBox
            // 
            heightBox.Anchor = AnchorStyles.Left | AnchorStyles.Right;
            heightBox.DecimalPlaces = 1;
            heightBox.Location = new Point(12, 83);
            heightBox.Margin = new Padding(4, 5, 4, 5);
            heightBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            heightBox.Name = "heightBox";
            heightBox.Size = new Size(204, 27);
            heightBox.TabIndex = 3;
            heightBox.Value = new decimal(new int[] { 20, 0, 0, 0 });
            // 
            // label8
            // 
            label8.AutoSize = true;
            label8.Location = new Point(15, 58);
            label8.Margin = new Padding(4, 0, 4, 0);
            label8.Name = "label8";
            label8.Size = new Size(95, 20);
            label8.TabIndex = 14;
            label8.Text = "Высота (мм)";
            // 
            // heightBox1
            // 
            heightBox1.Location = new Point(1943, 280);
            heightBox1.Margin = new Padding(4, 5, 4, 5);
            heightBox1.Name = "heightBox1";
            heightBox1.Size = new Size(132, 27);
            heightBox1.TabIndex = 13;
            // 
            // panel5
            // 
            panel5.Controls.Add(rowsColsPicture);
            panel5.Dock = DockStyle.Fill;
            panel5.Location = new Point(1, 465);
            panel5.Margin = new Padding(0);
            panel5.Name = "panel5";
            panel5.Size = new Size(300, 231);
            panel5.TabIndex = 4;
            // 
            // rowsColsPicture
            // 
            rowsColsPicture.Dock = DockStyle.Fill;
            rowsColsPicture.Location = new Point(0, 0);
            rowsColsPicture.Margin = new Padding(4, 5, 4, 5);
            rowsColsPicture.Name = "rowsColsPicture";
            rowsColsPicture.Size = new Size(300, 231);
            rowsColsPicture.TabIndex = 1;
            rowsColsPicture.TabStop = false;
            // 
            // panel6
            // 
            panel6.Controls.Add(colsBox);
            panel6.Controls.Add(label5);
            panel6.Controls.Add(rowsBox);
            panel6.Controls.Add(label6);
            panel6.Dock = DockStyle.Fill;
            panel6.Location = new Point(302, 465);
            panel6.Margin = new Padding(0);
            panel6.Name = "panel6";
            panel6.Size = new Size(409, 231);
            panel6.TabIndex = 5;
            // 
            // colsBox
            // 
            colsBox.Location = new Point(8, 118);
            colsBox.Margin = new Padding(4, 5, 4, 5);
            colsBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            colsBox.Name = "colsBox";
            colsBox.Size = new Size(160, 27);
            colsBox.TabIndex = 5;
            colsBox.ValueChanged += okButton_Click;
            // 
            // label5
            // 
            label5.AutoSize = true;
            label5.Location = new Point(4, 95);
            label5.Margin = new Padding(4, 0, 4, 0);
            label5.Name = "label5";
            label5.Size = new Size(159, 20);
            label5.TabIndex = 12;
            label5.Text = "Количество столбцов";
            // 
            // rowsBox
            // 
            rowsBox.Location = new Point(8, 52);
            rowsBox.Margin = new Padding(4, 5, 4, 5);
            rowsBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            rowsBox.Name = "rowsBox";
            rowsBox.Size = new Size(160, 27);
            rowsBox.TabIndex = 4;
            rowsBox.ValueChanged += okButton_Click;
            // 
            // label6
            // 
            label6.AutoSize = true;
            label6.Location = new Point(4, 28);
            label6.Margin = new Padding(4, 0, 4, 0);
            label6.Name = "label6";
            label6.Size = new Size(132, 20);
            label6.TabIndex = 10;
            label6.Text = "Количество строк";
            // 
            // rowsBox1
            // 
            rowsBox1.Location = new Point(1917, 406);
            rowsBox1.Margin = new Padding(4, 5, 4, 5);
            rowsBox1.Name = "rowsBox1";
            rowsBox1.Size = new Size(132, 27);
            rowsBox1.TabIndex = 9;
            // 
            // colsBox1
            // 
            colsBox1.Location = new Point(1917, 474);
            colsBox1.Margin = new Padding(4, 5, 4, 5);
            colsBox1.Name = "colsBox1";
            colsBox1.Size = new Size(132, 27);
            colsBox1.TabIndex = 11;
            // 
            // panel7
            // 
            panel7.Controls.Add(volumePicture);
            panel7.Dock = DockStyle.Fill;
            panel7.Location = new Point(1, 697);
            panel7.Margin = new Padding(0);
            panel7.Name = "panel7";
            panel7.Size = new Size(300, 92);
            panel7.TabIndex = 6;
            // 
            // volumePicture
            // 
            volumePicture.Dock = DockStyle.Fill;
            volumePicture.Location = new Point(0, 0);
            volumePicture.Margin = new Padding(4, 5, 4, 5);
            volumePicture.Name = "volumePicture";
            volumePicture.Size = new Size(300, 92);
            volumePicture.TabIndex = 1;
            volumePicture.TabStop = false;
            // 
            // panel8
            // 
            panel8.Controls.Add(volumeBox);
            panel8.Controls.Add(label7);
            panel8.Dock = DockStyle.Fill;
            panel8.Location = new Point(302, 697);
            panel8.Margin = new Padding(0);
            panel8.Name = "panel8";
            panel8.Size = new Size(409, 92);
            panel8.TabIndex = 7;
            // 
            // volumeBox
            // 
            volumeBox.Location = new Point(8, 37);
            volumeBox.Margin = new Padding(4, 5, 4, 5);
            volumeBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            volumeBox.Name = "volumeBox";
            volumeBox.Size = new Size(160, 27);
            volumeBox.TabIndex = 6;
            // 
            // label7
            // 
            label7.AutoSize = true;
            label7.Location = new Point(4, 12);
            label7.Margin = new Padding(4, 0, 4, 0);
            label7.Name = "label7";
            label7.Size = new Size(97, 20);
            label7.TabIndex = 16;
            label7.Text = "Объём (мкл)";
            // 
            // volumeBox1
            // 
            volumeBox1.Location = new Point(1917, 577);
            volumeBox1.Margin = new Padding(4, 5, 4, 5);
            volumeBox1.Name = "volumeBox1";
            volumeBox1.Size = new Size(132, 27);
            volumeBox1.TabIndex = 15;
            // 
            // panel9
            // 
            panel9.Controls.Add(diameterPicture);
            panel9.Dock = DockStyle.Fill;
            panel9.Location = new Point(1, 790);
            panel9.Margin = new Padding(0);
            panel9.Name = "panel9";
            panel9.Size = new Size(300, 231);
            panel9.TabIndex = 8;
            // 
            // diameterPicture
            // 
            diameterPicture.Dock = DockStyle.Fill;
            diameterPicture.Location = new Point(0, 0);
            diameterPicture.Margin = new Padding(4, 5, 4, 5);
            diameterPicture.Name = "diameterPicture";
            diameterPicture.Size = new Size(300, 231);
            diameterPicture.TabIndex = 1;
            diameterPicture.TabStop = false;
            // 
            // panel10
            // 
            panel10.Controls.Add(diameterBox);
            panel10.Controls.Add(label11);
            panel10.Dock = DockStyle.Fill;
            panel10.Location = new Point(302, 790);
            panel10.Margin = new Padding(0);
            panel10.Name = "panel10";
            panel10.Size = new Size(409, 231);
            panel10.TabIndex = 9;
            // 
            // diameterBox
            // 
            diameterBox.DecimalPlaces = 1;
            diameterBox.Location = new Point(8, 43);
            diameterBox.Margin = new Padding(4, 5, 4, 5);
            diameterBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            diameterBox.Name = "diameterBox";
            diameterBox.Size = new Size(160, 27);
            diameterBox.TabIndex = 7;
            diameterBox.Value = new decimal(new int[] { 5, 0, 0, 0 });
            diameterBox.ValueChanged += okButton_Click;
            // 
            // label11
            // 
            label11.AutoSize = true;
            label11.Location = new Point(4, 17);
            label11.Margin = new Padding(4, 0, 4, 0);
            label11.Name = "label11";
            label11.Size = new Size(106, 20);
            label11.TabIndex = 16;
            label11.Text = "Диаметр (мм)";
            // 
            // diameterBox1
            // 
            diameterBox1.Location = new Point(1917, 674);
            diameterBox1.Margin = new Padding(4, 5, 4, 5);
            diameterBox1.Name = "diameterBox1";
            diameterBox1.Size = new Size(132, 27);
            diameterBox1.TabIndex = 15;
            // 
            // panel11
            // 
            panel11.Controls.Add(panel13);
            panel11.Dock = DockStyle.Fill;
            panel11.Location = new Point(302, 1486);
            panel11.Margin = new Padding(0);
            panel11.Name = "panel11";
            panel11.Size = new Size(409, 231);
            panel11.TabIndex = 10;
            // 
            // panel13
            // 
            panel13.Controls.Add(yOffsetBox);
            panel13.Controls.Add(label15);
            panel13.Controls.Add(xOffsetBox);
            panel13.Controls.Add(label16);
            panel13.Dock = DockStyle.Fill;
            panel13.Location = new Point(0, 0);
            panel13.Margin = new Padding(0);
            panel13.Name = "panel13";
            panel13.Size = new Size(409, 231);
            panel13.TabIndex = 14;
            // 
            // yOffsetBox
            // 
            yOffsetBox.DecimalPlaces = 1;
            yOffsetBox.Location = new Point(8, 122);
            yOffsetBox.Margin = new Padding(4, 5, 4, 5);
            yOffsetBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            yOffsetBox.Name = "yOffsetBox";
            yOffsetBox.Size = new Size(160, 27);
            yOffsetBox.TabIndex = 12;
            yOffsetBox.Value = new decimal(new int[] { 10, 0, 0, 0 });
            yOffsetBox.ValueChanged += okButton_Click;
            // 
            // label15
            // 
            label15.AutoSize = true;
            label15.Location = new Point(4, 95);
            label15.Margin = new Padding(4, 0, 4, 0);
            label15.Name = "label15";
            label15.Size = new Size(125, 20);
            label15.TabIndex = 16;
            label15.Text = "Отступ по Y (мм)";
            // 
            // xOffsetBox
            // 
            xOffsetBox.DecimalPlaces = 1;
            xOffsetBox.Location = new Point(8, 54);
            xOffsetBox.Margin = new Padding(4, 5, 4, 5);
            xOffsetBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            xOffsetBox.Name = "xOffsetBox";
            xOffsetBox.Size = new Size(160, 27);
            xOffsetBox.TabIndex = 11;
            xOffsetBox.Value = new decimal(new int[] { 10, 0, 0, 0 });
            xOffsetBox.ValueChanged += okButton_Click;
            // 
            // label16
            // 
            label16.AutoSize = true;
            label16.Location = new Point(4, 28);
            label16.Margin = new Padding(4, 0, 4, 0);
            label16.Name = "label16";
            label16.Size = new Size(126, 20);
            label16.TabIndex = 14;
            label16.Text = "Отступ по X (мм)";
            // 
            // panel12
            // 
            panel12.Controls.Add(offsetPicture);
            panel12.Dock = DockStyle.Fill;
            panel12.Location = new Point(1, 1486);
            panel12.Margin = new Padding(0);
            panel12.Name = "panel12";
            panel12.Size = new Size(300, 231);
            panel12.TabIndex = 11;
            // 
            // offsetPicture
            // 
            offsetPicture.Dock = DockStyle.Fill;
            offsetPicture.Location = new Point(0, 0);
            offsetPicture.Margin = new Padding(4, 5, 4, 5);
            offsetPicture.Name = "offsetPicture";
            offsetPicture.Size = new Size(300, 231);
            offsetPicture.TabIndex = 1;
            offsetPicture.TabStop = false;
            // 
            // panel17
            // 
            panel17.Controls.Add(depthPicture);
            panel17.Dock = DockStyle.Fill;
            panel17.Location = new Point(1, 1022);
            panel17.Margin = new Padding(0);
            panel17.Name = "panel17";
            panel17.Size = new Size(300, 231);
            panel17.TabIndex = 16;
            // 
            // depthPicture
            // 
            depthPicture.Dock = DockStyle.Fill;
            depthPicture.Location = new Point(0, 0);
            depthPicture.Margin = new Padding(4, 5, 4, 5);
            depthPicture.Name = "depthPicture";
            depthPicture.Size = new Size(300, 231);
            depthPicture.TabIndex = 1;
            depthPicture.TabStop = false;
            // 
            // panel18
            // 
            panel18.Controls.Add(depthBox);
            panel18.Controls.Add(label12);
            panel18.Dock = DockStyle.Fill;
            panel18.Location = new Point(302, 1022);
            panel18.Margin = new Padding(0);
            panel18.Name = "panel18";
            panel18.Size = new Size(409, 231);
            panel18.TabIndex = 17;
            // 
            // depthBox
            // 
            depthBox.DecimalPlaces = 1;
            depthBox.Location = new Point(8, 42);
            depthBox.Margin = new Padding(4, 5, 4, 5);
            depthBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            depthBox.Name = "depthBox";
            depthBox.Size = new Size(160, 27);
            depthBox.TabIndex = 8;
            // 
            // label12
            // 
            label12.AutoSize = true;
            label12.Location = new Point(4, 17);
            label12.Margin = new Padding(4, 0, 4, 0);
            label12.Name = "label12";
            label12.Size = new Size(102, 20);
            label12.TabIndex = 14;
            label12.Text = "Глубина (мм)";
            // 
            // depthBox1
            // 
            depthBox1.Location = new Point(1917, 742);
            depthBox1.Margin = new Padding(4, 5, 4, 5);
            depthBox1.Name = "depthBox1";
            depthBox1.Size = new Size(132, 27);
            depthBox1.TabIndex = 13;
            // 
            // panel19
            // 
            panel19.Controls.Add(spacePicture);
            panel19.Dock = DockStyle.Fill;
            panel19.Location = new Point(1, 1254);
            panel19.Margin = new Padding(0);
            panel19.Name = "panel19";
            panel19.Size = new Size(300, 231);
            panel19.TabIndex = 18;
            // 
            // spacePicture
            // 
            spacePicture.Dock = DockStyle.Fill;
            spacePicture.Location = new Point(0, 0);
            spacePicture.Margin = new Padding(4, 5, 4, 5);
            spacePicture.Name = "spacePicture";
            spacePicture.Size = new Size(300, 231);
            spacePicture.TabIndex = 1;
            spacePicture.TabStop = false;
            // 
            // panel20
            // 
            panel20.Controls.Add(ySpaceBox);
            panel20.Controls.Add(label13);
            panel20.Controls.Add(xSpaceBox);
            panel20.Controls.Add(label14);
            panel20.Dock = DockStyle.Fill;
            panel20.Location = new Point(302, 1254);
            panel20.Margin = new Padding(0);
            panel20.Name = "panel20";
            panel20.Size = new Size(409, 231);
            panel20.TabIndex = 19;
            // 
            // ySpaceBox
            // 
            ySpaceBox.DecimalPlaces = 1;
            ySpaceBox.Location = new Point(8, 125);
            ySpaceBox.Margin = new Padding(4, 5, 4, 5);
            ySpaceBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            ySpaceBox.Name = "ySpaceBox";
            ySpaceBox.Size = new Size(160, 27);
            ySpaceBox.TabIndex = 10;
            ySpaceBox.Value = new decimal(new int[] { 10, 0, 0, 0 });
            ySpaceBox.ValueChanged += okButton_Click;
            // 
            // label13
            // 
            label13.AutoSize = true;
            label13.Location = new Point(4, 98);
            label13.Margin = new Padding(4, 0, 4, 0);
            label13.Name = "label13";
            label13.Size = new Size(158, 20);
            label13.TabIndex = 16;
            label13.Text = "Расстояние по Y (мм)";
            // 
            // xSpaceBox
            // 
            xSpaceBox.DecimalPlaces = 1;
            xSpaceBox.Location = new Point(8, 55);
            xSpaceBox.Margin = new Padding(4, 5, 4, 5);
            xSpaceBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            xSpaceBox.Name = "xSpaceBox";
            xSpaceBox.Size = new Size(160, 27);
            xSpaceBox.TabIndex = 9;
            xSpaceBox.Value = new decimal(new int[] { 10, 0, 0, 0 });
            xSpaceBox.ValueChanged += okButton_Click;
            // 
            // label14
            // 
            label14.AutoSize = true;
            label14.Location = new Point(4, 31);
            label14.Margin = new Padding(4, 0, 4, 0);
            label14.Name = "label14";
            label14.Size = new Size(159, 20);
            label14.TabIndex = 14;
            label14.Text = "Расстояние по Х (мм)";
            // 
            // xSpaceBox1
            // 
            xSpaceBox1.Location = new Point(1943, 874);
            xSpaceBox1.Margin = new Padding(4, 5, 4, 5);
            xSpaceBox1.Name = "xSpaceBox1";
            xSpaceBox1.Size = new Size(132, 27);
            xSpaceBox1.TabIndex = 13;
            // 
            // ySpaceBox1
            // 
            ySpaceBox1.Location = new Point(1943, 940);
            ySpaceBox1.Margin = new Padding(4, 5, 4, 5);
            ySpaceBox1.Name = "ySpaceBox1";
            ySpaceBox1.Size = new Size(132, 27);
            ySpaceBox1.TabIndex = 15;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.AutoScroll = true;
            tableLayoutPanel1.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutPanel1.ColumnCount = 2;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 300F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 409F));
            tableLayoutPanel1.Controls.Add(panel20, 1, 6);
            tableLayoutPanel1.Controls.Add(panel19, 0, 6);
            tableLayoutPanel1.Controls.Add(panel18, 1, 5);
            tableLayoutPanel1.Controls.Add(panel17, 0, 5);
            tableLayoutPanel1.Controls.Add(panel12, 0, 7);
            tableLayoutPanel1.Controls.Add(panel11, 0, 7);
            tableLayoutPanel1.Controls.Add(panel10, 1, 4);
            tableLayoutPanel1.Controls.Add(panel9, 0, 4);
            tableLayoutPanel1.Controls.Add(panel8, 1, 3);
            tableLayoutPanel1.Controls.Add(panel7, 0, 3);
            tableLayoutPanel1.Controls.Add(panel6, 1, 2);
            tableLayoutPanel1.Controls.Add(panel5, 0, 2);
            tableLayoutPanel1.Controls.Add(panel4, 1, 1);
            tableLayoutPanel1.Controls.Add(panel3, 0, 1);
            tableLayoutPanel1.Controls.Add(panel2, 1, 0);
            tableLayoutPanel1.Controls.Add(panel1, 0, 0);
            tableLayoutPanel1.Location = new Point(679, 31);
            tableLayoutPanel1.Margin = new Padding(0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 8;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 92F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 231F));
            tableLayoutPanel1.Size = new Size(688, 1000);
            tableLayoutPanel1.TabIndex = 0;
            // 
            // yOffsetBox1
            // 
            yOffsetBox1.Location = new Point(1943, 1122);
            yOffsetBox1.Margin = new Padding(4, 5, 4, 5);
            yOffsetBox1.Name = "yOffsetBox1";
            yOffsetBox1.Size = new Size(132, 27);
            yOffsetBox1.TabIndex = 15;
            // 
            // xOffsetBox1
            // 
            xOffsetBox1.Location = new Point(1943, 1054);
            xOffsetBox1.Margin = new Padding(4, 5, 4, 5);
            xOffsetBox1.Name = "xOffsetBox1";
            xOffsetBox1.Size = new Size(132, 27);
            xOffsetBox1.TabIndex = 13;
            // 
            // okButton
            // 
            okButton.Location = new Point(559, 1055);
            okButton.Margin = new Padding(4, 5, 4, 5);
            okButton.Name = "okButton";
            okButton.Size = new Size(100, 35);
            okButton.TabIndex = 16;
            okButton.Text = "OK";
            okButton.UseVisualStyleBackColor = true;
            okButton.Click += okButton_Click;
            // 
            // typeBox
            // 
            typeBox.FormattingEnabled = true;
            typeBox.Items.AddRange(new object[] { "nose_tablet", "probe_tablet", "Source" });
            typeBox.Location = new Point(56, 80);
            typeBox.Margin = new Padding(4, 5, 4, 5);
            typeBox.Name = "typeBox";
            typeBox.Size = new Size(160, 28);
            typeBox.TabIndex = 17;
            typeBox.SelectedIndexChanged += typeBox_SelectedIndexChanged;
            // 
            // nameBox
            // 
            nameBox.Location = new Point(56, 180);
            nameBox.Margin = new Padding(4, 5, 4, 5);
            nameBox.Name = "nameBox";
            nameBox.Size = new Size(244, 27);
            nameBox.TabIndex = 18;
            nameBox.TextChanged += nameBox_TextChanged;
            // 
            // descriptionBox
            // 
            descriptionBox.Location = new Point(56, 271);
            descriptionBox.Margin = new Padding(4, 5, 4, 5);
            descriptionBox.Name = "descriptionBox";
            descriptionBox.Size = new Size(361, 215);
            descriptionBox.TabIndex = 19;
            descriptionBox.Text = "";
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(52, 49);
            label3.Margin = new Padding(4, 0, 4, 0);
            label3.Name = "label3";
            label3.Size = new Size(93, 20);
            label3.TabIndex = 20;
            label3.Text = "Тип модуля:";
            // 
            // label4
            // 
            label4.AutoSize = true;
            label4.Location = new Point(52, 154);
            label4.Margin = new Padding(4, 0, 4, 0);
            label4.Name = "label4";
            label4.Size = new Size(80, 20);
            label4.TabIndex = 21;
            label4.Text = "Название:";
            // 
            // label9
            // 
            label9.AutoSize = true;
            label9.Location = new Point(52, 246);
            label9.Margin = new Padding(4, 0, 4, 0);
            label9.Name = "label9";
            label9.Size = new Size(82, 20);
            label9.TabIndex = 22;
            label9.Text = "Описание:";
            // 
            // label17
            // 
            label17.AutoSize = true;
            label17.Location = new Point(55, 529);
            label17.Margin = new Padding(4, 0, 4, 0);
            label17.Name = "label17";
            label17.Size = new Size(169, 20);
            label17.TabIndex = 25;
            label17.Text = "Предпросмотр модуля";
            // 
            // saveButton
            // 
            saveButton.Location = new Point(428, 1055);
            saveButton.Margin = new Padding(4, 5, 4, 5);
            saveButton.Name = "saveButton";
            saveButton.Size = new Size(100, 35);
            saveButton.TabIndex = 26;
            saveButton.Text = "Сохранить";
            saveButton.UseVisualStyleBackColor = true;
            saveButton.Click += saveButton_Click;
            // 
            // cancelButton
            // 
            cancelButton.Location = new Point(319, 1055);
            cancelButton.Margin = new Padding(4, 5, 4, 5);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(100, 35);
            cancelButton.TabIndex = 27;
            cancelButton.Text = "Отменить";
            cancelButton.UseVisualStyleBackColor = true;
            cancelButton.Click += cancelButton_Click;
            // 
            // imageBox
            // 
            imageBox.Location = new Point(59, 569);
            imageBox.Margin = new Padding(4, 5, 4, 5);
            imageBox.Name = "imageBox";
            imageBox.Size = new Size(600, 462);
            imageBox.TabIndex = 23;
            imageBox.TabStop = false;
            // 
            // ModuleEditor
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(cancelButton);
            Controls.Add(saveButton);
            Controls.Add(label17);
            Controls.Add(imageBox);
            Controls.Add(label9);
            Controls.Add(label4);
            Controls.Add(label3);
            Controls.Add(descriptionBox);
            Controls.Add(nameBox);
            Controls.Add(typeBox);
            Controls.Add(okButton);
            Controls.Add(colsBox1);
            Controls.Add(yOffsetBox1);
            Controls.Add(ySpaceBox1);
            Controls.Add(xOffsetBox1);
            Controls.Add(depthBox1);
            Controls.Add(xSpaceBox1);
            Controls.Add(diameterBox1);
            Controls.Add(volumeBox1);
            Controls.Add(heightBox1);
            Controls.Add(rowsBox1);
            Controls.Add(widthBox1);
            Controls.Add(tableLayoutPanel1);
            Controls.Add(lenghtBox1);
            Margin = new Padding(4, 5, 4, 5);
            Name = "ModuleEditor";
            Size = new Size(1452, 1137);
            panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)sizesPicture).EndInit();
            panel2.ResumeLayout(false);
            panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)widthBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)lenghtBox).EndInit();
            panel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)hightPicture).EndInit();
            panel4.ResumeLayout(false);
            panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)heightBox).EndInit();
            panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)rowsColsPicture).EndInit();
            panel6.ResumeLayout(false);
            panel6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)colsBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)rowsBox).EndInit();
            panel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)volumePicture).EndInit();
            panel8.ResumeLayout(false);
            panel8.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)volumeBox).EndInit();
            panel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)diameterPicture).EndInit();
            panel10.ResumeLayout(false);
            panel10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)diameterBox).EndInit();
            panel11.ResumeLayout(false);
            panel13.ResumeLayout(false);
            panel13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)yOffsetBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)xOffsetBox).EndInit();
            panel12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)offsetPicture).EndInit();
            panel17.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)depthPicture).EndInit();
            panel18.ResumeLayout(false);
            panel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)depthBox).EndInit();
            panel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)spacePicture).EndInit();
            panel20.ResumeLayout(false);
            panel20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)ySpaceBox).EndInit();
            ((System.ComponentModel.ISupportInitialize)xSpaceBox).EndInit();
            tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)imageBox).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox widthBox1;
        private System.Windows.Forms.TextBox lenghtBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TextBox heightBox1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.TextBox colsBox1;
        private System.Windows.Forms.TextBox rowsBox1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox volumeBox1;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox diameterBox1;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox yOffsetBox1;
        private System.Windows.Forms.TextBox xOffsetBox1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Panel panel17;
        private System.Windows.Forms.Panel panel18;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox depthBox1;
        private System.Windows.Forms.Panel panel19;
        private System.Windows.Forms.Panel panel20;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox ySpaceBox1;
        private System.Windows.Forms.TextBox xSpaceBox1;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.NumericUpDown lenghtBox;
        private System.Windows.Forms.NumericUpDown widthBox;
        private System.Windows.Forms.NumericUpDown heightBox;
        private System.Windows.Forms.NumericUpDown colsBox;
        private System.Windows.Forms.NumericUpDown rowsBox;
        private System.Windows.Forms.NumericUpDown volumeBox;
        private System.Windows.Forms.NumericUpDown diameterBox;
        private System.Windows.Forms.NumericUpDown yOffsetBox;
        private System.Windows.Forms.NumericUpDown xOffsetBox;
        private System.Windows.Forms.NumericUpDown depthBox;
        private System.Windows.Forms.NumericUpDown ySpaceBox;
        private System.Windows.Forms.NumericUpDown xSpaceBox;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.ComboBox typeBox;
        private System.Windows.Forms.TextBox nameBox;
        private System.Windows.Forms.RichTextBox descriptionBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.PictureBox imageBox;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.PictureBox sizesPicture;
        private System.Windows.Forms.PictureBox hightPicture;
        private System.Windows.Forms.PictureBox rowsColsPicture;
        private System.Windows.Forms.PictureBox volumePicture;
        private System.Windows.Forms.PictureBox diameterPicture;
        private System.Windows.Forms.PictureBox offsetPicture;
        private System.Windows.Forms.PictureBox depthPicture;
        private System.Windows.Forms.PictureBox spacePicture;
        private System.Windows.Forms.Button saveButton;
        private System.Windows.Forms.Button cancelButton;
    }
}
