﻿namespace ChemRobotDesktop.CustomControls
{
    partial class PlateWindow
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            plateControl1 = new PlateControl();
            label3 = new Label();
            label2 = new Label();
            label1 = new Label();
            plateNameBox = new TextBox();
            liquidsComboBox = new ComboBox();
            volumeBox = new NumericUpDown();
            cancelButton = new Button();
            saveNamesAndLiquidsButton = new Button();
            clearWellsButton = new Button();
            saveButton = new Button();
            tableLayoutPanel = new TableLayoutPanel();
            tableLayoutPanel1 = new TableLayoutPanel();
            okButton = new Button();
            tableLayoutPanel4 = new TableLayoutPanel();
            ((System.ComponentModel.ISupportInitialize)volumeBox).BeginInit();
            tableLayoutPanel.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            tableLayoutPanel4.SuspendLayout();
            SuspendLayout();
            // 
            // plateControl1
            // 
            plateControl1.Anchor = AnchorStyles.None;
            plateControl1.BackColor = Color.Transparent;
            plateControl1.Location = new Point(6, 16);
            plateControl1.Margin = new Padding(0);
            plateControl1.Name = "plateControl1";
            plateControl1.Size = new Size(731, 567);
            plateControl1.TabIndex = 0;
            // 
            // label3
            // 
            label3.Anchor = AnchorStyles.Bottom;
            label3.AutoSize = true;
            label3.Font = new Font("Segoe UI", 13.8F, FontStyle.Regular, GraphicsUnit.Point);
            label3.Location = new Point(540, 22);
            label3.Name = "label3";
            label3.Size = new Size(150, 31);
            label3.TabIndex = 1;
            label3.Text = "Объём (мкл):";
            // 
            // label2
            // 
            label2.Anchor = AnchorStyles.Bottom;
            label2.AutoSize = true;
            label2.Font = new Font("Segoe UI", 13.8F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(340, 22);
            label2.Name = "label2";
            label2.Size = new Size(121, 31);
            label2.TabIndex = 1;
            label2.Text = "Жидкость:";
            // 
            // label1
            // 
            label1.Anchor = AnchorStyles.Bottom;
            label1.AutoSize = true;
            label1.Font = new Font("Segoe UI", 13.8F, FontStyle.Regular, GraphicsUnit.Point);
            label1.Location = new Point(83, 22);
            label1.Name = "label1";
            label1.Size = new Size(119, 31);
            label1.TabIndex = 0;
            label1.Text = "Название:";
            // 
            // plateNameBox
            // 
            plateNameBox.Anchor = AnchorStyles.None;
            plateNameBox.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            plateNameBox.Location = new Point(13, 66);
            plateNameBox.Margin = new Padding(3, 4, 3, 4);
            plateNameBox.Name = "plateNameBox";
            plateNameBox.Size = new Size(259, 41);
            plateNameBox.TabIndex = 1;
            // 
            // liquidsComboBox
            // 
            liquidsComboBox.Anchor = AnchorStyles.None;
            liquidsComboBox.BackColor = SystemColors.InactiveCaption;
            liquidsComboBox.DropDownStyle = ComboBoxStyle.DropDownList;
            liquidsComboBox.FlatStyle = FlatStyle.Popup;
            liquidsComboBox.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            liquidsComboBox.FormattingEnabled = true;
            liquidsComboBox.Location = new Point(308, 65);
            liquidsComboBox.Margin = new Padding(3, 4, 3, 4);
            liquidsComboBox.Name = "liquidsComboBox";
            liquidsComboBox.Size = new Size(185, 42);
            liquidsComboBox.TabIndex = 2;
            liquidsComboBox.Click += liquidsComboBox_Click;
            // 
            // volumeBox
            // 
            volumeBox.Anchor = AnchorStyles.None;
            volumeBox.BackColor = SystemColors.Window;
            volumeBox.Font = new Font("Segoe UI", 12F, FontStyle.Regular, GraphicsUnit.Point);
            volumeBox.Location = new Point(546, 66);
            volumeBox.Margin = new Padding(3, 4, 3, 4);
            volumeBox.Maximum = new decimal(new int[] { 10000, 0, 0, 0 });
            volumeBox.Name = "volumeBox";
            volumeBox.Size = new Size(138, 41);
            volumeBox.TabIndex = 3;
            // 
            // cancelButton
            // 
            cancelButton.Anchor = AnchorStyles.None;
            cancelButton.FlatStyle = FlatStyle.Popup;
            cancelButton.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            cancelButton.Location = new Point(558, 137);
            cancelButton.Margin = new Padding(3, 4, 3, 4);
            cancelButton.Name = "cancelButton";
            cancelButton.Size = new Size(114, 45);
            cancelButton.TabIndex = 5;
            cancelButton.Text = "Отменить";
            cancelButton.UseVisualStyleBackColor = true;
            cancelButton.Click += cancelButton_Click;
            // 
            // saveNamesAndLiquidsButton
            // 
            saveNamesAndLiquidsButton.Anchor = AnchorStyles.None;
            saveNamesAndLiquidsButton.FlatStyle = FlatStyle.Popup;
            saveNamesAndLiquidsButton.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            saveNamesAndLiquidsButton.Location = new Point(343, 137);
            saveNamesAndLiquidsButton.Margin = new Padding(3, 4, 3, 4);
            saveNamesAndLiquidsButton.Name = "saveNamesAndLiquidsButton";
            saveNamesAndLiquidsButton.Size = new Size(114, 45);
            saveNamesAndLiquidsButton.TabIndex = 5;
            saveNamesAndLiquidsButton.Text = "Сохранить";
            saveNamesAndLiquidsButton.UseVisualStyleBackColor = true;
            saveNamesAndLiquidsButton.Click += saveNamesAndLiquidsButton_Click;
            // 
            // clearWellsButton
            // 
            clearWellsButton.Anchor = AnchorStyles.None;
            clearWellsButton.FlatStyle = FlatStyle.Popup;
            clearWellsButton.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            clearWellsButton.Location = new Point(58, 137);
            clearWellsButton.Margin = new Padding(3, 4, 3, 4);
            clearWellsButton.Name = "clearWellsButton";
            clearWellsButton.Size = new Size(170, 45);
            clearWellsButton.TabIndex = 4;
            clearWellsButton.Text = "Очистить ячейки";
            clearWellsButton.UseVisualStyleBackColor = true;
            clearWellsButton.Click += clearWellsButton_Click;
            // 
            // saveButton
            // 
            saveButton.Anchor = AnchorStyles.Right;
            saveButton.FlatStyle = FlatStyle.Popup;
            saveButton.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            saveButton.Location = new Point(560, 223);
            saveButton.Margin = new Padding(3, 4, 57, 4);
            saveButton.Name = "saveButton";
            saveButton.Size = new Size(114, 53);
            saveButton.TabIndex = 6;
            saveButton.Text = "Сохранить";
            saveButton.UseVisualStyleBackColor = true;
            saveButton.Click += cancelButton_Click;
            // 
            // tableLayoutPanel
            // 
            tableLayoutPanel.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            tableLayoutPanel.BackColor = Color.Transparent;
            tableLayoutPanel.ColumnCount = 1;
            tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 743F));
            tableLayoutPanel.Controls.Add(plateControl1, 0, 0);
            tableLayoutPanel.Controls.Add(tableLayoutPanel1, 0, 1);
            tableLayoutPanel.Location = new Point(0, 0);
            tableLayoutPanel.Margin = new Padding(0);
            tableLayoutPanel.Name = "tableLayoutPanel";
            tableLayoutPanel.RowCount = 2;
            tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 600F));
            tableLayoutPanel.RowStyles.Add(new RowStyle());
            tableLayoutPanel.Size = new Size(743, 1033);
            tableLayoutPanel.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.Anchor = AnchorStyles.None;
            tableLayoutPanel1.AutoSize = true;
            tableLayoutPanel1.BackColor = Color.Transparent;
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 514F));
            tableLayoutPanel1.Controls.Add(okButton, 0, 2);
            tableLayoutPanel1.Controls.Add(saveButton, 0, 1);
            tableLayoutPanel1.Controls.Add(tableLayoutPanel4, 0, 0);
            tableLayoutPanel1.Location = new Point(6, 616);
            tableLayoutPanel1.Margin = new Padding(0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 3;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 200F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 100F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 100F));
            tableLayoutPanel1.Size = new Size(731, 400);
            tableLayoutPanel1.TabIndex = 1;
            // 
            // okButton
            // 
            okButton.Anchor = AnchorStyles.Right;
            okButton.FlatStyle = FlatStyle.Popup;
            okButton.Font = new Font("Segoe UI", 10.8F, FontStyle.Regular, GraphicsUnit.Point);
            okButton.Location = new Point(560, 323);
            okButton.Margin = new Padding(3, 4, 57, 4);
            okButton.Name = "okButton";
            okButton.Size = new Size(114, 53);
            okButton.TabIndex = 8;
            okButton.Text = "ОК";
            okButton.UseVisualStyleBackColor = true;
            okButton.Click += cancelButton_Click;
            // 
            // tableLayoutPanel4
            // 
            tableLayoutPanel4.Anchor = AnchorStyles.Top;
            tableLayoutPanel4.BackColor = Color.Transparent;
            tableLayoutPanel4.ColumnCount = 3;
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 286F));
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 229F));
            tableLayoutPanel4.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 200F));
            tableLayoutPanel4.Controls.Add(cancelButton, 2, 2);
            tableLayoutPanel4.Controls.Add(volumeBox, 2, 1);
            tableLayoutPanel4.Controls.Add(saveNamesAndLiquidsButton, 1, 2);
            tableLayoutPanel4.Controls.Add(liquidsComboBox, 1, 1);
            tableLayoutPanel4.Controls.Add(clearWellsButton, 0, 2);
            tableLayoutPanel4.Controls.Add(plateNameBox, 0, 1);
            tableLayoutPanel4.Controls.Add(label3, 2, 0);
            tableLayoutPanel4.Controls.Add(label1, 0, 0);
            tableLayoutPanel4.Controls.Add(label2, 1, 0);
            tableLayoutPanel4.Location = new Point(8, 0);
            tableLayoutPanel4.Margin = new Padding(0);
            tableLayoutPanel4.Name = "tableLayoutPanel4";
            tableLayoutPanel4.RowCount = 3;
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Absolute, 53F));
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Absolute, 67F));
            tableLayoutPanel4.RowStyles.Add(new RowStyle(SizeType.Absolute, 80F));
            tableLayoutPanel4.Size = new Size(714, 200);
            tableLayoutPanel4.TabIndex = 7;
            // 
            // PlateWindow
            // 
            AutoScaleDimensions = new SizeF(8F, 20F);
            AutoScaleMode = AutoScaleMode.Font;
            AutoSize = true;
            AutoSizeMode = AutoSizeMode.GrowAndShrink;
            BackColor = SystemColors.InactiveBorder;
            Controls.Add(tableLayoutPanel);
            Margin = new Padding(0);
            Name = "PlateWindow";
            Size = new Size(743, 1033);
            ((System.ComponentModel.ISupportInitialize)volumeBox).EndInit();
            tableLayoutPanel.ResumeLayout(false);
            tableLayoutPanel.PerformLayout();
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel4.ResumeLayout(false);
            tableLayoutPanel4.PerformLayout();
            ResumeLayout(false);
        }

        #endregion
        private PlateControl plateControl1;
        private Label label3;
        private Label label2;
        private Label label1;
        private TextBox plateNameBox;
        private ComboBox liquidsComboBox;
        private NumericUpDown volumeBox;
        private Button clearWellsButton;
        private Button saveNamesAndLiquidsButton;
        private Button cancelButton;
        private Button saveButton;
        private TableLayoutPanel tableLayoutPanel;
        private TableLayoutPanel tableLayoutPanel1;
        private TableLayoutPanel tableLayoutPanel4;
        private Button okButton;
    }
}
