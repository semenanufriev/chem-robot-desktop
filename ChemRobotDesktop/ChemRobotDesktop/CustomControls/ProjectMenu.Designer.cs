﻿namespace ChemRobotDesktop.CustomControls
{
    partial class ProjectMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            ProtocolFileSplitContainer = new SplitContainer();
            FileActionsTableLayoutPanel = new TableLayoutPanel();
            label1 = new Label();
            ImportProtocolButton = new Button();
            ExportProtocolButton = new Button();
            CreateProtocolButton = new Button();
            panel1 = new Panel();
            tableLayoutPanel2 = new TableLayoutPanel();
            label4 = new Label();
            tableLayoutPanel3 = new TableLayoutPanel();
            protocolNameBox = new TextBox();
            authorBox = new TextBox();
            label3 = new Label();
            label2 = new Label();
            tableLayoutPanel5 = new TableLayoutPanel();
            descriptionBox = new TextBox();
            label8 = new Label();
            pictureBox2 = new PictureBox();
            tableLayoutPanel1 = new TableLayoutPanel();
            textBox1 = new TextBox();
            label6 = new Label();
            openFileDialog1 = new OpenFileDialog();
            saveFileDialog1 = new SaveFileDialog();
            ((System.ComponentModel.ISupportInitialize)ProtocolFileSplitContainer).BeginInit();
            ProtocolFileSplitContainer.Panel1.SuspendLayout();
            ProtocolFileSplitContainer.Panel2.SuspendLayout();
            ProtocolFileSplitContainer.SuspendLayout();
            FileActionsTableLayoutPanel.SuspendLayout();
            panel1.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            tableLayoutPanel3.SuspendLayout();
            tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).BeginInit();
            tableLayoutPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // ProtocolFileSplitContainer
            // 
            ProtocolFileSplitContainer.BackColor = SystemColors.ButtonFace;
            ProtocolFileSplitContainer.Dock = DockStyle.Fill;
            ProtocolFileSplitContainer.FixedPanel = FixedPanel.Panel1;
            ProtocolFileSplitContainer.IsSplitterFixed = true;
            ProtocolFileSplitContainer.Location = new Point(0, 0);
            ProtocolFileSplitContainer.Margin = new Padding(0);
            ProtocolFileSplitContainer.Name = "ProtocolFileSplitContainer";
            // 
            // ProtocolFileSplitContainer.Panel1
            // 
            ProtocolFileSplitContainer.Panel1.BackColor = SystemColors.GradientActiveCaption;
            ProtocolFileSplitContainer.Panel1.Controls.Add(FileActionsTableLayoutPanel);
            ProtocolFileSplitContainer.Panel1.RightToLeft = RightToLeft.No;
            ProtocolFileSplitContainer.Panel1MinSize = 200;
            // 
            // ProtocolFileSplitContainer.Panel2
            // 
            ProtocolFileSplitContainer.Panel2.BackColor = SystemColors.ButtonFace;
            ProtocolFileSplitContainer.Panel2.Controls.Add(panel1);
            ProtocolFileSplitContainer.Panel2.RightToLeft = RightToLeft.No;
            ProtocolFileSplitContainer.RightToLeft = RightToLeft.No;
            ProtocolFileSplitContainer.Size = new Size(1205, 626);
            ProtocolFileSplitContainer.SplitterDistance = 250;
            ProtocolFileSplitContainer.SplitterWidth = 1;
            ProtocolFileSplitContainer.TabIndex = 1;
            // 
            // FileActionsTableLayoutPanel
            // 
            FileActionsTableLayoutPanel.BackColor = Color.Transparent;
            FileActionsTableLayoutPanel.ColumnCount = 1;
            FileActionsTableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            FileActionsTableLayoutPanel.Controls.Add(label1, 0, 0);
            FileActionsTableLayoutPanel.Controls.Add(ImportProtocolButton, 0, 4);
            FileActionsTableLayoutPanel.Controls.Add(ExportProtocolButton, 0, 5);
            FileActionsTableLayoutPanel.Controls.Add(CreateProtocolButton, 0, 2);
            FileActionsTableLayoutPanel.Dock = DockStyle.Fill;
            FileActionsTableLayoutPanel.Location = new Point(0, 0);
            FileActionsTableLayoutPanel.Margin = new Padding(0);
            FileActionsTableLayoutPanel.Name = "FileActionsTableLayoutPanel";
            FileActionsTableLayoutPanel.RowCount = 7;
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 40F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 30F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            FileActionsTableLayoutPanel.RowStyles.Add(new RowStyle());
            FileActionsTableLayoutPanel.Size = new Size(250, 626);
            FileActionsTableLayoutPanel.TabIndex = 1;
            // 
            // label1
            // 
            label1.Dock = DockStyle.Fill;
            label1.Font = new Font("Segoe UI", 17F, FontStyle.Bold, GraphicsUnit.Point);
            label1.Location = new Point(3, 3);
            label1.Margin = new Padding(3);
            label1.Name = "label1";
            label1.Size = new Size(244, 34);
            label1.TabIndex = 0;
            label1.Text = "Протокол";
            label1.TextAlign = ContentAlignment.TopCenter;
            // 
            // ImportProtocolButton
            // 
            ImportProtocolButton.Anchor = AnchorStyles.None;
            ImportProtocolButton.BackColor = SystemColors.InactiveCaption;
            ImportProtocolButton.FlatStyle = FlatStyle.Popup;
            ImportProtocolButton.Font = new Font("Segoe UI", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            ImportProtocolButton.Location = new Point(25, 173);
            ImportProtocolButton.Name = "ImportProtocolButton";
            ImportProtocolButton.Size = new Size(200, 44);
            ImportProtocolButton.TabIndex = 2;
            ImportProtocolButton.Text = "Открыть";
            ImportProtocolButton.UseVisualStyleBackColor = false;
            ImportProtocolButton.Click += ImportProtocolButton_Click;
            // 
            // ExportProtocolButton
            // 
            ExportProtocolButton.Anchor = AnchorStyles.None;
            ExportProtocolButton.BackColor = SystemColors.InactiveCaption;
            ExportProtocolButton.FlatStyle = FlatStyle.Popup;
            ExportProtocolButton.Font = new Font("Segoe UI", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            ExportProtocolButton.Location = new Point(25, 223);
            ExportProtocolButton.Name = "ExportProtocolButton";
            ExportProtocolButton.Size = new Size(200, 44);
            ExportProtocolButton.TabIndex = 3;
            ExportProtocolButton.Text = "Сохранить";
            ExportProtocolButton.UseVisualStyleBackColor = false;
            ExportProtocolButton.Click += ExportProtocolButton_Click;
            // 
            // CreateProtocolButton
            // 
            CreateProtocolButton.Anchor = AnchorStyles.None;
            CreateProtocolButton.BackColor = SystemColors.InactiveCaption;
            CreateProtocolButton.FlatStyle = FlatStyle.Popup;
            CreateProtocolButton.Font = new Font("Segoe UI", 16.2F, FontStyle.Regular, GraphicsUnit.Point);
            CreateProtocolButton.Location = new Point(25, 93);
            CreateProtocolButton.Name = "CreateProtocolButton";
            CreateProtocolButton.Size = new Size(200, 44);
            CreateProtocolButton.TabIndex = 1;
            CreateProtocolButton.Text = "Создать";
            CreateProtocolButton.UseVisualStyleBackColor = false;
            CreateProtocolButton.Click += CreateProtocolButton_Click;
            // 
            // panel1
            // 
            panel1.Controls.Add(tableLayoutPanel2);
            panel1.Dock = DockStyle.Fill;
            panel1.Location = new Point(0, 0);
            panel1.Name = "panel1";
            panel1.Size = new Size(954, 626);
            panel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.BackColor = SystemColors.InactiveBorder;
            tableLayoutPanel2.ColumnCount = 1;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.Controls.Add(label4, 0, 0);
            tableLayoutPanel2.Controls.Add(tableLayoutPanel3, 0, 1);
            tableLayoutPanel2.Controls.Add(tableLayoutPanel5, 0, 2);
            tableLayoutPanel2.Controls.Add(pictureBox2, 0, 3);
            tableLayoutPanel2.Dock = DockStyle.Fill;
            tableLayoutPanel2.Location = new Point(0, 0);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 4;
            tableLayoutPanel2.RowStyles.Add(new RowStyle());
            tableLayoutPanel2.RowStyles.Add(new RowStyle());
            tableLayoutPanel2.RowStyles.Add(new RowStyle());
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Absolute, 118F));
            tableLayoutPanel2.Size = new Size(954, 626);
            tableLayoutPanel2.TabIndex = 13;
            // 
            // label4
            // 
            label4.Anchor = AnchorStyles.Left;
            label4.AutoSize = true;
            label4.Font = new Font("Segoe UI", 18F, FontStyle.Regular, GraphicsUnit.Point);
            label4.Location = new Point(3, 0);
            label4.Name = "label4";
            label4.Size = new Size(163, 32);
            label4.TabIndex = 5;
            label4.Text = "Подробности";
            // 
            // tableLayoutPanel3
            // 
            tableLayoutPanel3.AutoSize = true;
            tableLayoutPanel3.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            tableLayoutPanel3.ColumnCount = 2;
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel3.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 50F));
            tableLayoutPanel3.Controls.Add(protocolNameBox, 0, 1);
            tableLayoutPanel3.Controls.Add(authorBox, 0, 1);
            tableLayoutPanel3.Controls.Add(label3, 1, 0);
            tableLayoutPanel3.Controls.Add(label2, 0, 0);
            tableLayoutPanel3.Dock = DockStyle.Fill;
            tableLayoutPanel3.Location = new Point(3, 35);
            tableLayoutPanel3.Name = "tableLayoutPanel3";
            tableLayoutPanel3.RowCount = 2;
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel3.RowStyles.Add(new RowStyle(SizeType.Absolute, 50F));
            tableLayoutPanel3.Size = new Size(948, 70);
            tableLayoutPanel3.TabIndex = 14;
            // 
            // protocolNameBox
            // 
            protocolNameBox.BackColor = SystemColors.InactiveBorder;
            protocolNameBox.Dock = DockStyle.Fill;
            protocolNameBox.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point);
            protocolNameBox.Location = new Point(3, 23);
            protocolNameBox.Name = "protocolNameBox";
            protocolNameBox.Size = new Size(468, 26);
            protocolNameBox.TabIndex = 13;
            protocolNameBox.TextChanged += protocolNameBox_TextChanged;
            // 
            // authorBox
            // 
            authorBox.BackColor = SystemColors.InactiveBorder;
            authorBox.Dock = DockStyle.Fill;
            authorBox.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point);
            authorBox.Location = new Point(477, 23);
            authorBox.Name = "authorBox";
            authorBox.Size = new Size(468, 26);
            authorBox.TabIndex = 7;
            authorBox.TextChanged += authorBox_TextChanged;
            // 
            // label3
            // 
            label3.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label3.AutoSize = true;
            label3.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label3.Location = new Point(477, 0);
            label3.Name = "label3";
            label3.Size = new Size(60, 20);
            label3.TabIndex = 4;
            label3.Text = "Автор:";
            // 
            // label2
            // 
            label2.Anchor = AnchorStyles.Left;
            label2.AutoSize = true;
            label2.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label2.Location = new Point(3, 0);
            label2.Name = "label2";
            label2.Size = new Size(87, 20);
            label2.TabIndex = 3;
            label2.Text = "Название:";
            // 
            // tableLayoutPanel5
            // 
            tableLayoutPanel5.AutoSize = true;
            tableLayoutPanel5.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            tableLayoutPanel5.BackColor = SystemColors.InactiveBorder;
            tableLayoutPanel5.ColumnCount = 1;
            tableLayoutPanel5.ColumnStyles.Add(new ColumnStyle());
            tableLayoutPanel5.Controls.Add(descriptionBox, 0, 1);
            tableLayoutPanel5.Controls.Add(label8, 0, 0);
            tableLayoutPanel5.Dock = DockStyle.Fill;
            tableLayoutPanel5.Location = new Point(3, 111);
            tableLayoutPanel5.Name = "tableLayoutPanel5";
            tableLayoutPanel5.RowCount = 2;
            tableLayoutPanel5.RowStyles.Add(new RowStyle());
            tableLayoutPanel5.RowStyles.Add(new RowStyle());
            tableLayoutPanel5.Size = new Size(948, 52);
            tableLayoutPanel5.TabIndex = 18;
            // 
            // descriptionBox
            // 
            descriptionBox.BackColor = SystemColors.InactiveBorder;
            descriptionBox.Dock = DockStyle.Fill;
            descriptionBox.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point);
            descriptionBox.Location = new Point(3, 23);
            descriptionBox.Name = "descriptionBox";
            descriptionBox.Size = new Size(1019, 26);
            descriptionBox.TabIndex = 14;
            descriptionBox.TextChanged += descriptionBox_TextChanged;
            // 
            // label8
            // 
            label8.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label8.AutoSize = true;
            label8.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label8.Location = new Point(3, 0);
            label8.Name = "label8";
            label8.Size = new Size(87, 20);
            label8.TabIndex = 4;
            label8.Text = "Описание:";
            // 
            // pictureBox2
            // 
            pictureBox2.BackColor = Color.Transparent;
            pictureBox2.Dock = DockStyle.Fill;
            pictureBox2.Image = Properties.Resources.pip_big_clean;
            pictureBox2.InitialImage = null;
            pictureBox2.Location = new Point(0, 166);
            pictureBox2.Margin = new Padding(0);
            pictureBox2.Name = "pictureBox2";
            pictureBox2.Size = new Size(954, 460);
            pictureBox2.SizeMode = PictureBoxSizeMode.Zoom;
            pictureBox2.TabIndex = 17;
            pictureBox2.TabStop = false;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 3;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 30F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 70F));
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, 75F));
            tableLayoutPanel1.Controls.Add(textBox1, 0, 1);
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Absolute, 20F));
            tableLayoutPanel1.Size = new Size(200, 100);
            tableLayoutPanel1.TabIndex = 0;
            // 
            // textBox1
            // 
            textBox1.Dock = DockStyle.Fill;
            textBox1.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point);
            textBox1.Location = new Point(3, 23);
            textBox1.Name = "textBox1";
            textBox1.Size = new Size(31, 26);
            textBox1.TabIndex = 13;
            // 
            // label6
            // 
            label6.Anchor = AnchorStyles.Bottom | AnchorStyles.Left;
            label6.AutoSize = true;
            label6.Font = new Font("Microsoft Sans Serif", 12F, FontStyle.Regular, GraphicsUnit.Point);
            label6.Location = new Point(127, 0);
            label6.Name = "label6";
            label6.Size = new Size(46, 20);
            label6.TabIndex = 12;
            label6.Text = "Color";
            // 
            // ProjectMenu
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            BackColor = SystemColors.InactiveBorder;
            Controls.Add(ProtocolFileSplitContainer);
            Name = "ProjectMenu";
            Size = new Size(1205, 626);
            ProtocolFileSplitContainer.Panel1.ResumeLayout(false);
            ProtocolFileSplitContainer.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)ProtocolFileSplitContainer).EndInit();
            ProtocolFileSplitContainer.ResumeLayout(false);
            FileActionsTableLayoutPanel.ResumeLayout(false);
            panel1.ResumeLayout(false);
            tableLayoutPanel2.ResumeLayout(false);
            tableLayoutPanel2.PerformLayout();
            tableLayoutPanel3.ResumeLayout(false);
            tableLayoutPanel3.PerformLayout();
            tableLayoutPanel5.ResumeLayout(false);
            tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)pictureBox2).EndInit();
            tableLayoutPanel1.ResumeLayout(false);
            tableLayoutPanel1.PerformLayout();
            ResumeLayout(false);
        }

        #endregion

        private SplitContainer ProtocolFileSplitContainer;
        private TableLayoutPanel FileActionsTableLayoutPanel;
        private Label label1;
        private Button ImportProtocolButton;
        private Button ExportProtocolButton;
        private Button CreateProtocolButton;
        private Panel panel1;
        private TableLayoutPanel tableLayoutPanel2;
        private Label label4;
        private Label label8;
        private TableLayoutPanel tableLayoutPanel1;
        private TextBox textBox1;
        private Label label6;
        private OpenFileDialog openFileDialog1;
        private SaveFileDialog saveFileDialog1;
        private TableLayoutPanel tableLayoutPanel5;
        private TextBox descriptionBox;
        private PictureBox pictureBox2;
        private TableLayoutPanel tableLayoutPanel3;
        private TextBox protocolNameBox;
        private TextBox authorBox;
        private Label label3;
        private Label label2;
    }
}
