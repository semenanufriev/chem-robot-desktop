﻿using ChemRobotDesktop.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChemRobotDesktop.CustomControls
{
    public partial class WaitStepMenu : UserControl
    {
        public delegate void Method(ProtocolStep protocolStep);
        public event Method StepCreatedEvent;
        public event Method StepDeletedEvent;
        public event Method StepEditCanceledEvent;
        private ProtocolStepWait protocolStep;
        private int stepId;
        private List<Zone> Zones;

        public WaitStepMenu(int stepId, List<Zone> Zones, ProtocolStep step = null)
        {
            InitializeComponent();
            this.stepId = stepId;
            this.Zones = Zones;
            fillParamsFromStep(step);
        }

        private void fillParamsFromStep(ProtocolStep step)
        {
            if (step is ProtocolStepWait waitStep)
            {
                // Устанавливаем выбранное значение для sourcePlateBox
                int hours, minutes, seconds;
                hours = waitStep.WaitTimeSeconds / 3600;
                minutes = (waitStep.WaitTimeSeconds - hours * 3600) / 60;
                seconds = waitStep.WaitTimeSeconds - hours * 3600 - minutes * 60;
                hoursBox.Value = (decimal)hours;
                minutesBox.Value = (decimal)minutes;
                secondsBox.Value = (decimal)seconds;
                stepId = waitStep.Id;
                this.protocolStep = waitStep;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            int waitTime = (int)(hoursBox.Value * 3600 + minutesBox.Value * 60 + secondsBox.Value);

            protocolStep = new ProtocolStepWait(stepId, waitTime);
            try
            {
                protocolStep.ExecuteStep(Zones);
                StepCreatedEvent.Invoke(protocolStep);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            StepEditCanceledEvent.Invoke(null);
        }

        private void deleteStepButton_Click(object sender, EventArgs e)
        {
            if (protocolStep != null)
            {
                StepDeletedEvent.Invoke(protocolStep);
            }
        }
    }
}
