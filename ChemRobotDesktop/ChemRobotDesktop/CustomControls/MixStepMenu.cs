﻿using ChemRobotDesktop.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WellName = System.String;

namespace ChemRobotDesktop.CustomControls
{
    public partial class MixStepMenu : UserControl
    {
        public delegate void Method(ProtocolStep protocolStep);
        public event Method StepCreatedEvent;
        public event Method StepDeletedEvent;
        public event Method StepEditCanceledEvent;
        private ProtocolStepMix protocolStep;
        private int stepId;
        private List<Zone> Zones;
        List<WellName> selectedSourceWells = new List<WellName>();
        public MixStepMenu(int stepId, List<Zone> Zones, ProtocolStep step = null)
        {
            InitializeComponent();
            this.Zones = Zones;
            this.stepId = stepId;
            sourcePlateBox.Items.Clear();

            foreach (var zone in Zones)
            {
                if (zone.plate != null && (zone.plate.Type == PLATE_TYPE.WELL_PLATE || zone.plate.Type == PLATE_TYPE.RESERVOIR))
                {
                    // Создаем элемент ComboBox, который содержит название Plate и её objectId
                    sourcePlateBox.Items.Add(new KeyValuePair<int, string>(zone.plate.ObjectId, zone.plate.Name));
                }
            }

            // Настройка ComboBox для отображения названий Plate
            sourcePlateBox.DisplayMember = "Value";
            sourcePlateBox.ValueMember = "Key";

            tipChangeModeBox.Items.Add(new KeyValuePair<TIP_CHANGE_MODE, string>(TIP_CHANGE_MODE.ONCE_AT_START, "Один раз в начале этапа"));
            tipChangeModeBox.Items.Add(new KeyValuePair<TIP_CHANGE_MODE, string>(TIP_CHANGE_MODE.NEVER, "Никогда"));
            tipChangeModeBox.Items.Add(new KeyValuePair<TIP_CHANGE_MODE, string>(TIP_CHANGE_MODE.FOR_EVERY_WELL, "Для каждой ячейки"));
            tipChangeModeBox.DisplayMember = "Value";
            tipChangeModeBox.ValueMember = "Key";
            tipChangeModeBox.SelectedIndex = 0;
            fillParamsFromStep(step);
        }


        private void fillParamsFromStep(ProtocolStep step)
        {
            if (step is ProtocolStepMix mixStep)
            {
                // Устанавливаем выбранное значение для sourcePlateBox
                sourcePlateBox.SelectedItem = sourcePlateBox.Items.Cast<KeyValuePair<int, string>>().FirstOrDefault(item => item.Key == mixStep.SourcePlateId);
                selectedSourceWells = mixStep.SourceWells.ToList();
                tipChangeModeBox.SelectedItem = tipChangeModeBox.Items.Cast<KeyValuePair<TIP_CHANGE_MODE, string>>().FirstOrDefault(item => item.Key == mixStep.TipChangeMode);
                // Устанавливаем значения для volumeBox и mixCountBox
                volumeBox.Value = (decimal)mixStep.MixVolume;
                mixCountBox.Value = mixStep.MixCount;
                updateWellsButtonsText();
                stepId = mixStep.Id;
                this.protocolStep = mixStep;
            }
        }


        private void updateWellsButtonsText()
        {
            if (selectedSourceWells.Count > 0)
            {
                selectSourceWellsButton.Text = "Выбраны ячейки (" + selectedSourceWells.Count.ToString() + ")";
            }
            else
            {
                selectSourceWellsButton.Text = "Выберите ячейки";
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {

            int sourcePlateId = -1;
            if (sourcePlateBox.SelectedItem is KeyValuePair<int, string> selectedSourcePair)
            {
                sourcePlateId = selectedSourcePair.Key;
            }


            TIP_CHANGE_MODE tipChangeMode = TIP_CHANGE_MODE.NEVER;
            if (tipChangeModeBox.SelectedItem is KeyValuePair<TIP_CHANGE_MODE, string> tipChangeModePair)
            {

                tipChangeMode = tipChangeModePair.Key;
            }


            float MixVolume = (float)volumeBox.Value;
            int MixCount = (int)mixCountBox.Value;
            protocolStep = new ProtocolStepMix(stepId, sourcePlateId, selectedSourceWells, MixCount, MixVolume, tipChangeMode);
            try
            {
                protocolStep.ExecuteStep(Zones);
                StepCreatedEvent.Invoke(protocolStep);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            StepEditCanceledEvent.Invoke(null);
        }

        private void selectSourceWellsButton_Click(object sender, EventArgs e)
        {
            PlateWindow plateWindow = new PlateWindow();
            if (sourcePlateBox.SelectedItem is KeyValuePair<int, string> selectedPair)
            {

                int selectedPlateId = selectedPair.Key;
                foreach (var zone in Zones)
                {
                    if (zone.plate != null && zone.plate.ObjectId == selectedPlateId)
                    {
                        selectedSourceWells = plateWindow.showWellsSelectionMenu(zone.plate, selectedSourceWells);

                        updateWellsButtonsText();
                        break;

                    }
                }
            }
        }

        private void deleteStepButton_Click(object sender, EventArgs e)
        {
            if (protocolStep != null)
            {
                StepDeletedEvent.Invoke(protocolStep);
            }
        }

        private void tipChangeModeBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
