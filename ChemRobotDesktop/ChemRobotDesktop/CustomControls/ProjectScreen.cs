﻿using ChemRobotDesktop.Classes;


namespace ChemRobotDesktop.CustomControls
{
    public partial class ProjectScreen : UserControl
    {
        public Project project;
        public delegate void ProjectMenuWindowShowed();
        public event ProjectMenuWindowShowed projectMenuWindowShowed;
        public delegate void Method(Project project);
        public event Method ProjectCreated;


        public ProjectScreen(ref Project project)
        {
            InitializeComponent();
            this.project = project;
        }


        public void showInWindow()
        {
            projectMenuWindowShowed?.Invoke();
            Control control = this;
            Form tmpForm = new Form();
            tmpForm.ControlBox = false;
            tmpForm.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            tmpForm.StartPosition = FormStartPosition.CenterParent;
            tmpForm.BackColor = SystemColors.InactiveBorder;


            control.Show();

            control.Location = new Point(0, 0);
            int padding = 10;
            control.Anchor = AnchorStyles.Top;
            control.Padding = new Padding(padding, padding, 0, padding);
            tmpForm.ClientSize = control.Size;
            tmpForm.AutoSize = true;
            tmpForm.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            tmpForm.MaximumSize = control.Size + new Size(SystemInformation.VerticalScrollBarWidth * 1, 20);
            tmpForm.MinimumSize = control.Size + new Size(SystemInformation.VerticalScrollBarWidth * 1, 0);
            tmpForm.AutoScroll = true;

            tmpForm.FormClosed += (sender, e) => tmpForm.Controls.Remove(control);

            tmpForm.Controls.Add(control);

            tmpForm.FormClosed += (sender, e) =>
            {
                tmpForm.Controls.Remove(control);
            };

            tmpForm.Load += (sender, e) => tmpForm.BringToFront();

            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
            timer.Interval = 5; // период проверки в миллисекундах
            timer.Tick += (sender, e) =>
            {
                if (control.IsDisposed)
                {
                    tmpForm.Close();
                    timer.Stop();
                    return;
                }
                if (!control.Visible)
                {
                    tmpForm.Close();
                    tmpForm.Controls.Remove(control);
                    timer.Stop();
                }
            };
            timer.Start();
            tmpForm.ShowDialog();
            timer.Dispose();

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void okButton_Click(object sender, EventArgs e)
        {
            bool saved = saveProject();
            if (saved)
            {
                this.Hide();
                ProjectCreated?.Invoke(project);

            }
        }


        public bool saveProject()
        {
            saveFileDialog1.Filter = "Protocol files (*.robot)|*.robot|Все файлы (*.*)|*.*";
            string projectsDirectory = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"Projects"));
            if (!Directory.Exists(projectsDirectory))
            {
                Directory.CreateDirectory(projectsDirectory);
            }

            saveFileDialog1.DefaultExt = "robot";
            saveFileDialog1.FileName = projectName.Text + "." + saveFileDialog1.DefaultExt;
            saveFileDialog1.InitialDirectory = projectsDirectory;
            DialogResult result = saveFileDialog1.ShowDialog();

            string file = saveFileDialog1.FileName;
            if (result == DialogResult.OK)
            {
                project.ClearData();
                project.Name = projectName.Text;
                project.Author = projectAuthor.Text;
                project.Description = projectDescription.Text;
                project.SaveToFile(file);
                saveFileDialog1.FileName = "";
                return true;
            }
            return false;

        }

    }
}