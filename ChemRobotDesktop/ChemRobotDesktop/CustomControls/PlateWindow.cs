﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChemRobotDesktop.Classes;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;
using WellName = System.String;

namespace ChemRobotDesktop.CustomControls
{
    public partial class PlateWindow : UserControl
    {
        public List<Liquid> liquids = new List<Liquid>();
        //private Plate plate;
        public PlateWindow()
        {
            InitializeComponent();
            //tableLayoutPanel.AutoSize = true;
            //tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100f));
            //tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.AutoSize));
            for (int row = 1; row <= tableLayoutPanel.RowCount; row++)
            {
                //tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 0));
            }
            //showNamesAndLiquidsButtons();
        }

        public Plate showNamesAndLiquidsMenu(ref Plate plate, ref List<Liquid> liquids)
        {
            //plateControl1.plate = recivedPlate.DeepCopy();
            showNamesAndLiquidsButtons();
            //showSelectWellsButtons();
            //showViewLiquidsButtons();
            this.liquids = liquids;
            List<KeyValuePair<int, string>> items = new List<KeyValuePair<int, string>>();

            foreach (var liquid in liquids)
            {
                items.Add(new KeyValuePair<int, string>(liquid.Id, liquid.Name));
            }
            liquidsComboBox.Items.Clear();

            // Устанавливаем источник данных
            liquidsComboBox.DataSource = items;

            // Указываем, что отображаемым свойством является 'Value' из KeyValuePair
            liquidsComboBox.DisplayMember = "Value";

            // Устанавливаем начальный индекс (нет выбранного элемента)
            liquidsComboBox.SelectedIndex = -1;

            // Подписываемся на событие изменения выбранного элемента, если нужно
            //liquidsComboBox.SelectedIndexChanged += new EventHandler(yourEventHandler);

            plateNameBox.Text = plate.Name;

            plateControl1.plate = plate.DeepCopy();
            plateControl1.init();
            //plate = null;
            showInWindow();
            return plateControl1.plate;

        }


        public List<WellName> showWellsSelectionMenu(Plate plate, List<WellName> selectedWells)
        {

            showSelectWellsButtons();
            //showViewLiquidsButtons();



            //plateNameBox.Text = plate.Name;

            plateControl1.plate = plate.DeepCopy();
            plateControl1.init(selectedWells);
            //plateControl1.SelectedWellNames = selectedWells;
            //plate = null;
            this.Visible = true;
            showInWindow();
            return plateControl1.SelectedWellNames;

        }


        public void showViewLiquidsMenu(Plate plate)
        {
            showViewLiquidsButtons();
            //showSelectWellsButtons();
            //showViewLiquidsButtons();



            //plateNameBox.Text = plate.Name;

            plateControl1.plate = plate.DeepCopy();
            plateControl1.init();
            //plateControl1.SelectedWellNames = selectedWells;
            //plate = null;
            this.Visible = true;
            showInWindow();
            return;

        }


        public void showInWindow()
        {

            Control control = this;
            Form tmpForm = new Form();

            tmpForm.ControlBox = false;
            tmpForm.FormBorderStyle = FormBorderStyle.FixedToolWindow;
            tmpForm.StartPosition = FormStartPosition.CenterParent;
            control.Anchor = AnchorStyles.Top;
            tmpForm.ClientSize = tableLayoutPanel.Size;

            tmpForm.AutoScroll = false;

            tmpForm.FormClosed += (sender, e) => tmpForm.Controls.Remove(control);

            tmpForm.Controls.Add(control);

            tmpForm.Load += (sender, e) => tmpForm.BringToFront();
            System.Windows.Forms.Timer timer = new System.Windows.Forms.Timer();
            timer.Interval = 5; // период проверки в миллисекундах
            timer.Tick += (sender, e) =>
            {
                if (control.IsDisposed)
                {
                    tmpForm.Close();
                    timer.Stop();
                    return;
                }
                if (!control.Visible)
                {
                    tmpForm.Close();
                    tmpForm.Controls.Remove(control);
                    //tmpForm.Controls.Clear();
                    //control.Dispose();
                    timer.Stop();
                }
            };
            timer.Start();
            //control.Hide();
            tmpForm.ShowDialog();
            timer.Dispose();

        }

        private void saveNamesAndLiquidsButton_Click(object sender, EventArgs e)
        {

            plateControl1.plate.Name = plateNameBox.Text;


            if (plateControl1.SelectedWellNames.Count > 0)
            {
                if (volumeBox.Value > 0)
                {
                    if (liquidsComboBox.SelectedIndex != -1)
                    {
                        foreach (var WellName in plateControl1.SelectedWellNames)
                        {
                            var selectedLiquid = liquids[liquidsComboBox.SelectedIndex];
                            plateControl1.plate.Wells[WellName].addLiquid(ref selectedLiquid, (float)volumeBox.Value);
                        }
                    }
                }

            }
            plateControl1.updateImage();
            this.Hide();

        }



        private void showNamesAndLiquidsButtons()
        {
            tableLayoutPanel4.Visible = true;
            okButton.Visible = false;
            saveButton.Visible = false;
            tableLayoutPanel4.Show();
            tableLayoutPanel1.AutoSize = true;
            tableLayoutPanel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            tableLayoutPanel1.RowStyles[0].SizeType = SizeType.Absolute;
            tableLayoutPanel1.RowStyles[0].Height = 150;
            tableLayoutPanel1.RowStyles[1].SizeType = SizeType.Absolute;
            tableLayoutPanel1.RowStyles[1].Height = 0;
            tableLayoutPanel1.RowStyles[2].SizeType = SizeType.Absolute;
            tableLayoutPanel1.RowStyles[2].Height = 0;
            tableLayoutPanel.RowStyles[1].SizeType = SizeType.Absolute;
            tableLayoutPanel.Height = (int)(tableLayoutPanel.RowStyles[0].Height + tableLayoutPanel1.RowStyles[0].Height);
        }

        private void showSelectWellsButtons()
        {
            //tableLayoutPanel4.Hide();
            tableLayoutPanel4.Visible = false;
            okButton.Visible = false;
            saveButton.Visible = true;
            tableLayoutPanel1.AutoSize = true;
            tableLayoutPanel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            tableLayoutPanel1.RowStyles[0].SizeType = SizeType.Absolute;
            tableLayoutPanel1.RowStyles[0].Height = 0;
            tableLayoutPanel1.RowStyles[1].SizeType = SizeType.Absolute;
            tableLayoutPanel1.RowStyles[1].Height = 75;
            tableLayoutPanel1.RowStyles[2].SizeType = SizeType.Absolute;
            tableLayoutPanel1.RowStyles[2].Height = 0;
            tableLayoutPanel.RowStyles[1].SizeType = SizeType.Absolute;
            tableLayoutPanel.Height = (int)(tableLayoutPanel.RowStyles[0].Height + tableLayoutPanel1.RowStyles[1].Height);
        }

        private void showViewLiquidsButtons()
        {
            tableLayoutPanel4.Visible = false;
            saveButton.Visible = false;
            okButton.Visible = true;
            tableLayoutPanel1.AutoSize = true;
            tableLayoutPanel1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            tableLayoutPanel1.RowStyles[0].SizeType = SizeType.Absolute;
            tableLayoutPanel1.RowStyles[0].Height = 0;
            tableLayoutPanel1.RowStyles[1].SizeType = SizeType.Absolute;
            tableLayoutPanel1.RowStyles[1].Height = 0;
            tableLayoutPanel1.RowStyles[2].SizeType = SizeType.Absolute;
            tableLayoutPanel1.RowStyles[2].Height = 75;
            tableLayoutPanel.RowStyles[1].SizeType = SizeType.Absolute;
            tableLayoutPanel.Height = (int)(tableLayoutPanel.RowStyles[0].Height + tableLayoutPanel1.RowStyles[2].Height);
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            this.Hide();
            plateControl1.plate = null;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void clearWellsButton_Click(object sender, EventArgs e)
        {

            plateControl1.plate.clearWells(
            plateControl1.SelectedWellNames);
            plateControl1.SelectedWellNames.Clear();
            plateControl1.updateImage();

        }

        private void liquidsComboBox_Click(object sender, EventArgs e)
        {
            liquidsComboBox.DroppedDown = true;
        }
    }
}
