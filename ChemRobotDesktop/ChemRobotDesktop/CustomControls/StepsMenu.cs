﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ChemRobotDesktop.Classes;

namespace ChemRobotDesktop.CustomControls
{
    public partial class StepsMenu : UserControl
    {
        public delegate void Method(STEP_TYPE type, ProtocolStep step);
        public delegate void Method_2(int currentStep);
        public event Method AddStepEvent;
        public event Method EditStepEvent;
        public event Method_2 StepSelectedEvent;
        List<ProtocolStep> ProtocolSteps;
        public enum INSTRUCTION_TYPE : int
        {
            TRANSFER = 0,   //  0 
            MIX,            //  1 
            DELAY           //  2

        };



        public StepsMenu()
        {
            InitializeComponent();
            List<KeyValuePair<int, string>> items = new List<KeyValuePair<int, string>>();
            items.Add(new KeyValuePair<int, string>((int)INSTRUCTION_TYPE.TRANSFER, "Переместить"));
            items.Add(new KeyValuePair<int, string>((int)INSTRUCTION_TYPE.MIX, "Смешать"));
            items.Add(new KeyValuePair<int, string>((int)INSTRUCTION_TYPE.DELAY, "Ожидание"));

            stepTypesBox.DataSource = items;
            stepTypesBox.DisplayMember = "Value";
            stepTypesBox.SelectedIndex = -1;
            stepTypesBox.SelectedIndexChanged += stepTypesBox_SelectedIndexChanged;

        }



        List<Button> stepsButtons = new List<Button>();
        List<PictureBox> editButtons = new List<PictureBox>();
        //List<InstructionStep> instructionSteps = new List<InstructionStep>();
        int steps = 0;
        int activeStep = -1;
        Color passiveCellColor = Color.LightBlue;
        Color activeCellColor = Color.CadetBlue;
        Color errorCellColor = Color.Orange;

        List<int> incorrectSteps = new List<int>();


        public void init(ref List<ProtocolStep> ProtocolSteps)
        {
            startingStateButton.Click += startingStateButton_Click;
            startingStateButton.Enabled = true;

            startingStateButton.PerformClick();
            this.ProtocolSteps = ProtocolSteps;
            activeStep = -1;
            updateStepsList(activeStep);
            updateButtonsColor();
            stepTypesBox.SelectedIndex = -1;

        }





        public void updateStepsList(int activeStep = 0, List<Zone> zones = null)
        {
            incorrectSteps.Clear();
            int rowHeight = 50; // задаем высоту строки
            int rowWidth = 150;
            float fontSize = 12f;


            stepsButtons.Clear();
            editButtons.Clear();

            stepstPanel.Controls.Clear();
            stepstPanel.RowCount = ProtocolSteps.Count;
            stepstPanel.ColumnCount = 2;
            Color passiveCellColor = Color.LightBlue;
            Font font = new Font("Segoe UI", fontSize, FontStyle.Regular, GraphicsUnit.Point);







            for (int i = 0; i < ProtocolSteps.Count; i++)
            {

                Button tmp = new Button();
                tmp.Tag = ProtocolSteps[i];
                switch (ProtocolSteps[i].Type)
                {
                    case STEP_TYPE.TRANSFER:
                        tmp.Text = "Перемещение";
                        break;
                    case STEP_TYPE.MIX:
                        tmp.Text = "Смешивание";
                        break;
                    case STEP_TYPE.WAIT:
                        tmp.Text = "Ожидание";
                        break;
                }
                tmp.TextAlign = ContentAlignment.MiddleCenter;
                tmp.Dock = DockStyle.Fill;
                tmp.BackColor = passiveCellColor;

                //Fix it later (Check errors in step)
                if (zones != null)
                {

                    try
                    {
                        zones = ProtocolSteps[i].ExecuteStep(zones);
                    }
                    catch (Exception ex)
                    {
                        incorrectSteps.Add(i);
                        tmp.BackColor = errorCellColor;
                    }
                }



                tmp.FlatStyle = FlatStyle.Popup;
                tmp.FlatAppearance.BorderSize = 0;
                tmp.Font = font;


                ToolTip tmpTip = new ToolTip();
                //toolTip1.SetToolTip(tmp, instructionSteps[i].getName(true));
                stepstPanel.Controls.Add(tmp, 0, i);
                stepsButtons.Add(tmp);

                PictureBox tmpEdit = new PictureBox();
                tmpEdit.Click += editStep;
                tmpEdit.Size = new Size(rowHeight, rowHeight);
                tmpEdit.SizeMode = PictureBoxSizeMode.StretchImage;
                tmpEdit.Image = Properties.Resources.settings_icon;
                tmpEdit.Cursor = System.Windows.Forms.Cursors.Hand;
                tmpEdit.Tag = tmp;
                editButtons.Add(tmpEdit);
                stepstPanel.Controls.Add(tmpEdit, 1, i);



            }



            //stepstPanel.Controls.Add(new PictureBox() { BackColor = Color.Orange /*robot.liquids[i].color*/ }, 1, robot.instructionSteps.Count + 1);
            //-----------------------------------------------------



            int totalH = 0;
            for (int i = 0; i < stepstPanel.RowCount; i++)
            {
                stepstPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, rowHeight));
                totalH += rowHeight;
            }
            stepstPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, rowHeight));
            stepstPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Absolute, rowWidth));
            stepstPanel.Width = rowWidth + rowHeight;
            stepstPanel.Height = totalH;

            subscribe2Actions();

            if (activeStep >= 0)
            {
                if (activeStep == stepsButtons.Count)
                {
                    finalStateButton.PerformClick();
                }
                else
                {
                    stepsButtons[activeStep].PerformClick();
                }
            }
            else
            {
                startingStateButton.PerformClick();
            }


        }


        private void subscribe2Actions()
        {
            for (int i = 0; i < stepsButtons.Count; i++)
            {
                Button B = stepsButtons[i];
                B.Click += stepButton_Click;
                B.Click += (sender, e) =>
                {
                    ProtocolStep step = (ProtocolStep)B.Tag;
                };

            }


            foreach (PictureBox B in editButtons)
            {
                B.Click += (sender, e) =>
                {
                    Button tmp = B.Tag as Button;
                    tmp.PerformClick();

                    //lightStepZones(-1);

                    //robot.time2EditStepClick();
                };
            }
        }




        public void editStep(object sender, EventArgs e)
        {
            PictureBox b = sender as PictureBox;
            ProtocolStep step = (ProtocolStep)((Button)b.Tag).Tag;
            EditStepEvent.Invoke(step.Type, step);
        }

        private void stepButton_Click(object sender, EventArgs e)
        {



            Button button = sender as Button;
            button.BackColor = activeCellColor;
            int prevActiveStep = activeStep;
            activeStep = stepsButtons.FindLastIndex(b => b == button);
            updateButtonsColor();
            allowAddingSteps();
            StepSelectedEvent.Invoke(activeStep);


        }


        private void editButton_Click(object sender, EventArgs e)
        {




        }


        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ToolStripMenuItem M = sender as ToolStripMenuItem;
            Button B = (Button)M.Tag;
            ProtocolStep delStep = (ProtocolStep)B.Tag;
            int delStepIndex = ProtocolSteps.FindLastIndex(b => b == delStep);
            if (activeStep >= delStepIndex)
            {
                activeStep--;
            }

        }

        private void stepTypesBox_Click(object sender, EventArgs e)
        {
            ComboBox tmp = sender as ComboBox;
            if (activeStep != ProtocolSteps.Count)
            {
                AllowDrop = true;

                tmp.DroppedDown = true;
            }
            else
            {
                tmp.AllowDrop = false;
            }
        }

        private void checkAddStepPossibility()
        {
            if (activeStep != ProtocolSteps.Count)
            {
                stepTypesBox.Show();
                stepTypesBox.DroppedDown = false;
                addStepButton.Show();
                //showStepsListButton.Show();

                //stepTypesBox.AllowDrop = true;
                //stepTypesBox.DropDownStyle = ComboBoxStyle.DropDownList;
                //stepTypesBox.DropDown -= new EventHandler(comboBox1_DropDown);

            }
            else
            {
                stepTypesBox.Hide();
                stepTypesBox.DroppedDown = false;
                addStepButton.Hide();
                //showStepsListButton.Hide();
                //stepTypesBox.DropDownStyle = ComboBoxStyle.Simple;
                //stepTypesBox.AllowDrop = false;
            }
        }

        private void stepTypesBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (stepTypesBox.SelectedIndex >= 0)
            {
                var selectedItem = (KeyValuePair<int, string>)stepTypesBox.SelectedItem;

                AddStepEvent?.Invoke((STEP_TYPE)selectedItem.Key, null);
            }
        }

        private void showStepsListButton_Click(object sender, EventArgs e)
        {


            if (stepTypesBox.DroppedDown)
            {

                stepTypesBox.DroppedDown = false;
            }
            else
            {
                stepTypesBox.DroppedDown = true;
            }
        }

        private void allowAddingSteps()
        {
            stepTypesBox.Show();
            stepTypesBox.DroppedDown = false;
            addStepButton.Show();
        }

        private void prohibitAddingSteps()
        {
            stepTypesBox.Hide();
            stepTypesBox.DroppedDown = false;
            addStepButton.Hide();
        }

        private void startingStateButton_Click(object sender, EventArgs e)
        {
            allowAddingSteps();
            activeStep = -1;
            updateButtonsColor();
            StepSelectedEvent.Invoke(activeStep);
        }

        private void finalStateButton_Click(object sender, EventArgs e)
        {
            prohibitAddingSteps();
            activeStep = ProtocolSteps.Count();
            updateButtonsColor();
            StepSelectedEvent.Invoke(activeStep);
        }

        private void updateButtonsColor()
        {

            startingStateButton.BackColor = passiveCellColor;
            finalStateButton.BackColor = passiveCellColor;
            for (int i = 0; i < ProtocolSteps.Count; i++)
            {
                stepsButtons[i].BackColor = passiveCellColor;
                if (i == activeStep)
                {
                    stepsButtons[i].BackColor = activeCellColor;
                }
            }

            if (activeStep == -1)
            {
                startingStateButton.BackColor = activeCellColor;

            }
            if (activeStep == ProtocolSteps.Count)
            {
                finalStateButton.BackColor = activeCellColor;

            }

            for (int i = 0; i < incorrectSteps.Count; i++)
            {
                stepsButtons[incorrectSteps[i]].BackColor = errorCellColor;
            }

        }

        public bool checkForErrors(List<Zone> zones)
        {
            for (int i = 0; i < ProtocolSteps.Count; i++)
            {
                try
                {
                    zones = ProtocolSteps[i].ExecuteStep(zones);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    return false;
                }
            }
            return true;
        }

        private void tableLayoutPanel1_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
