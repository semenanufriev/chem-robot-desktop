﻿using ChemRobotDesktop.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ChemRobotDesktop.CustomControls
{
    public partial class LabwareEditor : UserControl
    {
        private Dictionary<int, Plate> plates = new Dictionary<int, Plate>();
        private PlatesList platesList;


        public LabwareEditor()
        {

            InitializeComponent();
            this.plateParamsMenu1.PlateParamChanged += new EventHandler(updatePlateImage);
            this.plateParamsMenu1.PlateParamChangedMethod(null, null);
            hideEditor(null, null);
            initPlateTypeComboBox();

        }

        public void init(ref PlatesList plates)
        {
            this.platesList = plates;

        }





        private void initPlateTypeComboBox()
        {
            List<KeyValuePair<int, string>> items = new List<KeyValuePair<int, string>>();
            items.Add(new KeyValuePair<int, string>((int)PLATE_TYPE.WELL_PLATE, "Пробирки"));
            items.Add(new KeyValuePair<int, string>((int)PLATE_TYPE.TIP_RACK, "Носики"));
            items.Add(new KeyValuePair<int, string>((int)PLATE_TYPE.RESERVOIR, "Резервуар"));
            plateTypeComboBox.Items.Clear();
            plateTypeComboBox.DataSource = items;
            plateTypeComboBox.DisplayMember = "Value";
            plateTypeComboBox.SelectedIndex = -1;
            plateTypeComboBox.SelectedIndexChanged += new EventHandler(showPlateEditor);
        }

        private void showEditor(object sender, EventArgs e)
        {


            //comboBox1.
            splitContainer2.Panel1Collapsed = false;
            tableLayoutPanel1.RowStyles[0].SizeType = SizeType.Percent;
            tableLayoutPanel1.RowStyles[0].Height = 50;
            tableLayoutPanel1.RowStyles[1].SizeType = SizeType.Percent;
            tableLayoutPanel1.RowStyles[1].Height = 50;
        }

        private void hideEditor(object sender, EventArgs e)
        {
            splitContainer2.Panel1Collapsed = true;
            tableLayoutPanel1.RowStyles[0].SizeType = SizeType.Percent;
            tableLayoutPanel1.RowStyles[0].Height = 0;
            tableLayoutPanel1.RowStyles[1].SizeType = SizeType.Percent;
            tableLayoutPanel1.RowStyles[1].Height = 100;
        }



        private void updatePlateImage(object sender, EventArgs e)
        {

            PlatePictureBox.Image = this.plateParamsMenu1.plate.getImage();
        }

        private void plateTypeComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void showPlateEditor(object sender, EventArgs e)
        {
            ComboBox stepTypesBox = sender as ComboBox;


            if (stepTypesBox.SelectedIndex != -1)
            {
                KeyValuePair<int, string> selectedValue = (KeyValuePair<int, string>)plateTypeComboBox.SelectedValue;
                plateTypeLabel.Text = selectedValue.Value;
                int Id = 0;
                if (platesList.plates.Count > 0)
                {
                    Id = platesList.plates.Keys.Max();
                }
                Id += 1;
                plateParamsMenu1.createNewEditor((PLATE_TYPE)selectedValue.Key, Id);
                showEditor(null, null);
            }

        }

        private void CreateProtocolButton_Click(object sender, EventArgs e)
        {
            if (plateTypeComboBox.DroppedDown)
            {
                plateTypeComboBox.DroppedDown = false;
            }
            else
            {
                plateTypeComboBox.DroppedDown = true;
            }
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            string labwareDirectory = Path.GetFullPath(Path.Combine(Directory.GetCurrentDirectory(), @"../../../Labware"));
            plateParamsMenu1.savePlate(labwareDirectory);
            hideEditor(null, null);
        }

        private void editlButton_Click(object sender, EventArgs e)
        {
            platesList.plateSelected += PlateSelected;
            platesList.showInWindow();

        }

        void PlateSelected(int? id)
        {
            if (id.HasValue)
            {
                plateParamsMenu1.fillEditorWithPlateData(platesList.plates[id.Value]);
                EventArgs args = new EventArgs();
                args.Equals(id.Value);
                showEditor(null, args);
            }

            platesList.plateSelected -= PlateSelected;
        }

        private void FileActionsTableLayoutPanel_Paint(object sender, PaintEventArgs e)
        {

        }

        private void plateParamsMenu1_Load(object sender, EventArgs e)
        {

        }
    }
}
