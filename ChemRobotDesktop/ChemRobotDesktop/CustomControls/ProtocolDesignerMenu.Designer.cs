﻿namespace ChemRobotDesktop.CustomControls
{
    partial class ProtocolDesignerMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            splitContainer1 = new SplitContainer();
            tableLayoutPanel2 = new TableLayoutPanel();
            stepsMenu1 = new StepsMenu();
            tableLayoutPanel1 = new TableLayoutPanel();
            workspace1 = new Workspace();
            connectionControl1 = new ConnectionControl();
            ((System.ComponentModel.ISupportInitialize)splitContainer1).BeginInit();
            splitContainer1.Panel1.SuspendLayout();
            splitContainer1.Panel2.SuspendLayout();
            splitContainer1.SuspendLayout();
            tableLayoutPanel2.SuspendLayout();
            tableLayoutPanel1.SuspendLayout();
            SuspendLayout();
            // 
            // splitContainer1
            // 
            splitContainer1.Dock = DockStyle.Fill;
            splitContainer1.FixedPanel = FixedPanel.Panel1;
            splitContainer1.IsSplitterFixed = true;
            splitContainer1.Location = new Point(0, 0);
            splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            splitContainer1.Panel1.AutoScroll = true;
            splitContainer1.Panel1.BackColor = SystemColors.GradientActiveCaption;
            splitContainer1.Panel1.Controls.Add(tableLayoutPanel2);
            // 
            // splitContainer1.Panel2
            // 
            splitContainer1.Panel2.BackColor = SystemColors.InactiveBorder;
            splitContainer1.Panel2.Controls.Add(tableLayoutPanel1);
            splitContainer1.Size = new Size(1369, 696);
            splitContainer1.SplitterDistance = 250;
            splitContainer1.SplitterWidth = 1;
            splitContainer1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            tableLayoutPanel2.AutoScroll = true;
            tableLayoutPanel2.CellBorderStyle = TableLayoutPanelCellBorderStyle.Single;
            tableLayoutPanel2.ColumnCount = 1;
            tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.Controls.Add(stepsMenu1, 0, 0);
            tableLayoutPanel2.Controls.Add(connectionControl1, 0, 1);
            tableLayoutPanel2.Dock = DockStyle.Fill;
            tableLayoutPanel2.Location = new Point(0, 0);
            tableLayoutPanel2.Name = "tableLayoutPanel2";
            tableLayoutPanel2.RowCount = 2;
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Absolute, 150F));
            tableLayoutPanel2.Size = new Size(250, 696);
            tableLayoutPanel2.TabIndex = 1;
            // 
            // stepsMenu1
            // 
            stepsMenu1.BackColor = SystemColors.GradientActiveCaption;
            stepsMenu1.Dock = DockStyle.Fill;
            stepsMenu1.Location = new Point(4, 4);
            stepsMenu1.Name = "stepsMenu1";
            stepsMenu1.Size = new Size(225, 616);
            stepsMenu1.TabIndex = 2;
            // 
            // tableLayoutPanel1
            // 
            tableLayoutPanel1.ColumnCount = 1;
            tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Controls.Add(workspace1, 0, 1);
            tableLayoutPanel1.Dock = DockStyle.Fill;
            tableLayoutPanel1.Location = new Point(0, 0);
            tableLayoutPanel1.Name = "tableLayoutPanel1";
            tableLayoutPanel1.RowCount = 2;
            tableLayoutPanel1.RowStyles.Add(new RowStyle());
            tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
            tableLayoutPanel1.Size = new Size(1118, 696);
            tableLayoutPanel1.TabIndex = 1;
            // 
            // workspace1
            // 
            workspace1.currentZones = null;
            workspace1.Dock = DockStyle.Fill;
            workspace1.Location = new Point(0, 0);
            workspace1.Margin = new Padding(0);
            workspace1.Name = "workspace1";
            workspace1.protocolSteps = null;
            workspace1.RightToLeft = RightToLeft.No;
            workspace1.Size = new Size(1118, 696);
            workspace1.startingZones = null;
            workspace1.TabIndex = 0;
            // 
            // connectionControl1
            // 
            connectionControl1.Anchor = AnchorStyles.None;
            connectionControl1.AutoSize = true;
            connectionControl1.AutoSizeMode = AutoSizeMode.GrowAndShrink;
            connectionControl1.Location = new Point(16, 636);
            connectionControl1.Margin = new Padding(0);
            connectionControl1.Name = "connectionControl1";
            connectionControl1.Size = new Size(200, 125);
            connectionControl1.TabIndex = 3;
            // 
            // ProtocolDesignerMenu
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            Controls.Add(splitContainer1);
            Name = "ProtocolDesignerMenu";
            Size = new Size(1369, 696);
            splitContainer1.Panel1.ResumeLayout(false);
            splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)splitContainer1).EndInit();
            splitContainer1.ResumeLayout(false);
            tableLayoutPanel2.ResumeLayout(false);
            tableLayoutPanel2.PerformLayout();
            tableLayoutPanel1.ResumeLayout(false);
            ResumeLayout(false);
        }

        #endregion

        private SplitContainer splitContainer1;
        public Workspace workspace1;
        private TableLayoutPanel tableLayoutPanel1;
        private TableLayoutPanel tableLayoutPanel2;
        private StepsMenu stepsMenu1;
        private ConnectionControl connectionControl1;
    }
}
