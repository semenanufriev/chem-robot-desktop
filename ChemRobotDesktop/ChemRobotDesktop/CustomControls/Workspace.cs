﻿using ChemRobotDesktop.Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.Common;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
//using System.Windows.Forms.DataVisualization.Charting;
using static ChemRobotDesktop.CustomControls.PlatesList;
using static ChemRobotDesktop.CustomControls.Workspace;
using static System.Windows.Forms.Design.AxImporter;
using static System.Windows.Forms.VisualStyles.VisualStyleElement.StartPanel;

namespace ChemRobotDesktop.CustomControls
{


    public partial class Workspace : UserControl
    {
        public delegate void Method(int step, List<Zone> zones);
        public event Method StartingStateChanged;

        TableLayoutPanel tableLayoutPanel;

        public struct ButtonAction
        {

            public string text;
            public Action<int> Action;
            public ButtonAction(string text, Action<int> action)
            {
                this.text = text;
                Action = action;
            }
        }


        List<ButtonAction> EmptyPlateButtonActions = new List<ButtonAction>();
        List<ButtonAction> NonEmptyPlateInitialButtonActions = new List<ButtonAction>();
        List<ButtonAction> inactiveNonEmptyPlateInitialButtonActions = new List<ButtonAction>();
        List<ButtonAction> FinalPlateButtonActions = new List<ButtonAction>();
        PlatesList platesList = new PlatesList();
        private int? selectedZone;
        int rows = 3, cols = 4;
        public List<Zone> startingZones { get; set; }
        public List<Zone> currentZones { get; set; }
        public List<ProtocolStep> protocolSteps { get; set; }
        public int currentStep = -1;
        int plateWidth = 127 * 6, plateHeight = 85 * 6; //(127 * 6, 85 * 6
        Padding platePadding = new Padding(5);
        float TableOffset = 0.1f;
        int newPlateObjectId = 0;

        int numberOfZones;
        List<Liquid> liquids;

        public void ViewLiquids(int ZoneId) {
            PlateWindow control = new PlateWindow();
            control.showViewLiquidsMenu(currentZones[ZoneId].plate);
            //MessageBox.Show("ViewLiquids"); 
        }
        public void NameAndLiquids(int ZoneId)
        {
            PlateWindow control = new PlateWindow();
            Plate tmp = control.showNamesAndLiquidsMenu(ref startingZones[ZoneId].plate, ref liquids);
            if (tmp != null)
            {
                startingZones[ZoneId].plate = tmp.DeepCopy();
            }
            renderZone(startingZones[ZoneId], false);
            currnetStepChanged(-1);
            StartingStateChanged.Invoke(-1, currentZones);
            //MessageBox.Show("Name & Liquids"); 
        }
        public void DuplicatePlate(int ZoneId)
        {
            duplicateZonePlate(ZoneId);
            currnetStepChanged(-1);
            
            //MessageBox.Show("DuplicatePlate"); 
        }
        public void DeletePlate(int ZoneId)
        {
            removePlateFromZone(ZoneId);
            currnetStepChanged(-1);
            StartingStateChanged.Invoke(-1, currentZones);
            //MessageBox.Show("DeletePlate"); 
        }
        public void AddLabwareToPlate(int ZoneId)
        {
            selectedZone = ZoneId;
            platesList.plateSelected += addPlateToZone;
            platesList.showInWindow();
            currnetStepChanged(-1);
            //MessageBox.Show("Add labware"); 
        }

        public Plate plate1 = new Plate();



        public void init(ref PlatesList platesList, ref List<Liquid> liquids, ref List<ProtocolStep> protocolSteps, ref List<Zone> StartingZones, int rows, int cols)
        {
            

            this.cols = cols;
            this.rows = rows;
            this.startingZones = StartingZones;
            this.liquids = liquids;
            this.platesList = platesList;
            this.protocolSteps = protocolSteps;
            this.DoubleBuffered = true;
            this.SizeChanged += new EventHandler(tableLayoutPanel_Resize);
            this.currentStep = -1;
            numberOfZones = rows * cols;
            //startingZones = new List<Zone>(numberOfZones);

            Panel platePanel = new Panel
            {
                Size = new Size(200, 200),
                Location = new Point(200, 200),
                BorderStyle = BorderStyle.None
            };


            //startingZones = new List<Zone>();
            
            /*
            for (int i = 0; i < numberOfZones; i++)
            {
                Zone zone = new Zone();
                zone.id = i;
                zone.pictureBox.Name = i.ToString();
                startingZones.Add(zone);
            }
            */
            
            
            fillZoneDefaultImages();
            for (int i = 0; i < numberOfZones; i++)
            {


                renderZone(startingZones[i]);
            }
            currentZones = startingZones.ToList();
            CreateTableLayoutPanel(startingZones);
           
        }

        public void UpdateProject()
        {
            currentStep = -1;
            fillZoneDefaultImages();
            for (int i = 0; i < numberOfZones; i++)
            {
                startingZones[i].updateState();
            }  
            CreateTableLayoutPanel(startingZones);
        }


        private void CreateTableLayoutPanel(List<Zone> zones)
        {
            this.Controls.Clear();
            tableLayoutPanel = new TableLayoutPanel
            {
                Dock = DockStyle.None,
                ColumnCount = cols, // Установите количество столбцов
                RowCount = rows,    // Установите количество строк
                Size = new Size(plateWidth * cols, plateHeight * rows),
                CellBorderStyle = TableLayoutPanelCellBorderStyle.None, // Установите видимые границы
                BackgroundImage = drawWorkspaceBackImage(),
                BackgroundImageLayout = ImageLayout.Stretch,
                RightToLeft = RightToLeft.No,
                BackColor = SystemColors.InactiveBorder
            };
            tableLayoutPanel.Controls.Clear();
            // Установите размеры ячеек
            // Установите размеры столбцов
            for (int col = 0; col < tableLayoutPanel.ColumnCount; col++)
            {
                tableLayoutPanel.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 20f));
            }

            // Установите размеры строк
            for (int row = 0; row < tableLayoutPanel.RowCount; row++)
            {
                tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 50f));
            }



            // Добавьте PictureBox элементы в ячейки TableLayoutPanel
            for (int row = 0; row < rows; row++)
            {
                for (int col = 0; col < cols; col++) // Начинаем с последнего столбца и идем к первому
                {
                    int index = row * cols + col;
                    if (index < zones.Count)
                    {
                        tableLayoutPanel.Controls.Add(zones[index].pictureBox, col, row);
                        zones[index].pictureBox.MouseEnter += new EventHandler(SwapImageAndTag);
                        zones[index].pictureBox.MouseLeave += new EventHandler(SwapImageAndTag);
                        zones[index].pictureBox.MouseMove += new MouseEventHandler(PictureBox_MouseMove);

                    }
                }
            }


            this.Controls.Add(tableLayoutPanel);

            tableLayoutPanel_Resize(null, null);
        }

        private void TableLayoutPanel_MouseEnter(object? sender, EventArgs e)
        {
            throw new NotImplementedException();
        }

        public void renderZone(Zone zone, bool subscibeToButtons = true)
        {
            zone.updateState();

            if (currentStep == -1)
            {
                if (zone.plate != null)
                {
                    if(zone.plate.Type == PLATE_TYPE.TIP_RACK || zone.plate.Type == PLATE_TYPE.DROP_PLACE)
                    {
                        CreateVirtualButtons(ref zone.pictureBox, inactiveNonEmptyPlateInitialButtonActions, subscibeToButtons);
                    }
                    else
                    {
                        CreateVirtualButtons(ref zone.pictureBox, NonEmptyPlateInitialButtonActions, subscibeToButtons);
                    }
                   
                }
                else
                {
                    CreateVirtualButtons(ref zone.pictureBox, EmptyPlateButtonActions, subscibeToButtons);
                }
            }
            else
            {

                if (zone.plate != null)
                {
                    if (zone.plate.Type == PLATE_TYPE.RESERVOIR || zone.plate.Type == PLATE_TYPE.WELL_PLATE)
                    {
                        CreateVirtualButtons(ref zone.pictureBox, FinalPlateButtonActions, subscibeToButtons);
                    }
                }
            }

        }


        public void fillActions()
        {

            NonEmptyPlateInitialButtonActions.Add(new ButtonAction("Название и жидкость", NameAndLiquids));
            NonEmptyPlateInitialButtonActions.Add(new ButtonAction("Дублировать", DuplicatePlate));
            NonEmptyPlateInitialButtonActions.Add(new ButtonAction("Удалить", DeletePlate));

            EmptyPlateButtonActions.Add(new ButtonAction("Добавить \nоборудование", AddLabwareToPlate));

            FinalPlateButtonActions.Add(new ButtonAction("Посмотреть жидкости", ViewLiquids));

            inactiveNonEmptyPlateInitialButtonActions.Add(new ButtonAction("Дублировать", DuplicatePlate));
            inactiveNonEmptyPlateInitialButtonActions.Add(new ButtonAction("Удалить", DeletePlate));

        }
        public void renderStartingDeckZones()
        {

            PictureBox platePictureBox = new PictureBox
            {
                Tag = Properties.Resources.robot_liquid_clear,
                SizeMode = PictureBoxSizeMode.Zoom,
                BackColor = Color.Transparent
            };

            CreateVirtualButtons(ref platePictureBox, EmptyPlateButtonActions);
        }



        private void SwapImageAndTag(object sender, EventArgs e)
        {
            if (sender is PictureBox pictureBox)
            {
                // Извлечение объекта pictureBoxTag из Tag
                pictureBoxTag tag = (pictureBoxTag)pictureBox.Tag;

                // Сохраните текущее изображение во временной переменной
                Image tempImage = pictureBox.Image;

                // Замените изображение на значение из Tag
                pictureBox.Image = tag.image;

                // Обновите Tag новым изображением
                tag.image = tempImage;

                // Присвоить обновленный объект Tag обратно PictureBox
                pictureBox.Tag = tag;
            }
        }


        public Workspace()
        {

            InitializeComponent();
            fillActions();
            //init();

        }

        Image CreateVirtualButtons(ref PictureBox pictureBox, List<ButtonAction> actions, bool subscribeToButtons = true)
        {
            if (pictureBox.Tag is not pictureBoxTag Tag)
            {
                throw new InvalidOperationException("BackgroundImage должен быть установлен перед вызовом этого метода.");
            }

            // Создаем копию фонового изображения

            Bitmap image = new Bitmap(Tag.image);

            using (Graphics fig = Graphics.FromImage(image))
            {

                fig.SmoothingMode = SmoothingMode.AntiAlias;
                // Применяем затемнение к изображению
                float cornerRadius = (float)Tag.image.Width / 10.0f;
                //Plate.DrawFilledRoundedRectangle(fig, new SolidBrush(Color.FromArgb(160, 0, 0, 0)), 0, 0, image.Width, image.Height, cornerRadius);
                int offset = image.Width / 100;
                Plate.DrawRoundedRectangle(fig, null, offset, offset, image.Width - offset * 2, image.Height - offset * 2, cornerRadius, new SolidBrush(Color.FromArgb(160, 0, 0, 0)));
                // Рисование текста (если необходимо)
                float scaleFactor = (float)image.Height / 200.0f;

                // Подбор размера шрифта
                int fontSize = (int)(20 * scaleFactor); // 12 - базовый размер шрифта

                // Создание шрифта с масштабированным размером
                Font font = new Font("Segoe UI", fontSize, FontStyle.Regular);
                Brush textBrush = new SolidBrush(Color.White);
                StringFormat stringFormat = new StringFormat
                {
                    Alignment = StringAlignment.Near,
                    LineAlignment = StringAlignment.Near,
                    FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.NoClip
                };

                int buttonHeight = image.Height / actions.Count;
                List<Rectangle> buttons = new List<Rectangle>();
                for (int i = 0; i < actions.Count; i++)
                {
                    // Измерение размера текста
                    Size textSize = TextRenderer.MeasureText(actions.ElementAt(i).text, font, new Size(image.Width, buttonHeight), TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.NoPadding);
                    // Вычисление координат для текста и прямоугольника
                    float textX = (image.Width - textSize.Width) / 2f;
                    float textY = i * buttonHeight + (buttonHeight - textSize.Height) / 2f;
                    Rectangle backgroundRect = new Rectangle((int)textX, (int)(textY), textSize.Width, textSize.Height);
                    buttons.Add(backgroundRect);
                    TextRenderer.DrawText(fig, actions.ElementAt(i).text, font, new Point((int)textX, (int)textY), Color.White, TextFormatFlags.Left | TextFormatFlags.Top | TextFormatFlags.NoPadding);

                }
                Tag.handCursorArea = buttons;
                Tag.image = image;
                pictureBox.Tag = Tag;
            }

            if (subscribeToButtons)
            {
                pictureBox.MouseClick += (sender, e) =>
                {
                    PictureBox pb = sender as PictureBox;
                    // Определение зоны клика по вертикали
                    if (actions.Count > 0)
                    {
                        int zoneIndex = e.Y * actions.Count / pb.Height;
                        // Ограничение индекса допустимым диапазоном
                        zoneIndex = Math.Min(zoneIndex, actions.Count - 1);
                        // Вызов соответствующего метода
                        actions.ElementAt(zoneIndex).Action.Invoke(int.Parse(pb.Name));
                    }
                };
            }
            return image;
        }

        private void PictureBox_MouseMove(object sender, MouseEventArgs e)
        {
            if (sender is PictureBox pictureBox && pictureBox.Image != null)
            {
                pictureBoxTag tmpTag = (pictureBoxTag)pictureBox.Tag;
                List<Rectangle> handCursorAreas = tmpTag.handCursorArea;

                // Вычисление масштабного коэффициента
                float scaleX = (float)pictureBox.Image.Width / pictureBox.Width;
                float scaleY = (float)pictureBox.Image.Height / pictureBox.Height;

                // Масштабирование координат курсора
                Point scaledPoint = new Point((int)(e.X * scaleX), (int)(e.Y * scaleY));

                bool isInHandCursorArea = handCursorAreas.Any(area => area.Contains(scaledPoint));

                pictureBox.Cursor = isInHandCursorArea ? Cursors.Hand : Cursors.Default;
            }
        }


        public Image drawWorkspaceBackImage()
        {
            int offset = platePadding.All;
            offset = plateWidth / 12;
            Bitmap Image = new Bitmap(plateWidth * cols, plateHeight * rows);
            float penScale = 0.02f;

            //

            Graphics fig;
            fig = Graphics.FromImage(Image);
            fig.Clear(SystemColors.InactiveBorder);
            Pen myPen = new Pen(Brushes.LightGray);

            myPen.Width = plateWidth / 20;


            List<Point> pointsFrom = new List<Point>();
            List<Point> pointsTo = new List<Point>();
            List<int> penSizes = new List<int>();

            for (int i = 0; i <= rows; i++)
            {
                for (int q = 0; q < cols; q++)
                {
                    pointsFrom.Add(new Point(q * plateWidth + offset, i * plateHeight));
                    pointsTo.Add(new Point((q + 1) * plateWidth - offset, i * plateHeight));
                    if (i != 0 && i != rows)
                    {
                        penSizes.Add((int)(plateWidth * penScale));
                    }
                    else
                    {
                        int tmp = (int)(plateWidth * penScale * 2);
                        penSizes.Add((int)(plateWidth * penScale * 2));
                    }
                }
            }



            for (int i = 0; i <= cols; i++)
            {
                for (int q = 0; q < rows; q++)
                {

                    pointsFrom.Add(new Point(i * plateWidth, q * plateHeight + offset));
                    pointsTo.Add(new Point(i * plateWidth, (q + 1) * plateHeight - offset));




                    if (i % cols == 0)
                    {
                        penSizes.Add((int)(plateWidth * penScale * 2));
                    }
                    else
                    {
                        penSizes.Add((int)(plateWidth * penScale));
                    }
                }
            }


            for (int i = 0; i < pointsFrom.Count; i++)
            {
                myPen.Width = penSizes[i];
                fig.DrawLine(myPen, pointsFrom[i], pointsTo[i]);
            }


            return Image; //раскомментировать

        }


        private void fillZoneDefaultImages()
        {
            Font font = new Font("Segoe UI", 200, FontStyle.Bold);
            Brush textBrush = new SolidBrush(Color.Black);
            StringFormat stringFormat = new StringFormat
            {
                Alignment = StringAlignment.Near,
                LineAlignment = StringAlignment.Near,
                FormatFlags = StringFormatFlags.NoWrap | StringFormatFlags.NoClip
            };

            for (int i = 0; i < numberOfZones; i++)
            {

                Bitmap zoneImage = new Bitmap(plateWidth, plateHeight);
                Graphics tmpFig;
                tmpFig = Graphics.FromImage(zoneImage);
                tmpFig.TextRenderingHint = System.Drawing.Text.TextRenderingHint.AntiAliasGridFit;

                Size textSize = TextRenderer.MeasureText((i + 1).ToString(), font, new Size(zoneImage.Width, zoneImage.Height), TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter | TextFormatFlags.NoPadding);
                // Вычисление координат для текста и прямоугольника
                float textX = (zoneImage.Width - textSize.Width) / 2f;
                float textY = (zoneImage.Height - textSize.Height) / 2f + 10;
                Rectangle backgroundRect = new Rectangle((int)textX, (int)textY, textSize.Width, textSize.Height);
                TextRenderer.DrawText(tmpFig, (i + 1).ToString(), font, new Point((int)textX, (int)textY), Color.LightGray, TextFormatFlags.Left | TextFormatFlags.Top | TextFormatFlags.NoPadding);
                startingZones[i].defaultImage = zoneImage;
            }
        }

        private void tableLayoutPanel_Resize(object sender, EventArgs e)
        {
            tableLayoutPanel.SuspendLayout();
            //tableLayoutPanel.Visible = false;
            // Рассчитываем размеры с учетом отступа
            Size sizeWithOffset = new Size(
                (int)(this.Size.Width * (1 - 2 * TableOffset)),
                (int)(this.Size.Height * (1 - 2 * TableOffset))
            );

            // Применяем соотношение сторон к размеру с отступом
            float originalAspectRatio = (float)(plateWidth * cols) / (float)(plateHeight * rows);
            if (sizeWithOffset.Width != 0 && sizeWithOffset.Height != 0)
            {
                if ((float)sizeWithOffset.Width / sizeWithOffset.Height > originalAspectRatio)
                {
                    // Ограничиваем по высоте
                    tableLayoutPanel.Width = (int)(sizeWithOffset.Height * originalAspectRatio);
                    tableLayoutPanel.Height = sizeWithOffset.Height;
                }
                else
                {
                    // Ограничиваем по ширине
                    tableLayoutPanel.Height = (int)(sizeWithOffset.Width / originalAspectRatio);
                    tableLayoutPanel.Width = sizeWithOffset.Width;
                }
            }

            // Центрирование tableLayoutPanel в родительском элементе
            tableLayoutPanel.Location = new Point(
                (this.Width - tableLayoutPanel.Width) / 2,
                (this.Height - tableLayoutPanel.Height) / 2
            );

            tableLayoutPanel.ResumeLayout();
            //tableLayoutPanel.Visible = true;

        }



        private void addPlateToZone(int? id)
        {

            if (id.HasValue && selectedZone.HasValue)
            {
                int ZoneId = selectedZone.Value;

                startingZones[selectedZone.Value].addPlate(platesList.plates[id.Value].DeepCopy());

                newPlateObjectId = startingZones
                    .Where(zone => zone.plate != null) // Убедиться, что пластина существует
                    .Max(zone => zone.plate.ObjectId) + 1;
                startingZones[selectedZone.Value].plate.ObjectId = newPlateObjectId;
                //newPlateObjectId++;
                renderZone(startingZones[selectedZone.Value]);
                startingZones[ZoneId].pictureBox.MouseEnter += new EventHandler(SwapImageAndTag);
                startingZones[ZoneId].pictureBox.MouseLeave += new EventHandler(SwapImageAndTag);
                startingZones[ZoneId].pictureBox.MouseMove += new MouseEventHandler(PictureBox_MouseMove);
                int row = ZoneId / cols;
                int col = ZoneId % cols;
                Control oldControl = tableLayoutPanel.GetControlFromPosition(col, row);
                if (oldControl != null)
                {
                    tableLayoutPanel.Controls.Remove(oldControl);
                }

                // Добавление нового элемента в ячейку
                tableLayoutPanel.Controls.Add(startingZones[ZoneId].pictureBox, col, row);
            }
            platesList.plateSelected -= addPlateToZone; ;
            selectedZone = null;
        }


        private void duplicateZonePlate(int originalZoneId)
        {
            int? newZoneId = null;
            for (int i = 0; i < numberOfZones; i++)
            {
                if (startingZones[i].plate == null)
                {
                    newZoneId = i;
                    break;
                }
            }
            if (newZoneId.HasValue)
            {

                startingZones[newZoneId.Value].addPlate(startingZones[originalZoneId].plate.DeepCopy());       
                newPlateObjectId = startingZones
                    .Where(zone => zone.plate != null) // Убедиться, что пластина существует
                    .Max(zone => zone.plate.ObjectId) + 1;
                startingZones[newZoneId.Value].plate.ObjectId = newPlateObjectId;
                renderZone(startingZones[newZoneId.Value]);
                startingZones[newZoneId.Value].pictureBox.MouseEnter += new EventHandler(SwapImageAndTag);
                startingZones[newZoneId.Value].pictureBox.MouseLeave += new EventHandler(SwapImageAndTag);
                startingZones[newZoneId.Value].pictureBox.MouseMove += new MouseEventHandler(PictureBox_MouseMove);
                int row = newZoneId.Value / cols;
                int col = newZoneId.Value % cols;
                Control oldControl = tableLayoutPanel.GetControlFromPosition(col, row);
                if (oldControl != null)
                {
                    tableLayoutPanel.Controls.Remove(oldControl);
                }

                // Добавление цифры к названию нового дубликата
                Plate originalPlate = startingZones[originalZoneId].plate;
                Plate newPlate = originalPlate.DeepCopy();
                int CopyNumber = 1;
                while (startingZones.Any(zone => zone.plate != null && zone.plate.Name == (originalPlate.Name + " (" + CopyNumber + ")")))
                {
                    CopyNumber++;
                }
                newPlate.Name = originalPlate.Name + " (" + CopyNumber + ")";
                startingZones[newZoneId.Value].addPlate(newPlate);

                // Добавление нового элемента в ячейку
                tableLayoutPanel.Controls.Add(startingZones[newZoneId.Value].pictureBox, col, row);
            }

        }

        void removePlateFromZone(int ZoneId)
        {

            //addPlateToZone(selectedZone.Value);
            startingZones[ZoneId].removePlate();
            renderZone(startingZones[ZoneId]);
            int row = ZoneId / cols;
            int col = ZoneId % cols;
            Control oldControl = tableLayoutPanel.GetControlFromPosition(col, row);
            if (oldControl != null)
            {
                tableLayoutPanel.Controls.Remove(oldControl);
            }
            startingZones[ZoneId].pictureBox.MouseEnter += new EventHandler(SwapImageAndTag);
            startingZones[ZoneId].pictureBox.MouseLeave += new EventHandler(SwapImageAndTag);
            startingZones[ZoneId].pictureBox.MouseMove += new MouseEventHandler(PictureBox_MouseMove);
            tableLayoutPanel.Controls.Add(startingZones[ZoneId].pictureBox, col, row);

        }

        public void liquidsUpdated()
        {
            for (int i = 0; i < numberOfZones; i++)
            {
                if (currentZones[(int)i].plate != null)
                {
                    startingZones[(int)i].plate.updateLiquidsData(liquids);
                }
                renderZone(startingZones[i], false);
            }
            currnetStepChanged(currentStep);
        }

        public void currnetStepChanged(int StepNumber)
        {
            this.currentStep = StepNumber;

            if (currentStep == -1)
            {
                currentZones.Clear();
                currentZones = startingZones.ToList();
                cloneZones(currentZones, startingZones);
                //startingZones.Clear();
                //startingZones = currentZones.ToList();
                foreach (Zone zone in startingZones)
                {
                    renderZone(zone, true);
                }
                CreateTableLayoutPanel(startingZones);
            }

            else
            {
                cloneZones(startingZones, currentZones);
                //currentZones.Clear();
                //currentZones = startingZones.ToList();

                for (int i = 0; i <= currentStep; i++)
                {
                    try
                    {
                        if (i < protocolSteps.Count)
                        {

                            currentZones = protocolSteps[i].ExecuteStep(currentZones).ToList();
                        }
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show(ex.Message);
                    }
                }
                foreach (Zone zone in currentZones)
                {
                    renderZone(zone, true);
                }
                CreateTableLayoutPanel(currentZones);
            }



        }

        private void cloneZones( List<Zone> src,  List<Zone> dst)
        {
            dst.Clear();
            for(int i = 0; i < src.Count; i++)
            {
                dst.Add(new Zone(i));
                if (src[i].plate != null)
                {
                    dst[i].addPlate(src[i].plate.DeepCopy());
                   
                }
                dst[i].defaultImage = src[i].defaultImage;
            }
        }

        public List<Zone> GetZonesAtStep(int stepNumber)
        {
            List<Zone> result = new List<Zone>();
            cloneZones(startingZones, result);
            //currentZones.Clear();
            //currentZones = startingZones.ToList();

            for (int i = 0; i <= stepNumber; i++)
            {
                try
                {
                    if (i < protocolSteps.Count)
                    {

                        result = protocolSteps[i].ExecuteStep(result).ToList();
                    }
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
            }
            foreach (Zone zone in result)
            {
                renderZone(zone, true);
            }
            return result;
        }

    }

    public class pictureBoxTag
    {
        public Image image;
        public List<Rectangle> handCursorArea;

        public pictureBoxTag(Image image)
        {
            this.image = image;
            handCursorArea = new List<Rectangle>();
        }
    }
}
