﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using ChemRobotDesktop.Classes;

namespace ChemRobotDesktop
{
    public partial class LiquidsMenu : UserControl
    {

        public delegate void Method();
        public event Method LiquidsUpdated;
        public List<Liquid> Liquids;
        public Liquid liquid = new Liquid();

        public LiquidsMenu()
        {
            InitializeComponent();
            showTip();
            splitContainer1.BringToFront();

        }

        public void init(ref List<Liquid> Liquids)
        {
            this.Liquids = Liquids;
            updateLiquidsList();
        }

        public void LoadData(List<Liquid> Liquids)
        {
            this.Liquids.Clear();
            this.Liquids = Liquids.ToList();
            LiquidsUpdated.Invoke();
        }


        private void newLiquidButton_Click(object sender, EventArgs e)
        {
            createNewLiquid();
            showEditor();
        }



        private void button1_Click_1(object sender, EventArgs e)
        {

            splitContainer2.Panel1Collapsed = false;
            splitContainer2.Panel2Collapsed = true;

        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            liquidNameBox.Text = null;
            liquidDescriptionBox.Text = null;
            liquid.Color = Color.Transparent;
            showTip();
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            if (liquidNameBox.Text != "")
            {
                liquid.Name = liquidNameBox.Text;
                liquidNameBox.Text = null;
                liquid.Description = liquidDescriptionBox.Text;
                liquidDescriptionBox.Text = null;
                if (liquid.Color == Color.Transparent)
                {
                    Random randonGen = new Random();
                    Color randomColor = Color.FromArgb(randonGen.Next(255), randonGen.Next(255), randonGen.Next(255));

                    liquid.Color = randomColor;
                }
                addLiquid();
                liquid.clear();
                LiquidsUpdated.Invoke();
            }
        }

        public void createNewLiquid()
        {
            Random randonGen = new Random();
            Color randomColor = Color.FromArgb(randonGen.Next(255), randonGen.Next(255), randonGen.Next(255));
            liquidNameBox.Text = null;
            liquidDescriptionBox.Text = null;
            liquid.Color = randomColor;
            liquid.Id = 0;
            if (Liquids.Count != 0)
            {
                liquid.Id = Liquids.Last().Id + 1;
            }
            colorBox.BackColor = liquid.Color;
        }
        private void deleteButton_Click(object sender, EventArgs e)
        {
            removeLiquid();
            showTip();
        }

        public void removeLiquid()
        {
            int index = Liquids.FindIndex(L => L.Id == liquid.Id);
            if (index != -1)
            {
                Liquids.RemoveAt(index);
                LiquidsUpdated.Invoke();
            }
            updateLiquidsList();
            showTip();
        }

        private void colorButton_Click(object sender, EventArgs e)
        {
            colorDialog2.ShowDialog();
            liquid.Color = colorDialog2.Color;
            colorBox.BackColor = liquid.Color;
            Console.WriteLine("Color = " + liquid.Color.ToString());

        }
        public void updateLiquidsList()
        {
            liquidsPanel.Controls.Clear();
            liquidsPanel.RowCount = Liquids.Count + 1;
            TableLayoutRowStyleCollection styles =
            this.liquidsPanel.RowStyles;

            liquidsPanel.RowCount = Liquids.Count + 1;
            for (int i = 0; i < Liquids.Count; i++)
            {


                if (liquidsPanel.RowStyles.Count <= i)
                {
                    liquidsPanel.RowStyles.Add(new RowStyle(SizeType.Absolute, 20));
                }
                else
                {
                    liquidsPanel.RowStyles[i].SizeType = SizeType.Absolute;
                    liquidsPanel.RowStyles[i].Height = 20;
                }


                Button tmp = new Button();
                tmp.FlatStyle = FlatStyle.Flat;
                tmp.Click += edit;
                tmp.Tag = Liquids[i];
                tmp.Text = Liquids[i].Name;
                tmp.TextAlign = ContentAlignment.MiddleCenter;
                tmp.AutoSize = false;
                tmp.Dock = DockStyle.Fill;
                tmp.Font = new System.Drawing.Font("Segoe UI", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));

                liquidsPanel.Controls.Add(tmp, 0, i);
                liquidsPanel.Controls.Add(new PictureBox() { BackColor = Liquids[i].Color }, 1, i);


            }

            foreach (RowStyle style in styles)
            {
                // Set the row height to 50 pixels.  
                style.SizeType = SizeType.Absolute;
                style.Height = 50;
            }

        }

        public void edit(object sender, EventArgs e)
        {
            Button tmp = sender as Button;
            Liquid tmpLiquid = (Liquid)tmp.Tag;
            liquid.Name = tmpLiquid.Name;
            liquid.Color = tmpLiquid.Color;
            liquid.Description = tmpLiquid.Description;
            liquid.Id = tmpLiquid.Id;
            showData();
            showEditor();

        }

        private void showData()
        {
            liquidNameBox.Text = liquid.Name;
            liquidDescriptionBox.Text = liquid.Description;
            colorBox.BackColor = liquid.Color;
        }

        private void showEditor()
        {
            splitContainer2.Panel1Collapsed = false;
            splitContainer2.Panel2Collapsed = true;
        }

        private void showTip()
        {
            splitContainer2.Panel1Collapsed = true;
            splitContainer2.Panel2Collapsed = false;
        }

        public void addLiquid()
        {


            int index = Liquids.FindIndex(L => L.Id == liquid.Id);
            if (index != -1)
            {
                Liquids[index].Name = liquid.Name;
                Liquids[index].Description = liquid.Description;
                Liquids[index].Color = liquid.Color;
                LiquidsUpdated.Invoke();

            }
            else
            {
                Liquids.Add(liquid.deepCopy());
            }

            updateLiquidsList();
            showTip();

        }

    }
}
