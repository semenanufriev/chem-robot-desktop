﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ChemRobotDesktop.CustomControls
{
    public partial class ModuleEditor : UserControl
    {

        public bool editing = false;
        string editName;

        public ModuleEditor()
        {
            InitializeComponent();



            sizesPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            hightPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            rowsColsPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            diameterPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            depthPicture.SizeMode = PictureBoxSizeMode.StretchImage;
            spacePicture.SizeMode = PictureBoxSizeMode.StretchImage;
            offsetPicture.SizeMode = PictureBoxSizeMode.StretchImage;

            /*

            sizesPicture.Image = ChemRobotDesktop.Properties.Resources.expl_sizes;
            hightPicture.Image = ChemRobotDesktop.Properties.Resources.expl_height;
            rowsColsPicture.Image = ChemRobotDesktop.Properties.Resources.expl_rows_cols;
            diameterPicture.Image = ChemRobotDesktop.Properties.Resources.expl_diam;
            depthPicture.Image = ChemRobotDesktop.Properties.Resources.expl_depth;
            spacePicture.Image = ChemRobotDesktop.Properties.Resources.expl_dist;
            offsetPicture.Image = ChemRobotDesktop.Properties.Resources.expl_offset;
            */
            //     Image = ChemRobotDesktop.Properties.Resources.add;





        }




        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void okButton_Click(object sender, EventArgs e)
        {
            /*
            module = new Robot_old.Module();
            module.name = nameBox.Text;
            module.description = descriptionBox.Text;
            module.type = typeBox.SelectedItem.ToString();
            module.size = new PointF((float)lenghtBox.Value, (float)widthBox.Value);
            module.diameter = (float)diameterBox.Value;
            module.gridElements = new Point((int)colsBox.Value, (int)rowsBox.Value);
            module.gridStep = new PointF((float)xSpaceBox.Value, (float)ySpaceBox.Value);
            module.zeroCoord = new PointF((float)xOffsetBox.Value, (float)yOffsetBox.Value);
            module.max_h = (float)heightBox.Value;
            module.depth = (float)depthBox.Value;
            module.fillPoints(new PointF(0, 0));
            module.drawImageFromData();
            imageBox.SizeMode = PictureBoxSizeMode.StretchImage;
            imageBox.Image = module.Image;

            */
        }






        public void showModuledata()
        {
            /*
            module = m;
            int index = typeBox.Items.IndexOf(m.type);
            typeBox.SelectedIndex = index;
            lenghtBox.Value = (decimal)m.size.X;
            widthBox.Value = (decimal)m.size.Y;
            diameterBox.Value = (decimal)m.diameter;
            colsBox.Value = m.gridElements.X;
            rowsBox.Value = m.gridElements.Y;
            xSpaceBox.Value = (decimal)m.gridStep.X;
            ySpaceBox.Value = (decimal)m.gridStep.Y;
            xOffsetBox.Value = (decimal)m.zeroCoord.X;
            yOffsetBox.Value = (decimal)m.zeroCoord.Y;
            imageBox.Image = m.Image;
            nameBox.Text = m.name;
            descriptionBox.Text = m.description;
            editing = true;

            */

        }

        public void newModule()
        {
            /*
            editing = false;
            module = new Robot_old.Module();
            lenghtBox.Value = 0;
            widthBox.Value = 0;
            diameterBox.Value = 0;
            colsBox.Value = 0;
            rowsBox.Value = 0;
            xSpaceBox.Value = 0;
            ySpaceBox.Value = 0;
            xOffsetBox.Value = 0;
            yOffsetBox.Value = 0;
            imageBox.Image = null;
            nameBox.Text = null;
            descriptionBox.Text = null;
            */
        }

        public void clear()
        {
            editing = false;
            //module = null;
            lenghtBox.Value = 0;
            widthBox.Value = 0;
            diameterBox.Value = 0;
            colsBox.Value = 0;
            rowsBox.Value = 0;
            xSpaceBox.Value = 0;
            ySpaceBox.Value = 0;
            xOffsetBox.Value = 0;
            yOffsetBox.Value = 0;
            imageBox.Image = null;
            nameBox.Text = null;
            descriptionBox.Text = null;

        }


        public void editModule()
        {

            editing = true;
            /*
            editName = m.name;
            showModuledata(m);
            */
        }

        private void saveButton_Click(object sender, EventArgs e)
        {
            /*
            if (editing)
            {
                File.Delete(robot.workDirectory + editName + ".json");

            }


            okButton_Click(null, null);


            Robot_old.saveModuleToJson(module, "C:/Users/semen/OneDrive/Pictures/chem_images/modules/");
            */
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            clear();
        }

        private void nameBox_TextChanged(object sender, EventArgs e)
        {

        }

        private void typeBox_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
